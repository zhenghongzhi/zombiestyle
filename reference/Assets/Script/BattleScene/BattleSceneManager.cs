using UnityEngine;
using System.Collections;

public class BattleSceneManager : MonoBehaviour {
    public Transform playerInitPosition;
    public ZombieWaveSpawn zombieSpawner;


    public void OnStartBattleScene()
    {

    }

    public void OnExitBattleScene()
    {
        GameObject player = GameObject.FindGameObjectWithTag("Player");
        player.GetComponent<Rigidbody>().isKinematic = true;
        player.GetComponent<PlayerAnimation>().enabled = false;
        player.GetComponent<PlayerMovementMoter>().enabled = false;
        player.GetComponent<TPSController>().enabled = false;
        player.GetComponent<PadInput>().enabled = false;
        player.GetComponent<SpecialSkillsController>().enabled = false;
        player.transform.FindChild("light").gameObject.SetActive(false);
        player.transform.FindChild("shadows").gameObject.SetActive(false);
        player.animation.Play();
        foreach (SkinnedMeshRenderer smr in player.GetComponentsInChildren<SkinnedMeshRenderer>())
        {
            smr.enabled = false;
        }
    }
}
