using UnityEngine;
using System.Collections;


public class ExplosionItem : MonoBehaviour {
    public int life;
    public float explosionHeight;
    public GameObject explosionPrefab;
    public LayerMask dmgLayers;
    public Color onHitColor;
    public float explosionDmg;

    Color mainColor;

    void Awake()
    {
        mainColor = renderer.material.color;
    }

    //Message Method
	void ApplyDamage(BuffDmgInfo buffDmg)
    {
        OnDamageApplying((int)buffDmg.dmg, buffDmg.applier);
    }

    //Message Method
    void ApplyDamage(BulletInfo bulletHit)
    {
        OnDamageApplying((int)bulletHit.dmg, bulletHit.shooter);
    }

    void OnDamageApplying(int dmg, GameObject applier)
    {
        if (applier.tag != "Player" || life < 0)
            return;
        life -= dmg;
        if (life <= 0)
        {
            GameObject explosion = Instantiate(explosionPrefab) as GameObject;
            explosion.transform.position = transform.position + new Vector3(0, explosionHeight, 0);
            ExplosionDmg script = explosion.AddComponent<ExplosionDmg>();
            script.dmgObjectLayer = dmgLayers;
            script.dmgInfo = new BuffDmgInfo(explosionDmg, 1, applier);
            Destroy(gameObject);
        }
        else
            StartCoroutine(OnHitChangeColor());

    }

    IEnumerator OnHitChangeColor()
    {
        renderer.material.color = onHitColor;
        yield return new WaitForSeconds(0.1f);
        renderer.material.color = mainColor;
    }
}
