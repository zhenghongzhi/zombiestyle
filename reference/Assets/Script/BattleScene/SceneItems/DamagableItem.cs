using UnityEngine;
using System.Collections;

public class DamagableItem : MonoBehaviour {
    public float life;
    public Color onHitColor;
    public Color DamagedColor;
    public Renderer mainRender;

    Color mainColor;

    void Awake()
    {
        mainColor = mainRender.material.color;
    }

    //Message Method
    void ApplyDamage(BuffDmgInfo buffDmg)
    {
        OnDamageApplying((int)buffDmg.dmg, buffDmg.applier);
    }

    //Message Method
    void ApplyDamage(BulletInfo bulletHit)
    {
        OnDamageApplying((int)bulletHit.dmg, bulletHit.shooter);
    }

    void OnDamageApplying(int dmg, GameObject applier)
    {
        if (applier.tag != "Player" || life < 0)
            return;
        life -= dmg;
        animation.Play("damage");
        if (life <= 0)
        {
            animation.Play("broken");
            mainRender.material.color = DamagedColor;
        }
        else
            StartCoroutine(OnHitChangeColor());

    }

    IEnumerator OnHitChangeColor()
    {
        mainRender.material.color = onHitColor;
        yield return new WaitForSeconds(0.1f);
        mainRender.material.color = mainColor;
    }
}
