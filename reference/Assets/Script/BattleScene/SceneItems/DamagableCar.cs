using UnityEngine;
using System.Collections;

public class DamagableCar : MonoBehaviour {
    public float totalLife;
    public float dmg;
    public LayerMask dmgMask;
    public Texture2D brokenTex;
    public Texture2D destroyTex;
    public GameObject brokenFirePrefab;
    public GameObject explosionPrefab;
    public Color onHitColor;
    public Color DamagedColor;
    public Renderer mainRender;

    GameObject fire;
    Color mainColor;
    int stage = 0;
    float life;

    void Awake()
    {
        life = totalLife;
        mainColor = mainRender.material.color;
    }

    //Message Method
    void ApplyDamage(BuffDmgInfo buffDmg)
    {
        OnDamageApplying((int)buffDmg.dmg, buffDmg.applier);
    }

    //Message Method
    void ApplyDamage(BulletInfo bulletHit)
    {
        OnDamageApplying((int)bulletHit.dmg, bulletHit.shooter);
    }

    void OnDamageApplying(int dmg, GameObject applier)
    {
        if (applier.tag != "Player" || life < 0)
            return;
        life -= dmg;
        if (stage == 0)
        {
            if (life < totalLife * 0.9f)
            {
                animation.Play("damage1");
                stage = 1;
            }
            else
                StartCoroutine(OnHitChangeColor());
        }
        if (stage == 1)
        {
            if (life < totalLife * 0.3)
            {
                animation.Play("damage2");
                if (brokenFirePrefab)
                    fire = Instantiate(brokenFirePrefab, transform.position, Quaternion.identity) as GameObject;
                mainRender.material.mainTexture = brokenTex;
                stage = 2;
            }
            else
                StartCoroutine(OnHitChangeColor());
        }
        if (stage == 2)
        {
            if (life <= 0)
            {
                animation.Play("destroy");
                if (explosionPrefab)
                {
                    if (fire)
                        Destroy(fire);
                    GameObject expo = Instantiate(explosionPrefab, transform.position, Quaternion.identity) as GameObject;
                    ExplosionDmg script = expo.AddComponent<ExplosionDmg>();
                    script.dmgObjectLayer = dmgMask;
                    script.dmgInfo = new BuffDmgInfo(dmg, 1, applier);
                }
                mainRender.material.mainTexture = destroyTex;
                mainRender.material.color = DamagedColor;
                Destroy(this);
            }
            else
                StartCoroutine(OnHitChangeColor());
        }
    }

    IEnumerator OnHitChangeColor()
    {
        mainRender.material.color = onHitColor;
        yield return new WaitForSeconds(0.1f);
        mainRender.material.color = mainColor;
    }
}
