using UnityEngine;
using System.Collections;

[RequireComponent(typeof(SphereCollider))]
public class Chees : MonoBehaviour {

    public int addHp;
    public float rotSpeed;
    public GameObject eatenEffect;

    GameObject effect;
    void Update()
    {
        transform.localEulerAngles = new Vector3(0, transform.localEulerAngles.y + Time.deltaTime * rotSpeed, 0);
    }

    void OnEaten(GameObject eater)
    {
        eater.GetComponent<CharacterStatus>().HpRecovery(addHp);
        effect = Instantiate(eatenEffect, transform.position, Quaternion.identity) as GameObject;
        renderer.enabled = false;
        StartCoroutine(DestroyEffect());
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag != "Player")
            return;
        OnEaten(other.gameObject);
    }

    IEnumerator DestroyEffect()
    {
        yield return new WaitForSeconds(effect.GetComponentInChildren<ParticleSystem>().duration);
        Destroy(effect);
        Destroy(transform.root.gameObject);
    }
}
