using UnityEngine;
using System.Collections;

public class DestrableItem : MonoBehaviour {
    public float life;
    public Color onHitColor;
    public Color DamagedColor;
    public float remainTimeAfterDestroyed;

    Color mainColor;


    void Awake()
    {
        mainColor = GetComponentInChildren<Renderer>().material.color;
    }

    //Message Method
    void ApplyDamage(BuffDmgInfo buffDmg)
    {
        OnDamageApplying((int)buffDmg.dmg, buffDmg.applier);
    }

    //Message Method
    void ApplyDamage(BulletInfo bulletHit)
    {
        OnDamageApplying((int)bulletHit.dmg, bulletHit.shooter);
    }

    void OnDamageApplying(int dmg, GameObject applier)
    {
        if (applier.tag != "Player" || life < 0)
            return;
        life -= dmg;
        if (life <= 0)
        {
            animation.Play();
            SetMainColor(DamagedColor);
            StartCoroutine(DestroyGameObject());
        }
        else
            StartCoroutine(OnHitChangeColor());

    }

    IEnumerator OnHitChangeColor()
    {
        SetMainColor(onHitColor);
        yield return new WaitForSeconds(0.1f);
        SetMainColor(mainColor);
    }

    IEnumerator DestroyGameObject()
    {
        yield return new WaitForSeconds(remainTimeAfterDestroyed);
        transform.position -= new Vector3(0, Time.deltaTime, 0);
        Destroy(collider);
    }

    void SetMainColor(Color color)
    {
        foreach (Renderer r in GetComponentsInChildren<Renderer>())
        {
            r.material.color = color;
        }
    }
}
