using UnityEngine;
using System.Text;
using System.Collections.Generic;

public class BattleSceneGUI : MonoBehaviour {
    public CharacterStatus characterStatus;
    public Weapon weapon;
    public ZombieWaveSpawn zombieSpawner;

    public GUISkin mySkin;

    public Texture2D characterInfoBg;
    public Texture2D moneyInfoBg;
    public Texture2D gunInfoBg;
    public Texture2D gradeIcon;
    public Texture2D staminaIcon;
    public Texture2D goldIcon;
    public Texture2D DiamondIcon;

    public Texture2D gradeBarBg;
    public Texture2D gradeValueBar;
    public Texture2D gradeValueBarEnd;
    public Texture2D staminaBarBg;
    public Texture2D staminaValueBar;
    public Texture2D staminaValueBarEnd;

    public Texture2D zombieWaveInfoBg;
    public Texture2D zombieWaveOnIcon;
    public Texture2D zombieWaveOffIcon;
    public Texture2D settingPanelBg;
    public float waveInfoShowTime;

    public HorizontalSlider musicSlider;
    public HorizontalSlider soundSlider;

    public Texture texTaskPanelBg;
    int taskInterval = 50;

    int level;
    int expPercentage;
    int hpPercentage;
    //int wave = 0;
    float timeCounter = 0;
    bool showWaveInfo = false;
    bool isPause = false;
    bool showPausePanel;
    bool showSettingPanel;

    Texture2D[] itemIcons;
    Texture2D gunIcon;
    Texture2D characterHeadIcon;
    GlobalPlayerInfo playerInfo;

    //Font Styles
    GUIStyle levelStyle;
    GUIStyle lifeStyle;
    GUIStyle moneyStyle;
    GUIStyle expStyle;
    GUIStyle loaderSizeStyle;
    GUIStyle bulletNumStyle;
    GUIStyle waveStyle;
    GUIStyle settingStyle;

    //用到的需本地化的文本
    string passCondition;
    string todayTask;
    string finished;

    //***任务相关显示数据
    //当前地图的任务数据
    public MapTaskData maptaskData;
    //用户的任务数据
    public List<TaskData> playertaskData;
    //任务的静态数据
    public List<MissionData> missionData;

    void Awake()
    {
        //Init Data References
        characterStatus = GameObject.FindGameObjectWithTag("Player").GetComponent<CharacterStatus>();
        weapon = GameObject.FindGameObjectWithTag("Player").GetComponent<Weapon>();
        zombieSpawner = GameObject.FindGameObjectWithTag("ZombieSpawner").GetComponent<ZombieWaveSpawn>();
        
        //Load Dynamic Textures
        if (GameObject.Find("GlobalData"))
        {
            playerInfo = GameObject.Find("GlobalData").GetComponent<GlobalPlayerInfo>();
            characterHeadIcon = Resources.Load("Battle/bg_" + playerInfo.SelectedCharData.charName) as Texture2D;
            gunIcon = Resources.Load("Shop/bg_" +
                StaticGameData.GetWeaponData(playerInfo.SelectedCharData.outfit.currentGunID).modelName) as Texture2D;
        }

        //Change Font Size According to Screen Resolution Dynamically
        levelStyle = new GUIStyle(mySkin.GetStyle("labLevel"));
        lifeStyle = new GUIStyle(mySkin.GetStyle("labLife"));
        moneyStyle = new GUIStyle(mySkin.GetStyle("labMoney"));
        expStyle = new GUIStyle(mySkin.GetStyle("labExp"));
        loaderSizeStyle = new GUIStyle(mySkin.GetStyle("labLoaderSize"));
        bulletNumStyle = new GUIStyle(mySkin.GetStyle("labBulletNum"));
        waveStyle = new GUIStyle(mySkin.GetStyle("labZombieWave"));
        settingStyle = new GUIStyle(mySkin.GetStyle("labSettingItem"));

        levelStyle.fontSize = (int)(levelStyle.fontSize * CsGUI.YRate);
        lifeStyle.fontSize = (int)(lifeStyle.fontSize * CsGUI.YRate);
        moneyStyle.fontSize = (int)(moneyStyle.fontSize * CsGUI.YRate);
        expStyle.fontSize = (int)(expStyle.fontSize * CsGUI.YRate);
        loaderSizeStyle.fontSize = (int)(loaderSizeStyle.fontSize * CsGUI.YRate);
        bulletNumStyle.fontSize = (int)(bulletNumStyle.fontSize * CsGUI.YRate);
        waveStyle.fontSize = (int)(waveStyle.fontSize * CsGUI.YRate);
        settingStyle.fontSize = (int)(settingStyle.fontSize * CsGUI.YRate);

        TextScript text = StaticGameScripts.GetSceneScript("Task");
        passCondition = text.GetScript("PassCondition");
        todayTask = text.GetScript("TodayTask");
        finished = text.GetScript("Finished");

        if (playerInfo.sceneID > 0)
        {
            TaskModule.Instance.GetDataByScene(playerInfo.sceneID, out maptaskData, out playertaskData, out missionData);
        }        
    }

    void Update()
    {
        if (showWaveInfo)
        {
            timeCounter += Time.deltaTime;
            if (timeCounter > waveInfoShowTime)
            {
                showWaveInfo = false;
                timeCounter = 0;
            }
        }
    }

#region GUI Related Functions
	void OnGUI()
    {
        level = characterStatus.level;
        expPercentage = characterStatus.currentExp * 100 / StaticGameData.GetLevelExp(level);
        hpPercentage = characterStatus.currentHp * 100 / characterStatus.MaxHp;
        GUI.skin = mySkin;
        DrawMiddleInfoPanel();
        DrawCharacterInfo();
        DrawCharacterStatus();
        DrawGunInfo();
        //DrawItemButtons();
        if (GUI.Button(CsGUI.CRect(433, 720, 158, 48), "", "btnShop"))
        {

        }

        if (showPausePanel)
            DrawPausePanel();
        else if (showSettingPanel)
            DrawSettingPanel();
        else if (showWaveInfo)
            DrawWaveInfo();

    }

    #region Main GUI
    void DrawCharacterInfo()
    {
        if (characterHeadIcon)
            GUI.DrawTexture(CsGUI.CRect(14, 15, 102, 78), characterHeadIcon);
        GUI.DrawTexture(CsGUI.CRect(0, 0, 376, 104), characterInfoBg);
        GUI.Label(CsGUI.CRect(36, 76, 70, 25), "LV " + level, levelStyle);
    }

    void DrawCharacterStatus()
    {
        //Stamina
        GUI.DrawTexture(CsGUI.CRect(134, 24, 146, 26), staminaBarBg, ScaleMode.StretchToFill);
        GUI.DrawTexture(CsGUI.CRect(134, 24, 1.4f * hpPercentage, 26), staminaValueBar, ScaleMode.StretchToFill);
        GUI.DrawTexture(CsGUI.CRect(134 + 1.4f * hpPercentage, 24, 6, 26), staminaValueBarEnd);
        GUI.DrawTexture(CsGUI.CRect(124, 23, 33, 30), staminaIcon);
        GUI.Label(CsGUI.CRect(170, 26, 80, 26), characterStatus.currentHp.ToString(), lifeStyle);
        //Exp
        GUI.DrawTexture(CsGUI.CRect(136, 55, 139, 17), gradeBarBg, ScaleMode.StretchToFill);
        GUI.DrawTexture(CsGUI.CRect(136, 55, 1.26f * expPercentage, 17), gradeValueBar, ScaleMode.StretchToFill);
        GUI.DrawTexture(CsGUI.CRect(136 + 1.26f * expPercentage, 55, 23, 17), gradeValueBarEnd);
        GUI.DrawTexture(CsGUI.CRect(121, 51, 31, 30), gradeIcon);
        GUI.Label(CsGUI.CRect(170, 55, 80, 17), expPercentage + "%", expStyle);

    }

    void DrawMiddleInfoPanel()
    {
        GUI.DrawTexture(CsGUI.CRect(543, 0, 256, 57), moneyInfoBg);
        GUI.DrawTexture(CsGUI.CRect(337, 0, 256, 57), moneyInfoBg);
        //Gold
        GUI.DrawTexture(CsGUI.CRect(376, 3, 40, 40), goldIcon);
        GUI.Label(CsGUI.CRect(420, 10, 130, 25), playerInfo.playerData.gold.ToString(), moneyStyle);
        if (GUI.Button(CsGUI.CRect(563, 12, 24, 24), "", "btnAddMoney"))
        {

        }
        //Diamond
        GUI.DrawTexture(CsGUI.CRect(586, 3, 40, 40), DiamondIcon);
        GUI.Label(CsGUI.CRect(629, 10, 130, 25), playerInfo.playerData.diamond.ToString(), moneyStyle);
        if (GUI.Button(CsGUI.CRect(768, 12, 24, 24), "", "btnAddMoney"))
        {

        }
        //Pause
        bool pause = GUI.Toggle(CsGUI.CRect(797, 0, 52, 41), isPause, "", "btnPause");
        if (GUI.Button(CsGUI.CRect(782, 0, 80, 80), "", "noneButton"))
        {
            pause = !isPause;
        }
        if(pause != isPause)
        {
            isPause = pause;
            Time.timeScale = isPause ? 0 : 1;
            showPausePanel = isPause;
        }
    }

    void DrawGunInfo()
    {
        //GUI.DrawTexture(CsGUI.CRect(800, 0, 224, 98), gunInfoBg);
        if(gunIcon)
            GUI.DrawTexture(CsGUI.CRect(880, 0, 144, 104), gunIcon);
        GUI.Label(CsGUI.CRect(900, 80, 80, 30), "/" + weapon.LoaderSize, loaderSizeStyle);//can optimize
        GUI.Label(CsGUI.CRect(820, 80, 80, 30), weapon.CurrentLoaderSize.ToString(), bulletNumStyle);
    }

    void DrawItemButtons()
    {
        //skill1
    //    GUI.DrawTexture(CsGUI.CRect(843, 490, 66, 67), itemIcons[0]);
        if (GUI.Button(CsGUI.CRect(870, 455, CsGUI.CY(90), CsGUI.CY(90), UIRelative.PositionRelative), "", "btnSkill"))
        {

        }
        //skill2;
   //     GUI.DrawTexture(CsGUI.CRect(750, 522, 66, 67), itemIcons[1]);
        if (GUI.Button(CsGUI.CRect(765, 467, CsGUI.CY(90), CsGUI.CY(90), UIRelative.PositionRelative), "", "btnSkill"))
        {

        }
        //skill3
  //      GUI.DrawTexture(CsGUI.CRect(701, 600, 66, 67), itemIcons[2]);
        if (GUI.Button(CsGUI.CRect(691, 570, CsGUI.CY(90), CsGUI.CY(90), UIRelative.PositionRelative), "", "btnSkill"))
        {

        }
        //skill4
   //     GUI.DrawTexture(CsGUI.CRect(722, 690, 66, 67), itemIcons[3]);
        if (GUI.Button(CsGUI.CRect(702, 680, CsGUI.CY(90), CsGUI.CY(90), UIRelative.PositionRelative), "", "btnSkill"))
        {

        }
    }

    void DrawWaveInfo()
    {
        DrawSkullPanel(320, 390);
        StringBuilder waveMsg = new StringBuilder((zombieSpawner.currentWaveIndex + 1).ToString());
        switch(zombieSpawner.currentWaveIndex % 10)
        {
            case 0:
                waveMsg.Append("st");
                break;
            case 1:
                waveMsg.Append("nd");
                break;
            case 2:
                waveMsg.Append("rd");
                break;
            default:
                waveMsg.Append("th");
                break;
        }
        waveMsg.Append(" WAVE !");
        GUI.Label(CsGUI.CRect(410, 460, 230, 60), waveMsg.ToString(), waveStyle);
    }

    void DrawSkullPanel(float lx, float ly)
    {
        GUI.DrawTexture(CsGUI.CRect(lx, ly, 428, 162), zombieWaveInfoBg);
        float length = zombieSpawner.totalWaveNum * 35;
        float startX = 536 - length * .5f;
        for (int i = 0; i < zombieSpawner.totalWaveNum; ++i)
        {
            if (i > zombieSpawner.currentWaveIndex)
                GUI.DrawTexture(CsGUI.CRect(startX, ly + 139, 35, 39), zombieWaveOffIcon);
            else
                GUI.DrawTexture(CsGUI.CRect(startX, ly + 139, 35, 39), zombieWaveOnIcon);
            startX += 35;
        }
    }
    #endregion

    #region Pause GUI
    void DrawPausePanel()
    {
        //DrawSkullPanel(320, 200);
        /*GUI.Label(CsGUI.CRect(435, 270, 200, 65), "Pause", waveStyle);
        if (GUI.Button(CsGUI.CRect(245, 324, 131, 140), "", "btnMenu"))
        {
            Time.timeScale = 1;
            Application.LoadLevel("SelectLevel");
        }
        if (GUI.Button(CsGUI.CRect(388, 368, 131, 140), "", "btnRestart"))
        {
        }
        if (GUI.Button(CsGUI.CRect(531, 389, 131, 140), "", "btnContinue"))
        {
            showPausePanel = false;
            isPause = false;
            Time.timeScale = 1;
        }
        if (GUI.Button(CsGUI.CRect(674, 324, 131, 140), "", "btnSetting"))
        {
            showSettingPanel = true;
            showPausePanel = false;
        }*/
        DrawTaskPanel();
    }

    void DrawTaskPanel()
    {
        GUI.DrawTexture(CsGUI.CRect(200, 200, 624, 368), texTaskPanelBg);

        if (playertaskData.Count != 0)
        {
            GUI.Label(CsGUI.CRect(500, 200, 400, 50), passCondition);
            GUI.Label(CsGUI.CRect(300, 250, 400, 50), maptaskData.WinCondition);

            GUI.Label(CsGUI.CRect(500, 300, 400, 50), todayTask);

            for (int i = 0; i < playertaskData.Count; ++i)
            {
                //text
                GUI.Label(CsGUI.CRect(300, 350 + i * taskInterval, 200, 100), missionData[i].Des);
                //process
                if (playertaskData[i].State == TaskState.Finished)
                {
                    GUI.Label(CsGUI.CRect(500, 350 + i * taskInterval, 100, 100), finished);
                }
                else
                {
                    GUI.Label(CsGUI.CRect(500, 350 + i * taskInterval, 100, 100), playertaskData[i].Arg + "/" + missionData[i].ObjValue2);
                }
                //reward
                GUI.Label(CsGUI.CRect(600, 350 + i * taskInterval, 300, 100), missionData[i].GainGold + " " + missionData[i].GainExp);
            }
        }
        else
        {
            GUI.Label(CsGUI.CRect(500, 300, 400, 50), passCondition);
            GUI.Label(CsGUI.CRect(300, 350, 400, 50), maptaskData.WinCondition);
        }

        //back button
        if (GUI.Button(CsGUI.CRect(300, 500, 50, 50), "", "btnRestart"))
        {
            Time.timeScale = 1;
            GameObject g = GameObject.Find("BattleSceneManager") as GameObject;
            if (g)
                g.GetComponent<BattleSceneManager>().OnExitBattleScene();
            Application.LoadLevel("SelectLevel");
        }
        //setting button
        if (GUI.Button(CsGUI.CRect(450, 500, 50, 50), "", "btnSetting"))
        {
            showSettingPanel = true;
            showPausePanel = false;
        }
        //continue button
        if (GUI.Button(CsGUI.CRect(600, 500, 50, 50), "", "btnContinue"))
        {
            showPausePanel = false;
            isPause = false;
            Time.timeScale = 1;
        }
    }
    #endregion

    #region Setting GUI
    void DrawSettingPanel()
    {
        GUI.DrawTexture(CsGUI.CRect(320, 100, 429, 325), settingPanelBg);
        GUI.Label(CsGUI.CRect(440, 160, 190, 60), "Settings", waveStyle);
        GUI.contentColor = Color.blue;
        GUI.Label(CsGUI.CRect(360, 250, 120, 60), "Music", settingStyle);
        GUI.contentColor = Color.red;
        GUI.Label(CsGUI.CRect(360, 315, 120, 60), "Sound", settingStyle);
        GUI.contentColor = Color.white;

        playerInfo.playerData.settings.music = musicSlider.Draw(CsGUI.CRect(520, 270, 181, 13),
            0, 1, playerInfo.playerData.settings.music, new Vector2(CsGUI.CX(37), CsGUI.CX(37)));
        playerInfo.playerData.settings.sound = soundSlider.Draw(CsGUI.CRect(520, 330, 181, 13), 
            0, 1, playerInfo.playerData.settings.sound, new Vector2(CsGUI.CX(37), CsGUI.CX(37)));

        if (GUI.Button(CsGUI.CRect(465, 450, 131, 140), "", "btnContinue"))
        {
            isPause = false;
            Time.timeScale = 1;
            showSettingPanel = false;
        }
    }
    #endregion

#endregion

    public void ShowNewWaveInfo()
    {
        showWaveInfo = true;
    }
}
