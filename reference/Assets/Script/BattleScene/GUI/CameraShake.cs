using UnityEngine;
using System.Collections;

public class CameraShake : ImageEffectBase {

    public float shakeInterval = 0.1f;
    public float maxShakeUV = 0.02f;

    void Start()
    {
        enabled = false;
    }

    void OnEnable()
    {
        StartCoroutine(Shake());
    }

	void OnRenderImage (RenderTexture source, RenderTexture destination)
    {
        source.wrapMode = TextureWrapMode.Clamp;
        Graphics.Blit(source, destination, material);
    }

    IEnumerator Shake()
    {
        while (enabled)
        {
            material.SetFloat("_DeltaX", Random.Range(-maxShakeUV, maxShakeUV));
            material.SetFloat("_DeltaY", Random.Range(-maxShakeUV, maxShakeUV));
            yield return new WaitForSeconds(shakeInterval);
        }
    }

    public void ShakeForSeconds(float time)
    {
        enabled = true;
        StartCoroutine(WaitForShakeEnd(time));
    }

    IEnumerator WaitForShakeEnd(float time)
    {
        yield return new WaitForSeconds(time);
        enabled = false;
    }
}
