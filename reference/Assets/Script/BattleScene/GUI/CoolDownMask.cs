using UnityEngine;
using System.Collections;

[RequireComponent(typeof(Camera))]
public class CoolDownMask : ImageEffectBase {
    public Texture2D maskTex;

    //GameObject screenQuad = null;
    public float cd;
    public float duration;

    public RenderTexture MaskTexture
    {
        get { return camera.targetTexture; }
        set { camera.targetTexture = value; }
    }

    void Awake()
    {
        camera.enabled = false;
        cd = 0;
        duration = 1;
//         if (!screenQuad)
//         {
//             screenQuad = new GameObject();
//             screenQuad.transform.position = transform.TransformPoint(0, 0, 3);
//             screenQuad.transform.parent = transform;
//             screenQuad.layer = LayerMask.NameToLayer("cdMask");
// 
//             MeshFilter filter = screenQuad.AddComponent<MeshFilter>();
//             Mesh quadPlane = new Mesh();
//             Vector3[] verts = new Vector3[4];
//             verts[0] = new Vector3(-1, -1, 0);
//             verts[1] = new Vector3(1, -1, 0);
//             verts[2] = new Vector3(1, 1, 0);
//             verts[3] = new Vector3(-1, 1, 0);
//             quadPlane.vertices = verts;
//             int[] indexs = { 0, 2, 1, 2, 0, 3 };
//             quadPlane.triangles = indexs;
//             Vector2[] uvs = { new Vector2(0, 0), new Vector2(1, 0), new Vector2(1, 1), new Vector2(0, 1) };
//             quadPlane.uv = uvs;
//             filter.mesh = quadPlane;
// 
//             MeshRenderer render = screenQuad.AddComponent<MeshRenderer>();
//             render.material = new Material(Shader.Find("ZombieStyle/ScreenAliasCdMask"));
//             render.material.mainTexture = maskTex;
//             render.material.SetFloat("_CDAlphaMask", 0);
//             render.material.SetFloat("_DurationAlphaMask", 1);
//         }
    }

    public void SetPercentage(float cd, float duration)
    {
        if (!camera.enabled)
            camera.enabled = true;
        //if (screenQuad)
        //{
        //    screenQuad.renderer.material.SetFloat("_CDAlphaMask", cd);
        //    screenQuad.renderer.material.SetFloat("_DurationAlphaMask", duration);
        //}
        this.cd = cd;
        this.duration = duration;
    }

    public void DisableMask()
    {
        camera.enabled = false;
    }

    void OnRenderImage (RenderTexture source, RenderTexture destination)
    {
        material.SetFloat("_CDAlphaMask", cd);
        material.SetFloat("_DurationAlphaMask", duration);
        Graphics.Blit(maskTex, destination, material);
	}
}
