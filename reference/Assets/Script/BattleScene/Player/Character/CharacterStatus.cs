using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class BuffAttribute
{
    public int hp = 0;
    public float hpPer = 0;
    public int atk = 0;
    public float atkPer = 0;
    public int def = 0;
    public float defPer = 0;
    public float mSpeed = 0;
    public float mSpeedPer = 0;
    public float aSpeed = 0;
    public float aSpeedPer = 0;
    public int loader = 0;
    public float loaderPer = 0;
    public int range = 0;
    public float rangePer = 0;
    public float critProb = 0;
    public float critRate = 0;
}

/// <summary>
/// Store & Handle Character Attributes
/// Apply Damage
/// </summary>
public class CharacterStatus : MonoBehaviour {
    public int level;
    public int currentExp;
    public int currentHp;
    [HideInInspector]
    public PlayerBuffManager buffManager;
    [HideInInspector]
    public BuffAttribute buffAttribute;
    [HideInInspector]
    public LevelData attributes;
    [HideInInspector]
    public bool alive = true;
    Color mainColor;
    float timeRecorder = 0;

    CharacterStoreData charData;
    string debugInfo;

    public int MaxHp
    {
        get{ return (int)((attributes.HP + buffAttribute.hp) * (1 + buffAttribute.hpPer)); }
    }
    public int Atk
    {
        get
        {
            return (int)((attributes.ATK + buffAttribute.atk) * (1 + buffAttribute.atkPer));
        }
    }
    public int Def
    {
        get
        {
            return (int)((attributes.DEF + buffAttribute.def) * (1 + buffAttribute.defPer));
        }
    }
    public float MSpeed
    {
        get { return (4 + attributes.MoveSpeed + buffAttribute.mSpeed) * (1 + buffAttribute.mSpeedPer); }
    }
    public float ASpeedPer
    {
        get { return 1 + buffAttribute.aSpeedPer; }
    }
    public float LoaderPer
    {
        get { return buffAttribute.loaderPer; }
    }
    public float RangePer
    {
        get { return buffAttribute.rangePer; }
    }
    public float CritProb
    {
        get { return attributes.Crit + buffAttribute.critProb; }
    }
    public float CritRate
    {
        get { return attributes.CritRate + buffAttribute.critRate; }
    }

    void Awake()
    {
        mainColor = Color.white;
        buffManager = GetComponent<PlayerBuffManager>();
        buffAttribute = new BuffAttribute();
        if (GameObject.Find("GlobalData") != null)
        {
            //Load Character Data from Record
            charData = GameObject.Find("GlobalData").GetComponent<GlobalPlayerInfo>().SelectedCharData;
            level = charData.currentLevel;
            currentExp = charData.currentExp;
            //Load & Set Character Level Attribute form Static Data
            attributes = StaticGameData.GetCharacterLevelData(charData.charID, level);
            currentHp = MaxHp;
        }
    }

    void ApplyDamage(int dmg)
    {
        dmg = (int)(dmg * (1 - Def * 0.01f));
        if (dmg < 1)
            dmg = 1;
        currentHp -= dmg;
        if (currentHp <= 0)
        {
            currentHp = 0;
            OnStartDying();
        }
    }

    void ApplyDamage(BuffDmgInfo atkInfo)
    {
        ApplyDamageInfo(atkInfo.dmg, atkInfo.buff, atkInfo.applier);
    }

    void ApplyDamage(BulletInfo bullet)
    {
        ApplyDamageInfo(bullet.dmg, bullet.effect, bullet.shooter);
    }

    void ApplyDamageInfo(float dmg, int effect, GameObject shooter)
    {
        dmg = dmg * (100.0f - Def) * 0.01f;
        if (dmg < 1)
            dmg = 1;

        if (buffManager.HasBuff(PlayerBuff.DmgAbsorb))
        {
            float shieldEnergy = buffManager.GetBuffData(PlayerBuff.DmgAbsorb).GetFloat("AbsorbAmount");
            if (dmg <= shieldEnergy)
            {
                Debug.Log(shieldEnergy);
                buffManager.GetBuffData(PlayerBuff.DmgAbsorb).SetFloat("AbsorbAmount", shieldEnergy - dmg);
                dmg = 0;
            }
            else if(shieldEnergy > 0)
            {
                dmg -= shieldEnergy;
                buffManager.GetBuffData(PlayerBuff.DmgAbsorb).SetFloat("AbsorbAmount", 0);
            }
        }

        currentHp -= (int)dmg;
        if(dmg > 0)
            StartCoroutine(OnHurtChangeColor(effect == 2 ? Color.cyan : Color.red));

        if (buffManager.HasBuff(PlayerBuff.DmgRebound))
        {
            int rebondDmg = (int)(dmg * buffManager.GetBuffData(PlayerBuff.DmgRebound).GetFloat("ReboundPercentage"));
            if (rebondDmg < 1) rebondDmg = 1;
            shooter.SendMessage("ApplyDamage", new BuffDmgInfo(rebondDmg, 0, gameObject));
        }
        if (currentHp <= 0)
        {
            currentHp = 0;
            OnStartDying();
        }
        else if (effect == 2)
        {
            buffManager.AddBuff(PlayerBuff.Frozen);
        }
    }

    public void HpRecovery(int hp)
    {
        int lackHp = MaxHp - currentHp;
        int addHp = hp < lackHp ? hp : lackHp;
        currentHp += addHp;

    }

    void OnKilledEnemy(int exp)
    {
        currentExp += exp;
        if (charData != null)
            charData.currentExp = currentExp;
        if (currentExp >= StaticGameData.GetLevelExp(level))
        {
            currentExp -= StaticGameData.GetLevelExp(level);
            level++;
            if (charData != null)
                charData.currentExp = currentExp;

            attributes = StaticGameData.GetCharacterLevelData(charData.charID, level);
            currentHp = MaxHp;
            if (charData != null)
                charData.currentLevel = level;
        }
    }

    void OnGUI()
    {
    }

    void Update()
    {
        if (!alive)
        {
            timeRecorder += Time.deltaTime;
            if (timeRecorder > 2)
            {
                /*if (networkView)
                {
                    if(NetworkManager.Instance.IsServer)
                        networkView.RPC("CharacterOnAlive", RPCMode.All);
                }
                else
                    CharacterOnAlive();*/
            }
        }
    }

    void OnStartDying()
    {
        rigidbody.isKinematic = true;
        collider.enabled = false;
        GetComponent<TPSController>().enabled = false;
        GetComponent<PlayerMovementMoter>().isActive = false;
        GetComponent<PlayerAnimation>().OnStartDying();
        GetComponent<Weapon>().OnStopShooting();
    }

    public void OnDeath()
    {
        if (!alive)
            return;
        alive = false;
        foreach (Renderer r in GetComponentsInChildren<Renderer>())
        {
            r.enabled = false;
        }
        timeRecorder = 0;
    }

    /*[RPC]
    void CharacterOnAlive()
    {
        currentHp = MaxHp;
        collider.enabled = true;
        rigidbody.isKinematic = false;
        transform.position = Vector3.zero;
        GetComponent<PlayerAnimation>().OnAlive();
        alive = true;
        foreach (Renderer r in GetComponentsInChildren<Renderer>())
        {
            r.enabled = true;
        }
        GetComponent<TPSController>().enabled = true;
        GetComponent<PlayerMovementMoter>().isActive = true;
        GetComponent<Weapon>().OnReloadComplete();
        foreach (Transform t in GetComponent<Weapon>().gunTransforms)
        {
            t.FindChild("MuzzleFlash").renderer.enabled = false;
        }
        
    }*/

    IEnumerator OnHurtChangeColor(Color color)
    {
        foreach (SkinnedMeshRenderer smr in GetComponentsInChildren<SkinnedMeshRenderer>())
        {
            smr.material.color = color;
        }
        yield return new WaitForSeconds(0.2f);
        foreach (SkinnedMeshRenderer smr in GetComponentsInChildren<SkinnedMeshRenderer>())
        {
            smr.material.color = mainColor;
        }
    }
}
