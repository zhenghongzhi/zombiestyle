using UnityEngine;
using System.Collections;

/***************************************************
 * Resbonsibility : Store Character's Static Level Attribute,
 *                      Call Level Up to Refresh.
 * 
 * 
 * **************************************************/
public class CharacterAttribute{
    public int maxHP;
    public int attack;
    public int defend;
    public int range;
    public float hitProb;
    public float critProb;
    public float critRate;
    public int moveSpeed;
    public int load;
	
    public void LevelUp()
    {

    }

    public void LoadLevel(int level, int charID)
    {

    }
}
