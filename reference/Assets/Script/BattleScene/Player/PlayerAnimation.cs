using UnityEngine;
using System.Collections;
using System.Collections.Generic;

/*-------------------------------------------
* Serializeable Animation Data Structure
* Store Animation and Direction And Speed
-------------------------------------------*/
[System.Serializable]
public class MoveAnimation {
	// The animation clip
	public AnimationClip clip;
	
	// The velocity of the walk or run cycle in this clip
	public Vector3 velocity;
	
	// Store the current weight of this animation
	[HideInInspector]
	public float weight;
	
	// Keep track of whether this animation is currently the best match
	[HideInInspector]
	public bool currentBest = false;
	
	// The speed and angle is directly derived from the velocity,
	// but since it's slightly expensive to calculate them
	// we do it once in the beginning instead of in every frame.
	[HideInInspector]
	public float speed;
	[HideInInspector]
	public float angle;

	
	public void Init () {
		velocity.y = 0;
		speed = velocity.magnitude;
		angle = PlayerAnimation.HorizontalAngle (velocity);
	}
}

public enum CharacterUpperAnimState
{
    Holding,
    Attacking,
    Reloading,
    Resting,
    Dying,
}

#region Animation State Machine
/*-----------------------------------------------
 * Use State Machine Structure to Handle
 * the Complicated Player Animations.
 * The Machine Structure is :
 *  -----------------------------
 *  |       AliveMachine         |
 *  |              |                    |
 *  |  ShootState(Machine)  | <--------> DieState
 *  |              |                    |
 *  |  MoveState(Machine)   |
 *  -----------------------------
 *  -----------------------------------------------*/

#region Interfaces and Base Classes
public enum StateName
{
    Alive,
    Shoot,
    Move,
    Idle,
    Die,
}

public interface IState
{
    StateName Name
    {
        get;
    }

    IStateMachine SuperState
    {
        get;
        set;
    }

    void OnEnterState();
    void StateUpdate();
    void OnExitState();
}

public interface IStateMachine : IState
{
    void SetState(StateName stateName);
    IState GetState(StateName stateName);
}

public class AnimStateBase : IState
{
    protected StateName stateName;
    protected PlayerAnimation animController; //Mono Object, Access Animations & State Determination Parameters through this
    protected AnimStateMachineBase superState = null; //Upper level State(Machine), Access Other States through this

    public StateName Name
    {
        get { return stateName; }
    }

    public IStateMachine SuperState
    {
        get { return superState; }
        set { superState = value as AnimStateMachineBase; }
    }

    protected AnimStateBase(PlayerAnimation anim, StateName stateName)
    {
        this.animController = anim;
        this.stateName = stateName;
    }

    public virtual void OnEnterState() { }

    public virtual void StateUpdate() { }

    public virtual void OnExitState() { }
}

public class AnimStateMachineBase : IStateMachine
{
    protected StateName stateName;
    protected PlayerAnimation animController;
    protected AnimStateMachineBase superState = null;
    public Dictionary<StateName, IState> states;

    public StateName Name
    {
        get { return stateName; }
    }

    public IStateMachine SuperState
    {
        get { return superState; }
        set { superState = value as AnimStateMachineBase; }
    }

    protected AnimStateMachineBase(PlayerAnimation anim, StateName name)
    {
        animController = anim;
        stateName = name;
        states = new Dictionary<StateName, IState>();
    }

    public void RegisterState(IState state)
    {
        if (!states.ContainsKey(state.Name))
        {
            states.Add(state.Name, state);
            state.SuperState = this;
        }
    }

    public virtual void SetState(StateName stateName) { }
    public virtual IState GetState(StateName stateName) { return states[stateName]; }
    public virtual void OnEnterState() { }
    public virtual void StateUpdate() { }
    public virtual void OnExitState() { }
}
#endregion

public class MoveAnimState : AnimStateBase
{
    float minWalkSpeed = .5f;
    float maxIdleSpeed = 2;
    float idleWeight = 0;
    float restTime;
    MoveAnimation bestAnimation = null;

    public MoveAnimState(PlayerAnimation playerAnim)
        : base(playerAnim, StateName.Move)
    {
    }

    public override void OnEnterState()
    {
        idleWeight = 0;
        bestAnimation = null; 
        foreach (MoveAnimation moveAnimation in animController.moveAnimations)
        {
            animController.animation[moveAnimation.clip.name].enabled = true;
        }
        animController.animation[animController.run.name].enabled = true;
        animController.animation[animController.run.name].weight = 0;

        animController.animation[animController.idle.name].enabled = true;
        animController.animation[animController.idle.name].weight = 1;
    }

    public override void StateUpdate()
    {
        if (((ShootAnimState)superState.GetState(StateName.Shoot)).Active)
            animController.animation.Stop(animController.rest.name);
        //determine idle weight, so that it won't hide lower body run animations
        idleWeight = 1 - Mathf.Lerp(1 - idleWeight, Mathf.InverseLerp(minWalkSpeed, maxIdleSpeed, animController.speed), 0.1f);
        animController.animation[animController.idle.name].weight = idleWeight;

        if (animController.speed > 0)
        {
            restTime = 0;
            if (!((ShootAnimState)superState.GetState(StateName.Shoot)).Active)//No Animation for Upper Body, Use Full Body Running Animation
            {
                animController.animation[animController.run.name].weight = 1 - idleWeight;
                if (animController.animation[animController.rest.name].weight > 0)
                    animController.animation.Blend(animController.rest.name, 0, 0.2f);
            }
            else//Select Best Lower Body Moving Animation
            {
                animController.animation[animController.run.name].weight = 0;
                float smallestDiff = Mathf.Infinity;
                foreach (MoveAnimation moveAnimation in animController.moveAnimations)
                {
                    float angleDiff = Mathf.Abs(Mathf.DeltaAngle(animController.angle, moveAnimation.angle));
                    float speedDiff = Mathf.Abs(animController.speed - moveAnimation.speed);
                    float diff = angleDiff + speedDiff;
                    if (moveAnimation == bestAnimation)
                        diff *= 0.9f;

                    if (diff < smallestDiff)
                    {
                        bestAnimation = moveAnimation;
                        smallestDiff = diff;
                    }
                }
                animController.animation.CrossFade(bestAnimation.clip.name, .2f, PlayMode.StopSameLayer);
            }
        }
        else//Play Idle (disable run animation to let idle animation control the bones of character from lower anim level)
        {
            animController.animation.Blend(animController.run.name, 0, .2f);
            if (((ShootAnimState)superState.GetState(StateName.Shoot)).Active)//Always player first frame if shooting
            {
                animController.animation[animController.idle.name].time = 0;
            }
            else
            {
                restTime += Time.deltaTime;
            }
            bestAnimation = null;
        }

        if (restTime > animController.rest.length * 2)
        {
            animController.animation.Blend(animController.rest.name, 1);
            restTime = 0;
        }
    }

    public override void OnExitState()
    {
        //Stop all move & idle animation
        foreach (MoveAnimation moveAnimation in animController.moveAnimations)
        {
            animController.animation[moveAnimation.clip.name].enabled = true;
            animController.animation[moveAnimation.clip.name].weight = 0;
        }
        animController.animation[animController.run.name].enabled = true;
        animController.animation[animController.run.name].weight = 0;

        animController.animation[animController.idle.name].enabled = false;
        animController.animation[animController.idle.name].weight = 1;
    }
}

public class ShootAnimState : AnimStateBase
{
    public bool active = false;//Whether there's A Running Upper Body Animation
    public bool reloading = false;//Whether reload Animation are playing
    public bool needReload = false;//Whether need to reload after one shot
    public bool shoot = false;//Whether shoot Animation are playing

    public bool Active
    {
        get { return active; }
    }

    public ShootAnimState(PlayerAnimation playerAnim) : base(playerAnim, StateName.Shoot)
    {}

    public override void OnEnterState() 
    {
        active = false;
        reloading = false;
        needReload = false;
        shoot = false;
        animController.animation[animController.shoot.name].enabled = true;
        animController.animation[animController.reload.name].enabled = true;
        animController.animation[animController.shoot.name].weight = 0;
        animController.animation[animController.reload.name].weight = 0;
    }

    public override void StateUpdate()
    {
        if (reloading)
        {
            if (!animController.rightJoystickActive && animController.animation[animController.reload.name].time > animController.reload.length - 0.2f)
            {
                reloading = false;
                animController.weapon.OnReloadComplete();
                animController.animation.Blend(animController.reload.name, 0, .3f);
                active = false;
            }
            if (!animController.animation[animController.reload.name].enabled)//Complete Reload Animation
            {
                reloading = false;
                animController.weapon.OnReloadComplete();
                if (!animController.rightJoystickActive)
                    active = false;
            }
        }
        else// not reloading
        {
            //Check if Start Shooting
            if (!shoot && animController.rightJoystickActive && Vector3.Dot(animController.transform.forward, animController.GetComponent<PlayerMovementMoter>().faceDir) > 0.9f)
            {
                shoot = true;
                active = true;
                animController.weapon.OnStartShooting();
                animController.animation[animController.shoot.name].time = 0;
                animController.animation.Blend(animController.shoot.name, 1, .2f);
                return;
            }

            if (animController.shoot.wrapMode == WrapMode.Once)
            {
                if (!animController.rightJoystickActive && shoot && animController.animation[animController.shoot.name].time > animController.shoot.length - 0.3f)
                {
                    active = false;
                    shoot = false;
                    animController.animation.Blend(animController.shoot.name, 0, .3f);
                }
                if (!animController.animation[animController.shoot.name].enabled)//Change Animation After a Shoot Animation Completed
                {
                    if (needReload)//First Check Reloading
                    {
                        reloading = true;
                        needReload = false;
                        shoot = false;
                        animController.animation.Blend(animController.reload.name, 1, .2f);
                    }
                    else if (!animController.rightJoystickActive)//Second Check Stop
                    {
                        active = false;
                        shoot = false;
                    }
                    else if (shoot)//Play Shoot Animation Again
                    {
                        animController.weapon.OnStartShooting();
                        animController.animation.Play(animController.shoot.name);
                    }
                }
            }
            else //animController.shoot.wrapMode == WrapMode.Loop
            {
                if (needReload)
                {
                    reloading = true;
                    needReload = false;
                    animController.animation.Blend(animController.reload.name, 1, .2f);
                    animController.animation.Blend(animController.shoot.name, 0, .2f);
                    animController.weapon.OnStopShooting();
                    shoot = false;
                }
                else if (shoot && !animController.rightJoystickActive && animController.animation[animController.shoot.name].time > animController.shoot.length - 0.2f)
                {
                    shoot = false;
                    active = false;
                    animController.animation.Blend(animController.shoot.name, 0, .2f);
                    animController.weapon.OnStopShooting();
                } 
            }
        }
    }

    public override void OnExitState() 
    {
        active = false;
        animController.animation[animController.shoot.name].weight = 0;
        animController.animation[animController.reload.name].weight = 0;
    }

    public void OnNeedReload()
    {
        needReload = true;
    }
}

public class AliveAnimStateMachine : AnimStateMachineBase
{
    public AliveAnimStateMachine(PlayerAnimation anim) : base(anim, StateName.Alive)
    {
        RegisterState(new MoveAnimState(anim));
        RegisterState(new ShootAnimState(anim));
    }

    public override void OnEnterState() 
    {
        states[StateName.Move].OnEnterState();
        states[StateName.Shoot].OnEnterState();
    }

    public override void StateUpdate()
    {
        states[StateName.Move].StateUpdate();
        states[StateName.Shoot].StateUpdate();
    }

    public override void OnExitState() 
    {
        states[StateName.Move].OnExitState();
        states[StateName.Shoot].OnExitState();
    }
}

public class DieAnimState : AnimStateBase
{
    public DieAnimState(PlayerAnimation anim)
        : base(anim, StateName.Die)
    {

    }

    public override void OnEnterState() 
    {
        animController.animation.Play(animController.dead.name);
    }

    public override void StateUpdate() 
    {
        if (!animController.animation[animController.dead.name].enabled)
        {
            animController.GetComponent<CharacterStatus>().OnDeath();
        }
    }

    public override void OnExitState() 
    {
        animController.animation[animController.dead.name].enabled = false;
        animController.animation[animController.dead.name].weight = 0;
    }
}
#endregion

/*--------------------------------------------------
*Attached Object : Root of Player Model
*Resbonsibility : Control Animation Blending According
*				  to transform movement and rotation
*				  Rotate UpperBody and LowerBody Separately
*Colabration Class: 
*	MoveAnimation (Store different animations of direction)
*	Animation Component (play animationClip)
*	Transform Component (decide animations)
--------------------------------------------------*/
public class PlayerAnimation : MonoBehaviour
{
    //Animation Control Bones
	public Transform rootBone;
    public Transform upperBodyBone;
    public Transform leftLegBone;
    public Transform rightLegBone;

    //Animation Clips
    public AnimationClip idle;
    public AnimationClip shoot;
    public AnimationClip run;
    public AnimationClip rest;
    public AnimationClip reload;
    public AnimationClip dead;
	public MoveAnimation[] moveAnimations;

    //Movement Parameters
    [HideInInspector]
    public float speed = 0;
    //Lower Body Rotation & Animation Determination Parameters
    [HideInInspector]
    public float angle = 0;
    private Vector3 lastPosition = Vector3.zero;
    private Vector3 velocity = Vector3.zero;
    private Vector3 localVelocity = Vector3.zero;
	private  float lowerBodyDeltaAngle = 0;
	private  Vector3 lowerBodyForwardTarget = Vector3.forward;
	private  Vector3 lowerBodyForward = Vector3.forward;
    //Animation Switch Parameters
    [HideInInspector]
    public bool rightJoystickActive;
    [HideInInspector]
    public bool leftJoystickActive;
    private AliveAnimStateMachine aliveStateMachine;
    private DieAnimState dieState;
    private IState currentState;

    public Weapon weapon
    {
        get { return GetComponent<Weapon>(); }
    }


    /// <summary>
    /// Setting Animations' State: layer, affect bones, default play state
    /// </summary>
    /// 
    void Awake()
    {
        //Layer 1
        foreach (MoveAnimation moveAnimation in moveAnimations)
        {
            moveAnimation.Init();
            animation[moveAnimation.clip.name].layer = 1;
            animation[moveAnimation.clip.name].enabled = true;
            animation[moveAnimation.clip.name].AddMixingTransform(leftLegBone);
            animation[moveAnimation.clip.name].AddMixingTransform(rightLegBone);
        }
        animation.SyncLayer(1);

        //Layer 2
        animation[idle.name].layer = 2;
        animation[idle.name].enabled = true;
        animation[idle.name].weight = 1;

        //Layer 3
        animation[run.name].layer = 4;
        animation[run.name].enabled = true;
        animation[run.name].weight = 0;

        animation[shoot.name].layer = 3;
        animation[shoot.name].weight = 0;
        animation[shoot.name].speed = 1f;
        animation[shoot.name].enabled = true;
        //animationComponent[shoot.name].AddMixingTransform(upperBodyBone);

        //Layer 4
        animation[reload.name].layer = 4;
        animation[reload.name].weight = 0;
        animation[reload.name].enabled = true;
        animation[reload.name].AddMixingTransform(upperBodyBone);

        //Layer 5
        animation[rest.name].layer = 5;
        animation[rest.name].wrapMode = WrapMode.Once;
        animation[rest.name].enabled = false;
        animation[rest.name].weight = 0;

        //Layer 10
        animation[dead.name].layer = 10;
        animation[dead.name].enabled = false;
        animation[dead.name].weight = 0;
    }

	void Start () {
        lastPosition = transform.position;
        aliveStateMachine = new AliveAnimStateMachine(this);
        dieState = new DieAnimState(this);
        currentState = aliveStateMachine;
        currentState.OnEnterState();	
	}

#region Message Functions

    public void OnStartReload()
    {
        ((ShootAnimState)aliveStateMachine.GetState(StateName.Shoot)).OnNeedReload();
    }

    public void OnStartDying()
    {
        currentState.OnExitState();
        currentState = dieState;
        currentState.OnEnterState();
    }

    /// <summary>
    /// Reset All Animations' play state : enabled and weight
    /// </summary>
    public void OnAlive()
    {
        currentState.OnExitState();
        currentState = aliveStateMachine;
        currentState.OnEnterState();
        lastPosition = transform.position; 
    }
#endregion

//     void OnGUI()
//     {
// //         ShootAnimState shootState = aliveStateMachine.GetState(StateName.Shoot) as ShootAnimState;
// //         GUILayout.BeginArea(CsGUI.CRect(30, 300, 200, 300));
// //         GUILayout.Label("Active : " + shootState.Active);
// //         GUILayout.Label("Shoot : " + shootState.shoot);
// //         GUILayout.Label("Reload : " + shootState.reloading);
// //         GUILayout.Label("NeedReload : " + shootState.needReload);
// //         GUILayout.Label(run.name + " " + animation[run.name].weight + " " + animation[run.name].layer + " " + animation[run.name].enabled);
// //         GUILayout.Label(shoot.name + " " + animation[shoot.name].weight + " " + animation[shoot.name].layer + " " + animation[shoot.name].enabled);
// //         GUILayout.Label(idle.name + " " + animation[idle.name].weight + " " + animation[idle.name].layer + " " + animation[idle.name].enabled);
// //         GUILayout.EndArea();
// 
//     }

    //Set Character Movement Parameters in FixedUpdate
	void FixedUpdate () {
        velocity = (transform.position - lastPosition) / Time.deltaTime;
        localVelocity = transform.InverseTransformDirection(velocity);
		localVelocity.y = 0;
		speed = localVelocity.magnitude;
		angle = HorizontalAngle (localVelocity);
        lastPosition = transform.position;
	}

    //Run Animation State Machine in Update
	void Update () {
        currentState.StateUpdate();
	}

    /// <summary>
    /// Handle Different Rotation Between Upper Body and Lower Body
    /// We need to apply this step after animation hands over the control of bones
    /// So put this step in LateUpdate
    /// </summary>
	void LateUpdate () {
        if (!rightJoystickActive)
            return;
		float idle  = 1 - Mathf.InverseLerp (/*minWalkSpeed*/.5f, /*maxIdleSpeed*/2, speed);
		if (idle < 1) {//Running
			// Calculate a weighted average of the animation velocities that are currently used
			Vector3 animatedLocalVelocity  = Vector3.zero;
			foreach(MoveAnimation moveAnimation  in moveAnimations) {
				// Ignore this animation if its weight is 0
				if (animation[moveAnimation.clip.name].weight == 0)
					continue;
				
				// Ignore this animation if its velocity is more than 90 degrees away from current velocity
				if (Vector3.Dot (moveAnimation.velocity, localVelocity) <= 0)
					continue;
				
				// Add velocity of this animation to the weighted average
				animatedLocalVelocity += moveAnimation.velocity * animation[moveAnimation.clip.name].weight;
			}
			
			// Calculate target angle to rotate lower body by in order
 			// to make feet run in the direction of the velocity
			float lowerBodyDeltaAngleTarget  = Mathf.DeltaAngle (
				HorizontalAngle (transform.rotation * animatedLocalVelocity),
				HorizontalAngle (velocity)
			);
			
			// Lerp the angle to smooth it a bit
			lowerBodyDeltaAngle = Mathf.LerpAngle (lowerBodyDeltaAngle, lowerBodyDeltaAngleTarget, Time.deltaTime * 10);
			
			// Update these so they're ready for when we go into idle
			lowerBodyForwardTarget = transform.forward;
			lowerBodyForward = Quaternion.Euler (0, lowerBodyDeltaAngle, 0) * lowerBodyForwardTarget;
		}
		else {//Idling
			// Turn the lower body towards it's target direction
            lowerBodyForward = Vector3.RotateTowards(lowerBodyForward, lowerBodyForwardTarget, Time.deltaTime * 540 * Mathf.Deg2Rad, 1);
			
			// Calculate delta angle to make the lower body stay in place
			lowerBodyDeltaAngle = Mathf.DeltaAngle (
                HorizontalAngle(transform.forward),
				HorizontalAngle (lowerBodyForward)
			);
			// If the body is twisted more than 60 degrees,
			// set a new target direction for the lower body, so it begins turning
			if (Mathf.Abs(lowerBodyDeltaAngle) > 60)
                lowerBodyForwardTarget = transform.forward;
		}

        // Create a Quaternion rotation from the rotation angle
        Quaternion lowerBodyDeltaRotation = Quaternion.Euler(0, lowerBodyDeltaAngle, 0);

        // Only Rotate the leg part of body by the angle
        leftLegBone.rotation = lowerBodyDeltaRotation * leftLegBone.rotation;
        rightLegBone.rotation = lowerBodyDeltaRotation * rightLegBone.rotation;
	}

	public static float HorizontalAngle (Vector3 direction) {
		return Mathf.Atan2 (direction.x, direction.z) * Mathf.Rad2Deg;
	}
}
