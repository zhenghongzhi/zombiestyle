using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public enum SkillEnum : int
{
    Rush = 5001,
    DmgRebound = 5002,
    LifeStrong = 5003,
    Penentrate = 5004,
    AngryThump = 5005,

    MineZone = 5006,
    SpeedFast = 5007,
    SuperLoader = 5008,
    DefStrong = 5009,
    Doom = 5010,

    TeddyDecoy = 5011,
    DmgAbsorb = 5012,
    SuperAttack = 5013,
    SuperCrit = 5014,
    Heal = 5015,

    SlowSmoke = 5016,
    FastShoot = 5017,
    Accurate = 5018,
    FastCoolDown = 5019,
    MentalStorm = 5020,
}

public interface ISpecialSkill
{
    float CDCounter
    {
        get;
    }

    float CDTime
    {
        get;
        set;
    }

    int SkillID
    {
        get;
    }

    bool Active
    {
        get;
    }

    bool Avaliable
    {
        get;
    }

    bool Driving { get; }

    void StartSkill();
    void UpdateSkill();
    void EndSkill();
}

public abstract class SkillBase : ISpecialSkill
{
    protected int skillID;
    protected SpecialSkillsController controller;
    protected float cdTime;
    protected float cdCounter;
    protected bool driving;
    protected bool avaliable;
    protected bool active;

    public float CDPercentage
    {
        get { return cdCounter / cdTime; }
    }

    public abstract float DurationPercentage
    {
        get;
    }

    public float CDCounter
    {
        get { return cdCounter; }
    }

    public float CDTime
    {
        get { return cdTime; }
        set { cdTime = value; }
    }

    public int SkillID
    {
        get { return skillID; }
    }

    public bool Active
    {
        get { return active; }
    }

    public bool Avaliable
    {
        get { return avaliable; }
    }

    public bool Driving
    {
        get { return driving; }
    }

    public SkillBase(int level, SkillData skillData, SpecialSkillsController controller)
    {
        cdTime = skillData.CDTime;
        cdCounter = 0;
        skillID = skillData.SkillID;
        driving = (skillData.SkillType == 1);
        this.controller = controller;
        active = false;
        avaliable = true;
        InitialParameters(level);
    }

    protected abstract void InitialParameters(int level);

    public abstract void StartSkill();

    public virtual void UpdateSkill()
    {
        if (avaliable)
            return;
        cdCounter += Time.deltaTime;
        if (cdCounter >= cdTime)
        {
            avaliable = true;
            cdCounter = 0;
        }
    }

    public abstract void EndSkill();
}

#region Cohen's Skills
public class RushSkill : SkillBase
{
    //Skill Attribute
    float rushSpeed = 20;
    float rushTime;
    //Skill Parameter
    GameObject skillFx;
    PlayerMovementMoter moter;
    float originSpeed;
    float originMass;
    float timeLeft;

    public override float DurationPercentage
    {
        get { return 1 - timeLeft / rushTime; }
    }

    public RushSkill(int level, SkillData skillData, SpecialSkillsController controller) : base(level, skillData, controller) { }

    protected override void InitialParameters(int level)
    {
        rushTime = 0.35f - 1.0f / (level * 5f);
    }

    public override void StartSkill()
    {
        controller.lockSkill = true;
        skillFx = GameObject.Instantiate(Resources.Load("Skill/SkillFX_5001")) as GameObject;
        skillFx.transform.parent = controller.transform;
        skillFx.transform.localPosition = new Vector3(0, 1, 1);
        skillFx.transform.localRotation = Quaternion.identity;
        moter = controller.GetComponent<PlayerMovementMoter>();
        AnimationState animState = controller.animation["charge"];
        animState.layer = 8;
        animState.time = 0;
        controller.animation.Blend("charge", 1, .1f);
        foreach (Transform t in controller.GetComponent<Weapon>().gunTransforms)
        {
            if (t)
            {
                foreach (Renderer r in t.GetComponentsInChildren<Renderer>())
                {
                    r.enabled = false;
                }
            }
        }
        
        originSpeed = moter.WalkSpeed;
        moter.WalkSpeed = rushSpeed;
        originMass = controller.rigidbody.mass;
        controller.rigidbody.mass = 2000;
        timeLeft = rushTime;
        active = true;
        avaliable = false;
    }

    public override void UpdateSkill()
    {
        base.UpdateSkill();

        if (!active)
            return;
        if (timeLeft > 0)
        {
            moter.WalkSpeed = rushSpeed;
            if (moter.moveDir.sqrMagnitude < 0.01f)
                moter.moveDir = controller.transform.forward.normalized;
            timeLeft -= Time.deltaTime;
        }
        else
        {
            EndSkill();
        }
    }

    public override void EndSkill()
    {
        controller.GetComponent<PlayerMovementMoter>().walkSpeed = originSpeed;
        moter.moveDir = Vector3.zero;
        controller.rigidbody.mass = originMass;
        controller.animation.Blend("charge", 0, .1f);
        foreach (Transform t in controller.GetComponent<Weapon>().gunTransforms)
        {
            if (t)
            {
                foreach (Renderer r in t.GetComponentsInChildren<Renderer>())
                {
                    if (r.name != "MuzzleFlash")
                        r.enabled = true;
                }
            }
        }
        GameObject.Destroy(skillFx);
        controller.lockSkill = false;
        active = false;
    }
}

public class DmgReboundSkill : SkillBase
{
    //Skill Attribute
    float rebondPercentage;
    float duration;
    //Skill Parameters
    CharacterStatus status;
    float timeLeft;
    GameObject shieldFX;

    public override float DurationPercentage
    {
        get { return 1 - timeLeft / duration; }
    }

    public DmgReboundSkill(int level, SkillData skillData, SpecialSkillsController controller) : base(level, skillData, controller) { }

    protected override void InitialParameters(int level)
    {
        rebondPercentage = level * 2;
        duration = 10 + level * 2;
    }

    public override void StartSkill()
    {
        controller.lockSkill = true;
        status = controller.GetComponent<CharacterStatus>();
        AnimationState animState = controller.animation["skill_5002"];
        animState.layer = 8;
        animState.time = 0;
        controller.animation.Blend("skill_5002", 1, .1f);
        controller.rigidbody.isKinematic = true;
        controller.GetComponent<PlayerMovementMoter>().isActive = false;
        controller.StartCoroutine(WaitForAnimEnd());
        BuffBase buffData = status.buffManager.AddBuff(PlayerBuff.DmgRebound);
        buffData.SetFloat("ReboundPercentage", rebondPercentage);
        timeLeft = duration;

        //Add Shield FX
        shieldFX = GameObject.Instantiate(Resources.Load("Skill/SkillFX_5002")) as GameObject;
        shieldFX.transform.parent = controller.transform;
        shieldFX.transform.localPosition = new Vector3(0, 0.04f, 0);

        active = true;
        avaliable = false;
    }

    public override void UpdateSkill()
    {
        base.UpdateSkill();

        if (!active)
            return;
        if (timeLeft > 0)
        {
            timeLeft -= Time.deltaTime;
        }
        else
        {
            EndSkill();
        }
    }

    public override void EndSkill()
    {
        status.buffManager.RemoveBuff(PlayerBuff.DmgRebound);

        //End Shield FX
        GameObject.Destroy(shieldFX);

        active = false;
    }

    IEnumerator WaitForAnimEnd()
    {
        yield return new WaitForSeconds(controller.animation["skill_5002"].length);
        controller.rigidbody.isKinematic = false;
        controller.GetComponent<PlayerMovementMoter>().isActive = true;
        controller.lockSkill = false;
    }
}

public class StrongSkill : SkillBase
{
    //Skill Attribute
    int extraLife;
    //Skill Parameters
    CharacterStatus status;
    GameObject fx;

    public override float DurationPercentage
    {
        get { return 1; }
    }

    public StrongSkill(int level, SkillData skillData, SpecialSkillsController controller)
        : base(level, skillData, controller)
    { }

    protected override void InitialParameters(int level)
    {
        extraLife = level * 10;
    }

    public override void StartSkill()
    {
        Debug.Log("Start Strong Skill");
        status = controller.GetComponent<CharacterStatus>();
        BuffBase buffData = status.buffManager.AddBuff(PlayerBuff.LifeStrong);
        if(buffData != null)
            buffData.SetFloat("ExtraLife", extraLife);
        status.buffAttribute.hp += extraLife;
        status.currentHp = status.MaxHp;
        fx = GameObject.Instantiate(Resources.Load("Skill/SkillFX_5002")) as GameObject;
        fx.transform.parent = controller.transform;
        fx.transform.localPosition = new Vector3(0, 0.1f, 0);
        fx.transform.localRotation = Quaternion.identity;
        active = true;
        avaliable = false;
    }

    public override void UpdateSkill()
    {
        base.UpdateSkill();
    }

    public override void EndSkill()
    {
        status.buffManager.RemoveBuff(PlayerBuff.LifeStrong);
        status.buffAttribute.hp -= extraLife;
        //End Shield FX
        GameObject.Destroy(fx);
        active = false;
    }
}

public class HighCritProbSkill : SkillBase
{
     //Skill Attribute
    float extraCritProb;
    //Skill Parameters
    CharacterStatus status;

    public override float DurationPercentage
    {
        get { return 1; }
    }

    public HighCritProbSkill(int level, SkillData skillData, SpecialSkillsController controller)
        : base(level, skillData, controller)
    { }

    protected override void InitialParameters(int level)
    {
        extraCritProb = level * 0.05f;
    }

    public override void StartSkill()
    {
        status = controller.GetComponent<CharacterStatus>();
        status.buffManager.AddBuff(PlayerBuff.HighCritProb);
        status.buffAttribute.critProb += extraCritProb;
        status.currentHp = status.MaxHp;
        active = true;
        avaliable = false;
    }

    public override void UpdateSkill()
    {
        base.UpdateSkill();
    }

    public override void EndSkill()
    {
        status.buffManager.RemoveBuff(PlayerBuff.HighCritProb);
        status.buffAttribute.critProb -= extraCritProb;
        //End Shield FX
        active = false;
    }
}

public class AreaStunSkill : SkillBase
{
    GameObject fx;

    public override float DurationPercentage
    {
        get { return 1; }
    }

    public AreaStunSkill(int level, SkillData skillData, SpecialSkillsController controller)
        : base(level, skillData, controller)
    { }

    protected override void InitialParameters(int level)
    {
    }

    public override void StartSkill()
    {
        controller.lockSkill = true;
        AnimationState animState = controller.animation["skill_5005"];
        animState.layer = 8;
        animState.time = 0;
        controller.rigidbody.isKinematic = true;
        controller.GetComponent<PlayerMovementMoter>().isActive = false;
        controller.animation.Blend("skill_5005", 1, .1f);
        active = true;
        avaliable = false;
        fx = GameObject.Instantiate(Resources.Load("Skill/SkillFX_5003")) as GameObject;
        controller.StartCoroutine(StunZombies());
    }

    public override void UpdateSkill()
    {
        base.UpdateSkill();
    }

    public override void EndSkill()
    {
        active = false;
    }

    IEnumerator StunZombies()
    {
        yield return new WaitForSeconds(0.25f);
        fx.transform.position = controller.transform.position;
        GameObject[] zombies = GameObject.FindGameObjectsWithTag("Zombie");
        foreach (GameObject zombie in zombies)
        {
            ZombieState state = zombie.GetComponent<ZombieAIStateController>().CurrentState;
            if (state != ZombieState.UnderGround && state != ZombieState.Dying && state != ZombieState.DieBlasting &&
                (zombie.transform.position - controller.transform.position).sqrMagnitude < 25)
            {
                zombie.GetComponent<ZombieBuffManager>().AddBuff(ZombieBuff.Stun);
//                 zombie.GetComponent<ZombieAIIdleState>().idleTime = 3;
//                 zombie.GetComponent<ZombieAIStateController>().SetState(ZombieState.Idle);
            }
        }
        EndSkill();
        yield return new WaitForSeconds(0.55f);
        controller.rigidbody.isKinematic = false;
        controller.GetComponent<PlayerMovementMoter>().isActive = true;
        GameObject.Destroy(fx);
        controller.lockSkill = false;
    }
}
#endregion

#region Roy's Skills
public class SpeedFastSkill : SkillBase
{
    float speedIncreasePer;
    float duration;
    CharacterStatus status;
    float timeLeft;

    public override float DurationPercentage
    {
        get { return 1 - timeLeft / duration; }
    }

    public SpeedFastSkill(int level, SkillData skillData, SpecialSkillsController controller)
        : base(level, skillData, controller)
    { }

    protected override void InitialParameters(int level)
    {
        speedIncreasePer = level * 0.1f;
        duration = 5 + level;
    }

    public override void StartSkill()
    {
        status = controller.GetComponent<CharacterStatus>();
        AnimationState animState = controller.animation["skill_5007"];
        animState.layer = 8;
        animState.time = 0;
        controller.animation.Blend("skill_5007", 1, .1f);
        status.buffManager.AddBuff(PlayerBuff.SpeedFast);
        status.buffAttribute.mSpeedPer += speedIncreasePer;
        active = true;
        avaliable = false;
        timeLeft = duration;
    }

    public override void UpdateSkill()
    {
        base.UpdateSkill();
        if (!active)
            return;
        if (timeLeft > 0)
        {
            timeLeft -= Time.deltaTime;
        }
        else
        {
            EndSkill();
        }
    }

    public override void EndSkill()
    {
        status.buffManager.RemoveBuff(PlayerBuff.SpeedFast);
        status.buffAttribute.mSpeedPer -= speedIncreasePer;
        //End Shield FX
        active = false;
    }
}

public class HardDefendSkill : SkillBase
{
    int extraDef;
    CharacterStatus status;

    public override float DurationPercentage
    {
        get { return 1; }
    }

    public HardDefendSkill(int level, SkillData skillData, SpecialSkillsController controller)
        : base(level, skillData, controller)
    { }

    protected override void InitialParameters(int level)
    {
        extraDef = level * 5;
    }

    public override void StartSkill()
    {
        status = controller.GetComponent<CharacterStatus>();
        status.buffManager.AddBuff(PlayerBuff.HardDefend);
        status.buffAttribute.def += extraDef;
        active = true;
        avaliable = false;
    }

    public override void UpdateSkill()
    {
        base.UpdateSkill();
    }

    public override void EndSkill()
    {
        status.buffManager.RemoveBuff(PlayerBuff.HardDefend);
        status.buffAttribute.def -= extraDef;
        //End Shield FX
        active = false;
    }
}

public class SuperLoaderSizeSkill : SkillBase
{
    float sizeIncreasePer;
    Weapon weapon;

    public override float DurationPercentage
    {
        get { return 1; }
    }

    public SuperLoaderSizeSkill(int level, SkillData skillData, SpecialSkillsController controller)
        : base(level, skillData, controller)
    { }

    protected override void InitialParameters(int level)
    {
        sizeIncreasePer = level * 0.1f;
    }

    public override void StartSkill()
    {
        weapon = controller.GetComponent<Weapon>();
        weapon.buffAttribute.loaderSizePer += sizeIncreasePer;
        active = true;
        avaliable = false;
    }

    public override void UpdateSkill()
    {
        base.UpdateSkill();
    }

    public override void EndSkill()
    {
        weapon.buffAttribute.loaderSizePer -= sizeIncreasePer;
        //End Shield FX
        active = false;
    }
}

public class MineZoneSkill : SkillBase
{
    //Skill Attribute
    int dmg;
    float radius;
    //Skill Parameter
    GameObject minePrefab;

    public override float DurationPercentage
    {
        get { return 1; }
    }

    public MineZoneSkill(int level, SkillData skillData, SpecialSkillsController controller) : base(level, skillData, controller) { }

    protected override void InitialParameters(int level)
    {
        dmg = 50 + level * 10;
        radius = 4 + level * 0.5f;
        minePrefab = Resources.Load("Skill/Landmine") as GameObject;
    }

    public override void StartSkill()
    {
        controller.lockSkill = true;
        //Player Throw animation
        AnimationState animState = controller.animation["skill_5006"];
        animState.layer = 8;
        animState.time = 0;
        controller.animation.Blend("skill_5006", 1, .1f);
        //Don't show Gun
        foreach (Transform t in controller.GetComponent<Weapon>().gunTransforms)
        {
            if (t)
            {
                foreach (Renderer r in t.GetComponentsInChildren<Renderer>())
                {
                    r.enabled = false;
                }
            }
        }
        //Stop Moving
        controller.rigidbody.isKinematic = true;
        controller.GetComponent<PlayerMovementMoter>().isActive = false;

        controller.StartCoroutine(WaitForThrowAnimation());
        active = true;
        avaliable = false;
    }

    public override void UpdateSkill()
    {
        base.UpdateSkill();
    }

    public override void EndSkill()
    {
        foreach (Transform t in controller.GetComponent<Weapon>().gunTransforms)
        {
            if (t)
            {
                foreach (Renderer r in t.GetComponentsInChildren<Renderer>())
                {
                    if (r.name != "MuzzleFlash")
                        r.enabled = true;
                }
            }
        }
        controller.rigidbody.isKinematic = false;
        controller.GetComponent<PlayerMovementMoter>().isActive = true;
        active = false;
        controller.lockSkill = false;
    }

    IEnumerator WaitForThrowAnimation()
    {
        yield return new WaitForSeconds(0.66f);
        GameObject mine = GameObject.Instantiate(minePrefab) as GameObject;
        mine.transform.position = controller.transform.TransformPoint(new Vector3(0.5f, 0.1f, 0.6f));
        mine.GetComponent<Landmine>().maxDmg = dmg;
        mine.GetComponent<Landmine>().radius = radius;
        mine.GetComponent<Landmine>().setter = controller.gameObject;
        yield return new WaitForSeconds(controller.animation["skill_5006"].length - 0.66f);
        EndSkill();
    }
}

public class BombingZoneSkill : SkillBase
{
    GameObject bombZonePrefab;
    float timeLeft;
    float duration;

    public override float DurationPercentage
    {
        get { return 1 - timeLeft / duration; }
    }

    public BombingZoneSkill(int level, SkillData skillData, SpecialSkillsController controller) : base(level, skillData, controller) { }

    protected override void InitialParameters(int level)
    {
        bombZonePrefab = Resources.Load("Skill/BombingZone") as GameObject;
        duration = 3 + level;
    }

    public override void StartSkill()
    {
        //Player Throw animation
        controller.lockSkill = true;
        AnimationState animState = controller.animation["skill_5010"];
        animState.layer = 8;
        animState.time = 0;
        controller.animation.Blend("skill_5010", 1, .1f);
        //Don't show Gun
        foreach (Transform t in controller.GetComponent<Weapon>().gunTransforms)
        {
            if (t)
            {
                foreach (Renderer r in t.GetComponentsInChildren<Renderer>())
                {
                    r.enabled = false;
                }
            }
        }
        //Stop Moving
        controller.rigidbody.isKinematic = true;
        controller.GetComponent<PlayerMovementMoter>().isActive = false;

        controller.StartCoroutine(WaitForThrowAnimation());
        active = true;
        avaliable = false;
        timeLeft = duration;
    }

    public override void UpdateSkill()
    {
        base.UpdateSkill();
        if (!active)
            return;
        if (timeLeft > 0)
        {
            timeLeft -= Time.deltaTime;
        }
        else
            EndSkill();
    }

    public override void EndSkill()
    {
        active = false;
    }

    IEnumerator WaitForThrowAnimation()
    {
        yield return new WaitForSeconds(0.83f);
        GameObject bombZone = GameObject.Instantiate(bombZonePrefab) as GameObject;
        bombZone.transform.position = controller.transform.position - Camera.mainCamera.transform.TransformDirection(new Vector3(5f, -10, 10));
        bombZone.GetComponent<BombingZone>().setter = controller.gameObject;
        bombZone.GetComponent<BombingZone>().duration = duration;
        bombZone.GetComponent<BombingZone>().areaSize = 5;
        yield return new WaitForSeconds(controller.animation["skill_5010"].length - 0.66f);
        foreach (Transform t in controller.GetComponent<Weapon>().gunTransforms)
        {
            if (t)
            {
                foreach (Renderer r in t.GetComponentsInChildren<Renderer>())
                {
                    if (r.name != "MuzzleFlash")
                        r.enabled = true;
                }
            }
        }
        controller.rigidbody.isKinematic = false;
        controller.GetComponent<PlayerMovementMoter>().isActive = true;
        controller.lockSkill = false;
    }
}
#endregion

#region Ruby's Skills
public class TeddyDecoySkill : SkillBase 
{
    //Skill Attribute
    float duration;
    //Skill Parameter
    GameObject teddyPrefab;
    float timeLeft;

    public override float DurationPercentage
    {
        get { return 1 - timeLeft / duration; }
    }

    public TeddyDecoySkill(int level, SkillData skillData, SpecialSkillsController controller) : base(level, skillData, controller) { }

    protected override void InitialParameters(int level)
    {
        duration = 4 + level;
        teddyPrefab = Resources.Load("Skill/TeddyBearDecoy") as GameObject;
    }

    public override void StartSkill()
    {
        controller.lockSkill = true;
        //Player Throw animation
         AnimationState animState = controller.animation["skill_5011"];
         animState.layer = 8;
         animState.time = 0;
         controller.animation.Blend("skill_5011", 1, .1f);
        //Don't show Gun
        foreach (Transform t in controller.GetComponent<Weapon>().gunTransforms)
        {
            if (t)
            {
                foreach (Renderer r in t.GetComponentsInChildren<Renderer>())
                {
                    r.enabled = false;
                }
            }
        }
        controller.rigidbody.isKinematic = true;
        controller.GetComponent<PlayerMovementMoter>().isActive = false;
        controller.StartCoroutine(WaitForThrowAnimation());
        active = true;
        avaliable = false;
    }

    public override void UpdateSkill()
    {
        base.UpdateSkill();
        if (!active)
            return;
        if (timeLeft > 0)
        {
            timeLeft -= Time.deltaTime;
        }
    }

    public override void EndSkill()
    {
        foreach (Transform t in controller.GetComponent<Weapon>().gunTransforms)
        {
            if (t)
            {
                foreach (Renderer r in t.GetComponentsInChildren<Renderer>())
                {
                    if (r.name != "MuzzleFlash")
                        r.enabled = true;
                }
            }
        }
        controller.rigidbody.isKinematic = false;
        controller.GetComponent<PlayerMovementMoter>().isActive = true;
        active = false;
        controller.lockSkill = false;
    }

    IEnumerator WaitForThrowAnimation()
    {
        yield return new WaitForSeconds(0.3f);

        GameObject mine = GameObject.Instantiate(teddyPrefab) as GameObject;
        mine.transform.position = controller.transform.TransformPoint(new Vector3(0, 1.6f, 0.2f));
        mine.rigidbody.AddForce(controller.transform.TransformDirection(0, 0, 3), ForceMode.Impulse);
        mine.GetComponent<TeddyBear>().duration = duration;
        timeLeft = duration;

        yield return new WaitForSeconds(0.5f);
        EndSkill();
    }
}

public class DmgAbsorbSkill : SkillBase
{
    //Skill Attribute
    float absorbAmount;
    float duration;
    //Skill Parameters
    CharacterStatus status;
    float timeLeft;
    GameObject fx;

    public override float DurationPercentage
    {
        get 
        {
            if (status.buffManager.HasBuff(PlayerBuff.DmgAbsorb))
            {
                return 1 - Mathf.Min(timeLeft / duration, 
                    status.buffManager.GetBuffData(PlayerBuff.DmgAbsorb).GetFloat("AbsorbAmount") / absorbAmount);
            }
            else
                return 1;
        }
    }

    public DmgAbsorbSkill(int level, SkillData skillData, SpecialSkillsController controller) : base(level, skillData, controller) { }

    protected override void InitialParameters(int level)
    {
        absorbAmount = 20 * level;
        duration = 10 + level;
    }

    public override void StartSkill()
    {
        controller.lockSkill = true;
        AnimationState animState = controller.animation["skill_5012"];
        animState.layer = 8;
        animState.time = 0;
        controller.animation.Blend("skill_5012", 1, .1f);
        controller.rigidbody.isKinematic = true;
        controller.GetComponent<PlayerMovementMoter>().isActive = false;
        controller.StartCoroutine(WaitForAnimEnd());
        status = controller.GetComponent<CharacterStatus>();
        BuffBase buffData = status.buffManager.AddBuff(PlayerBuff.DmgAbsorb);
        buffData.SetFloat("AbsorbAmount", absorbAmount);
        timeLeft = duration;

        //Add Shield FX
        fx = GameObject.Instantiate(Resources.Load("Skill/SkillFX_5012")) as GameObject;
        fx.transform.parent = controller.transform;
        fx.transform.localPosition = new Vector3(0, 0.1f, 0);
        fx.transform.localRotation = Quaternion.identity;

        active = true;
        avaliable = false;
    }

    IEnumerator WaitForAnimEnd()
    {
        yield return new WaitForSeconds(controller.animation["skill_5012"].length);
        controller.rigidbody.isKinematic = false;
        controller.GetComponent<PlayerMovementMoter>().isActive = true;
        controller.lockSkill = false;
    }

    public override void UpdateSkill()
    {
        base.UpdateSkill();

        if (!active)
            return;
        if (timeLeft > 0)
        {
            timeLeft -= Time.deltaTime;
        }
        else
        {
            EndSkill();
        }
    }

    public override void EndSkill()
    {
        status.buffManager.RemoveBuff(PlayerBuff.DmgAbsorb);

        //End Shield FX
        GameObject.Destroy(fx);

        active = false;
    }
}

public class MeikenSkill : SkillBase
{
    int healHp;
    GameObject fx;

    public override float DurationPercentage
    {
        get { return 1; }
    }

    public MeikenSkill(int level, SkillData skillData, SpecialSkillsController controller)
        : base(level, skillData, controller)
    { }

    protected override void InitialParameters(int level)
    {
        healHp = 50;
    }

    public override void StartSkill()
    {
        controller.lockSkill = true;
        AnimationState animState = controller.animation["skill_5015"];
        animState.layer = 8;
        animState.time = 0;
        controller.animation.Blend("skill_5015", 1, .1f);
        controller.rigidbody.isKinematic = true;
        controller.GetComponent<PlayerMovementMoter>().isActive = false;
        fx = GameObject.Instantiate(Resources.Load("Skill/SkillFX_5015")) as GameObject;
        fx.transform.parent = controller.transform;
        fx.transform.localPosition = new Vector3(0, 1, 0);
        controller.StartCoroutine(WaitForAnimEnd());
        //Don't show Gun
        foreach (Transform t in controller.GetComponent<Weapon>().gunTransforms)
        {
            if (t)
            {
                foreach (Renderer r in t.GetComponentsInChildren<Renderer>())
                {
                    r.enabled = false;
                }
            }
        }

        CharacterStatus status = controller.GetComponent<CharacterStatus>();
        status.currentHp += healHp;
        if (status.currentHp > status.MaxHp)
            status.currentHp = status.MaxHp;
        avaliable = false;
    }

    public override void UpdateSkill()
    {
        base.UpdateSkill();
    }

    public override void EndSkill()
    {
        foreach (Transform t in controller.GetComponent<Weapon>().gunTransforms)
        {
            if (t)
            {
                foreach (Renderer r in t.GetComponentsInChildren<Renderer>())
                {
                    if (r.name != "MuzzleFlash")
                        r.enabled = true;
                }
            }
        }
        controller.rigidbody.isKinematic = false;
        controller.GetComponent<PlayerMovementMoter>().isActive = true;
        active = false;
        controller.lockSkill = false;
        if (fx)
            GameObject.Destroy(fx);
    }

    IEnumerator WaitForAnimEnd()
    {
        yield return new WaitForSeconds(controller.animation["skill_5015"].length);
        EndSkill();
    }
}

public class SuperAtkSkill : SkillBase
{
    float atkPer;
    Weapon weapon;

    public override float DurationPercentage
    {
        get { return 1; }
    }

    public SuperAtkSkill(int level, SkillData skillData, SpecialSkillsController controller)
        : base(level, skillData, controller)
    { }

    protected override void InitialParameters(int level)
    {
        atkPer = level * 0.1f;
    }

    public override void StartSkill()
    {
        weapon = controller.GetComponent<Weapon>();
        weapon.buffAttribute.dmgPerBulletPer += atkPer;
        active = true;
        avaliable = false;
    }

    public override void UpdateSkill()
    {
        base.UpdateSkill();
    }

    public override void EndSkill()
    {
        weapon.buffAttribute.dmgPerBulletPer -= atkPer;
        //End Shield FX
        active = false;
    }
}

public class HighCritRateSkill : SkillBase
{
    float critRatePer;
    CharacterStatus status;

    public override float DurationPercentage
    {
        get { return 1; }
    }

    public HighCritRateSkill(int level, SkillData skillData, SpecialSkillsController controller)
        : base(level, skillData, controller)
    { }

    protected override void InitialParameters(int level)
    {
        critRatePer = level * 0.1f;
    }

    public override void StartSkill()
    {
        status = controller.GetComponent<CharacterStatus>();
        status.buffAttribute.critRate += critRatePer;
        active = true;
        avaliable = false;
    }

    public override void UpdateSkill()
    {
        base.UpdateSkill();
    }

    public override void EndSkill()
    {
        status.buffAttribute.critRate -= critRatePer;
        //End Shield FX
        active = false;
    }
}
#endregion

#region Bernard's Skills
public class FastShootSkill : SkillBase
{
    //Skill Attribute
    float speedRate;
    float duration;
    //Skill Parameters
    Weapon weapon;
    float timeLeft;

    public override float DurationPercentage
    {
        get 
        {
            return 1 - timeLeft/duration;
        }
    }

    public FastShootSkill(int level, SkillData skillData, SpecialSkillsController controller) : base(level, skillData, controller) { }

    protected override void InitialParameters(int level)
    {
        speedRate = 0.1f * level;
        duration = 10 + level;
    }

    public override void StartSkill()
    {
        controller.lockSkill = true;
        AnimationState animState = controller.animation["skill_5017"];
        animState.layer = 8;
        animState.time = 0;
        controller.animation.Blend("skill_5017", 1, .1f);
        controller.rigidbody.isKinematic = true;
        controller.GetComponent<PlayerMovementMoter>().isActive = false;
        controller.StartCoroutine(WaitForAnimEnd());


        weapon = controller.GetComponent<Weapon>();
        weapon.buffAttribute.cdTimePer += speedRate;
        weapon.ChangeCDTime();
        timeLeft = duration;

        //Add Shield FX

        active = true;
        avaliable = false;
    }

    public override void UpdateSkill()
    {
        base.UpdateSkill();

        if (!active)
            return;
        if (timeLeft > 0)
        {
            timeLeft -= Time.deltaTime;
        }
        else
        {
            EndSkill();
        }
    }

    public override void EndSkill()
    {
        weapon.buffAttribute.cdTimePer -= speedRate;
        weapon.ChangeCDTime();

        active = false;
    }

    IEnumerator WaitForAnimEnd()
    {
        yield return new WaitForSeconds(controller.animation["skill_5017"].length);
        controller.rigidbody.isKinematic = false;
        controller.GetComponent<PlayerMovementMoter>().isActive = true;
        controller.lockSkill = false;
    }
}

public class AutoMachineGunSkill : SkillBase
{
    //Skill Attribute
    float duration;
    //Skill Parameters
    float timeLeft;

    public override float DurationPercentage
    {
        get 
        {
            return 1 - timeLeft/duration;
        }
    }

    public AutoMachineGunSkill(int level, SkillData skillData, SpecialSkillsController controller) : base(level, skillData, controller) { }

    protected override void InitialParameters(int level)
    {
        duration = 30;
    }

    public override void StartSkill()
    {
        controller.lockSkill = true;
        AnimationState animState = controller.animation["skill_5020"];
        animState.layer = 8;
        animState.time = 0;
        controller.animation.Blend("skill_5020", 1, .1f);
        controller.rigidbody.isKinematic = true;
        controller.GetComponent<PlayerMovementMoter>().isActive = false;
        foreach (Transform t in controller.GetComponent<Weapon>().gunTransforms)
        {
            if (t)
            {
                foreach (Renderer r in t.GetComponentsInChildren<Renderer>())
                {
                    r.enabled = false;
                }
            }
        }
        controller.StartCoroutine(WaitForAnimEnd());

        timeLeft = duration;

        //Add Shield FX

        active = true;
        avaliable = false;
    }

    public override void UpdateSkill()
    {
        base.UpdateSkill();

        if (!active)
            return;
        if (timeLeft > 0)
        {
            timeLeft -= Time.deltaTime;
        }
        else
        {
            EndSkill();
        }
    }

    public override void EndSkill()
    {
        
        active = false;
    }

    IEnumerator WaitForAnimEnd()
    {
        yield return new WaitForSeconds(1.5f);
        GameObject machineGun = GameObject.Instantiate(Resources.Load("Skill/AutoMachineGun")) as GameObject;
        machineGun.transform.position = controller.transform.TransformPoint(new Vector3(0.3f, 0, 0.3f));
        machineGun.transform.eulerAngles = controller.transform.eulerAngles;
        machineGun.GetComponent<AutoMachineGun>().duration = duration;
        machineGun.GetComponent<AutoMachineGun>().setter = controller.gameObject;
        yield return new WaitForSeconds(controller.animation["skill_5020"].length - 1.5f); 
        foreach (Transform t in controller.GetComponent<Weapon>().gunTransforms)
        {
            if (t)
            {
                foreach (Renderer r in t.GetComponentsInChildren<Renderer>())
                {
                    if (r.name != "MuzzleFlash")
                        r.enabled = true;
                }
            }
        }
        controller.rigidbody.isKinematic = false;
        controller.GetComponent<PlayerMovementMoter>().isActive = true;
        controller.lockSkill = false;
    }
}
#endregion

public class SpecialSkillsController : MonoBehaviour {
    public GUISkin mySkin;
    public GameObject cdMaskPrefab;
    public RenderTexture[] maskRTs;
    [HideInInspector]
    public bool lockSkill = false;
    CharacterStoreData charData;
    CoolDownMask[] cdMasks;
    ISpecialSkill[] skills;
    Rect[] rects;

    void Awake()
    {
        skills = new ISpecialSkill[4];
        rects = new Rect[4];
        rects[0] = CsGUI.CRect(870, 455, CsGUI.CY(90), CsGUI.CY(90), UIRelative.PositionRelative);
        rects[1] = CsGUI.CRect(765, 467, CsGUI.CY(90), CsGUI.CY(90), UIRelative.PositionRelative);
        rects[2] = CsGUI.CRect(691, 570, CsGUI.CY(90), CsGUI.CY(90), UIRelative.PositionRelative);
        rects[3] = CsGUI.CRect(702, 680, CsGUI.CY(90), CsGUI.CY(90), UIRelative.PositionRelative);
        cdMasks = new CoolDownMask[4];
    }

	void Start () {
	    
	}

    void OnGUI()
    {
        GUI.skin = mySkin;
        for (int i = 0; i < 4; ++i)
        {
            if (skills[i] != null)
            {
                if (skills[i].Driving)
                {
                    GUI.enabled = skills[i].Avaliable && !lockSkill;
                    if (CsGUI.MultiTouchButton(rects[i], StaticGameData.GetSkillData(skills[i].SkillID).SkillName + " Lv:" + charData.GetSkill(skills[i].SkillID).skillLevel, "btnSkill") && skills[i].Avaliable)
                    {
                        skills[i].StartSkill();
                    }
                    if (!skills[i].Avaliable)
                    {
                        //Draw cd mask;
                        GUI.DrawTexture(rects[i], GetCoolDownMask(i).MaskTexture);
                    }   
                }
                else
                    GUI.Label(rects[i], StaticGameData.GetSkillData(skills[i].SkillID).SkillName + " Lv:" + charData.GetSkill(skills[i].SkillID).skillLevel, "btnSkill");
            }
            else
                GUI.Label(rects[i], "", "btnSkill");
            GUI.enabled = true;
        }
    }

	void LateUpdate () {
        int i = 0;
	    foreach (SkillBase skill in skills)
	    {
            if (skill != null)
            {
                if (!skill.Avaliable)
                {
                    GetCoolDownMask(i).SetPercentage(skill.CDPercentage, skill.DurationPercentage);
                }
                else
                    GetCoolDownMask(i).DisableMask();
                skill.UpdateSkill();
            }
            ++i;
	    }
	}

    //Called after battle scene was loaded, create Skill Objects
    public void LoadSkills()
    {
        charData = GameObject.Find("GlobalData").GetComponent<GlobalPlayerInfo>().SelectedCharData;
        List<int> skillIDs = charData.outfit.currentPackage;
        for (int i = 0; i < 4; ++i)
        {
            AddSkill(StaticGameData.GetSkillData(skillIDs[i]), i);
        }
    }

    CoolDownMask GetCoolDownMask(int i)
    {
        if (cdMasks[i] == null)
        {
            GameObject g = (GameObject)Instantiate(cdMaskPrefab);
            g.transform.position = new Vector3(0, 100 + 30 * i, 0);
            cdMasks[i] = g.GetComponent<CoolDownMask>();
            cdMasks[i].MaskTexture = maskRTs[i];
        }
        return cdMasks[i];
    }

    void AddSkill(SkillData skill, int pos)
    {
        skills[pos] = CreateSkillInstance(skill);
        if (skills[pos] != null && !skills[pos].Driving)
            skills[pos].StartSkill();
    }

    ISpecialSkill CreateSkillInstance(SkillData skill)
    {
        if (skill == null)
            return null;

        switch (skill.SkillID)
        {
            case (int)SkillEnum.Rush:
                return new RushSkill(charData.GetSkill(skill.SkillID).skillLevel, skill, this);
            case (int)SkillEnum.DmgRebound:
                return new DmgReboundSkill(charData.GetSkill(skill.SkillID).skillLevel, skill, this);
            case (int)SkillEnum.LifeStrong:
                return new StrongSkill(charData.GetSkill(skill.SkillID).skillLevel, skill, this);
            case (int)SkillEnum.SpeedFast:
                return new SpeedFastSkill(charData.GetSkill(skill.SkillID).skillLevel, skill, this);
            case (int)SkillEnum.DefStrong:
                return new HardDefendSkill(charData.GetSkill(skill.SkillID).skillLevel, skill, this);
            case (int)SkillEnum.DmgAbsorb:
                return new DmgAbsorbSkill(charData.GetSkill(skill.SkillID).skillLevel, skill, this);
            case (int)SkillEnum.Heal:
                return new MeikenSkill(charData.GetSkill(skill.SkillID).skillLevel, skill, this);
            case (int)SkillEnum.Penentrate:
                return new HighCritProbSkill(charData.GetSkill(skill.SkillID).skillLevel, skill, this);
            case (int)SkillEnum.SuperLoader:
                return new SuperLoaderSizeSkill(charData.GetSkill(skill.SkillID).skillLevel, skill, this);
            case (int)SkillEnum.SuperAttack:
                return new SuperAtkSkill(charData.GetSkill(skill.SkillID).skillLevel, skill, this);
            case (int)SkillEnum.SuperCrit:
                return new HighCritRateSkill(charData.GetSkill(skill.SkillID).skillLevel, skill, this);
            case (int)SkillEnum.FastShoot:
                return new FastShootSkill(charData.GetSkill(skill.SkillID).skillLevel, skill, this);
            case (int)SkillEnum.Accurate:
                return new SuperAtkSkill(charData.GetSkill(skill.SkillID).skillLevel, skill, this);
            case (int)SkillEnum.AngryThump:
                return new AreaStunSkill(charData.GetSkill(skill.SkillID).skillLevel, skill, this);
            case (int)SkillEnum.MineZone:
                return new MineZoneSkill(charData.GetSkill(skill.SkillID).skillLevel, skill, this);
            case (int)SkillEnum.TeddyDecoy:
                return new TeddyDecoySkill(charData.GetSkill(skill.SkillID).skillLevel, skill, this);
            case (int)SkillEnum.MentalStorm:
                return new AutoMachineGunSkill(charData.GetSkill(skill.SkillID).skillLevel, skill, this);
            case (int)SkillEnum.Doom:
                return new BombingZoneSkill(charData.GetSkill(skill.SkillID).skillLevel, skill, this);
        }
        return null;
    }
}
