using UnityEngine;
using System.Collections;

public class TeddyBear : MonoBehaviour {
    [HideInInspector]
    public float duration;

    float timeCounter;
    float lastY;

	// Use this for initialization
	void Start () {
        timeCounter = 0;
	}

    void FixedUpdate()
    {
        if (!rigidbody.isKinematic)
        {
            Debug.Log(rigidbody.velocity.y);
            if (transform.position.y < 1 && Mathf.Abs(rigidbody.velocity.y) < 0.01f)
            {
                rigidbody.isKinematic = true;
            }
        }
    }

	// Update is called once per frame
	void Update () {
        timeCounter += Time.deltaTime;
        if (timeCounter > duration)
            Destroy(gameObject);
	}
}
