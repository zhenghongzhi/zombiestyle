using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class AutoMachineGun : MonoBehaviour {
    //Skill Attributes
    [HideInInspector]
    public float duration;
    [HideInInspector]
    public GameObject setter;

    //Object Attributes
    public Transform[] fireTrans;
    public float alertRange = 10;
    public float aimTime = 1;
    public float rotateAnglePerSec = 180;
    //Atk Attributes
    public GameObject bulletPrefab;
    public float cdTime = 0.1f;
    public float bulletSpeed = 20;
    public float dmgPerBullet = 1;

    public Transform target;
    BulletSpawner[] bulletSpawner;
    float durationTimeCount;
    float cdTimeCount;
    float aimTimeCount;
    float sqrRange;
    public bool changeTarget = false;
    public bool aimAtTarget = false;

	// Use this for initialization
	void Start () {
        InitBulletSpawner();
        sqrRange = alertRange * alertRange;
        animation["Take 001"].speed = cdTime / 0.083f;
        aimTimeCount = 0;
        cdTimeCount = 0;
	}
	
	// Update is called once per frame
	void Update () {
        durationTimeCount += Time.deltaTime;
        if (durationTimeCount > duration)
        {
            foreach (BulletSpawner spawner in bulletSpawner)
            {
                spawner.Destroy();
            }
            Destroy(gameObject);
        }

        cdTimeCount += Time.deltaTime;
        if(!HasTarget())
            DetermineTarget();
        if (target)
        {
            RotateTowardsTarget();
            if (changeTarget)
            {
                aimTimeCount += Time.deltaTime;
                if (aimTimeCount > aimTime)
                {
                    aimTimeCount = 0;
                    changeTarget = false;
                    animation.Play();
                }
            }
            else if (aimAtTarget)
            {
                if (cdTimeCount > cdTime)
                {
                    cdTimeCount = 0;
                    foreach (BulletSpawner spawner in bulletSpawner)
                    {
                        spawner.Spawn(-1, 3);
                    }
                }
            }
        }
        else
        {
            transform.RotateAroundLocal(Vector3.up, Time.deltaTime);
        }
	}

    void InitBulletSpawner()
    {
        bulletSpawner = new BulletSpawner[fireTrans.Length];
        int i = 0;
        foreach (Transform t in fireTrans)
        {
            bulletSpawner[i] = new BulletSpawner();
            bulletSpawner[i].bulletPrefab = bulletPrefab;
            bulletSpawner[i].spawnTrans = t;
            BulletInfo info = new BulletInfo();
            info.dmg = dmgPerBullet;
            info.shooter = setter;
            info.effect = 0;
            bulletSpawner[i].InitSpawner(10, 20, 4, "AutoMachineGun", info, false);
            ++i;
        }
    }

    void DetermineTarget()
    {
        animation.Stop();
        changeTarget = true;
        if (InRange(setter.transform.position))
        {
            GameObject[] zombies = GameObject.FindGameObjectsWithTag("Zombie");
            int priority = 0; //0 : not dangerous state, 1 : chase state, 2 : atk state
            float minSqrDist = Mathf.Infinity;
            foreach (GameObject zombie in zombies)
            {
                //Priority Filter
                ZombieAIStateController zombieController = zombie.GetComponent<ZombieAIStateController>();
                if (zombieController.CurrentState == ZombieState.DieBlasting || zombieController.CurrentState == ZombieState.Dying || zombieController.CurrentState == ZombieState.UnderGround)
                    continue;
                if(priority == 1 && (zombieController.target != setter.transform ||
                    (zombieController.CurrentState != ZombieState.Attacking && zombieController.CurrentState != ZombieState.Chasing)))
                {
                    continue;
                }
                else if(priority == 2 && (zombieController.target != setter.transform || zombieController.CurrentState != ZombieState.Attacking))
                {
                    continue;
                }
                //Distance compare
                float sqrDist = SqrDist(zombie.transform.position);
                if(sqrDist > sqrRange || sqrDist < 2)
                    continue;
                if (sqrDist < minSqrDist)
                {
                    if (zombieController.target == setter.transform)
                    {
                        if (zombieController.CurrentState == ZombieState.Chasing)
                            priority = 1;
                        else if (zombieController.CurrentState == ZombieState.Attacking)
                            priority = 2;
                    }
                    minSqrDist = sqrDist;
                    target = zombie.transform;
                }
            }
        }
        else
        {
            GameObject[] zombies = GameObject.FindGameObjectsWithTag("Zombie");
            float minSqrDist = Mathf.Infinity;
            foreach (GameObject zombie in zombies)
            {
                //Distance compare
                float sqrDist = SqrDist(zombie.transform.position);
                if (sqrDist > sqrRange)
                    continue;
                if (sqrDist < minSqrDist)
                {
                    minSqrDist = sqrDist;
                    target = zombie.transform;
                }
            }
        }
    }

    void RotateTowardsTarget()
    {
        Vector3 targetInLocal = transform.InverseTransformPoint(target.transform.position);
        targetInLocal.y = 0;
        if(targetInLocal.normalized.z < 0.9)
        {
            aimAtTarget = false;
            if(targetInLocal.x < 0)
                transform.eulerAngles = new Vector3(0, transform.eulerAngles.y - rotateAnglePerSec * Time.deltaTime, 0);
            else    
                transform.eulerAngles = new Vector3(0, transform.eulerAngles.y + rotateAnglePerSec * Time.deltaTime, 0);
        }
        else
        {
            aimAtTarget = true;
            transform.LookAt(target.transform);
            transform.eulerAngles = new Vector3(0, transform.eulerAngles.y, 0);
        }
    }

    bool InRange(Vector3 pos)
    {
        float dx = pos.x - transform.position.x;
        float dz = pos.z - transform.position.z;
        return (dx * dx + dz * dz) < sqrRange;
    }

    float SqrDist(Vector3 pos)
    {
        float dx = pos.x - transform.position.x;
        float dz = pos.z - transform.position.z;
        return dx * dx + dz * dz;
    }

    bool HasTarget()
    {
        if (target)
        {
            ZombieAIStateController zombieController = target.GetComponent<ZombieAIStateController>();
            if (zombieController.CurrentState == ZombieState.DieBlasting || zombieController.CurrentState == ZombieState.Dying || zombieController.CurrentState == ZombieState.UnderGround)
            {
                target = null;
                return false;
            }
            return true;
        }
        return false;
    }
}
