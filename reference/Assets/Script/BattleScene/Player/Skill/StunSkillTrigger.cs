using UnityEngine;
using System.Collections;

[RequireComponent(typeof(BoxCollider))]
public class StunSkillTrigger : MonoBehaviour {

    void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Zombie")
        {
            other.GetComponent<ZombieAIIdleState>().idleTime = 3;
            other.GetComponent<ZombieAIStateController>().SetState(ZombieState.Idle);
        }
    }
}
