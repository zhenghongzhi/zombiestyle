using UnityEngine;
using System.Collections;

public class BombingZone : MonoBehaviour {
    public GameObject MisslePrefab;
    [HideInInspector]
    public GameObject setter;
    public LayerMask dmgLayers;
    public float areaSize; 
    public float dmgPerMissle = 100;
    public float frequency = 0.1f;
    public float duration = 3;

    float timeCount;
    Quaternion q;

    void Start()
    {
        timeCount = duration;
        Vector3 direction = Camera.mainCamera.transform.TransformDirection(new Vector3(0.5f, -1, 1));
        q = Quaternion.LookRotation(direction);
        StartCoroutine(RandomFireMissle());
    }

    void Update()
    {
        timeCount -= Time.deltaTime;
    }

    IEnumerator RandomFireMissle()
    {
        while (timeCount > 0)
        {
            Debug.Log("Fire Missle");
            GameObject missle = Instantiate(MisslePrefab, RandomPosition(), q) as GameObject;
            Missle script = missle.GetComponent<Missle>();
            script.dmgLayers = dmgLayers;
            script.info = new BuffDmgInfo(dmgPerMissle, 1, setter);
            yield return new WaitForSeconds(frequency);
        }
        Destroy(gameObject);
    }

    Vector3 RandomPosition()
    {
        return new Vector3(
            Random.Range(transform.position.x - areaSize, transform.position.x + areaSize),
            transform.position.y,
            Random.Range(transform.position.z - areaSize, transform.position.z + areaSize)
            );
    }
}
