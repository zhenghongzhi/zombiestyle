using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public enum PlayerBuff
{
    DmgRebound,
    LifeStrong,
    SpeedFast,
    HardDefend,
    DmgAbsorb,
    HighCritProb,
    Frozen,
}

public class BuffBase
{
    public PlayerBuff buffID;
    public bool auto;
    protected CharacterStatus charState;
    protected Dictionary<string, float> buffData;

    public BuffBase(PlayerBuff buff, CharacterStatus charState)
    {
        buffID = buff;
        this.charState = charState;
        buffData = new Dictionary<string, float>();
        auto = false;
    }

    public void SetFloat(string name, float value)
    {
        if (!buffData.ContainsKey(name))
            buffData.Add(name, value);
        else
            buffData[name] = value;
    }

    public float GetFloat(string name)
    {
        return buffData[name];
    }

    public virtual void OnBuffBegin() { }
    public virtual void OnBuffUpdate() { }
    public virtual void OnBuffEnd() { }
}

public class FrozenBuff : BuffBase
{
    GameObject iceModel;

    public FrozenBuff(PlayerBuff buff, CharacterStatus charState) : base(buff, charState) 
    {
        auto = true;
    }

    public override void OnBuffBegin()
    {
        charState.rigidbody.isKinematic = true;
        charState.GetComponent<PlayerMovementMoter>().isActive = false;
        iceModel = GameObject.Instantiate(Resources.Load("Buff/ICE3B_B"), charState.transform.position, charState.transform.rotation) as GameObject;
        iceModel.animation.Stop();
        charState.StartCoroutine(WaitForUnfreeze());
    }

    IEnumerator WaitForUnfreeze()
    {
        yield return new WaitForSeconds(1);
        iceModel.animation.Play();
        charState.rigidbody.isKinematic = false;
        charState.GetComponent<PlayerMovementMoter>().isActive = true;
        charState.StartCoroutine(DestroyBuffEffect(iceModel));
        charState.buffManager.RemoveBuff(buffID);
    }

    IEnumerator DestroyBuffEffect(GameObject buff)
    {
        yield return new WaitForSeconds(1);
        GameObject.Destroy(buff);
    }
}

public class PlayerBuffManager : MonoBehaviour
{
    CharacterStatus charState;
    List<BuffBase> buffList;
    Dictionary<PlayerBuff, BuffBase> buffSet;

    void Awake()
    {
        charState = GetComponent<CharacterStatus>();
        buffList = new List<BuffBase>();
        buffSet = new Dictionary<PlayerBuff, BuffBase>();
    }

    public BuffBase AddBuff(PlayerBuff buff)
    {
        BuffBase buffBase = null;
        if (!buffSet.ContainsKey(buff))
        {
            buffBase = CreateBuff(buff);
            buffSet.Add(buff, buffBase);
            buffList.Add(buffBase);
        }
        if (buffBase != null && buffBase.auto)
            buffBase.OnBuffBegin();
        return buffBase;
    }

    public void RemoveBuff(PlayerBuff buff)
    {
        if (buffSet.ContainsKey(buff))
        {
            buffList.Remove(buffSet[buff]);
            buffSet.Remove(buff);
        }
    }

    public bool HasBuff(PlayerBuff buff)
    {
        return buffSet.ContainsKey(buff);
    }

    public BuffBase GetBuffData(PlayerBuff buff)
    {
        if (buffSet.ContainsKey(buff))
            return buffSet[buff];
        else
            return null;
    }

    BuffBase CreateBuff(PlayerBuff buff)
    {
        switch (buff)
        {
            case PlayerBuff.Frozen:
                return new FrozenBuff(buff, charState);
            default:
                return new BuffBase(buff, charState);
        }
    }
}

public class BuffDmgInfo
{
    public float dmg;
    public int buff;
    public GameObject applier;

    public BuffDmgInfo(float dmg, int buff, GameObject applier)
    {
        this.dmg = dmg;
        this.buff = buff;
        this.applier = applier;
    }
}
