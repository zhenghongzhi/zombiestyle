using UnityEngine;
using System.Collections;

[RequireComponent(typeof(Collider))]
public class Landmine : MonoBehaviour {
    public GameObject explosionPrefab;
    public float startDelay = 1;
    [HideInInspector]
    public GameObject setter;
    [HideInInspector]
    public float radius;
    [HideInInspector]
    public int maxDmg;

    bool explore = false;
    float timeCounter = 0;

	// Use this for initialization
	void Start () {
        GetComponent<Collider>().enabled = false;
        StartCoroutine(WaitForStart());
	}
	
	// Update is called once per frame
	void Update () {
	    
	}

    IEnumerator WaitForStart()
    {
        yield return new WaitForSeconds(startDelay);
        GetComponent<Collider>().enabled = true;
    }

    void OnTriggerEnter(Collider obj)
    {
        if (obj.tag == "Zombie")
        {
            renderer.enabled = false;
            collider.enabled = false;
            if(explosionPrefab)
                Instantiate(explosionPrefab, transform.position, transform.rotation);
            GameObject[] zombies = GameObject.FindGameObjectsWithTag("Zombie");
            float exploRange = radius * radius;
            foreach (GameObject g in zombies)
            {
                float sqrDist = (g.transform.position - transform.position).sqrMagnitude;
                if (sqrDist < exploRange)
                {
                    g.SendMessage("ApplyDamage", new BuffDmgInfo(maxDmg, 1, setter));
                }
            }
            Destroy(gameObject);
        }     
    }
}
