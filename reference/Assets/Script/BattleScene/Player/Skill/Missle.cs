using UnityEngine;
using System.Collections;

public class Missle : MonoBehaviour
{
    public float speed;
    public ParticleEmitter smoke;
    public GameObject explosionPrefab;
    public LayerMask ignoreLayers;
    [HideInInspector]
    public LayerMask dmgLayers;
    [HideInInspector]
    public BuffDmgInfo info;

    void Update()
    {
        transform.position += transform.forward * Time.deltaTime * speed;
    }

    void Bomb()
    {
        GameObject expo = GameObject.Instantiate(explosionPrefab, transform.position, Quaternion.identity) as GameObject;
        ExplosionDmg script = expo.AddComponent<ExplosionDmg>();
        script.dmgObjectLayer = dmgLayers;
        script.dmgInfo = info;
        Camera.mainCamera.GetComponent<CameraShake>().ShakeForSeconds(0.5f);
        Destroy(gameObject);
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Player")
            return;
        //A rocket bullet always can't penetrate
        int otherLayer = 1 << other.gameObject.layer;
        if ((otherLayer & ignoreLayers.value) == 0)
            Bomb();
    }
}
