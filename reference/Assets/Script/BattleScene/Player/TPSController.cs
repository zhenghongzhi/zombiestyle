using UnityEngine;
using System.Collections;

/*--------------------------------------------------
*Attached Object : Root of Player Model
*Resbonsibility : Convert Input Dir from PadInput into 
*				World Dir and send to PlayerMovementMoter
*Colabration Class: 
*	PadInput(receive input massage from that);
*	PlayerMovementMoter(for actual movement and turn calcutation)
*	PlayerAnimation (apply shoot animation)
--------------------------------------------------*/
[RequireComponent(typeof(PlayerAnimation))]
[RequireComponent(typeof(PlayerMovementMoter))]
public class TPSController : MonoBehaviour {

    public bool remotePlayer = true;
	public PadInput inputDev;//Get touch control information
	PlayerMovementMoter moter;
	PlayerAnimation animationController;
    CharacterStatus charStat;
	
	// Use this for initialization
	void Awake () {
		moter = (PlayerMovementMoter)GetComponent<PlayerMovementMoter>();
		animationController = (PlayerAnimation)GetComponent<PlayerAnimation>();
        charStat = GetComponent<CharacterStatus>();
	}

	void ApplyInputControl()
	{
		if(inputDev == null)
			return;
        if (inputDev.GetMoveDir() != Vector2.zero)
        {
            animationController.leftJoystickActive = true;
            Vector3 v = Camera.main.transform.forward * inputDev.GetMoveDir().y + Camera.main.transform.right * inputDev.GetMoveDir().x;
            v.y = 0;
            moter.moveDir = v.normalized;
        }
        else
        {
            animationController.leftJoystickActive = false;
            moter.moveDir = Vector3.zero;
        }

        if (inputDev.GetShootDir() != Vector2.zero)
        {
            animationController.rightJoystickActive = true;
            moter.WalkSpeed = charStat.MSpeed * 0.6f;
            Vector3 v = Camera.main.transform.forward * inputDev.GetShootDir().y + Camera.main.transform.right * inputDev.GetShootDir().x;
            v.y = 0;
            moter.faceDir = v.normalized;
        }
        else
        {
            animationController.rightJoystickActive = false;
            moter.WalkSpeed = charStat.MSpeed;
            if (moter.moveDir != Vector3.zero)
                moter.faceDir = moter.moveDir;
        }
	}

	// Update is called once per frame
	void Update () {
        if (!remotePlayer)
            ApplyInputControl();
	}
}
