using UnityEngine;
using System.Collections;

/*--------------------------------------------------
*Attached Object : Root of Movable Model
*Resbonsibility : Apply movement through rigidbody
*Usage : Set moveDir to move model
*		 Set faceDir to rotate model
*Colabration Class: 
*	Rigidbody(receive input massage from that)
--------------------------------------------------*/
[RequireComponent(typeof(Rigidbody))]
public class PlayerMovementMoter : MonoBehaviour {

    public float walkSpeed = 4;
	public float moveDamp = 50;
	public float turnDamp = 10;
    public AnimationClip walkAnim;
    public float walkAnimAdaptSpeed;
    public AnimationClip runAnim;
    public float runAnimAdaptSpeed;
    [HideInInspector]
    public bool isActive = true;
    [SerializeField]
    public Vector3 moveDir;
    [SerializeField]
    public Vector3 faceDir;

    Vector3 lastMoveDir = Vector3.zero;
	Vector3 lastFaceDir = Vector3.zero;
    //用于一帧没有旋转完毕时旋转角度的计算
    Vector3 lastNot0FaceDir = Vector3.zero;
    bool turning = false;

    private Vector3 frameMoveDir = Vector3.zero;
    private Vector3 frameFaceDir = Vector3.zero;
    [HideInInspector]
    public bool bLocal = false;


    public float WalkSpeed
    {
        get { return walkSpeed; }
        set
        {
            walkSpeed = value;
            if (walkAnim)
                animation[walkAnim.name].speed = walkSpeed / walkAnimAdaptSpeed;
            if (runAnim)
                animation[runAnim.name].speed = walkSpeed / runAnimAdaptSpeed;
        }
    }
	// Use this for initialization
	void Start () {
	}
	
	// Update is called once per frame
    void Update()
    {
        if (bLocal)
        {
            if (moveDir != frameMoveDir || faceDir != frameFaceDir)
            {
                StateSync sCmd = new StateSync(LobbyNetwork.Instance.MyPlayerId, CsUtil.ToWitVector3(moveDir), CsUtil.ToWitVector3(faceDir));
                if (!NetworkManager.Instance.IsServer)
                {
                    NetworkManager.Instance.SendProtocol(sCmd);
                }
                else
                {
                    NetworkManager.Instance.BroadcastProtocol(sCmd, LobbyNetwork.Instance.MyRoom.PlayerMan.CollectId());
                }
                frameMoveDir = moveDir;
                frameFaceDir = faceDir;
            }            
        }        
    }

	void FixedUpdate () {
        if (!isActive)
            return;

		Vector3 targetVelocity = moveDir * WalkSpeed;

		Vector3 deltaVelocity = targetVelocity - rigidbody.velocity;

		if(rigidbody.useGravity)
			deltaVelocity.y = 0;

		rigidbody.AddForce (deltaVelocity * moveDamp, ForceMode.Acceleration);
		
		// Setup player to face facingDirection, or if that is zero, then the movementDirection
		if (faceDir == Vector3.zero)
			faceDir = moveDir;
		
		// Make the character rotate towards the target rotation
        if (faceDir != lastFaceDir && faceDir != Vector3.zero)
        {
            lastNot0FaceDir = faceDir;
            turning = true;
        }

        if (turning)
        {
            float rotationAngle = AngleAroundAxis(transform.forward, lastNot0FaceDir, Vector3.up);
            if (Mathf.Abs(rotationAngle) > 180 - Mathf.Abs(rotationAngle * Time.deltaTime * turnDamp) || Mathf.Abs(rotationAngle) < Mathf.Abs(rotationAngle * Time.deltaTime * turnDamp))
            {
                transform.rotation = Quaternion.LookRotation(faceDir);
                turning = false;
                lastNot0FaceDir = Vector3.zero;
            }
            else
            {
                transform.Rotate(Vector3.up, rotationAngle * Time.deltaTime * turnDamp);
            }
        }

		lastFaceDir = faceDir;
	}

	// The angle between dirA and dirB around axis
	static float AngleAroundAxis (Vector3 dirA, Vector3 dirB, Vector3 axis) {
	    // Project A and B onto the plane orthogonal target axis
	    dirA = dirA - Vector3.Project (dirA, axis);
	    dirB = dirB - Vector3.Project (dirB, axis);
	   
	    // Find (positive) angle between A and B
	    float angle = Vector3.Angle (dirA, dirB);
	   
	    // Return angle multiplied with 1 or -1
	    return angle * (Vector3.Dot (axis, Vector3.Cross (dirA, dirB)) < 0 ? -1 : 1);
	}

}
