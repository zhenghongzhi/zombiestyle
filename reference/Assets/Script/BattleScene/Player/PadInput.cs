using UnityEngine;
using System.Collections;

/*--------------------------------------------------
*Attached Object : Required by TSPController
*Resbonsibility : Draw Character Controller GUI, and
*				  handle corresponding input events
*Usage : Call GetMoveDir() to get move Vector
*		 Call GetShootDir() to get face Vector
--------------------------------------------------*/
public class PadInput : MonoBehaviour {

	public Vector2 leftCenterPos = new Vector2(112, 660);
	public Vector2 rightCenterPos = new Vector2(875, 660);
    public float ctrlButtonRadius = 100;
    public float leftCtrlRange = 70;
    public float rightCtrlRange = 65;

    public Texture2D leftCtrlBG;
    public Texture2D rightCtrlBG;
    public GUISkin mySkin;
	Vector2 rightButtonPos;
	Vector2 leftButtonPos;
	Vector2 moveDir;
	Vector2 shootDir;
    float sqrLeftRange;
    float sqrRightRange;
    float sqrButtonRad;
	bool leftDrag = false;
	bool rightDrag = false;
#if (UNITY_IPHONE || UNITY_ANDROID) && !UNITY_EDITOR
	int leftFingerID = 0;
	int rightFingerID = 0;
#endif

	public Vector2 GetMoveDir()
	{
		return new Vector2(moveDir.x, -moveDir.y);
	}

	public Vector2 GetShootDir()
	{
		return new Vector2(shootDir.x, -shootDir.y);
	}

	// Use this for initialization
    void Start()
    {
        leftCenterPos.x *= CsGUI.XRate;
        leftCenterPos.y *= CsGUI.YRate;
        rightCenterPos.x *= CsGUI.XRate;
        rightCenterPos.y *= CsGUI.YRate;
		rightButtonPos = rightCenterPos;
		leftButtonPos = leftCenterPos;
        sqrButtonRad = ctrlButtonRadius * ctrlButtonRadius * CsGUI.XRate * CsGUI.YRate;
        sqrLeftRange = leftCtrlRange * leftCtrlRange * CsGUI.XRate * CsGUI.YRate;
        sqrRightRange = rightCtrlRange * rightCtrlRange * CsGUI.XRate * CsGUI.YRate;
		Input.multiTouchEnabled = true;
	}

	static Vector2 ConvertInputPos(Vector2 inputPos)
	{
		return new Vector2(inputPos.x, Screen.height - inputPos.y);
	}

	void OnGUI() 
	{
		Vector2 leftFingerPos = Vector2.zero;
		Vector2 rightFingerPos = Vector2.zero;

#if UNITY_EDITOR || UNITY_STANDALONE_WIN
        leftFingerPos = PadInput.ConvertInputPos(Input.mousePosition);
		rightFingerPos = PadInput.ConvertInputPos(Input.mousePosition);
        moveDir = Vector2.zero;
        if (Input.GetKey(KeyCode.W))
        {
            moveDir += new Vector2(0, -1);
        }
        if (Input.GetKey(KeyCode.S))
        {
            moveDir += new Vector2(0, 1);
        }
        if (Input.GetKey(KeyCode.A))
        {
            moveDir += new Vector2(-1, 0);
        }
        if (Input.GetKey(KeyCode.D))
        {
            moveDir += new Vector2(1, 0);
        }
        shootDir = Vector2.zero;
        if (Input.GetKey(KeyCode.UpArrow))
        {
            shootDir += new Vector2(0, -1);
        }
        if (Input.GetKey(KeyCode.DownArrow))
        {
            shootDir += new Vector2(0, 1);
        }
        if (Input.GetKey(KeyCode.LeftArrow))
        {
            shootDir += new Vector2(-1, 0);
        }
        if (Input.GetKey(KeyCode.RightArrow))
        {
            shootDir += new Vector2(1, 0);
        }

		if(Input.GetMouseButtonDown(0))
		{
			if(new Vector2(Input.mousePosition.x - leftCenterPos.x, Screen.height - Input.mousePosition.y - leftCenterPos.y).sqrMagnitude < sqrButtonRad)
			{
				leftDrag = true;
			}
            else if (new Vector2(Input.mousePosition.x - rightCenterPos.x, Screen.height - Input.mousePosition.y - rightCenterPos.y).sqrMagnitude < sqrButtonRad)
			{
				rightDrag = true;
			}
		}
		else if(Input.GetMouseButtonUp(0))
		{
			leftDrag = false;
			rightDrag = false;
			rightButtonPos = rightCenterPos;
			leftButtonPos = leftCenterPos;
			moveDir = Vector2.zero;
			shootDir = Vector2.zero;
		}
#endif

#if (UNITY_IPHONE || UNITY_ANDROID) && !UNITY_EDITOR
		//Began Touch Detection
		foreach(Touch t in Input.touches)
		{
			//Detect new touches
			if(t.phase == TouchPhase.Began)
			{
				if(!leftDrag)
				{
					float sqrDist = new Vector2(t.position.x - leftCenterPos.x, Screen.height - t.position.y - leftCenterPos.y).sqrMagnitude;
					if(sqrDist < sqrButtonRad)
					{
						leftDrag = true;
						leftFingerID = t.fingerId;
						leftFingerPos = PadInput.ConvertInputPos(t.position);
					} 
				}
				if(!rightDrag)
				{
					float sqrDist = new Vector2(t.position.x - rightCenterPos.x, Screen.height - t.position.y - rightCenterPos.y).sqrMagnitude;
					if(sqrDist < sqrButtonRad)
					{
						rightDrag = true;
						rightFingerID = t.fingerId;
						rightFingerPos = PadInput.ConvertInputPos(t.position);
					} 
				}
			}//End Detect new touchese
			//Track active touches
			else
			{
				if(leftDrag && t.fingerId == leftFingerID)
				{
					if(t.phase == TouchPhase.Ended)
					{
						leftFingerID = -1;
						leftDrag = false;
						leftButtonPos = leftCenterPos;
						moveDir = Vector2.zero;
					}
					else
					{
						leftFingerPos = PadInput.ConvertInputPos(t.position);
					}
				}
				else if(rightDrag && t.fingerId == rightFingerID)
				{
					if(t.phase == TouchPhase.Ended)
					{
						rightFingerID = -1;
						rightDrag = false;
						rightButtonPos = rightCenterPos;
						shootDir = Vector2.zero;
					}
					else
					{
						rightFingerPos = PadInput.ConvertInputPos(t.position);
					}
				}//End Track rightFiner
			}//End Track active touches
		}//End Triverse all Touches
#endif


        if (leftDrag)
		{
			moveDir = (leftFingerPos - leftCenterPos).normalized;
            if (Vector2.SqrMagnitude(leftFingerPos - leftCenterPos) < sqrLeftRange)
                leftButtonPos = leftFingerPos;
            else
                leftButtonPos = moveDir * leftCtrlRange * CsGUI.XRate +leftCenterPos;
		}
		if(rightDrag)
		{
			shootDir = (rightFingerPos - rightCenterPos).normalized;
            if (Vector2.SqrMagnitude(rightFingerPos - rightCenterPos) < sqrRightRange)
				rightButtonPos = rightFingerPos;
			else
                rightButtonPos = shootDir * rightCtrlRange * CsGUI.XRate + rightCenterPos;
		}

        GUI.skin = mySkin;
        GUI.DrawTexture(new Rect(leftCenterPos.x - 78 * CsGUI.XRate, leftCenterPos.y - 78 * CsGUI.YRate,
            152 * CsGUI.XRate, 152 * CsGUI.YRate), leftCtrlBG);
        GUI.DrawTexture(new Rect(rightCenterPos.x - 78 * CsGUI.XRate, rightCenterPos.y - 78 * CsGUI.YRate,
            152 * CsGUI.XRate, 152 * CsGUI.YRate), rightCtrlBG);
        GUI.Button(new Rect(rightButtonPos.x - 45 * CsGUI.XRate, rightButtonPos.y - 45 * CsGUI.YRate,
            90 * CsGUI.XRate, 90 * CsGUI.YRate), "", "btnJoystick");
        GUI.Button(new Rect(leftButtonPos.x - 45 * CsGUI.XRate, leftButtonPos.y - 45 * CsGUI.YRate,
            90 * CsGUI.XRate, 90 * CsGUI.YRate), "", "btnJoystick");
	}


}
