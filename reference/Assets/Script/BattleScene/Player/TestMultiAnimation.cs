using UnityEngine;
using System.Collections;

public class TestMultiAnimation : MonoBehaviour {
    public Transform rootBone;
    public Transform upperRootBone;
    AnimationState right;
    AnimationState left;
    AnimationState idle;
    AnimationState forward;
    AnimationState backward;
	// Use this for initialization
	void Awake () {
        forward = animation["run_forward"];
        backward = animation["run_backward"];
        right = animation["run_right"];
        left = animation["run_left"];
        idle = animation["idle"];

        forward.layer = 1;
        backward.layer = 1;
        right.layer = 1;
        left.layer = 1;
        idle.layer = 2;
        idle.weight = .5f;
        idle.enabled = true;
    }
	
	// Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.W))
        {
            idle.weight = .5f;
            animation.CrossFade(left.name);
        }
        if (Input.GetKey(KeyCode.W))
        {
            //idle.weight = 0;
        }
        else if (Input.GetKeyUp(KeyCode.W))
        {
            //animation.CrossFade(idle.name);
            idle.weight = 1;
        }
        if (Input.GetKeyDown(KeyCode.S))
        {
            idle.weight = .5f;
            animation.CrossFade(backward.name, 0.1f);
        }
        else if (Input.GetKeyUp(KeyCode.S))
        {
            idle.weight = 1;
        }
//         if(!Input.GetKey(KeyCode.W) || !Input.GetKey(KeyCode.Space))
//             animation.CrossFade(idle.name, 0.1f);
	}
}
