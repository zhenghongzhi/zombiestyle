using UnityEngine;
using System.Collections;

public enum EnemyActiveState
{
    Birthing,
	Wondering,
	Chasing,
	Attacking,
    Dying,
    UnderGround,
}

/*--------------------------------------------------
*Attached Object : Root of Enemy Model
*Usage : Determine Eneny's action
*Colabration Class: 
--------------------------------------------------*/
[RequireComponent(typeof(ZombieAnimController))]
public class EnemyAI : MonoBehaviour {

	public Transform target;//chase target
	public float alertRange;//max chase distance
	public float attackRange;//max attack distance

	EnemyActiveState state = EnemyActiveState.UnderGround;
	float sqrAlertRange;
	float sqrAttackRange;
    ZombieAnimController animController;


	public EnemyActiveState GetAction()
	{
		return state;
	}

	float SqrDist()
	{
		Vector2 v = new Vector2(target.position.x - transform.position.x, target.position.z - transform.position.z);
		return v.sqrMagnitude;
	}

	// Use this for initialization
	void Start () {
		target = GameObject.FindWithTag("Player").transform;
		sqrAttackRange = attackRange * attackRange;
		sqrAlertRange = alertRange * alertRange;
	}
	
	// Update is called once per frame
	void Update () {
		if(target == null)
			return;

		float sqrDist = SqrDist();
		if(sqrDist > sqrAlertRange)
			state = EnemyActiveState.Wondering;
		else if(sqrDist < sqrAttackRange)
			state = EnemyActiveState.Attacking;
		else//sqrDist > sqrAttachRange && sqrDist < sqrAlertRange 
			state = EnemyActiveState.Chasing;
	}


}
