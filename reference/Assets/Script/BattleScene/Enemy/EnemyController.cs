using UnityEngine;
using System.Collections;

/*--------------------------------------------------
*Attached Object : Root of Enemy Model
*Usage : Eneny's action controller
*Colabration Class: 
*	EnemyAI(Decide which control method to use)
*	PlayerMovementMoter(for collision)
--------------------------------------------------*/
[RequireComponent(typeof(ZombieAnimController))]
[RequireComponent(typeof(PlayerMovementMoter))]
[RequireComponent(typeof(EnemyAI))]
public class EnemyController : MonoBehaviour {

	PlayerMovementMoter controller;
	EnemyAI aiState;

	float wonderTimeInterval = 4;
	float timeRecord = 0;

	// Use this for initialization
	void Start () {
		controller = (PlayerMovementMoter)GetComponent<PlayerMovementMoter>();
		aiState = (EnemyAI)GetComponent<EnemyAI>();
		Random.seed = Time.frameCount;
	}
	
	void Wonder()
	{

		if(Time.time - timeRecord > wonderTimeInterval)
		{
			float angle = Random.Range(0, 2 * Mathf.PI);
			timeRecord = Time.time;
			wonderTimeInterval = Random.Range(2, 5);
			controller.faceDir = new Vector3(Mathf.Cos(angle), 0, Mathf.Sin(angle));
		}
		controller.moveDir = transform.forward;
	}

	void Chase()
	{
		controller.faceDir = 
			new Vector3(aiState.target.position.x - transform.position.x, 0, aiState.target.position.z - transform.position.z);
		controller.moveDir = (transform.forward - Vector3.up);
	}

	void Attack()
	{

	}

	void ApplyAction()
	{
		if(controller == null)
			return;
		switch(aiState.GetAction())
		{
		case EnemyActiveState.Wondering:
			Wonder();
			break;
		case EnemyActiveState.Chasing:
			Chase();
			break;
		case EnemyActiveState.Attacking:
			Attack();
			break;
		}
	}

	void ApplyAnimation()
	{
		if(animation == null)
			return;	
	}

	// Update is called once per frame
	void Update () {
		ApplyAction();

		ApplyAnimation();
	}
}
