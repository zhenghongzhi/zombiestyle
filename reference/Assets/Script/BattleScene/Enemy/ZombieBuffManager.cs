using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public enum ZombieBuff
{
    Stun,
    Slowdown,
}

public class ZombieBuffBase
{
    public ZombieBuff buffType;
    public bool auto;
    protected ZombieBuffManager buffManager;

    public ZombieBuffBase(ZombieBuff buff, ZombieBuffManager state)
    {
        buffType = buff;
        this.buffManager = state;
        auto = false;
    }

    public virtual void OnBuffBegin() { }
    public virtual void OnBuffUpdate() { }
    public virtual void OnBuffEnd() 
    {
    }
}

public class StunBuff : ZombieBuffBase
{
    GameObject stunFx;

    public StunBuff(ZombieBuff buff, ZombieBuffManager state)
        : base(buff, state)
    {
        auto = true;
    }

    public override void OnBuffBegin()
    {
        buffManager.GetComponent<ZombieAIIdleState>().idleTime = 3;
        if (buffManager.GetComponent<ZombieAIStateController>().HasState(ZombieState.HitBack))
        {
            buffManager.GetComponent<ZombieAIStateController>().SetState(ZombieState.HitBack);
            buffManager.GetComponent<ZombieAIOnHitState>().lastState = ZombieState.Idle;
        }
        else
            buffManager.GetComponent<ZombieAIStateController>().SetState(ZombieState.Idle);
        stunFx = GameObject.Instantiate(Resources.Load("Buff/Buff_Stun")) as GameObject;
        stunFx.transform.parent = buffManager.transform;
        stunFx.transform.localPosition = buffManager.transform.FindChild("Header").localPosition;
        buffManager.StartCoroutine(WaitForStunEnd());
    }

    public override void OnBuffEnd()
    {
        if(stunFx)
            GameObject.Destroy(stunFx);
    }

    IEnumerator WaitForStunEnd()
    {
        yield return new WaitForSeconds(3);
        buffManager.RemoveBuff(buffType);
    }
}

public class ZombieBuffManager : MonoBehaviour {
    EnemyStatus status;
    List<ZombieBuffBase> buffList;
    Dictionary<ZombieBuff, ZombieBuffBase> buffSet;

    void Awake()
    {
        status = GetComponent<EnemyStatus>();
        buffList = new List<ZombieBuffBase>();
        buffSet = new Dictionary<ZombieBuff, ZombieBuffBase>();
    }

    public ZombieBuffBase AddBuff(ZombieBuff buff)
    {
        ZombieBuffBase buffBase = null;
        if (!buffSet.ContainsKey(buff))
        {
            buffBase = CreateBuff(buff);
            buffSet.Add(buff, buffBase);
            buffList.Add(buffBase);
        }
        if (buffBase != null && buffBase.auto)
            buffBase.OnBuffBegin();
        return buffBase;
    }

    public void RemoveBuff(ZombieBuff buff)
    {
        if (buffSet.ContainsKey(buff))
        {
            buffSet[buff].OnBuffEnd();
            buffList.Remove(buffSet[buff]);
            buffSet.Remove(buff);
        }
    }

    public void RemoveAll()
    {
        foreach (ZombieBuffBase buff in buffList)
        {
            buff.OnBuffEnd();
        }
        buffList.Clear();
        buffSet.Clear();
    }

    public bool HasBuff(ZombieBuff buff)
    {
        return buffSet.ContainsKey(buff);
    }

    public ZombieBuffBase GetBuffData(ZombieBuff buff)
    {
        if (buffSet.ContainsKey(buff))
            return buffSet[buff];
        else
            return null;
    }

    ZombieBuffBase CreateBuff(ZombieBuff buff)
    {
        switch (buff)
        {
            case ZombieBuff.Stun:
                return new StunBuff(buff, this);
            default:
                return new ZombieBuffBase(buff, this);
        }
    }
	
}
