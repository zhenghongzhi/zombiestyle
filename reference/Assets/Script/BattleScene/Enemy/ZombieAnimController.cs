using UnityEngine;
using System.Collections;

public class ZombieAnimController : MonoBehaviour {

    public AnimationClip idle;
    public AnimationClip birth;
    public AnimationClip dead;
    public AnimationClip run;
    public AnimationClip walk;
    public AnimationClip attack;

    bool alive = false;
    bool birthing = false;


    void Start()
    {
        animation.Stop();
        animation[idle.name].weight = 0;
        animation[idle.name].enabled = false;
        animation[idle.name].layer = 0;
        animation[run.name].weight = 0;
        animation[run.name].enabled = false;
        animation[run.name].layer = 0;
        animation[walk.name].weight = 0;
        animation[walk.name].enabled = false;
        animation[walk.name].layer = 0;
        animation[attack.name].weight = 0;
        animation[attack.name].enabled = false;
        animation[attack.name].layer = 0;
        animation[birth.name].weight = 0;
        animation[birth.name].enabled = false;
        animation[birth.name].layer = 2;
        animation[dead.name].weight = 0;
        animation[dead.name].enabled = false;
        animation[dead.name].layer = 1;
    }

    void Update()
    {
        if (!alive && Input.GetKeyUp(KeyCode.B))
            OnBirth();
        if (birthing)
        {
            if (animation[birth.name].time > birth.length - 0.4f)
            {
                EndBirth();
            }
        }
    }

    public void OnBirth()
    {
        birthing = true;
        animation.Play(idle.name);
        animation[birth.name].weight = 1;
        animation[birth.name].enabled = true;
    }

    void EndBirth()
    {
        animation.Blend(birth.name, 0, 0.4f);
        birthing = false;
        alive = true;
    }

    public void OnDead()
    {
    }

    public void Walk()
    {
    }

    public void Run()
    {
    }

    public void Idle()
    {
        animation.CrossFade(idle.name, 0.3f, PlayMode.StopSameLayer);
    }

    public void OnAttack()
    {
    }
	
}
