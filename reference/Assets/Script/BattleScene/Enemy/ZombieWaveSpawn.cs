using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;
#endif
using System.Collections.Generic;
using System.Collections;

[System.Serializable]
public class SpawnArea
{
    public float centerX;
    public float centerZ;
    public float widthX;
    public float widthZ;

    public float MinX
    {
        get { return centerX - widthX * .5f; }
    }

    public float MaxX
    {
        get { return centerX + widthX * .5f; }
    }

    public float MinZ
    {
        get { return centerZ - widthZ * .5f; }
    }

    public float MaxZ
    {
        get { return centerZ + widthZ * .5f; }
    }
}

public class ZombieWaveSpawn : MonoBehaviour {
    public int sceneID;
    public float spawnCD = 1;
    public float groundHeight = 0;
    public List<SpawnArea> spawnAreas;

    MonsterWaveData waveData;
    Dictionary<int, List<GameObject>> zombiesPool;
    [HideInInspector]
    public int currentWaveIndex = -1;
//    [HideInInspector]
    public int remainZombieThisWave = 0;

    public int totalUnspawnZombieNum;
    List<WaveZombie> unspawnZombies;
    Transform playerPosition;

    public int totalWaveNum
    {
        get { return waveData.zombieWaves.Count; }
    }

    float timeCount = 0;
    bool startSpawn = false;

	// Use this for initialization
	void Awake () {
        waveData = StaticGameData.MonsterWaves[sceneID];
        zombiesPool = new Dictionary<int, List<GameObject>>();
	}
	
    void Start()
    {
        playerPosition = GameObject.FindGameObjectWithTag("Player").transform;
    }

    void Update()
    {
        if (totalUnspawnZombieNum == 0 && remainZombieThisWave == 0 && !startSpawn)
        {
            timeCount += Time.deltaTime;
            if (timeCount >= spawnCD)
            {
                timeCount = 0;
                SpawnWave();
            }
        }

        if (startSpawn)
        {
            if (remainZombieThisWave < 5)
                SpawnZombie();
            timeCount += Time.deltaTime;
            if (timeCount >= spawnCD)
            {
                timeCount = 0;
                SpawnZombie();
            }
        }
    }

    void OnDestroy()
    {
        Resources.UnloadUnusedAssets();
    }

    public void SpawnWave()
    {
        startSpawn = true;
        currentWaveIndex++;
        if (currentWaveIndex == waveData.zombieWaves.Count)
        {
            GameObject.Find("GlobalData").GetComponent<GlobalPlayerInfo>().UnlockNextScene();
            Application.LoadLevel("SelectLevel");
            return;
        }

        Camera.mainCamera.SendMessage("ShowNewWaveInfo");
        WaveInfo waveInfo = waveData.zombieWaves[currentWaveIndex];
        remainZombieThisWave = 0;
        if (unspawnZombies != null)
            unspawnZombies.Clear();
        unspawnZombies = new List<WaveZombie>();
        foreach (WaveZombie w in waveInfo.zombieList)
        {
            WaveZombie z = new WaveZombie();
            z.zombieID = w.zombieID;
            z.num = w.num;
            unspawnZombies.Add(z);
            totalUnspawnZombieNum += w.num;
        }
    }

    void SpawnZombie()
    {
        remainZombieThisWave++;
        totalUnspawnZombieNum--;
        //1. random select a zombie of a type
        if (totalUnspawnZombieNum == 0)
            startSpawn = false;
        int index = Random.Range(0, unspawnZombies.Count);
        WaveZombie zombie = unspawnZombies[index];
        unspawnZombies[index].num--;
        if (unspawnZombies[index].num == 0)
            unspawnZombies.RemoveAt(index);

        //2.Select A Zombie Model from the Pool or Instantiate A New One
        if (!zombiesPool.ContainsKey(zombie.zombieID))
        {
            zombiesPool.Add(zombie.zombieID, new List<GameObject>());
        }
        GameObject zombieObject = null;
        foreach (GameObject z in zombiesPool[zombie.zombieID])
        {
            //Found Available One In The Pool
            if(z.GetComponent<ZombieAIStateController>().CurrentState == ZombieState.UnderGround)
            {
                zombieObject = z;
                break;
            }
        }
        if(!zombieObject)
        {
            zombieObject = InstantiaeNewZombie(zombie.zombieID);
            zombieObject.GetComponent<EnemyStatus>().data = StaticGameData.MonsterDatas[zombie.zombieID];
            zombiesPool[zombie.zombieID].Add(zombieObject);
        }

        //3.Alive This Zombie
        zombieObject.GetComponent<ZombieAIBirthState>().birthPosition = RandomPosition();
        zombieObject.GetComponent<ZombieAIStateController>().SetState(ZombieState.Birthing);
    }

    GameObject InstantiaeNewZombie(int zombieID)
    {
        GameObject zombie =
                    (GameObject)Instantiate(Resources.Load("zombie_" + StaticGameData.MonsterDatas[zombieID].ModelID));
        zombie.GetComponent<EnemyStatus>().data = StaticGameData.MonsterDatas[zombieID];
        zombie.GetComponent<EnemyStatus>().InitStatus();
        return zombie;
    }

    Vector3 RandomPosition()
    {
        //Select Closest Spawn Area
        SpawnArea area = spawnAreas[0];
        float minDist = new Vector2(playerPosition.position.x - area.centerX, playerPosition.position.z - area.centerZ).sqrMagnitude;
        for (int i = 1; i < spawnAreas.Count; ++i)
        {
            float dist = new Vector2(playerPosition.position.x - spawnAreas[i].centerX, playerPosition.position.z - spawnAreas[i].centerZ).sqrMagnitude;
            if (minDist > dist)
            {
                minDist = dist;
                area = spawnAreas[i];
            }
        }

        //Return Random Position In This Area
        return new Vector3(Random.Range(area.MinX, area.MaxX),
                groundHeight,
                Random.Range(area.MinZ, area.MaxZ));
    }


#if UNITY_EDITOR
    void OnDrawGizmosSelected()
    {
        foreach (SpawnArea area in spawnAreas)
        {
            Gizmos.DrawLine(new Vector3(area.MinX, groundHeight, area.MinZ),
                new Vector3(area.MinX, groundHeight, area.MaxZ));
            Gizmos.DrawLine(new Vector3(area.MinX, groundHeight, area.MaxZ),
                new Vector3(area.MaxX, groundHeight, area.MaxZ));
            Gizmos.DrawLine(new Vector3(area.MaxX, groundHeight, area.MaxZ),
                new Vector3(area.MaxX, groundHeight, area.MinZ));
            Gizmos.DrawLine(new Vector3(area.MaxX, groundHeight, area.MinZ),
                new Vector3(area.MinX, groundHeight, area.MinZ));
        }
    }
#endif
}
