using UnityEngine;
using System.Collections;

public class ZombieAIOnHitState : ZombieAIState {
    [HideInInspector]
    public ZombieState lastState;
    public float force = 40;

    void Awake()
    {
        state = ZombieState.HitBack;
        controller = GetComponent<ZombieAIStateController>();
        controller.RegisterState(this);
    }

    void OnEnable()
    {
        animation.Blend(controller.woundAnim.name, 1, .1f);
        controller.motionController.WalkSpeed = 0;
        rigidbody.AddForce(-transform.forward * force * rigidbody.mass, ForceMode.Impulse);
    }

    //Check if Birth Animation has played to an end
    protected override void OnStateDecision()
    {
        if (animation[controller.woundAnim.name].time > controller.woundAnim.length - .2f || !animation[controller.woundAnim.name].enabled)
        {
            if (lastState == ZombieState.HitBack)
                controller.SetState(ZombieState.Idle);
            else
                controller.SetState(lastState);
        }
    }

    void Update()
    {
        OnStateDecision();
    }

}
