using UnityEngine;
using System.Collections;

public class ZombieAIDeadSuicideState : ZombieAIState
{
    public GameObject explosionPrefab;
    public GameObject explosionEffect;
    public LayerMask dmgLayer;
    GameObject blast;
    float atkDmg = 0;

    void Awake()
    {
        state = ZombieState.Dying;
        controller = GetComponent<ZombieAIStateController>();
        controller.RegisterState(this);
    }

    void OnEnable()
    {
        EnemyStatus status = GetComponent<EnemyStatus>();
        atkDmg = status.data.Atk;
        rigidbody.isKinematic = true;
        collider.enabled = false;
        controller.motionController.enabled = false;
        controller.motionController.WalkSpeed = 0;
        animation.Stop();
        blast = GameObject.Instantiate(explosionPrefab, transform.position + new Vector3(0, 0.2f, 0), Quaternion.Euler(-90, Random.Range(0, 360), 0)) as GameObject;
        foreach (SkinnedMeshRenderer r in GetComponentsInChildren<SkinnedMeshRenderer>())
        {
            r.enabled = false;
        }

        GameObject explosion = Instantiate(explosionEffect) as GameObject;
        explosion.transform.position = transform.position + new Vector3(0, 1.5f, 0);
        ExplosionDmg script = explosion.AddComponent<ExplosionDmg>();
        script.dmgInfo = new BuffDmgInfo(atkDmg, 1, gameObject);
        script.dmgObjectLayer = dmgLayer;

        transform.position += new Vector3(0, -2, 0);
    }

    void OnDisable()
    {
        Debug.Log(GameObject.FindGameObjectWithTag("ZombieSpawner").GetComponent<ZombieWaveSpawn>().remainZombieThisWave);
        GameObject.FindGameObjectWithTag("ZombieSpawner").GetComponent<ZombieWaveSpawn>().remainZombieThisWave--;
        Destroy(blast);
    }

    void Update()
    {
        OnStateDecision();
    }

    //Check if Birth Animation has played to an end
    protected override void OnStateDecision()
    {
        if (!blast.animation.isPlaying)
            controller.SetState(ZombieState.UnderGround);
    }
}
