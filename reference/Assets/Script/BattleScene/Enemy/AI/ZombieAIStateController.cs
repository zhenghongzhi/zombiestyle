using UnityEngine;
using System.Collections.Generic;
using System.Collections;

[RequireComponent(typeof(Animation))]
[RequireComponent(typeof(PlayerMovementMoter))]
public class ZombieAIStateController : MonoBehaviour
{
    public ZombieState defaultState;
    public AnimationClip idleAnim;
    public AnimationClip birthAnim;
    public AnimationClip deadAnim;
    public AnimationClip runAnim;
    public AnimationClip walkAnim;
    public AnimationClip attackAnim;
    public AnimationClip woundAnim;
    public Transform target;
    public PlayerMovementMoter motionController;

    Dictionary<ZombieState, ZombieAIState> states;
    ZombieAIState activeStateScript = null;
    public ZombieState CurrentState
    {
        get { return activeStateScript.State; }
    }

    void Awake()
    {
        if (target == null)
        motionController = GetComponent<PlayerMovementMoter>();
        //Init AnimationStates In Awake, Otherwise It May Hide Other State Script Setting AnimationState
        InitAnimationState();
    }

    // Initial State Script Components
    void Start()
    {
        if (states.Count == 0)
            enabled = false;
        StartCoroutine(CheckTarget());
    }

    public void RegisterState(ZombieAIState stateScript)
    {
        if (states == null)
            states = new Dictionary<ZombieState, ZombieAIState>();
        if (!states.ContainsKey(stateScript.State))
            states.Add(stateScript.State, stateScript);
        stateScript.enabled = false;
    }

    //Set Enable Corresponding State Script
    public void SetState(ZombieState state)
    {
        if (activeStateScript != null)
        {
            if (activeStateScript.State == state)
                return;
            activeStateScript.enabled = false;
        }
        if (!states.ContainsKey(state))
        {
            Debug.LogWarning(name + " No " + state + " script attached");
            activeStateScript = states[defaultState];
        }
        else
        {
            if (state == ZombieState.HitBack)
                ((ZombieAIOnHitState)states[state]).lastState = activeStateScript.State;
            activeStateScript = states[state];
        }
        activeStateScript.enabled = true;
    }

    public bool HasState(ZombieState checkState)
    {
        foreach (ZombieState state in states.Keys)
        {
            if (state == checkState)
                return true;
        }
        return false;
    }

    void InitAnimationState()
    {
        animation.Stop();
        if (idleAnim)
        {
            animation[idleAnim.name].weight = 0;
            animation[idleAnim.name].enabled = false;
            animation[idleAnim.name].layer = 0;
        }
        if (runAnim)
        {
            animation[runAnim.name].weight = 0;
            animation[runAnim.name].enabled = false;
            animation[runAnim.name].layer = 0;
        }
        if (walkAnim)
        {
            animation[walkAnim.name].weight = 0;
            animation[walkAnim.name].enabled = false;
            animation[walkAnim.name].layer = 0;
        }
        if (attackAnim)
        {
            animation[attackAnim.name].weight = 0;
            animation[attackAnim.name].enabled = false;
            animation[attackAnim.name].layer = 1;
        }
        if (woundAnim)
        {
            animation[woundAnim.name].weight = 0;
            animation[woundAnim.name].enabled = false;
            animation[woundAnim.name].layer = 2;
        }
        if (birthAnim)
        {
            animation[birthAnim.name].weight = 0;
            animation[birthAnim.name].enabled = false;
            animation[birthAnim.name].layer = 3;
        }
        if (deadAnim)
        {
            animation[deadAnim.name].weight = 0;
            animation[deadAnim.name].enabled = false;
            animation[deadAnim.name].layer = 4;
        }
    }

    public float GetMinTargetSqrDistance()
    {
        if (target == null)
            return Mathf.Infinity;
        Vector2 xzOffset = new Vector2(target.position.x - transform.position.x, 
            target.position.z - transform.position.z);
        return xzOffset.sqrMagnitude;
    }

    IEnumerator CheckTarget()
    {
        while (enabled)
        {
            DetermineTarget();
            yield return new WaitForSeconds(1);
        }
    }

    public void DetermineTarget()
    {
        GameObject decoy = GameObject.FindGameObjectWithTag("Decoy");
        if (decoy == null)
        {
            target = null;
            GameObject[] players = GameObject.FindGameObjectsWithTag("Player");
            if (players.Length == 0)
                return;
            float minSqrDist = Mathf.Infinity;
            for (int i = 0; i < players.Length; ++i)
            {
                if (!players[i].GetComponent<CharacterStatus>().alive)
                    continue;
                float sqrDist = (transform.position - players[i].transform.position).sqrMagnitude;
                if (sqrDist < minSqrDist)
                {
                    target = players[i].transform;
                    minSqrDist = sqrDist;
                }
            }
        }
        else
            target = decoy.transform;
    }
}
