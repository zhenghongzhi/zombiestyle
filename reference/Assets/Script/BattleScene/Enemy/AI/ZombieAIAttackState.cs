using UnityEngine;
using System.Collections;

public class ZombieAIAttackState : ZombieAIState {
    [HideInInspector]
    public float atkRange = 1;
    [HideInInspector]
    public float atkDmg = 5;
    public float atkCd = 0.5f;
    public float startFXTime = 0.35f;
    public float endFXTime = 0.6f;
    public ZombieAttackFX fxScript;
    float sqrAtkRange;
    float timeCounter;
    bool applyAttack = false;

    public float AtkDmg
    {
        get { return atkDmg; }
        set
        {
            atkDmg = value;
            if(fxScript)
                fxScript.AtkDmg = (int)atkDmg;
        }
    }

    public float AtkRange
    {
        get { return atkRange; }
        set
        {
            atkRange = value;
            sqrAtkRange = value * value;
        }
    }

    void Awake()
    {
        state = ZombieState.Attacking;
        controller = GetComponent<ZombieAIStateController>();
        controller.RegisterState(this);
        if (fxScript)
            fxScript.attackFx.gameObject.SetActive(false);
    }

    //When Attack State Begin 
    //Start Playing Attack Animation
    void OnEnable()
    {
        EnemyStatus status = GetComponent<EnemyStatus>();
        AtkRange = status.data.AtkRange;
        AtkDmg = status.data.Atk;

        animation.CrossFade(controller.attackAnim.name, 0.1f, PlayMode.StopSameLayer);//attack layer = 1, warpMode = once
        animation.CrossFade(controller.idleAnim.name);//idle layer = 0
        controller.motionController.WalkSpeed = 0;
        timeCounter = 0;
        applyAttack = false;
        if (fxScript)
            fxScript.attackFx.gameObject.SetActive(true);
    }

    void OnDisable()
    {
        animation.Blend(controller.attackAnim.name, 0);
        animation.Blend(controller.idleAnim.name, 0);
    }

    //Check if the Player Left Attack Range Before the Next Attack Animation Start
    protected override void OnStateDecision()
    {
        if (controller.target == null)
        {
            controller.SetState(ZombieState.Wondering);
        }
        if((!animation[controller.attackAnim.name].enabled || 
            animation[controller.attackAnim.name].time > controller.attackAnim.length - 0.3f) &&
            sqrAtkRange < controller.GetMinTargetSqrDistance())
        {
            controller.SetState(ZombieState.Wondering);
        }
    }

    // Update is called once per frame
    void Update()
    {
        OnStateDecision();
        if (!animation[controller.attackAnim.name].enabled)
        {
            if (applyAttack)
            {
                applyAttack = false;
                fxScript.enabled = false;
            }
            timeCounter += Time.deltaTime;
            if (controller.target)
            {
                Vector3 toTarget = (controller.target.position - transform.position).normalized; 
                controller.motionController.faceDir = toTarget;

                if (timeCounter > atkCd && Vector3.Dot(toTarget, transform.forward) > 0.8f)
                {
                    timeCounter = 0;
                    animation.Blend(controller.attackAnim.name, 1, 0.1f);
                }
            }
        }
        else if (animation[controller.attackAnim.name].time > endFXTime)
        {
            if (applyAttack)
            {
                fxScript.enabled = false;
                applyAttack = false;
            }
        }
        else if (animation[controller.attackAnim.name].time > startFXTime)
        {
            if (!applyAttack)
            {
                if(fxScript)
                    fxScript.enabled = true;
                applyAttack = true;
            }
        }
    }
}
