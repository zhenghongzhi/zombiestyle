using UnityEngine;
using System.Collections;

public class ZombieAIWonderState : ZombieAIState
{
    public float walkSpeed = 1;
    public float alertDist = 6.0f;
    public float minTurnTime = 4;
    public float maxTurnTime = 3;
    Vector3 lastPosition;
    float wonderTimeInterval;
    float sqrAlertDist;
    float timeRecord = 0;
    float blockDetectionInterval = 1;

    public float AlertRange
    {
        get { return alertDist; }
        set
        {
            alertDist = value;
            sqrAlertDist = alertDist * alertDist;
        }
    }

    void Awake()
    {
        state = ZombieState.Wondering;
        controller = GetComponent<ZombieAIStateController>();
        controller.RegisterState(this);
        sqrAlertDist = alertDist * alertDist;
    }

    //When Idle State Begin 
    //Start Playing Idle Animation
    void OnEnable()
    {
        animation.CrossFade(controller.walkAnim.name);
        controller.motionController.WalkSpeed = GetComponent<EnemyStatus>().data.MSpeed;
        timeRecord = Time.deltaTime;
        wonderTimeInterval = minTurnTime;
        lastPosition = new Vector3(100, 100, 100);
        StartCoroutine(BlockDetection());
    }

    void OnDisable()
    {
        animation.Blend(controller.walkAnim.name, 0);
    }

    //Check if Birth Animation has played to an end
    protected override void OnStateDecision()
    {
        if (sqrAlertDist > controller.GetMinTargetSqrDistance())
        {
            controller.SetState(ZombieState.Chasing);
        }
    }

	// Update is called once per frame
    void Update()
    {
        OnStateDecision();
        if (Time.time - timeRecord > wonderTimeInterval)
        {
            Quaternion rotate = Quaternion.AngleAxis(Random.Range(-120, 120), Vector3.up);
            Vector3 dir = rotate * transform.forward;
            dir.y = 0;
            timeRecord = Time.time;
            wonderTimeInterval = Random.Range(minTurnTime, maxTurnTime);
            controller.motionController.faceDir = dir.normalized;
        }
        controller.motionController.moveDir = transform.forward;
	}

    IEnumerator BlockDetection()
    {
        while (enabled)
        {
            if ((transform.position - lastPosition).sqrMagnitude < 0.1f)
            {
                timeRecord -= wonderTimeInterval;
                blockDetectionInterval = 0.4f;
            }
            else if(blockDetectionInterval < 5)
                blockDetectionInterval += 0.4f;
            lastPosition = transform.position;
            yield return new WaitForSeconds(blockDetectionInterval);
        }
    }
}
