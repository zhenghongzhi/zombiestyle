using UnityEngine;
using System.Collections;

public class ZombieAISprintAtkState : ZombieAIState {

    public float holdbackTime = 1;
    float atkDmg;
    float sprintTime;
    bool hasApplyAttack;
    bool canAttack;

    void Awake()
    {
        state = ZombieState.Attacking;
        controller = GetComponent<ZombieAIStateController>();
        controller.RegisterState(this);
    }

    void OnEnable()
    {
        atkDmg = GetComponent<EnemyStatus>().data.Atk;
        PlayerMovementMoter moter = GetComponent<PlayerMovementMoter>();
        moter.WalkSpeed = 0;
        animation.CrossFade(controller.idleAnim.name);
        float sprintDistance = (controller.target.position - transform.position).magnitude;
        sprintTime = sprintDistance / 8;
        hasApplyAttack = false;
        canAttack = false;
        StartCoroutine(SprintAttack());
    }

    //Check if the Player Left Attack Range Before the Next Attack Animation Start
    protected override void OnStateDecision()
    {
    }

    void Update()
    {
        if (controller.target && canAttack && !hasApplyAttack)
        {
            if ((controller.target.position - transform.position).sqrMagnitude < 1.5f)
            {
                controller.target.gameObject.SendMessage("ApplyDamage", new BuffDmgInfo(atkDmg, 0, gameObject));
                hasApplyAttack = true;
            }
        }
    }

    IEnumerator SprintAttack()
    {
        yield return new WaitForSeconds(holdbackTime);
        if (!enabled)
            yield break;
        animation.CrossFade(controller.attackAnim.name, 0.1f);
        yield return new WaitForSeconds(0.189f);
        if (!enabled)
            yield break;
        animation[controller.attackAnim.name].speed = 0.3f / sprintTime;
        PlayerMovementMoter moter = GetComponent<PlayerMovementMoter>();
        moter.moveDir = transform.forward;
        moter.WalkSpeed = 8;
        canAttack = true;
        yield return new WaitForSeconds(sprintTime);
        if (!enabled)
            yield break;
        moter.WalkSpeed = 0;
        animation[controller.attackAnim.name].speed = 1;
        canAttack = false;
        yield return new WaitForSeconds(0.3f);
        if (!enabled)
            yield break;
        controller.SetState(ZombieState.Wondering);
    }
}
