using UnityEngine;
using System.Collections;

public class ZombieAISuicideAtkState : ZombieAIState {
    [HideInInspector]
    public float atkRange = 1;
    [HideInInspector]
    public float atkDmg = 5;
    public GameObject explosionEffect;
    public LayerMask dmgLayer;
    public float maxTime = 5;
    float sqrAtkRange;
    float timeCounter;
    bool isRed = false;

    public float AtkDmg
    {
        get { return atkDmg; }
        set
        {
            atkDmg = value;
        }
    }

    public float AtkRange
    {
        get { return atkRange; }
        set
        {
            atkRange = value;
            sqrAtkRange = value * value;
        }
    }

    void Awake()
    {
        state = ZombieState.Attacking;
        controller = GetComponent<ZombieAIStateController>();
        controller.RegisterState(this);
    }

    //When Attack State Begin 
    //Start Playing Attack Animation
    void OnEnable()
    {
        EnemyStatus status = GetComponent<EnemyStatus>();
        AtkRange = status.data.AtkRange;
        AtkDmg = status.data.Atk;
        controller.motionController.WalkSpeed = status.data.MSpeed * 2f;
        animation.Blend(controller.runAnim.name, 1, 0.1f);
        timeCounter = maxTime;
        isRed = true;
        StartCoroutine(ChangeColor());
    }

    void OnDisable()
    {
        animation.Blend(controller.runAnim.name, 0, 0.1f);
        GetComponentInChildren<SkinnedMeshRenderer>().material.color = Color.white;
    }

    //Check if the Player Left Attack Range Before the Next Attack Animation Start
    protected override void OnStateDecision()
    {
        if (controller.target == null)
        {
            controller.SetState(ZombieState.Wondering);
        }
    }

    // Update is called once per frame
    void Update()
    {
        timeCounter -= Time.deltaTime;
        OnStateDecision();
        if (controller.target != null)
        {
            controller.motionController.faceDir =
                new Vector3(controller.target.position.x - transform.position.x, 0, controller.target.position.z - transform.position.z);
            controller.motionController.moveDir = transform.forward;
        }
        if (timeCounter <= 0)
        {
            Boom();
        }
        else if (controller.GetMinTargetSqrDistance() < 2)
        {
            StartCoroutine(ShakeCamera());
            Boom();
        }
    }

    void Boom()
    {
        controller.SetState(ZombieState.DieBlasting);
        GameObject explosion = Instantiate(explosionEffect) as GameObject;
        explosion.transform.position = transform.position + new Vector3(0, 1.5f, 0);
        ExplosionDmg script = explosion.AddComponent<ExplosionDmg>();
        script.dmgInfo = new BuffDmgInfo(atkDmg, 1, gameObject);
        script.dmgObjectLayer = dmgLayer;
    }

    IEnumerator ChangeColor()
    {
        while (enabled)
        {
            GetComponentInChildren<SkinnedMeshRenderer>().material.color = isRed ? Color.red : Color.white;
            isRed = !isRed;
            yield return new WaitForSeconds(Mathf.Clamp(timeCounter * 0.1f, 0.1f, 0.4f));
        }
    }

    IEnumerator ShakeCamera()
    {
        Camera.mainCamera.GetComponent<CameraShake>().enabled = true;
        yield return new WaitForSeconds(0.5f);
        Camera.mainCamera.GetComponent<CameraShake>().enabled = false;
    }
}
