using UnityEngine;
using System.Collections;

public class ZombieAIIdleState : ZombieAIState
{
    public float idleTime = 1.0f;
    float timeCount = 0;

    void Awake()
    {
        state = ZombieState.Idle;
        controller = GetComponent<ZombieAIStateController>();
        controller.RegisterState(this);
    }

    //When Idle State Begin 
    //Start Playing Idle Animation
    void OnEnable()
    {
        animation.CrossFade(controller.idleAnim.name);
        controller.motionController.WalkSpeed = 0;
        timeCount = 0;
    }

    void OnDisable()
    {
        animation.Blend(controller.idleAnim.name, 0);
    }

    //Check if Birth Animation has played to an end
    protected override void OnStateDecision()
    {
        if (timeCount > idleTime)
        {
            controller.SetState(ZombieState.Wondering);
        }
    }

    void Update()
    {
        OnStateDecision();
        timeCount += Time.deltaTime;
    }
}
