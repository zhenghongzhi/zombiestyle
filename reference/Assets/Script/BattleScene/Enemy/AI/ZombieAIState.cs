using UnityEngine;
using System.Collections.Generic;

public enum ZombieState : int
{
    Birthing,
    Idle,
    Wondering,
    Chasing,
    Attacking,
    HitBack,
    Dying,
    DieBlasting,
    UnderGround,
}

[RequireComponent(typeof(ZombieAIStateController))]
public class ZombieAIState : MonoBehaviour
{
    protected ZombieState state;
    protected ZombieAIStateController controller;

    public ZombieState State
    {
        get { return state; }
    }

    protected void Init(ZombieState s)
    {
        state = s;
        controller = GetComponent<ZombieAIStateController>();
    } 

    protected void ChangeState(ZombieState newState)
    {
        controller.SetState(newState);
    }

    //Override This Function to Set State Change Determination
    protected virtual void OnStateDecision() { }
}

