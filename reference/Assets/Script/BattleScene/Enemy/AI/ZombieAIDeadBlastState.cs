using UnityEngine;
using System.Collections;

public class ZombieAIDeadBlastState : ZombieAIState
{
    public GameObject explosionPrefab;
    GameObject explosion;

    void Awake()
    {
        state = ZombieState.DieBlasting;
        controller = GetComponent<ZombieAIStateController>();
        controller.RegisterState(this);
    }

    void OnEnable()
    {
        rigidbody.isKinematic = true;
        collider.enabled = false;
        controller.motionController.enabled = false;
        controller.motionController.WalkSpeed = 0;
        animation.Stop();
        explosion = GameObject.Instantiate(explosionPrefab, transform.position + new Vector3(0, 0.2f, 0), Quaternion.Euler(-90, Random.Range(0, 360), 0)) as GameObject;
        foreach (SkinnedMeshRenderer r in GetComponentsInChildren<SkinnedMeshRenderer>())
        {
            r.enabled = false;
        }
        transform.position += new Vector3(0, -2, 0);
    }

    void OnDisable()
    {
        GameObject.FindGameObjectWithTag("ZombieSpawner").GetComponent<ZombieWaveSpawn>().remainZombieThisWave--;
        Destroy(explosion);
    }

    void Update()
    {
        OnStateDecision();
    }

    //Check if Birth Animation has played to an end
    protected override void OnStateDecision()
    {
        if (!explosion.animation.isPlaying)
            controller.SetState(ZombieState.UnderGround);
    }
}
