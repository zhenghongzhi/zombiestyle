using UnityEngine;
using System.Collections;

public class ZombieAIUnderGroundState : ZombieAIState {

    void Awake()
    {
        state = ZombieState.UnderGround;
        controller = GetComponent<ZombieAIStateController>();
        controller.RegisterState(this);
    }

    void OnEnable()
    {
        rigidbody.isKinematic = true;
        collider.enabled = false;
        controller.motionController.enabled = false;
        controller.motionController.WalkSpeed = 0;
        GetComponent<EnemyStatus>().healthBarPrefab.SetActive(false);
        foreach (SkinnedMeshRenderer r in GetComponentsInChildren<SkinnedMeshRenderer>())
        {
            r.enabled = false;
        }
    }

    void OnDisable()
    {
        collider.enabled = true;
        rigidbody.isKinematic = false;
        rigidbody.constraints = RigidbodyConstraints.FreezeAll;
        controller.motionController.enabled = true;
        controller.motionController.WalkSpeed = 0;
        foreach (SkinnedMeshRenderer r in GetComponentsInChildren<SkinnedMeshRenderer>())
        {
            r.enabled = true;
        }
    }
}
