using UnityEngine;
using System.Collections;

public class ZombieAIBirthState : ZombieAIState
{
    //fade length between birth and idle
    public GameObject groundHolePrefab;
    public float crossFadeLength = 0.4f;
    public Vector3 birthPosition;
    GameObject groundHole;
    float timeCounter;

    void Awake()
    {
        state = ZombieState.Birthing;
        controller = GetComponent<ZombieAIStateController>();
        controller.RegisterState(this);
    }

    //When Birth State Begin 
    //Start Playing Birth Animation
    void OnEnable()
    {
        timeCounter = controller.birthAnim.length;
        transform.position = birthPosition;
        GetComponent<EnemyStatus>().OnAlive();
        animation.Play(controller.idleAnim.name);
        animation[controller.birthAnim.name].enabled = true;
        animation[controller.birthAnim.name].weight = 1;
        if (groundHolePrefab)
            groundHole = (GameObject)GameObject.Instantiate(groundHolePrefab, transform.position, Quaternion.Euler(270, 0, 0));
    }

    //When Birth State End
    //Blend Animation from Birth to Idle
    void OnDisable() 
    {
        rigidbody.constraints = RigidbodyConstraints.FreezeRotation;
        animation.Blend(controller.birthAnim.name, 0, crossFadeLength);
        if (groundHole)
        {
            Destroy(groundHole);
            groundHole = null;
        }
    }
    
    void Update()
    {
        OnStateDecision();
    }

    //Check if Birth Animation has played to an end
    protected override void OnStateDecision()
    {
        timeCounter -= Time.deltaTime;
        //Use Time Counter in case Animation Stop Playing due to Clips Bounds Cut
        if (timeCounter < .2f)
        {
            controller.SetState(ZombieState.Idle);
        }
    }
}
