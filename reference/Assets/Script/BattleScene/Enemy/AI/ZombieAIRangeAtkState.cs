using UnityEngine;
using System.Collections;

public class ZombieAIRangeAtkState : ZombieAIState
{
    public GameObject bulletPrefab;
    public Transform shootPoint;
    [HideInInspector]
    public float atkRange = 4;
    [HideInInspector]
    public float atkDmg = 5;
    public int dmgEffect = 0;
    public float startAtkTime;
    public float atkCd = 0.5f;
    float sqrAtkRange;
    float timeCounter = 0;
    bool applyAttack = false;
    GameObject bulletInstance = null;
    Vector3 lastTargetPos;

    public float AtkRange
    {
        get { return atkRange; }
        set
        {
            atkRange = value;
            sqrAtkRange = value * value;
        }
    }

    void Awake()
    {
        state = ZombieState.Attacking;
        controller = GetComponent<ZombieAIStateController>();
        controller.RegisterState(this);
        sqrAtkRange = atkRange * atkRange;
    }

    //When Attack State Begin 
    //Start Playing Attack Animation
    void OnEnable()
    {
        rigidbody.isKinematic = true;
        EnemyStatus status = GetComponent<EnemyStatus>();
        AtkRange = status.data.AtkRange;
        atkDmg = status.data.Atk;
        animation.CrossFade(controller.idleAnim.name);//idle layer = 0
        controller.motionController.WalkSpeed = 0;
        applyAttack = false;
        lastTargetPos = controller.target.position;
    }

    void OnDisable()
    {
        rigidbody.isKinematic = false;
        animation.Blend(controller.attackAnim.name, 0);
        animation.Blend(controller.idleAnim.name, 0);
    }

    //Check if the Player Left Attack Range Before the Next Attack Animation Start
    protected override void OnStateDecision()
    {
        if (controller.target == null)
        {
            controller.SetState(ZombieState.Wondering);
        }
        if ((!animation[controller.attackAnim.name].enabled ||
            animation[controller.attackAnim.name].time > controller.attackAnim.length - 0.3f) &&
            sqrAtkRange < controller.GetMinTargetSqrDistance())
        {
            controller.SetState(ZombieState.Chasing);
        }
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        OnStateDecision();
        timeCounter += Time.deltaTime;
        if (controller.target != null)
        {
            Vector3 predictMovement = (controller.target.position - lastTargetPos) * 0.4f / Time.deltaTime;
            Vector3 toTarget = controller.target.position + predictMovement - transform.position;
            lastTargetPos = controller.target.position;
            toTarget.y = 0;
            if (!animation[controller.attackAnim.name].enabled)
            {
                controller.motionController.faceDir = toTarget;
                if (timeCounter >= 0 && Vector3.Dot(toTarget, transform.forward) > 0.9f)
                {
                    StartCoroutine(Shoot());
                    timeCounter = -atkCd;
                }
            }
        }
    }

    IEnumerator Shoot()
    {
        animation.CrossFade(controller.attackAnim.name, 0.1f, PlayMode.StopSameLayer);//attack layer = 1, warpMode = once
        yield return new WaitForSeconds(startAtkTime);
        if (!bulletInstance)
            bulletInstance = GameObject.Instantiate(bulletPrefab, shootPoint.position, shootPoint.rotation) as GameObject;
        else
        {
            bulletInstance.transform.position = shootPoint.position;
            bulletInstance.transform.rotation = shootPoint.rotation;
            bulletInstance.SetActive(true);
            bulletInstance.GetComponent<Bullet>().enabled = true;
        }
        BulletInfo info = bulletInstance.GetComponent<Bullet>().bulletInfo;      
        info.direction = shootPoint.forward;
        info.dmg = atkDmg;
        info.effect = dmgEffect;
        info.shooter = gameObject;
    }
}
