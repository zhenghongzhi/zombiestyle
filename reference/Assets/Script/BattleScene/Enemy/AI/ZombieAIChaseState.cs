using UnityEngine;
using System.Collections;

public class ZombieAIChaseState : ZombieAIState
{
    [HideInInspector]
    public float runSpeed = 2;
    [HideInInspector]
    public float chaseRange = 8;
    float sqrAtkDist;
  //  float sqrChaseRange;

    public float ChaseRange
    {
        get { return chaseRange; }
        set
        {
            chaseRange = value;
      //      sqrChaseRange = value * value;
        }
    }

    void Awake()
    {
        state = ZombieState.Chasing;
        controller = GetComponent<ZombieAIStateController>();
        controller.RegisterState(this);
     //   sqrChaseRange = chaseRange * chaseRange;
    }

    //When Idle State Begin 
    //Start Playing Idle Animation
    void OnEnable()
    {
        runSpeed = GetComponent<EnemyStatus>().data.MSpeed * 1.5f;
        sqrAtkDist = GetComponent<EnemyStatus>().data.AtkRange;
        sqrAtkDist = sqrAtkDist * sqrAtkDist;
        animation.CrossFade(controller.runAnim.name);
        controller.motionController.WalkSpeed = runSpeed;
    }

    void OnDisable()
    {
        animation.Blend(controller.runAnim.name, 0);
    }

    //Check if Birth Animation has played to an end
    protected override void OnStateDecision()
    {
        if (controller.target == null)
        {
            controller.SetState(ZombieState.Wondering);
        }
        float sqrToTargetDist = controller.GetMinTargetSqrDistance();
        if (sqrAtkDist > sqrToTargetDist && Vector3.Dot(controller.motionController.moveDir, transform.forward) > 0.9f)
        {
            controller.SetState(ZombieState.Attacking);
        }
//         else if (sqrChaseRange < sqrToTargetDist)
//         {
//             controller.SetState(ZombieState.Wondering);
//         }
    }

    // Update is called once per frame
    void Update()
    {
        OnStateDecision();
        if (controller.target != null)
        {
            controller.motionController.faceDir =
                new Vector3(controller.target.position.x - transform.position.x, 0, controller.target.position.z - transform.position.z);
            controller.motionController.moveDir = transform.forward;
        }
    }
}
