using UnityEngine;
using System.Collections;

public class ZombieAIRangeChaseState : ZombieAIState
{
    public float runSpeed = 1.5f;
    [HideInInspector]
    public float chaseRange = 8;
    float sqrAtkDist;

    public float ChaseRange
    {
        get { return chaseRange; }
        set
        {
            chaseRange = value;
        }
    }

    void Awake()
    {
        state = ZombieState.Chasing;
        controller = GetComponent<ZombieAIStateController>();
        controller.RegisterState(this);
    }

    //When Idle State Begin 
    //Start Playing Idle Animation
    void OnEnable()
    {
        sqrAtkDist = GetComponent<EnemyStatus>().data.AtkRange - 1;
        sqrAtkDist = sqrAtkDist * sqrAtkDist;
        animation.CrossFade(controller.runAnim.name);
        controller.motionController.WalkSpeed = runSpeed;
    }

    void OnDisable()
    {
        animation.Blend(controller.runAnim.name, 0);
    }

    //Check if Birth Animation has played to an end
    protected override void OnStateDecision()
    {
        if (controller.target == null)
        {
            controller.SetState(ZombieState.Wondering);
        }
        float sqrToTargetDist = controller.GetMinTargetSqrDistance();
        if (sqrAtkDist > sqrToTargetDist)
        {
            controller.SetState(ZombieState.Attacking);
        }
    }

    // Update is called once per frame
    void Update()
    {
        OnStateDecision();
        if (controller.target)
        {
            controller.motionController.faceDir =
                new Vector3(controller.target.position.x - transform.position.x, 0, controller.target.position.z - transform.position.z);
            controller.motionController.moveDir = transform.forward;
        }
    }
}
