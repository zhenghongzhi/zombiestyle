using UnityEngine;
using System.Collections;

public class ZombieAIDeadState : ZombieAIState {
    public float burySpeed;

    void Awake()
    {
        state = ZombieState.Dying;
        controller = GetComponent<ZombieAIStateController>();
        controller.RegisterState(this);
    }

    //When Idle State Begin 
    //Start Playing Idle Animation
    void OnEnable()
    {
        rigidbody.isKinematic = true;
        collider.enabled = false;
        controller.motionController.enabled = false;
        controller.motionController.WalkSpeed = 0;
        animation.Stop();
        animation[controller.deadAnim.name].weight = 0;
        animation.Blend(controller.deadAnim.name, 1, 0.1f);
    }

    void OnDisable()
    {
        animation.Blend(controller.deadAnim.name, 0);
        GameObject.FindGameObjectWithTag("ZombieSpawner").GetComponent<ZombieWaveSpawn>().remainZombieThisWave--;
    }

    //Check if Birth Animation has played to an end
    protected override void OnStateDecision()
    {
        if (transform.position.y < -2)
        {
            controller.SetState(ZombieState.UnderGround);
        }
    }

    void Update()
    {
        OnStateDecision();
        if (!animation[controller.deadAnim.name].enabled)
            transform.position += -Vector3.up * Time.deltaTime * burySpeed;
    }
}
