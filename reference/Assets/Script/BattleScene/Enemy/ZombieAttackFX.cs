using UnityEngine;
using System.Collections.Generic;

public class ZombieAttackFX : MonoBehaviour {
    [HideInInspector]
    public int AtkDmg = 5;
    public AttackFXBillbroad attackFx;
 
    bool hasAppliedDmg = false;

    void Awake()
    {
        enabled = false;
    }

    void OnEnable()
    {
        hasAppliedDmg = false;
        attackFx.enabled = true;
    }

    void OnTriggerEnter(Collider other) 
    {
        if (!hasAppliedDmg && other.gameObject.tag == "Player")
        {
            other.SendMessage("ApplyDamage", new BuffDmgInfo(AtkDmg, 0, gameObject.transform.root.gameObject), SendMessageOptions.DontRequireReceiver);
            hasAppliedDmg = true;
        }
    }
}
