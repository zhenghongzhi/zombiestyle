using UnityEngine;
using System.Collections;

public class EnemyStatus : MonoBehaviour {

    public MonsterData data;
    public float explosionRate = 0.5f;
    [HideInInspector]
    public float currentHealth = 100;
    public GameObject healthBarPrefab;
    public Texture2D instructFrame;
    [HideInInspector]
    public GameObject lastHitter;
    Texture2D zombieIcon;
    ZombieWaveSpawn spawner;
    GameObject healthBarInstance;
    Transform headTransform;
    float dmgAccum = 0;
    int onHitDmg;
    static Rect screenRect;

    static Rect ScreenRect
    {
        get 
        {
            if (screenRect.width == 0)
            {
                screenRect = new Rect(25, CsGUI.CY(88), CsGUI.CX(914), CsGUI.CY(594));
            }
            return screenRect;
        }
    }

    void Awake()
    {
        healthBarInstance = (GameObject)Instantiate(healthBarPrefab);
        HealthBar bar = healthBarInstance.GetComponent<HealthBar>();
        headTransform = transform.FindChild("Header");
        bar.followObject = headTransform.gameObject;
        GameObject g = GameObject.FindGameObjectWithTag("ZombieSpawner");
        if (g)
            spawner = g.GetComponent<ZombieWaveSpawn>();
    }

    void Start()
    {
        zombieIcon = Resources.Load("Battle/bg_Zombie_instructions_" + data.ModelID) as Texture2D;
        onHitDmg = (int)(data.HP * data.OnHitDmgPer);
        lastHitter = null;
    }

    void OnDestroy()
    {
        if (healthBarInstance)
            DestroyObject(healthBarInstance);
    }

    void OnGUI()
    {
        if (!spawner || spawner.remainZombieThisWave > 5)
            return;
        //Show Zombie Position On Screen When Outside Screen
        if (GetComponent<ZombieAIStateController>().CurrentState != ZombieState.UnderGround)
        {
            Vector3 screenPos = Camera.mainCamera.WorldToScreenPoint(headTransform.position);
            if (screenPos.z < 0)
                screenPos = -screenPos;
            screenPos.y = Screen.height - screenPos.y;
            if (!ScreenRect.Contains(screenPos))
            {
                screenPos.x = Mathf.Clamp(screenPos.x, ScreenRect.xMin, ScreenRect.xMax);
                screenPos.y = Mathf.Clamp(screenPos.y, ScreenRect.yMin, ScreenRect.yMax);
                float rotAngle = Mathf.Atan2(Screen.width * 0.5f - screenPos.x, screenPos.y - Screen.height * 0.5f);
                Matrix4x4 mat = GUI.matrix;
                GUIUtility.RotateAroundPivot(Mathf.Rad2Deg * rotAngle, new Vector2(screenPos.x + CsGUI.CY(30), screenPos.y + CsGUI.CY(43)));
                GUI.DrawTexture(new Rect(screenPos.x, screenPos.y, CsGUI.CY(59), CsGUI.CY(86)), instructFrame);
                GUI.matrix = mat;
                if(zombieIcon)
                    GUI.DrawTexture(new Rect(screenPos.x, screenPos.y, CsGUI.CY(59), CsGUI.CY(86)), zombieIcon);
            }
        }
    }

    void ApplyDamage(BulletInfo bullet)
    {
        //Because Zombie Dying State Is Set Through Message Send,
        //a delay could exists. Therefore we should prevent zombie
        //from apply another damage when they are dying.
        //Because when this situation happens, the ZombieSpawner will
        //receive multiple zombie dead message, which leads to counting mistake.
        if (currentHealth <= 0)
            return;

        float dmg = bullet.dmg * (100.0f - data.Def) * 0.01f;
        if (dmg < 1)
            dmg = 1;
        currentHealth -= dmg;
        dmgAccum += dmg > currentHealth ? currentHealth : dmg;
        healthBarInstance.GetComponent<HealthBar>().SetHealth(currentHealth / data.HP);
        if (Vector3.Dot(-bullet.direction, transform.forward) > 0.8f && dmgAccum > onHitDmg)
        {
            if (GetComponent<ZombieAIStateController>().CurrentState != ZombieState.Birthing && 
                GetComponent<ZombieAIStateController>().HasState(ZombieState.HitBack))
            {
                GetComponent<ZombieAIStateController>().SetState(ZombieState.HitBack);
                dmgAccum = 0;
            }
        }
        if (currentHealth <= 0)
        {
            OnDeath();
        }
        else if(GetComponent<ZombieAIStateController>().CurrentState == ZombieState.Wondering)
        {
            GetComponent<ZombieAIStateController>().SetState(ZombieState.Chasing);
        }
        lastHitter = bullet.shooter;
    }

    void ApplyDamage(BuffDmgInfo dmgInfo)
    {
        if (currentHealth <= 0)
            return;

        currentHealth -= dmgInfo.dmg;
        healthBarInstance.GetComponent<HealthBar>().SetHealth(currentHealth / data.HP);
        if (currentHealth <= 0)
        {
            OnDeath();
            if(dmgInfo.applier.tag == "Player")
                dmgInfo.applier.SendMessage("OnKilledEnemy", data.Exp);
        }
        else
        {
            switch (dmgInfo.buff)
            {
                case 1:
                    if (GetComponent<ZombieAIStateController>().CurrentState != ZombieState.Birthing)
                    {
                        if (GetComponent<ZombieAIStateController>().HasState(ZombieState.HitBack))
                        {
                            GetComponent<ZombieAIOnHitState>().force = 30;
                            GetComponent<ZombieAIStateController>().SetState(ZombieState.HitBack);
                        }
                    }
                    break;
            }
        }
    }

    public void InitStatus()
    {
        GetComponent<ZombieAIWonderState>().AlertRange = data.AlertRange;
    }

    void OnDeath()
    {
        healthBarInstance.SetActive(false);
        GetComponent<ZombieBuffManager>().RemoveAll();
        if (Random.value > 1 - explosionRate)
            GetComponent<ZombieAIStateController>().SetState(ZombieState.DieBlasting);
        else
            GetComponent<ZombieAIStateController>().SetState(ZombieState.Dying);
    }

    public void SendDeadMessage()
    {
        if(lastHitter!= null && lastHitter.tag == "Player")
            lastHitter.SendMessage("OnKilledEnemy", data.Exp);
    }

    public void OnAlive()
    {
        currentHealth = data.HP;
        healthBarInstance.SetActive(true);
        healthBarInstance.GetComponent<HealthBar>().SetHealth(1);
    }
}
