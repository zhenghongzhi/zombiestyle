using UnityEngine;
using System.Collections;

public class ExplosionDmg : MonoBehaviour {
    public LayerMask dmgObjectLayer;
    public BuffDmgInfo dmgInfo;
    SphereCollider sphereCollider;
    float time = 0;
    float maxSize = 0;
    float lerp = 0;
    
	// Use this for initialization
    void Start()
    {
        sphereCollider = GetComponent<SphereCollider>();
        maxSize = sphereCollider.radius;
        sphereCollider.radius = 0;
        time = GetComponent<ParticleEmitter>().maxEnergy;
	}
	
	// Update is called once per frame
	void Update () {
        time -= Time.deltaTime;
        if (time < 0)
        {
            Destroy(gameObject);
            return;
        }
        lerp = Mathf.InverseLerp(0, GetComponent<ParticleEmitter>().maxEnergy, time);
        float size = Mathf.Lerp(0, maxSize, 1 - lerp);
        sphereCollider.radius = size;
	}

    void OnTriggerEnter(Collider other)
    {
        int otherLayerMask = 1 << other.gameObject.layer;
        if ((otherLayerMask & dmgObjectLayer.value) > 0)
        {
            BuffDmgInfo info = new BuffDmgInfo(dmgInfo.dmg * (.5f + lerp * .5f), 1, dmgInfo.applier);
            other.gameObject.SendMessage("ApplyDamage", info, SendMessageOptions.DontRequireReceiver);
        }
    }
}
