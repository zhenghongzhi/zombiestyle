using UnityEngine;
using System.Collections.Generic;

/**************************************
 * Use to Manager Single Bullet Spawn
 * Combined in Gun type script
 * ************************************/
[System.Serializable]
public class BulletSpawner
{
    [HideInInspector]
    public string gunName;
    public GameObject bulletPrefab;
    public Transform spawnTrans;
	public GunFlashLight flashLight;
    public List<GameObject> bulletInstanceList;

    //Called in Start Func to instantiate bullet list
    public virtual void InitSpawner(int size, float bulletSpeed, float bulletLifeTime, string gunName, BulletInfo bulletInfo, bool canKillPlayer)
    {
        this.gunName = gunName;
        if (bulletPrefab == null)
        {
            Debug.LogWarning("No Bullet Prefab Attached" + gunName);
            return;
        }
        if (bulletInstanceList != null)
        {
            foreach (GameObject g in bulletInstanceList)
            {
                GameObject.Destroy(g);
            }
            bulletInstanceList.Clear();
        }
        bulletInstanceList = new List<GameObject>();
        for (int i = 0; i < size; ++i)
        {
            GameObject bulletInstance = (GameObject)GameObject.Instantiate(bulletPrefab);
            bulletInstance.transform.position = spawnTrans.position;
            bulletInstance.transform.rotation = spawnTrans.rotation;
            bulletInstance.name = gunName + "-bullet-" + i;
            Bullet bulletScript = bulletInstance.GetComponent<Bullet>();
            bulletScript.speed = bulletSpeed;
            bulletScript.lifeTime = bulletLifeTime;
            bulletScript.canKillPlayer = canKillPlayer;
            bulletScript.bulletInfo.effect = bulletInfo.effect;
            bulletScript.bulletInfo.dmg = bulletInfo.dmg;
            bulletScript.bulletInfo.shooter = bulletInfo.shooter;
            bulletInstance.SetActive(false);
            bulletInstanceList.Add(bulletInstance);
        }
    }

    //Called When Fire a Bullet
    public virtual void Spawn(float dmg, float randomRange = 0)
    {
        foreach (GameObject bullet in bulletInstanceList)
        {
            if (!bullet.activeSelf)
            {
                bullet.transform.position = spawnTrans.position;
                bullet.transform.rotation = spawnTrans.rotation;
                if (randomRange > 0)
                     bullet.transform.rotation *= Quaternion.AngleAxis(Random.Range(-randomRange, randomRange), Vector3.up);
                bullet.SetActive(true);
                Bullet script = bullet.GetComponent<Bullet>();
                script.enabled = true;
                if(dmg > 0)
                    script.bulletInfo.dmg = dmg;
                if(flashLight)
				    flashLight.Flash();
                return;
            }
        }
    }

    public void ExpendBulletObjAmount(int newSize)
    {
        if (newSize > bulletInstanceList.Count)
        {
            int expand = newSize - bulletInstanceList.Count;
            Bullet sampler = bulletInstanceList[0].GetComponent<Bullet>();
            for (int i = 0; i < expand; ++i)
            {
                GameObject bulletInstance = (GameObject)GameObject.Instantiate(bulletPrefab);
                bulletInstance.transform.position = spawnTrans.position;
                bulletInstance.transform.rotation = spawnTrans.rotation;
                bulletInstance.name = gunName + "-bullet-" + (bulletInstanceList.Count + i);
                Bullet bulletScript = bulletInstance.GetComponent<Bullet>();
                bulletScript.speed = sampler.speed;
                bulletScript.lifeTime = sampler.lifeTime;
                bulletScript.canKillPlayer = sampler.canKillPlayer;
                bulletScript.bulletInfo.effect = sampler.bulletInfo.effect;
                bulletScript.bulletInfo.dmg = sampler.bulletInfo.dmg;
                bulletScript.bulletInfo.shooter = sampler.bulletInfo.shooter;
                bulletInstance.SetActive(false);
                bulletInstanceList.Add(bulletInstance);
            }
        }
    }

    public void Destroy()
    {
        if (bulletInstanceList != null)
        {
            foreach (GameObject bullet in bulletInstanceList)
            {
                GameObject.Destroy(bullet);
            }
        }
    }
}

[System.Serializable]
public class MultiBulletSpwaner : BulletSpawner
{
    public int bulletNumPerRound;
    public float angleRange;
    float angleInterval;

    public override void InitSpawner(int size, float bulletSpeed, float bulletLifeTime, string gunName, BulletInfo bulletInfo, bool canKillPlayer)
    {
        if(bulletNumPerRound < 2)
            bulletNumPerRound = 2;
        angleInterval = angleRange / (bulletNumPerRound - 1);
        base.InitSpawner(size, bulletSpeed, bulletLifeTime, gunName, bulletInfo, canKillPlayer);
    }

    public override void Spawn(float dmg, float randomRange = 0)
    {
        int fireNum = 0;
        float rotateAnlge = -angleRange * 0.5f;

        foreach (GameObject bullet in bulletInstanceList)
        {
            if (!bullet.activeSelf)
            {
                bullet.transform.position = spawnTrans.position;
                Quaternion q = Quaternion.AngleAxis(rotateAnlge, Vector3.up);
                bullet.transform.rotation = spawnTrans.rotation * q;
                bullet.SetActive(true);
                Bullet script = bullet.GetComponent<Bullet>();
                script.enabled = true;
                if (dmg > 0)
                    script.bulletInfo.dmg = dmg;
                flashLight.Flash();
                fireNum++;
                rotateAnlge += angleInterval;
            }
            if (fireNum == bulletNumPerRound)
                return;
        }
    }
}

[System.Serializable]
public class WeaponAttribute
{
    [HideInInspector]
    public float cdTime;
    public float range;
    [HideInInspector]
    public int loaderSize;
    [HideInInspector]
    public float dmgPerBullet;
    public float bulletSpeed = 20;
    [HideInInspector]
    public float critProb;
    [HideInInspector]
    public float critRate;
}

public class WeaponBuffAttribute
{
    public float cdTimePer = 0;
    public float rangePer = 0;
    public float loaderSizePer = 0;
    public float dmgPerBulletPer = 0;
    public float critScalePer = 0;
    public float critProbPer = 0;
}

[RequireComponent(typeof(Animation))]//For Loading Animation Sync
public class Weapon : MonoBehaviour {
    public string gunName;
    public bool canKillPlayer;
    public Transform[] gunTransforms;
    public WeaponAttribute attribute;
    [HideInInspector]
    public WeaponBuffAttribute buffAttribute = new WeaponBuffAttribute();

    protected bool isOpenFire = false;
    protected bool isReloading = false;
    protected int currentLoaderSize;
    protected float timeCounter;

    protected float CritProb
    {
        get { return attribute.critProb; }
    }

    protected float CritRate
    {
        get { return attribute.critRate * (1 + buffAttribute.critScalePer); }
    }

    protected float DmgPerBullet
    {
        get { return attribute.dmgPerBullet * (1 + buffAttribute.dmgPerBulletPer); }
    }

    protected float CDTime
    {
        get { return attribute.cdTime / (1 + buffAttribute.cdTimePer); }
    }

    public bool IsShooting
    {
        get { return isOpenFire; }
        set { isOpenFire = value; }
    }

    public int LoaderSize
    {
        get { return (int)(attribute.loaderSize * (1 + buffAttribute.loaderSizePer)); }
    }

    public int CurrentLoaderSize
    {
        get { return currentLoaderSize; }
    }

    public virtual void ChangeCDTime()
    {
        AnimationClip shoot = GetComponent<PlayerAnimation>().shoot;
        animation[shoot.name].speed = shoot.length / CDTime;
        ExpandSpawner();
    }

    //Called in BattleScene's Awake before Battle Scene Started
    public virtual void InitWeapon(WeaponData weaponData, CharacterStatus charState, int weaponGrade)
    {
        attribute.cdTime = weaponData.atkRate[weaponGrade];
        attribute.dmgPerBullet = weaponData.dmg[weaponGrade] + charState.Atk * weaponData.atkRate[weaponGrade];
        attribute.loaderSize = weaponData.loaderSize[weaponGrade];
        attribute.critProb = charState.CritProb;
        attribute.critRate = charState.CritRate;
        OnReloadComplete();
    }

    //Called by PlayerAnimation After Reloading Animation Finished
    public virtual void OnReloadComplete()
    {
        isReloading = false;
        currentLoaderSize = LoaderSize;
    }

    //Called by PlayerAnimation When Player Start Shooting
    public virtual void OnStartShooting()
    {
        isOpenFire = true;
        foreach (Transform t in gunTransforms)
        {
            if (t.animation)
                t.animation.Play();
        }
    }

    //Called by PlayerAnimation When Player Stop Shooting
    public virtual void OnStopShooting()
    {
        isOpenFire = false;
        foreach (Transform t in gunTransforms)
        {
            if (t.animation)
                t.animation.Stop();
        }
    }

    protected void SpawnBullet(BulletSpawner spawner, float randomRange = 0)
    {
        if (Random.Range(0, 1.0f) > CritProb)
        {
            spawner.Spawn(DmgPerBullet * CritRate, randomRange);
        }
        else
            spawner.Spawn(-1, randomRange);
    }

    protected virtual void ExpandSpawner()
    {

    }
}
