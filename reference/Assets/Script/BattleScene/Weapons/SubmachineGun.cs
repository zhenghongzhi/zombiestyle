using UnityEngine;
using System.Collections;

public class SubmachineGun : Weapon
{
    public BulletSpawner handGun;
    public float randomRange = 5;

    public override void InitWeapon(WeaponData weaponData, CharacterStatus charState, int weaponGrade)
    {
        base.InitWeapon(weaponData, charState, weaponGrade);
        AnimationClip shoot = GetComponent<PlayerAnimation>().shoot;
        animation[shoot.name].speed = shoot.length / CDTime;
        int objAmount = (int)(attribute.range / (attribute.bulletSpeed * CDTime)) + 2;
        BulletInfo info = new BulletInfo();
        info.dmg = DmgPerBullet;
        info.effect = 0;
        info.shooter = gameObject;
        GameObject bulletPrefab = (GameObject)Resources.Load("Weapons/Weapons/Bullet/InstantBullet_" + gunName, typeof(GameObject));
        handGun.bulletPrefab = bulletPrefab;
        handGun.InitSpawner(objAmount, attribute.bulletSpeed, attribute.range / attribute.bulletSpeed, gunName, info, canKillPlayer);
        handGun.flashLight = transform.GetComponentInChildren<GunFlashLight>();
        timeCounter = CDTime;
    }

     //Update is called once per frame
    void Update()
    {
        if (isReloading)
            return;
        if (timeCounter <= CDTime)
            timeCounter += Time.deltaTime;
        if (!isOpenFire)
            return;
      
        if (CurrentLoaderSize == 0)
        {
            isReloading = true;
            gunTransforms[0].GetComponents<AudioSource>()[1].Play();
            GetComponent<PlayerAnimation>().OnStartReload();
            return;
        }

        if (timeCounter > CDTime)
        {
            timeCounter -= CDTime;
            currentLoaderSize--;
            gunTransforms[0].audio.Play();
            SpawnBullet(handGun, randomRange);
        }
    }

    protected override void ExpandSpawner()
    {
        int objAmount = (int)(attribute.range / (attribute.bulletSpeed * CDTime)) + 2;
        handGun.ExpendBulletObjAmount(objAmount);
    }
}
