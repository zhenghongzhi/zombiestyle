using UnityEngine;
using System.Collections;

public class ShotGun : Weapon {
    public MultiBulletSpwaner shotGun;

    public override void InitWeapon(WeaponData weaponData, CharacterStatus charState, int weaponGrade)
    {
        base.InitWeapon(weaponData, charState, weaponGrade);
        AnimationClip shoot = GetComponent<PlayerAnimation>().shoot;
        animation[shoot.name].speed = shoot.length / CDTime;
        int size = ((int)(attribute.range / (attribute.bulletSpeed * CDTime)) + 2) * shotGun.bulletNumPerRound;
        BulletInfo info = new BulletInfo();
        info.dmg = attribute.dmgPerBullet;
        info.effect = 0;
        info.shooter = gameObject;
        GameObject bulletPrefab = (GameObject)Resources.Load("Weapons/Weapons/Bullet/InstantBullet_" + gunName, typeof(GameObject));
        shotGun.bulletPrefab = bulletPrefab;
        shotGun.InitSpawner(size, attribute.bulletSpeed, attribute.range / attribute.bulletSpeed, "shotGun", info, canKillPlayer);
        shotGun.flashLight = transform.GetComponentInChildren<GunFlashLight>();
        timeCounter = CDTime;
    }

    //Override OnStartShooting Method to Spawn Bullet on The Very Beginning of Shoot Animation
    public override void OnStartShooting()
    {
        currentLoaderSize--;
        gunTransforms[0].audio.Play();
        SpawnBullet(shotGun);

        if (CurrentLoaderSize == 0)
        {
            isReloading = true;
            gunTransforms[0].GetComponents<AudioSource>()[1].Play();
            GetComponent<PlayerAnimation>().OnStartReload();
        }
    }

    protected override void ExpandSpawner()
    {
        int size = ((int)(attribute.range / (attribute.bulletSpeed * CDTime)) + 2) * shotGun.bulletNumPerRound;
        shotGun.ExpendBulletObjAmount(size);
    }
}
