using UnityEngine;
using System.Collections;

public class WeaponBonding : MonoBehaviour {
    public Transform gunHandlePoint;

	// Update is called once per frame
	void Start () {
        transform.localRotation = Quaternion.Inverse(gunHandlePoint.localRotation);
        transform.localPosition = -gunHandlePoint.TransformDirection(gunHandlePoint.localPosition);
	}
}
