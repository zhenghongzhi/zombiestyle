using UnityEngine;
using System.Collections;

public class RocketBullet : Bullet {
    public ParticleEmitter smoke;

	// Use this for initialization
	void Start () {
        canPenetrate = false;
	}

    void OnEnable()
    {
        smoke.Emit();
        timeCounter = lifeTime;
    }

    protected override void OnDisable()
    {
        base.OnDisable();
        smoke.emit = false;
        smoke.ClearParticles();
    }

    protected override void Update()
    {
        if (timeCounter <= 0)
        {
            Blast();
        }
        timeCounter -= Time.deltaTime;
        transform.position += transform.forward * Time.deltaTime * speed;
    }

    void Blast()
    {
        enabled = false;
        GameObject blast = (GameObject)Instantiate(sparkelPrefab, transform.position, Quaternion.identity);
        ExplosionDmg blaskScript = blast.AddComponent<ExplosionDmg>();
        blaskScript.dmgInfo = new BuffDmgInfo(bulletInfo.dmg, bulletInfo.effect, bulletInfo.shooter);
        blaskScript.dmgObjectLayer = damageObjectLayer;
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Player")
            return;
        //A rocket bullet always can't penetrate
        int otherLayer = 1 << other.gameObject.layer;
        if ((otherLayer & ignoreLayers.value) == 0)
            Blast();
    }
}
