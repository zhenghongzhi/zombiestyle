using UnityEngine;
using System.Collections;

public class SingleHandGun : Weapon {
    public BulletSpawner handGun;

    public override void InitWeapon(WeaponData weaponData, CharacterStatus charState, int weaponGrade)
    {
        base.InitWeapon(weaponData, charState, weaponGrade);
        AnimationClip shoot = GetComponent<PlayerAnimation>().shoot;
        animation[shoot.name].speed = shoot.length / CDTime;
        int prefabNumber = (int)(attribute.range / (attribute.bulletSpeed * CDTime)) + 2;
        BulletInfo info = new BulletInfo();
        info.dmg = DmgPerBullet;
        info.effect = 0;
        info.shooter = gameObject;
        GameObject bulletPrefab = (GameObject)Resources.Load("Weapons/Weapons/Bullet/InstantBullet_" + gunName, typeof(GameObject));
        handGun.bulletPrefab = bulletPrefab;
        handGun.InitSpawner(prefabNumber, attribute.bulletSpeed, attribute.range / attribute.bulletSpeed, gunName, info, canKillPlayer);
        handGun.flashLight = transform.GetComponentInChildren<GunFlashLight>();
        timeCounter = CDTime;
    }

    //Override OnStartShooting Method to Spawn Bullet on The Very Beginning of Shoot Animation
    public override void OnStartShooting()
    {
        currentLoaderSize--;
        gunTransforms[0].audio.Play();
        SpawnBullet(handGun);

        if (CurrentLoaderSize == 0)
        {
            isReloading = true;
            gunTransforms[0].GetComponents<AudioSource>()[1].Play();
            GetComponent<PlayerAnimation>().OnStartReload();
        }
    }

    protected override void ExpandSpawner()
    {
        int prefabNumber = (int)(attribute.range / (attribute.bulletSpeed * CDTime)) + 2;
        handGun.ExpendBulletObjAmount(prefabNumber);
    }
}
