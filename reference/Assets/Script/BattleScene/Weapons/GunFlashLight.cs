using UnityEngine;
using System.Collections;

public class GunFlashLight : MonoBehaviour {
    public Transform muzzleFlash;
	public float flashTime = 0.05f;
	public float sizeChange = 0.2f;
	float timeCouter = 0;
	Vector3 defaultScale;
	
	// Use this for initialization
	void Start () {
		GetComponent<Light>().enabled = false;
        if (muzzleFlash)
        {
            muzzleFlash.renderer.enabled = false;
			defaultScale = muzzleFlash.localScale;
        }
	}

    void OnEnable()
    {
        GetComponent<Light>().enabled = false;
        if (muzzleFlash)
        {
            muzzleFlash.renderer.enabled = false;
            defaultScale = muzzleFlash.localScale;
        }
    }
	
	public void Flash()
	{
		GetComponent<Light>().enabled = true;
        if (muzzleFlash)
        {
            muzzleFlash.renderer.enabled = true;
            Vector3 eular = muzzleFlash.localEulerAngles;
            eular.z = Random.Range(-180f, 180f);
			Vector3 scale = new Vector3(Random.Range(defaultScale.x-sizeChange, defaultScale.x + sizeChange), 
				defaultScale.y, Random.Range(defaultScale.z - sizeChange, defaultScale.z + sizeChange));
            muzzleFlash.localEulerAngles = eular;
			muzzleFlash.localScale = scale;
        }
		timeCouter = 0;
	}

	void Update()
	{
		if(GetComponent<Light>().enabled)
		{
			timeCouter += Time.deltaTime;
			if(timeCouter > flashTime)
			{
				GetComponent<Light>().enabled = false;
                if(muzzleFlash)
                    muzzleFlash.renderer.enabled = false;
			}
		}
	}
}
