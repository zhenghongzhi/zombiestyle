using UnityEngine;
using System.Collections.Generic;

public class DoubleHandGun : Weapon{
    public BulletSpawner leftGun;
    public BulletSpawner rightGun;
    public bool startFromLeftGun;

    bool isLeftTurn;//Decide which gun to fire
	
	// Use this for initialization
	void Start () {
        isLeftTurn = startFromLeftGun;
	}

    public override void ChangeCDTime()
    {
        AnimationClip shoot = GetComponent<PlayerAnimation>().shoot;
        animation[shoot.name].speed = shoot.length / CDTime * 0.5f;
        ExpandSpawner();
    }

    public override void InitWeapon(WeaponData weaponData, CharacterStatus charState, int weaponGrade)
    {
        base.InitWeapon(weaponData, charState, weaponGrade);
        AnimationClip shoot = GetComponent<PlayerAnimation>().shoot;
        animation[shoot.name].speed = shoot.length / CDTime * 0.5f;
        int size = (int)(attribute.range / (attribute.bulletSpeed * CDTime * 2)) + 2;
        BulletInfo info = new BulletInfo();
        info.dmg = attribute.dmgPerBullet;
        info.effect = 0;
        info.shooter = gameObject;
        GameObject bulletPrefab = (GameObject)Resources.Load("Weapons/Weapons/Bullet/InstantBullet_" + gunName, typeof(GameObject));
        leftGun.bulletPrefab = bulletPrefab;
        rightGun.bulletPrefab = bulletPrefab;
        leftGun.InitSpawner(size, attribute.bulletSpeed, attribute.range / attribute.bulletSpeed, "left", info, canKillPlayer);
        rightGun.InitSpawner(size, attribute.bulletSpeed, attribute.range / attribute.bulletSpeed, "right", info, canKillPlayer);
        leftGun.flashLight = transform.GetComponentsInChildren<GunFlashLight>()[0];
        rightGun.flashLight = transform.GetComponentsInChildren<GunFlashLight>()[1];
        timeCounter = CDTime;
    }

    public override void OnStartShooting()
    {
        isLeftTurn = startFromLeftGun;
        base.OnStartShooting();
    }

    public override void OnReloadComplete()
    {
        isLeftTurn = startFromLeftGun;
        base.OnReloadComplete();
    }

	// Update is called once per frame
    void Update()
    {
        if (isReloading)
            return;
        if(timeCounter < attribute.cdTime)
            timeCounter += Time.deltaTime;
        if (!isOpenFire)
            return;

        if (CurrentLoaderSize == 0)
        {
            isReloading = true;
            gunTransforms[0].GetComponents<AudioSource>()[1].Play();
            GetComponent<PlayerAnimation>().OnStartReload();
            return;
        }

        if (timeCounter >= CDTime)
        {
            timeCounter -= CDTime;
            currentLoaderSize--;
            gunTransforms[0].audio.Play();
            if (isLeftTurn)
                SpawnBullet(leftGun);
            else
                SpawnBullet(rightGun);

            isLeftTurn = !isLeftTurn;
        }
	}

    protected override void ExpandSpawner()
    {
        int size = (int)(attribute.range / (attribute.bulletSpeed * CDTime * 2)) + 2;
        leftGun.ExpendBulletObjAmount(size);
        rightGun.ExpendBulletObjAmount(size);
    }
}
