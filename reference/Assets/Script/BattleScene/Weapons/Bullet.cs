using UnityEngine;
using System.Collections;

[System.Serializable]
public class BulletInfo
{
    public float dmg;
    //-------Extend-------------
    public GameObject shooter;
    public Vector3 direction;
    public int effect;
}

[RequireComponent(typeof(BoxCollider))]
public class Bullet : MonoBehaviour {
    public float speed;
    public float lifeTime;
    public bool canPenetrate;
    public BulletInfo bulletInfo;
    public LayerMask damageObjectLayer;
    public GameObject sparkelPrefab;
    public GameObject bloodSprayPrefab;
    protected float timeCounter;
    public LayerMask ignoreLayers;
    public bool canKillPlayer;
    public bool billbroad = false;

    void Start()
    {
    }

    void OnEnable()
    {
        timeCounter = lifeTime;
        Vector3 localCameraDir = transform.InverseTransformDirection(Camera.mainCamera.transform.forward);
        float angle = Mathf.Rad2Deg * Mathf.Atan(-localCameraDir.x / localCameraDir.y);
        Quaternion q = Quaternion.AngleAxis(angle, Vector3.forward);
        transform.rotation *= q;
        bulletInfo.direction = transform.forward;
    }

    protected virtual void OnDisable()
    {
        gameObject.SetActive(false);
    }
	
	// Update is called once per frame
	protected virtual void Update () {
        if (timeCounter <= 0)
            enabled = false;
        timeCounter -= Time.deltaTime;
        transform.position += transform.forward * Time.deltaTime * speed;
        if (billbroad)
        {
            transform.GetChild(0).LookAt(Camera.mainCamera.transform);
        }
	}

    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject == bulletInfo.shooter)
            return;
        int hitLayerMask = 1 << other.gameObject.layer;
        //Ignore Hit Other Bullet
        if ((hitLayerMask & ignoreLayers.value) > 0)
            return;
        //Disable Bullet On Hit if the Bullet Cannot Penetrate
        if(!canPenetrate)
            enabled = false;

        //Apply Damage if is Enemy
        if ((damageObjectLayer.value & hitLayerMask) > 0 || (canKillPlayer && other.gameObject.layer == LayerMask.NameToLayer("otherPlayer")))
        {
            if (other.gameObject.layer != LayerMask.NameToLayer("damagableItem") && bloodSprayPrefab)
                Instantiate(bloodSprayPrefab, transform.position, Quaternion.LookRotation(-transform.forward));
            else if (sparkelPrefab)
                Instantiate(sparkelPrefab, transform.position, Quaternion.LookRotation(-transform.forward));
            other.gameObject.SendMessage("ApplyDamage", bulletInfo, SendMessageOptions.DontRequireReceiver);
        }
        else//Show sparkle otherwise
        {
            if(sparkelPrefab)
                Instantiate(sparkelPrefab, transform.position, Quaternion.LookRotation(-transform.forward));
            enabled = false;
        }
    }
}
