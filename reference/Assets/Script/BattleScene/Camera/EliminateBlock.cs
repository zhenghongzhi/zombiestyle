using UnityEngine;
using System.Collections;

[RequireComponent(typeof(CameraFollow))]
public class EliminateBlock : MonoBehaviour {

	public LayerMask blockerMask;
	RaycastHit hitInfo;
	Transform lastHit;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		if(Physics.Raycast(transform.position, 
			transform.forward, 
			out hitInfo, 
			GetComponent<CameraFollow>().fDist,
			blockerMask
			)
		)
		{
			if(lastHit == null || hitInfo.transform.name != lastHit.name)
			{
				foreach (AutoTransparent t in hitInfo.transform.GetComponentsInChildren<AutoTransparent>())
				{
                    t.SetMat(true);
				}
				if(lastHit != null)
				{
                    foreach (AutoTransparent t in lastHit.GetComponentsInChildren<AutoTransparent>())
                    {
                        t.SetMat(false);
                    }
				}
				lastHit = hitInfo.transform;
			}
		}
		else if(lastHit != null)
		{
            foreach (AutoTransparent t in lastHit.GetComponentsInChildren<AutoTransparent>())
            {
                t.SetMat(false);
            }
			lastHit = null;
		}
	}
}
