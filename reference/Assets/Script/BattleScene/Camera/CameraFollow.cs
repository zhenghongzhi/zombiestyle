using UnityEngine;
using System.Collections;

/*--------------------------------------------------
*Attached Object : MainCamera
*Resbonsibility : control main camera to follow the 
*				player at fixed angle.
--------------------------------------------------*/
public class CameraFollow : MonoBehaviour {

	public Transform player;
	public float fDist = 20;

    void OnLevelWasLoaded(int level)
    {
    }

    void Awake()
    {
        player = GameObject.FindGameObjectWithTag("Player").transform;
        player.position = transform.TransformPoint(new Vector3(0, 0, 5));
    }

	// Use this for initialization
	void Start () {
		transform.eulerAngles = new Vector3(45, 225, 0);
		if(player != null)
			transform.position = player.position - transform.forward * fDist;
	}
	
	// Update is called once per frame
	void Update () {
		if(player != null)
			transform.position = player.position - transform.forward * fDist;
	}
}
