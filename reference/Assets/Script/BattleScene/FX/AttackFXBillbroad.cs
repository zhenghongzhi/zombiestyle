using UnityEngine;
using System.Collections;

public class AttackFXBillbroad : MonoBehaviour {
    public float showTime = 0.1f;
    public bool billbroad = true;
    float timeCounter = 1;

    void Awake()
    {
        renderer.enabled = false;
    }

	void OnEnable()
    {
        if (billbroad)
        {
            transform.eulerAngles = new Vector3(-45, 45, 0);
            if (transform.parent != null)
            {
                Vector3 objectForwardLocal = transform.InverseTransformDirection(transform.parent.forward + transform.parent.right);
                objectForwardLocal.z = 0;
                transform.rotation *= Quaternion.FromToRotation(Vector3.up, objectForwardLocal);
            }
        }
        timeCounter = 0;
        renderer.enabled = true;
    }

    void OnDisable()
    {
        renderer.enabled = false;
    }

    void Update()
    {
        timeCounter += Time.deltaTime;
        if (timeCounter > showTime)
            enabled = false;
    }
}
