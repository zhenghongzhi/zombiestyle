using UnityEngine;
using System.Collections;

/*--------------------------------------------------
*Attached Object : Any Objects that may block camera view
*Usage : Call SetMat() to switch materials between
		transparent and opacity
*Colabration Class: 
*	MeshRenderer Component
--------------------------------------------------*/
[RequireComponent(typeof(MeshRenderer))]
public class AutoTransparent : MonoBehaviour {
	public Material originalMat;
	public Material transMat;

	public void SetMat(bool transparent)
	{
		if(transparent)
			renderer.sharedMaterial = transMat;
		else
			renderer.sharedMaterial = originalMat;
	}
}
