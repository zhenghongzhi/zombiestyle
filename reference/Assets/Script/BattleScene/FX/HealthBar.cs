using UnityEngine;
using System.Collections;

public class HealthBar : MonoBehaviour {
    public GameObject followObject;
    Material healthMat;

    // Use this for initialization
    void Awake()
    {
        healthMat = renderer.material;
        healthMat.SetFloat("_Health", 1);
    }

    // Update is called once per frame
    void Update()
    {
        if(followObject != null)
            transform.position = followObject.transform.position;
    }

    public void SetHealth(float healthPercentage)
    {
        healthMat.SetFloat("_Health", healthPercentage);
    }
}
