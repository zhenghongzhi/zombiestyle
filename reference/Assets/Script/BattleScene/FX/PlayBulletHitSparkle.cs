using UnityEngine;
using System.Collections;

public class PlayBulletHitSparkle : MonoBehaviour {
    
	// Use this for initialization
    void OnEnable()
    {
        GetComponent<ParticleEmitter>().emit = true;
    }
	
	// Update is called once per frame
	void Update () {
        if (!GetComponent<ParticleEmitter>().emit)
            Destroy(gameObject);
	}
}
