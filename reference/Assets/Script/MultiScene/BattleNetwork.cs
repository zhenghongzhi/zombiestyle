using UnityEngine;
using System.Collections;
using Wit;

public class BattleNetwork : INetworkModule
{
    MutilBattleView m_pView;

    /************************************************************************/
    /* Singleton                                                            */
    /************************************************************************/
    private static BattleNetwork m_pNetManager;
    public static BattleNetwork Instance
    {
        get
        {
            if (m_pNetManager == null)
            {
                m_pNetManager = new BattleNetwork();
            }
            return m_pNetManager;
        }
    }

    private BattleNetwork()
    {
        NetworkManager.Instance.AddModule(this);
    }

    public void Init()
    {
        if (m_pView == null)
        {
            m_pView = GameObject.Find("Network Peer").GetComponent<MutilBattleView>();
        }
    }

    public bool ClientProcessEvent(EventUnit eventUnit)
    {
        if (eventUnit.Type == EventType.SESSION_CLOSE)
        {
            if (m_pView != null)
            {
                return m_pView.OnDisconnectedFromServer();
            }
        }
        else if (eventUnit.Type == EventType.SESSION_PROTOCOL)
        {
            return OnClientProtocol(eventUnit.Sid, eventUnit.Protocol);
        }
        return false;
    }

    public bool ServerProcessEvent(EventUnit eventUnit)
    {
        if (eventUnit.Type == EventType.SESSION_CLOSE)
        {
            if (m_pView != null)
            {
                return m_pView.OnClientDisconnected(eventUnit.Sid);
            }
        }
        else if (eventUnit.Type == EventType.SESSION_PROTOCOL)
        {
            return OnServerProtocol(eventUnit.Sid, eventUnit.Protocol);
        }
        return false;
    }

    private bool OnClientProtocol(uint iSid, WitProtocol pProto)
    {
        ProtocolId iType = (ProtocolId)pProto.Type;
        switch (iType)
        {
            case ProtocolId.START_BATTLE:
                {
                    m_pView.StartGame();
                    return true;
                }
            case ProtocolId.STATE_SYNC:
                {
                    StateSync pCmd = pProto as StateSync;
                    m_pView.OnStateSync(iSid, pCmd.m_sMoveDir, pCmd.m_sFaceDir);
                    return true;
                }
            case ProtocolId.STATE_CORRECT:
                {
                    StateCorrect pCmd = pProto as StateCorrect;
                    m_pView.StateCorrect(pCmd.m_iId, pCmd.m_sPos, pCmd.m_sRotate);
                    return true;
                }
        }
        return false;
    }

    private bool OnServerProtocol(uint iSid, WitProtocol pProto)
    {
        ProtocolId iType = (ProtocolId)pProto.Type;
        switch (iType)
        {
            case ProtocolId.FINISH_LOADING:
                {
                    OnFinishLoading(iSid);
                    return true;
                }
            case ProtocolId.STATE_SYNC:
                {
                    StateSync pCmd = pProto as StateSync;
                    var list = LobbyNetwork.Instance.MyRoom.PlayerMan.CollectId();
                    list.Remove(iSid);
                    NetworkManager.Instance.BroadcastProtocol(pProto, list);
                    //���ش���
                    m_pView.OnStateSync(iSid, pCmd.m_sMoveDir, pCmd.m_sFaceDir);
                    return true;
                }
            case ProtocolId.STATE_CORRECT:
                {
                    StateCorrect pCmd = pProto as StateCorrect;
                    var list = LobbyNetwork.Instance.MyRoom.PlayerMan.CollectId();
                    list.Remove(iSid);
                    NetworkManager.Instance.BroadcastProtocol(pProto, list);
                    m_pView.StateCorrect(iSid, pCmd.m_sPos, pCmd.m_sRotate);
                    return true;
                }
        }
        return false;
    }

    private void OnFinishLoading(uint iSid)
    {
        PlayerManager pPlayerMan = LobbyNetwork.Instance.MyRoom.PlayerMan;
        Player p = pPlayerMan.GetPlayerById(iSid);
        if (p!=null)
        {
            p.m_bPrepared = true;
            if (pPlayerMan.IsAllPrepared())
            {
                NetworkManager.Instance.BroadcastProtocol(new StartBattle(), pPlayerMan.CollectId());
                m_pView.StartGame();
            }
        }
    }

}
