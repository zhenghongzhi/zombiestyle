using UnityEngine;
using System.Collections;
using Wit;
using System.Net;
using System.Collections.Generic;

class UdpProxy : WitSessionProxy
{
    public override bool OnStart()
    {
        WitDebug.Log("Start Udp Session.");
        return true;
    }

    public override bool OnClose()
    {
        WitDebug.Log("Close Udp Session.");
        return true;
    }

    public override bool OnProtocol(WitProtocol pProto, WitIpAddress sAddr)
    {
        if (!LobbyUdpClient.g_LobbyUdpClient.OnProtocol(pProto, sAddr))
        {
            WitDebug.Log("Protocol Process Failed. ProtocolId: " + pProto.Type);
        }
        return true;
    }
}

public class LobbyUdpClient
{
    //public 
    public Dictionary<string, HostInfo> m_sRoomList = new Dictionary<string, HostInfo>();

    //private
    //udp session
    private WitSession m_sSession;

    private static LobbyUdpClient m_LobbyNetwork;
    public static LobbyUdpClient g_LobbyUdpClient
    {
        get { return m_LobbyNetwork; }
    }

    private uint m_iSid;
    public uint Sid
    {
        get { return m_iSid; }
        set { m_iSid = value; }
    }
    private int m_iPort;

    private RoomInfo m_sMyRoom;

    public string LocalIP
    {
        get
        {
            IPHostEntry ipe = Dns.GetHostEntry(Dns.GetHostName());
            string result=string.Empty;
            for (int i = 0; i < ipe.AddressList.Length; ++i )
            {
                result += ipe.AddressList[i].ToString() + "\n";
            }
            return result;
        }
    }

    public bool Avialbe
    {
        get { return m_sSession != null; }
    }

    public bool Init(int iPort = CsConst.UDP_PORT)
    {
        if (m_sSession == null)
        {
            //register debug
            WitDebug.Clear();
            WitDebug.Add(Debug.Log);

            m_LobbyNetwork = this;

            UdpProxy pProxy = new UdpProxy();

            //start session
            m_sSession = new WitSession();
            m_sSession.StartUdpServer(iPort, pProxy);
            m_iPort = iPort;
        }        

        //request room
        BroadCastProtocol(new RequestRoom());

        return true;
    }

    public bool SendProtocol(WitProtocol pProto, WitIpAddress sAddr)
    {
        m_sSession.SendProtocol(pProto, sAddr);
        return true;
    }

    public bool BroadCastProtocol(WitProtocol pProto)
    {
        m_sSession.SendProtocol(pProto, new WitIpAddress(IPAddress.Broadcast, m_iPort));
        return true;
    }

    public bool OnProtocol(WitProtocol pProto, WitIpAddress pAddr)
    {
        Debug.Log("Receive Udp Protocol: " + pProto.Type);
        switch ((ProtocolId)pProto.Type)
        {
            case ProtocolId.ROOM_REQUEST:
                if (m_sMyRoom != null)
                {
                    SendProtocol(new RequestRoomRe(m_sMyRoom), pAddr);
                }
                return true;
            case ProtocolId.ROOM_REQUEST_RE:
                {
                    RequestRoomRe pCmd = pProto as RequestRoomRe;
                    if (!m_sRoomList.ContainsKey(pAddr.ToString()))
                    {
                        m_sRoomList.Add(pAddr.ToString(), new HostInfo(pAddr.ToString(), pCmd.m_sRoom));
                    }
                    return true;
                }
            case ProtocolId.ROOM_CREATE:
                {
                    CreateRoom pCmd = pProto as CreateRoom;
                    if (!m_sRoomList.ContainsKey(pAddr.ToString()))
                    {
                        m_sRoomList.Add(pAddr.ToString(), new HostInfo(pAddr.ToString(), pCmd.m_sRoom));
                    }
                    return true;
                }
            case ProtocolId.ROOM_CLOSE:
                {
                    if (m_sRoomList.ContainsKey(pAddr.ToString()))
                    {
                        m_sRoomList.Remove(pAddr.ToString());
                    }
                }
                return true;
            case ProtocolId.ROOM_SYNC:
                {
                    RoomSync pCmd = pProto as RoomSync;
                    if (m_sRoomList.ContainsKey(pAddr.ToString()))
                    {
                        m_sRoomList[pAddr.ToString()].m_sRoom.m_iPlayer = pCmd.m_iPlayerCount;
                    }
                }
                return true;
        }
        return false;
    }

    public void CreateRoom(RoomInfo sInfo)
    {
        //save room info
        m_sMyRoom = sInfo;
        BroadCastProtocol(new CreateRoom(m_sMyRoom));
    }

    public void CloseRoom()
    {
        if (m_sMyRoom != null)
        {
            m_sMyRoom = null;
            BroadCastProtocol(new CloseRoom());
        }        
    }

    public void Close()
    {
        CloseRoom();
        if (m_sSession != null)
        {
            m_sSession.Close();
            m_sSession = null;
        }
    }
}
