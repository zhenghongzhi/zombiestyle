using System.Collections.Generic;
using UnityEngine;
using Wit;

public class LobbyNetwork:INetworkModule
{
    //GUI member
    public LobbyRoomUI m_pRoomUI;

    //********************network member*******************
    public LobbyUdpClient m_pUdpClient;

    //room object
    private Room m_pRoom;
    public Room MyRoom
    {
        get { return m_pRoom; }
    }
    public Dictionary<string, HostInfo> HostMap
    {
        get { return m_pUdpClient.m_sRoomList;}
    }

    public uint MyPlayerId
    {
        get { return MyRoom.PlayerMan.MyPlayerId; }
    }

    //Singleton
    private static LobbyNetwork m_pNetManager;
    public static LobbyNetwork Instance
    {
        get 
        {
            if (m_pNetManager == null)
            {
                m_pNetManager = new LobbyNetwork();
            }
            return m_pNetManager;
        }
    }

    private LobbyNetwork()
    {
        NetworkManager.Instance.AddModule(this);

        m_pUdpClient = new LobbyUdpClient();
    }

    public void Init()
    {
        if (!m_pUdpClient.Avialbe)
        {
            m_pUdpClient.Init();
        }
        if (m_pRoomUI == null)
        {
            m_pRoomUI = Camera.main.GetComponent<LobbyRoomUI>();
        }
    }

    /// <summary>
    /// 关闭房间，关闭udp
    /// </summary>
    /// <returns></returns>
    public void Close()
    {
        CloseRoom();
        if (m_pUdpClient != null)
        {
            m_pUdpClient.Close();
        }
    }

    public bool CreateRoom(string roomname, string password, int dungeno)
    {
        RoomInfo sInfo = new RoomInfo(roomname, (uint)dungeno, (byte)1, password.Length>0);
        m_pRoom = new Room(sInfo, password);

        NetworkManager.Instance.StartServer();

        //broadcast create room message
        m_pUdpClient.CreateRoom(m_pRoom.Info);

        return true;
    }

    /// <summary>
    /// 关闭房间，udp不关闭，只广播房间关闭消息，同时关闭tcp的server
    /// </summary>
    /// <returns></returns>
    public void CloseRoom()
    {
        NetworkManager.Instance.Close();

        if (m_pUdpClient != null)
        {
            m_pUdpClient.CloseRoom();
        }
    }

    public Player AddHostPlayer(string sName, CharacterStoreData pCharData)
    {
        Player pPlayer = new Player(NetworkManager.Instance.Sid, sName, pCharData, true);
        m_pRoom.PlayerMan.AddPlayer(pPlayer);
        m_pRoom.PlayerMan.SetMyPlayer(pPlayer);

        return pPlayer;
    }

    public bool ClientProcessEvent(EventUnit eventUnit)
    {
        if (eventUnit.Type == EventType.SESSION_CONNECT)
        {
            if (m_pRoomUI != null)
            {
                return m_pRoomUI.OnConnectedToServer();
            }
        }
        else if (eventUnit.Type == EventType.SESSION_CLOSE)
        {
            if (m_pRoomUI != null)
            {
                return m_pRoomUI.OnDisconnectedFromServer();
            }
        }
        else if (eventUnit.Type == EventType.SESSION_PROTOCOL)
        {
            return OnClientProtocol(eventUnit.Sid, eventUnit.Protocol);
        }
        return false;
    }

    public bool ServerProcessEvent(EventUnit eventUnit)
    {
        if (eventUnit.Type == EventType.SESSION_CLOSE)
        {
            if (m_pRoomUI != null)
            {
                return m_pRoomUI.OnClientDisconnected(eventUnit.Sid);
            }
        }
        else if (eventUnit.Type == EventType.SESSION_PROTOCOL)
        {
            return OnServerProtocol(eventUnit.Sid, eventUnit.Protocol);
        }
        return false;
    }

#region Process Server Event
    private bool OnServerProtocol(uint iSid, WitProtocol pProto)
    {
        if (m_pRoom.OnProtocol(iSid, pProto))
        {
            return true;
        }

        return false;
    }
#endregion

#region Process Client Event
    public void Connect(string ip)
    {
        NetworkManager.Instance.StartClient(ip);

        //创建room
        m_pRoom = new Room(HostMap[ip].m_sRoom);
    }

    private bool OnClientProtocol(uint iSid, WitProtocol pProto)
    {
        ProtocolId iType = (ProtocolId)pProto.Type;
        switch (iType)
        {
            case ProtocolId.ENTER_ROOM_RE:
                {
                    EnterRoomRe pCmd = pProto as EnterRoomRe;
                    OnEnterRoomRe(iSid, pCmd);
                    return true;
                }
            case ProtocolId.PLAYER_ENTER_ROOM:
                {
                    PlayerEnterRoom pCmd = pProto as PlayerEnterRoom;
                    OnPlayerEnterRoom(iSid, pCmd);
                    return true;
                }
            case ProtocolId.PLAYER_LEAVEROOM:
                {
                    PlayerLeaveRoom pCmd = pProto as PlayerLeaveRoom;
                    OnPlayerLeaveRoom(iSid, pCmd);
                    return true;
                }
            case ProtocolId.PREPARE_SYNC:
                {
                    PrepareSync pCmd = pProto as PrepareSync;
                    m_pRoomUI.OnPrepareSync(pCmd.m_iId, pCmd.m_iPrepared);
                    return true;
                }
            case ProtocolId.ENTER_GAME_SCENE:
                {
                    m_pRoomUI.EnterArena();
                    return true;
                }
        }

        return false;
    }

    private bool OnEnterRoomRe(uint iSid, EnterRoomRe pCmd)
    {
        //进入房间失败，关闭本session
        if (pCmd.m_iSlot <= 0)
        {
            NetworkManager.Instance.Close();
            return false;
        }
        //否则将本角色加入房间角色列表，view层来做
        m_pRoomUI.OnEnterRoomRe(pCmd.m_iSlot, pCmd.m_iId);

        return true;
    }

    private bool OnPlayerEnterRoom(uint iSid, PlayerEnterRoom pCmd)
    {
        foreach (PlayerInfo p in pCmd.m_vPlayers)
        {
            Player pPlayer = new Player(p.m_iId, p.m_sName, CharacterStoreData.Deserialized(p.m_sCharXml), p.m_bPrepared);
            pPlayer.Slot = p.m_iSlot;
            MyRoom.PlayerMan.SetPlayer(pPlayer);

            //将本角色加入房间角色列表，view层来做
            m_pRoomUI.AddPlayer(pPlayer);
        }        

        return true;
    }

    private void OnPlayerLeaveRoom(uint iSid, PlayerLeaveRoom pCmd)
    {
        int iSlot = MyRoom.PlayerMan.GetPlayerById(pCmd.m_iId).Slot;
        MyRoom.PlayerMan.ClearPlayerById(pCmd.m_iId);
        m_pRoomUI.RemovePlayer(pCmd.m_iId, iSlot);
    }
#endregion
}
