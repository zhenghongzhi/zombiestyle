using UnityEngine;

//Draw a Column of Room Showcase
public class RoomShowcaseView : ITouchSlidePanelView
{
    HostInfo m_sUpRoom;
    HostInfo m_sDownRoom;

    GUISkin m_Skin;

    public float Width
    {
        get { return CsGUI.CX(310); }
    }

    public float Height
    {
        get { return 400; }
    }

    public RoomShowcaseView(HostInfo sUpRoom, HostInfo sDownRoom = null)
    {
        //m_Skin = skin;
        m_sUpRoom = sUpRoom;
        m_sDownRoom = sDownRoom;
    }

    public void OnDrawView(float posX, float posY)
    {
        if (m_sUpRoom != null)
        {
            DrawRoom(m_sUpRoom, posX, posY);
        }
        if (m_sDownRoom != null)
        {
            DrawRoom(m_sDownRoom, posX, posY + 274);
        }
    }

    void DrawRoom(HostInfo sInfo, float posX = 0, float posY = 0)
    {
        if (sInfo == null)
        {
            return;
        }
        //button
        if (GUI.Toggle(CsGUI.CRect(posX, posY, 261, 251), LobbyRoomUI.m_SelectRoom == sInfo.m_sHost, "", "roombtn"))
        {
            LobbyRoomUI.m_SelectRoom = sInfo.m_sHost;
        }
        //level label
        GUI.Label(CsGUI.CRect(182 + posX, 7 + posY, 82, 32), "LV." + 69);
        //scenename label
        GUI.Label(CsGUI.CRect(33 + posX, 85 + posY, 206, 66), "WILDERNESS ROAD");
        //room name label
        GUI.Label(CsGUI.CRect(15 + posX, 190 + posY, 230, 38), sInfo.m_sRoom.m_sRoomName);
        //player count label
        GUI.Label(CsGUI.CRect(218 + posX, 223 + posY, 52, 33), sInfo.m_sRoom.m_iPlayer + "/4");
        //lock texture
        if (sInfo.m_sRoom.m_bPassword)
        {
            GUI.Label(CsGUI.CRect(91 + posX, 36 + posY, 91, 113), "", "lockimg");
        }
    }
}