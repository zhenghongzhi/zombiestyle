using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class PlayerManager
{
    private Player[] m_sPlayers;
    public Player[] Players
    {
        get { return m_sPlayers; }
    }

    private Player m_pMyPlayer;
    public uint MyPlayerId
    {
        get 
        {
            if (m_pMyPlayer != null)
            {
                return m_pMyPlayer.m_iId;
            }
            return 0;
        }
    }

    private int m_iMaxPlayer;
    private int m_iCurPlayer;
    public int CurPlayer
    {
        get { return m_iCurPlayer; }
    }

    public PlayerManager(int iMaxPlayer = 4)
    {
        m_iMaxPlayer = iMaxPlayer;
        m_sPlayers = new Player[iMaxPlayer];
    }

    public bool IsFull
    {
        get { return m_iCurPlayer >= m_iMaxPlayer; }
    }

    public int MaxPlayer
    {
        get { return m_iMaxPlayer; }
    }

    public bool AddPlayer(Player sPlayer)
    {
        if (!IsFull)
        {
            for (int i = 0; i < m_iMaxPlayer; ++i )
            {
                if (m_sPlayers[i]==null)
                {
                    m_sPlayers[i] = sPlayer;
                    m_iCurPlayer++;
                    //位于房间内第几个空位
                    sPlayer.Slot = i+1;
                    return true;
                }
            }            
        }
        return false;
    }

    public bool SetMyPlayer(Player sPlayer)
    {
        SetPlayer(sPlayer);
        m_pMyPlayer = sPlayer;
        return true;
    }

    public bool SetPlayer(Player sPlayer)
    {
        if (sPlayer.Slot <= 0 || sPlayer.Slot > m_iMaxPlayer)
        {
            return false;
        }
        if (m_sPlayers[sPlayer.Slot-1] == null)
        {
            m_iCurPlayer++;
        }
        m_sPlayers[sPlayer.Slot-1] = sPlayer;
        return true;
    }

    public bool ClearPlayerById(uint id)
    {
        for (int i = 0; i < m_iMaxPlayer; ++i )
        {
            if (m_sPlayers[i] != null && m_sPlayers[i].m_iId == id)
            {
                m_sPlayers[i] = null;
                m_iCurPlayer--;
                return true;
            }
        }
        return false;
    }

    /// <summary>
    /// 根据房间内的栏位获取Player数据
    /// </summary>
    /// <param name="iSlot">从1开始到最大人数</param>
    /// <returns></returns>
    public Player GetPlayerBySlot(int iSlot)
    {
        if (iSlot>0 && iSlot <= m_iMaxPlayer)
        {
            return m_sPlayers[iSlot-1];
        }
        return null;
    }

    public Player GetPlayerById(uint id)
    {
        foreach (Player p in m_sPlayers)
        {
            if (p != null && p.m_iId == id)
            {
                return p;
            }
        }
        return null;
    }

    public bool IsAllPrepared()
    {
        foreach (Player p in m_sPlayers)
        {
            if (p != null && p.m_bPrepared == false)
            {
                return false;
            }
        }
        return true;
    }

    public void ResetPrepare()
    {
        foreach (Player p in m_sPlayers)
        {
            if (p != null)
            {
                p.m_bPrepared = false;
            }
        }
    }

    public List<uint> CollectId()
    {
        List<uint> vId = new List<uint>();
        foreach (Player p in m_sPlayers)
        {
            if (p != null)
            {
                vId.Add(p.m_iId);
            }
        }
        return vId;
    }
}
