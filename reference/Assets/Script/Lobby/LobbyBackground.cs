using UnityEngine;
using System.Collections;
using System.Collections.Generic;

/// <summary>
/// Add to An Empty GameObject to Draw 2D Background in 3D Scene.
/// The GameObject Must in Camera's Frustum, Otherwise it'll be Cut off,
/// And the Background won't Show Up
/// </summary>
[RequireComponent(typeof(MeshRenderer))]
[RequireComponent(typeof(MeshFilter))]
public class LobbyBackground : MonoBehaviour
{
    public Texture2D m_texBg;//background Texture
    public Shader m_sShaderBg;//draw background shader "Custom/ScreenAliasBackground"
    public Texture2D m_texPlayerBg;
    public Shader m_sShaderAlpha;//draw background shader "Custom/ScreenAliasBackground"

    public List<Rect> m_vPosition = new List<Rect>();

    List<GameObject> m_vObj = new List<GameObject>();

    void Awake()
    {
        //Create A Quad Mesh
        Mesh mBack = new Mesh();

        Vector3[] verts = new Vector3[4];
        verts[0] = new Vector3(-1, -1, 0);
        verts[1] = new Vector3(1, -1, 0);
        verts[2] = new Vector3(1, 1, 0);
        verts[3] = new Vector3(-1, 1, 0);
        mBack.vertices = verts;

        int[] indexs = { 0, 2, 1, 0, 3, 2 };
        mBack.triangles = indexs;

        Vector2[] uvs = { new Vector2(0, 0), new Vector2(1, 0), new Vector2(1, 1), new Vector2(0, 1) };
        mBack.uv = uvs;

        GetComponent<MeshFilter>().mesh = mBack;

        //Create Material
        Material mat = new Material(m_sShaderBg);
        mat.mainTexture = m_texBg;
        renderer.material = mat;
    }

    void CreatePlayerBg()
    {
        Vector3[] verts;
        int[] indexs = { 0, 2, 1, 0, 3, 2 };
        Vector2[] uvs = { new Vector2(0, 0), new Vector2(1, 0), new Vector2(1, 1), new Vector2(0, 1) };
        for (int i = 0; i < m_vPosition.Count; ++i)
        {
            GameObject t = new GameObject();
            t.AddComponent<MeshRenderer>();
            t.AddComponent<MeshFilter>();
            float xMin = (-1024 / 2.0f + m_vPosition[i].xMin) / (1024 / 2.0f);
            float yMax = (768 / 2.0f - m_vPosition[i].yMin) / (768 / 2.0f);
            float xMax = (-1024 / 2.0f + m_vPosition[i].xMax) / (1024 / 2.0f);
            float yMin = (768 / 2.0f - m_vPosition[i].yMax) / (768 / 2.0f);
            verts = new Vector3[4];
            verts[0] = new Vector3(xMin, yMin, 0);
            verts[1] = new Vector3(xMax, yMin, 0);
            verts[2] = new Vector3(xMax, yMax, 0);
            verts[3] = new Vector3(xMin, yMax, 0);
            Mesh m = new Mesh();
            m.vertices = verts;
            m.triangles = indexs;
            m.uv = uvs;
            t.GetComponent<MeshFilter>().mesh = m;
            Material mt = new Material(m_sShaderAlpha);
            mt.SetFloat("_Depth", 0.99f);
            mt.mainTexture = m_texPlayerBg;
            t.renderer.material = mt;
            m_vObj.Add(t);
        }
    }

    void DestroyBg()
    {
        foreach (GameObject m in m_vObj)
        {
            Destroy(m);
        }
    }
}
