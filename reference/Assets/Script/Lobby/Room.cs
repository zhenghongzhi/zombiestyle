using UnityEngine;
using System.Collections;
using Wit;
using System.Collections.Generic;

public class Room
{
    private RoomInfo m_sInfo;
    public RoomInfo Info
    {
        get { return m_sInfo; }
    }

    private string m_sPassword;
    private PlayerManager m_pPlayerMan;
    public PlayerManager PlayerMan
    {
        get { return m_pPlayerMan; }
    }

    public Room(RoomInfo sInfo, string sPwd = "")
    {
        m_sInfo = sInfo;
        m_sPassword = sPwd;
        m_pPlayerMan = new PlayerManager();
    }

    public bool KickPlayer(uint iSid)
    {
        WitSessionManager.Instance.CloseSession(iSid);
        return true;
    }

    public bool OnProtocol(uint iSid, WitProtocol pProto)
    {
        ProtocolId iType = (ProtocolId)pProto.Type;
        switch (iType)
        {
            case ProtocolId.REQ_ENTER_ROOM:
                {
                    ReqEnterRoom pCmd = pProto as ReqEnterRoom;
                    OnReqEnterRoom(iSid, pCmd);
                    return true;
                }
            case ProtocolId.PREPARE_SYNC:
                {
                    PrepareSync pCmd = pProto as PrepareSync;
                    OnPrepareSync(iSid, pCmd);
                    return true;
                }
                
        }
        return false;
    }

    private bool OnReqEnterRoom(uint iSid, ReqEnterRoom pCmd)
    {
        //判断密码是否正确
        if (pCmd.m_sPwd != m_sPassword)
        {
            NetworkManager.Instance.SendProtocol(new EnterRoomRe(), iSid);
            return false;
        }

        //判断是否能进入房间
        if (m_pPlayerMan.IsFull)
        {
            NetworkManager.Instance.SendProtocol(new EnterRoomRe(), iSid);
            return false;
        }

        Player pPlayer = new Player(iSid, pCmd.m_sPlayer.m_sName, CharacterStoreData.Deserialized(pCmd.m_sPlayer.m_sCharXml));
        m_pPlayerMan.AddPlayer(pPlayer);

        //进入成功后回复进入成功消息
        NetworkManager.Instance.SendProtocol(new EnterRoomRe(iSid, (sbyte)pPlayer.Slot), iSid);

        //同步房间内的玩家给刚进入玩家
        var vPlayers = new List<PlayerInfo>();
        foreach (Player p in m_pPlayerMan.Players)
        {
            if (p!=null && p.m_iId != iSid)
            {
                vPlayers.Add(p.GetInfo());
            }
        }
        NetworkManager.Instance.SendProtocol(new PlayerEnterRoom(vPlayers), iSid);

        //给所有客户端同步刚进入玩家
        vPlayers.Clear();
        vPlayers.Add(pPlayer.GetInfo());
        for (int i = 1; i < m_pPlayerMan.Players.Length; ++i)
        {
            if (m_pPlayerMan.Players[i] != null && i!=pPlayer.Slot-1)
            {
                NetworkManager.Instance.SendProtocol(new PlayerEnterRoom(vPlayers), m_pPlayerMan.Players[i].m_iId);
            }
        }

        //Udp通知房间数量同步
        LobbyNetwork.Instance.m_pUdpClient.BroadCastProtocol(new RoomSync((byte)m_pPlayerMan.CurPlayer));

        //通知view层显示
        LobbyNetwork.Instance.m_pRoomUI.SendMessage("AddPlayer", pPlayer);

        return true;
    }

    private void OnPrepareSync(uint iSid, PrepareSync pCmd)
    {
        Player player = m_pPlayerMan.GetPlayerById(iSid);
        if (player != null)
        {
            player.m_bPrepared = pCmd.m_iPrepared;
        }

        //通知其他房间内玩家
        pCmd.m_iId = iSid;
        foreach (Player p in PlayerMan.Players)
        {
            if (p != null)
            {
                if (p.m_iId == NetworkManager.Instance.Sid)
                {
                    continue;
                }
                NetworkManager.Instance.SendProtocol(pCmd, p.m_iId);
            }            
        }

        //通知view层
        LobbyNetwork.Instance.m_pRoomUI.OnPrepareSync(iSid, pCmd.m_iPrepared);
    }

}
