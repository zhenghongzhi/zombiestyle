using UnityEngine;
using System.Collections;
using System.Net;

public class Player
{
    //服务端分配的Id，即服务端的Sid
    public uint m_iId;

    public string m_sName;
    public CharacterStoreData m_pCharData;
    public bool m_bPrepared = false;
    public GameObject m_pModel;

    private int m_iSlot;
    public int Slot
    {
        get { return m_iSlot; }
        set { m_iSlot = value; }
    }

    public Player(uint id, string name, CharacterStoreData data, bool prepared = false)
    {
        m_iId = id;
        m_sName = name;
        m_pCharData = data;
        m_bPrepared = prepared;
    }

    public PlayerInfo GetInfo()
    {
        return new PlayerInfo(m_iId, m_sName, m_pCharData.Serialized(), (byte)m_iSlot, m_bPrepared);
    }
}
