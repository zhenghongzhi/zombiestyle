using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class LobbyRoomUI : MonoBehaviour 
{
    public GUISkin m_gSkin;

    public Texture m_texMainBg;
    //background with 2 button
    public Texture m_texBottomBg2;
    //background with 3 button
    public Texture m_texBottomBg3;
    public Texture m_texButtonBg;
    public Texture m_texCreateBg;
    public Texture m_texSearchBg;

    private GUIStyle m_styBtn;
    private GUIStyle m_styLabel;
    private GUIStyle m_styTexField;

    private HorizontalTouchSlidePanel m_RoomShowcasePanel;
    private List<ITouchSlidePanelView> m_RoomViews;

    //strings of texts
    string m_sQuick;
    string m_sJoin;
    string m_sCreate;
    string m_sEnter;
    string m_sCancel;
    string m_sRoomName;
    string m_sKick;
    string m_sQuit;
    string m_sStart;
    string m_sPrepare;
    string m_sUnPrepare;
    string m_sPassword;

    State m_eState = State.LOBBY;

    //the name when search rooms
    private string m_sFindName = "";
    //the password when enter the room that has password
    private string m_sInputPwd = "";
    //the name of room to create
    private string m_sCreateName = "";
    //the password of room to create
    private string m_sCreatePwd = "";

    public List<Transform> m_PlayerPositionList;

    enum State
    {
        LOBBY,
        CREATEROOM,
        SEARCH,
        HOSTINROOM,
        PLAYERINROOM,
        PASSWORD,
    }
    
    //********************network member*******************
    public LobbyNetwork m_pNetwork;
    //last update room list on GUI
    float m_fLastTime = -5;
    //update interval
    float m_fInterval = 5;
    Dictionary<uint, GameObject> m_mPlayerModel;

    GlobalPlayerInfo localPlayerInfo;
    string m_sMyPlayerName = "hzbc";

    public GameObject m_MyPlayer;
    public GameObject guiNamePrefab;

    public static string m_SelectRoom = "";

    private bool m_bPrepare = false;

    //**************************debug*************************
    List<string> debugInfos = new List<string>();
    private GameObject m_SelectChar;
    void Awake()
    {
        Application.runInBackground = true;
        if (m_pNetwork == null)
        {
            m_pNetwork = LobbyNetwork.Instance;
        }
        m_pNetwork.Init();

        localPlayerInfo = GameObject.Find("GlobalData").GetComponent<GlobalPlayerInfo>();
        m_MyPlayer = GameObject.FindWithTag("Player");
        m_MyPlayer.transform.position = GameObject.Find("MiddlePosition").transform.position;
        m_MyPlayer.transform.LookAt(Camera.mainCamera.transform);
        m_MyPlayer.transform.eulerAngles = new Vector3(0, m_MyPlayer.transform.eulerAngles.y, 0);
    }

    // Use this for initialization
    void Start()
    {
        m_styBtn = new GUIStyle(m_gSkin.GetStyle("btn"));
        m_styBtn.fontSize = (int)CsGUI.CY(m_styBtn.fontSize);
        m_styLabel = m_gSkin.GetStyle("label");
        m_styLabel.fontSize = (int)CsGUI.CY(m_styLabel.fontSize);
        m_styTexField = m_gSkin.GetStyle("textfield");
        m_styTexField.fontSize = (int)CsGUI.CY(m_styTexField.fontSize);

        TextScript sTexts = StaticGameScripts.GetSceneScript("Lobby");
        m_sQuick = sTexts.GetScript("QuickGame");
        m_sJoin = sTexts.GetScript("JoinGame");
        m_sCreate = sTexts.GetScript("CreateGame");
        m_sEnter = sTexts.GetScript("EnterRoom");
        m_sCancel = sTexts.GetScript("Cancel");
        m_sRoomName = sTexts.GetScript("RoomName");
        m_sKick = sTexts.GetScript("Kick");
        m_sQuit = sTexts.GetScript("Quit");
        m_sStart = sTexts.GetScript("Start");
        m_sPrepare = sTexts.GetScript("Prepare");
        m_sUnPrepare = sTexts.GetScript("UnPrepare");
        m_sPassword = sTexts.GetScript("Password");

        m_RoomViews = new List<ITouchSlidePanelView>();

        List<string> sKeys = new List<string>(m_pNetwork.HostMap.Keys);
        for (int i = 0; i < sKeys.Count; i += 2)
        {
            RoomShowcaseView view = null;
            if (i + 1 < m_pNetwork.HostMap.Count)
            {
                view = new RoomShowcaseView(m_pNetwork.HostMap[sKeys[i]], m_pNetwork.HostMap[sKeys[i + 1]]);
            }
            else
            {
                view = new RoomShowcaseView(m_pNetwork.HostMap[sKeys[i]]);
            }
            m_RoomViews.Add(view);
        }

        m_RoomShowcasePanel = new HorizontalTouchSlidePanel(CsGUI.CRect(70, 70, 885, 527), (int)ShopCategory.Weapon, m_gSkin.GetStyle("roomwnd"), m_RoomViews, 0, 300);

    }

    void Update()
    {
        //间隔刷新房间列表
        if (Time.timeSinceLevelLoad - m_fLastTime > m_fInterval)
        {
            UpdateRoomList();
        }

        //射线选取角色
        if (NetworkManager.Instance.IsServer)
        {
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            RaycastHit hit;
            if (Input.GetMouseButtonUp(0))
            {
                if (Physics.Raycast(ray, out hit))
                {
                    m_SelectChar = hit.collider.gameObject;
                }
            }  
        }              
    }

    void FixedUpdate()
    {
        NetworkManager.Instance.ProcessEvent();
    }

    void OnApplicationQuit()
    {
        Debug.Log("Quiting...");
        if (m_pNetwork != null)
        {
            m_pNetwork.Close();
        }
    }

    //Message Function Called by TopMenuGUI
    void Back()
    {
        switch (m_eState)
        {
            case State.LOBBY:
                m_pNetwork.Close();
                Destroy(gameObject);
                Application.LoadLevel("MainMenu");
        	    break;
            case State.HOSTINROOM:
                GameObject.Find("BackGround").SendMessage("DestroyBg");
                m_eState = State.LOBBY;
                m_pNetwork.CloseRoom();
                Resources.UnloadUnusedAssets();
                OnEnterLoby();
                break;
            case State.PLAYERINROOM:
                GameObject.Find("BackGround").SendMessage("DestroyBg");
                m_eState = State.LOBBY;
                LobbyNetwork.Instance.CloseRoom();
                Resources.UnloadUnusedAssets();
                OnEnterLoby();
                break;
            case State.CREATEROOM:
                m_sCreateName = "";
                m_sCreatePwd = "";
                m_eState = State.LOBBY;
                break;
            case State.SEARCH:
                m_sFindName = "";
                m_eState = State.LOBBY;
                break;
            case State.PASSWORD:
                m_sInputPwd = "";
                m_eState = State.LOBBY;
                break;
        }
    }

    //Set Position And Light in Lobby Scene
    void OnEnterLoby()
    {
        foreach (Transform t in m_PlayerPositionList)
        {
            t.FindChild("Spotlight").gameObject.SetActive(false);
        }
        Transform trans = GameObject.Find("MiddlePosition").transform;
        m_MyPlayer.transform.position = trans.position;
        SetLookCamera(m_MyPlayer);
        trans.FindChild("Spotlight").gameObject.SetActive(true);
    }

    void UpdateRoomList()
    {
        m_RoomViews.Clear();
        List<string> sKeys = new List<string>(m_pNetwork.HostMap.Keys);
        for (int i = 0; i < sKeys.Count; i += 2)
        {
            RoomShowcaseView view = null;
            if (i + 1 < m_pNetwork.HostMap.Count)
            {
                view = new RoomShowcaseView(m_pNetwork.HostMap[sKeys[i]], m_pNetwork.HostMap[sKeys[i + 1]]);
            }
            else
            {
                view = new RoomShowcaseView(m_pNetwork.HostMap[sKeys[i]]);
            }
            m_RoomViews.Add(view);
        }
        m_RoomShowcasePanel.Views = m_RoomViews;
    }

    public bool OnConnectedToServer()
    {
        if (Application.loadedLevelName == CsConst.LOBBY_SCENE)
        {
            m_mPlayerModel = new Dictionary<uint, GameObject>();
            m_eState = State.PLAYERINROOM;
            GameObject.Find("BackGround").SendMessage("CreatePlayerBg");
            PlayerInfo sInfo = new PlayerInfo(0, m_sMyPlayerName, localPlayerInfo.SelectedCharData.Serialized());
            NetworkManager.Instance.SendProtocol(new ReqEnterRoom(sInfo, m_sInputPwd));

            return true;
        }
        return false;
    }

    public bool OnDisconnectedFromServer()
    {
        if (Application.loadedLevelName == CsConst.LOBBY_SCENE)
        {
            m_eState = State.LOBBY;
            foreach (GameObject g in m_mPlayerModel.Values)
            {
                if (g != m_MyPlayer)
                    Destroy(g);
            }
            m_mPlayerModel.Clear();
            GameObject.Find("BackGround").SendMessage("DestroyBg");
            OnEnterLoby();

            return true;
        }
        return false;
    }

    /// <summary>
    /// 玩家掉线，服务端处理
    /// 1、删除本地玩家
    /// 2、通知房间内的其他玩家玩家离开
    /// </summary>
    /// <returns></returns>
    public bool OnClientDisconnected(uint id)
    {
        if (Application.loadedLevelName != CsConst.LOBBY_SCENE)
        {
            return false;
        }

        if (m_mPlayerModel.ContainsKey(id))
        {
            if (m_mPlayerModel[id] != m_MyPlayer)
            {
                Destroy(m_mPlayerModel[id]);
                m_mPlayerModel.Remove(id);
            }
            m_pNetwork.MyRoom.PlayerMan.ClearPlayerById(id);

            m_pNetwork.m_pUdpClient.BroadCastProtocol(new RoomSync((byte)m_pNetwork.MyRoom.PlayerMan.CurPlayer));

            PlayerLeaveRoom pCmd = new PlayerLeaveRoom(id);
            foreach (Player p in m_pNetwork.MyRoom.PlayerMan.Players)
            {
                if (p != null && p.m_iId != NetworkManager.Instance.Sid)
                {
                    NetworkManager.Instance.SendProtocol(pCmd, p.m_iId);
                }
            }
        }

        return true;
    }

    #region UI
    void OnGUI()
    {
        GUI.skin = m_gSkin;
        switch (m_eState)
        {
            case State.LOBBY:
                DrawLobby();
                break;
            case State.CREATEROOM:
                GUI.enabled = false;
                DrawLobby();
                GUI.enabled = true;
                DrawCreateRoom();
                break;
            case State.SEARCH:
                GUI.enabled = false;
                DrawLobby();
                GUI.enabled = true;
                DrawSearch();
                break;
            case State.HOSTINROOM:
                DrawHostRoom();
                break;
            case State.PLAYERINROOM:
                DrawPlayerRoom();
                break;
            case State.PASSWORD:
                DrawPassword();
                break;
        }

        //debug info
        int line = 0;
        foreach (string debug in debugInfos)
        {
            GUI.Label(CsGUI.CRect(800, 0 + line * 50, 200, 40), debug);
            ++line;
        }
    }

    void DrawLobby()
    {
        GUI.depth = -1;
        //bottom bg
        GUI.DrawTexture(CsGUI.CRect(0, 659, 1024, 109), m_texBottomBg3);

        //search button
        if (GUI.Button(CsGUI.CRect(933, 680, 80, 80), "", "search"))
        {
            m_eState = State.SEARCH;
        }

        //bottom button 1 Quick Game
        HostInfo quick = null;
        foreach (HostInfo r in m_pNetwork.HostMap.Values)
        {
            if (!r.m_sRoom.m_bPassword)
            {
                quick = r;
                break;
            }
        }
        if (quick == null)
        {
            GUI.enabled = false;
        }
        GUI.DrawTexture(CsGUI.CRect(153, 658, 202, 84), m_texButtonBg);
        if (GUI.Button(CsGUI.CRect(158, 662, 193, 76), m_sQuick, m_styBtn))
        {
            m_pNetwork.Connect(quick.m_sHost);
        }
        GUI.enabled = true;

        //bottom button 2
        if (m_SelectRoom.Length == 0)
        {
            GUI.enabled = false;
        }
        GUI.DrawTexture(CsGUI.CRect(413, 658, 202, 84), m_texButtonBg);
        if (GUI.Button(CsGUI.CRect(418, 662, 193, 76), m_sJoin, m_styBtn))
        {
            if (m_pNetwork.HostMap[m_SelectRoom].m_sRoom.m_bPassword)
            {
                m_eState = State.PASSWORD;
            }
            else
            {
                Debug.Log(m_SelectRoom);
                m_pNetwork.Connect(m_SelectRoom);
                m_SelectRoom = "";
            }
        }
        GUI.enabled = true;

        //bottom button 3
        GUI.DrawTexture(CsGUI.CRect(673, 658, 202, 84), m_texButtonBg);
        if (GUI.Button(CsGUI.CRect(678, 662, 193, 76), m_sCreate, m_styBtn))
        {
            m_eState = State.CREATEROOM;
        }

        GUI.Window(0, CsGUI.CRect(70, 86, 888, 528), DrawShowCase, "", "roomwnd");
        //DrawPageIcon(roomShowcasePanel);
        GUI.depth = 0;
    }

    void DrawCreateRoom()
    {
        GUI.DrawTexture(CsGUI.CRect(55,102,935,611), m_texCreateBg);

        //label name
        GUI.Label(CsGUI.CRect(135, 495, 143, 52), m_sRoomName, m_styLabel);
        m_sCreateName = GUI.TextField(CsGUI.CRect(278, 495, 260, 47), m_sCreateName, 13, m_styTexField);

        //label password
        GUI.Label(CsGUI.CRect(530, 495, 143, 52), m_sPassword, m_styLabel);
        m_sCreatePwd = GUI.TextField(CsGUI.CRect(613, 495, 260, 47), m_sCreatePwd, 13, m_styTexField);

        //bottom button --create
        GUI.DrawTexture(CsGUI.CRect(242, 594, 202, 84), m_texButtonBg);
        if (m_sCreateName.Length == 0)
        {
            GUI.enabled = false;
        }
        if (GUI.Button(CsGUI.CRect(247, 598, 193, 76), m_sEnter, m_styBtn))
        {
            //TODO 副本id
            if (m_pNetwork.CreateRoom(m_sCreateName, m_sCreatePwd, 1))
            {
                //change state
                m_eState = State.HOSTINROOM;
                GameObject.Find("BackGround").SendMessage("CreatePlayerBg");
                m_mPlayerModel = new Dictionary<uint, GameObject>();

                Player pPlayer = m_pNetwork.AddHostPlayer(m_sMyPlayerName, localPlayerInfo.SelectedCharData);
                if (pPlayer != null)
                {
                    AddPlayer(pPlayer);
                }
            }
        }
        GUI.enabled = true;

        //bottom button --cancel 
        GUI.DrawTexture(CsGUI.CRect(578, 594, 202, 84), m_texButtonBg);
        if (GUI.Button(CsGUI.CRect(583, 598, 193, 76), m_sCancel, m_styBtn))
        {
            Back();
        }
    }

    public void AddPlayer(Player pPlayer)
    {
        CharacterStoreData character = pPlayer.m_pCharData;
        //Remote Player
        if (pPlayer.m_iId != LobbyNetwork.Instance.MyPlayerId)
        {
            WeaponData weapon = StaticGameData.GetWeaponData(character.outfit.currentGunID);
            string objectName = character.charName + "-" + weapon.weaponType;
            GameObject remotePlayer = CharacterModelManager.CreateNewCharacter(objectName, weapon);
            remotePlayer.layer = LayerMask.NameToLayer("otherPlayer");
            remotePlayer.animation.Play(remotePlayer.GetComponent<PlayerAnimation>().idle.name);
            GameObject guiName = GameObject.Instantiate(guiNamePrefab) as GameObject;
            guiName.name = "GUI Name";
            guiName.transform.parent = remotePlayer.transform;
            guiName.GetComponent<GUIText>().text = pPlayer.m_sName;
            DontDestroyOnLoad(remotePlayer);
            pPlayer.m_pModel = remotePlayer;
            m_mPlayerModel.Add(pPlayer.m_iId, remotePlayer);
        }
        else
        {
            m_MyPlayer.layer = LayerMask.NameToLayer("otherPlayer");
            pPlayer.m_pModel = m_MyPlayer;
            m_mPlayerModel.Add(pPlayer.m_iId, m_MyPlayer);
        }
        SetPlayersTransforms();
    }

    //Set Positions And Lights in Room Scene
    void SetPlayersTransforms()
    {
        for (int i = 0; i < m_PlayerPositionList.Count; ++i)
        {
            m_PlayerPositionList[i].FindChild("Spotlight").gameObject.SetActive(false);
        }

        foreach (Player p in m_pNetwork.MyRoom.PlayerMan.Players)
        {
            if (p!=null)
            {
                p.m_pModel.transform.position = m_PlayerPositionList[p.Slot-1].position;
                SetLookCamera(p.m_pModel);
                m_PlayerPositionList[p.Slot - 1].FindChild("Spotlight").gameObject.SetActive(p.m_bPrepared);
            }
        }
    }

    //Set Player Look at Camera
    void SetLookCamera(GameObject g)
    {
        g.transform.LookAt(Camera.mainCamera.transform);
        g.transform.eulerAngles = new Vector3(0, g.transform.eulerAngles.y, 0);
    }

    void DrawSearch()
    {
        GUI.DrawTexture(CsGUI.CRect(238, 223, 546, 296), m_texSearchBg);

        //label
        GUI.Label(CsGUI.CRect(285, 302, 143, 52), m_sRoomName, m_styLabel);
        m_sFindName = GUI.TextField(CsGUI.CRect(455, 299, 260, 47), m_sFindName, 13,m_styTexField);

        //bottom button 1
        GUI.DrawTexture(CsGUI.CRect(287, 398, 202, 84), m_texButtonBg);
        if (GUI.Button(CsGUI.CRect(292, 402, 193, 76), m_sEnter, m_styBtn))
        {
        }

        //bottom button 2
        GUI.DrawTexture(CsGUI.CRect(526, 398, 202, 84), m_texButtonBg);
        if (GUI.Button(CsGUI.CRect(531, 402, 193, 76), m_sCancel, m_styBtn))
        {
            Back();
        }
    }

    void DrawHostRoom()
    {
        //bottom bg
        GUI.DrawTexture(CsGUI.CRect(0, 659, 1024, 109), m_texBottomBg3);

        //bottom button 1 Kick
        if (m_SelectChar == null)
        {
            GUI.enabled = false;
        }
        GUI.DrawTexture(CsGUI.CRect(153, 658, 202, 84), m_texButtonBg);
        if (GUI.Button(CsGUI.CRect(158, 662, 193, 76), m_sKick, m_styBtn))
        {
            foreach (Player p in m_pNetwork.MyRoom.PlayerMan.Players)
            {
                if (p != null && p.m_pModel == m_SelectChar)
                {
                    m_pNetwork.MyRoom.KickPlayer(p.m_iId);
                }
            }
        }
        GUI.enabled = true;

        //bottom button 2 Exit
        GUI.DrawTexture(CsGUI.CRect(413, 658, 202, 84), m_texButtonBg);
        if (GUI.Button(CsGUI.CRect(418, 662, 193, 76), m_sQuit, m_styBtn))
        {
            Back();
        }
        //bottom button 3 Start
        if (!LobbyNetwork.Instance.MyRoom.PlayerMan.IsAllPrepared())
        {
            GUI.enabled = false;
        }
        GUI.DrawTexture(CsGUI.CRect(673, 658, 202, 84), m_texButtonBg);
        if (GUI.Button(CsGUI.CRect(678, 662, 193, 76), m_sStart, m_styBtn))
        {
            NetworkManager.Instance.BroadcastProtocol(new EnterGameScene(), LobbyNetwork.Instance.MyRoom.PlayerMan.CollectId());
            EnterArena();
        }
        GUI.enabled = true;

        //search pic
        GUI.DrawTexture(CsGUI.CRect(933, 680, 80, 80), m_gSkin.GetStyle("search").normal.background);
    }

    void DrawPlayerRoom()
    {
        //bottom bg
        GUI.DrawTexture(CsGUI.CRect(0, 659, 1024, 109), m_texBottomBg2);

        //bottom button 1
        GUI.DrawTexture(CsGUI.CRect(153, 658, 202, 84), m_texButtonBg);
        if (GUI.Button(CsGUI.CRect(158, 662, 193, 76), m_sQuit, m_styBtn))
        {
            Back();
        }

        //bottom button 2
        GUI.DrawTexture(CsGUI.CRect(667, 658, 202, 84), m_texButtonBg);
        if (GUI.Button(CsGUI.CRect(672, 662, 193, 76), m_bPrepare?m_sUnPrepare:m_sPrepare, m_styBtn))
        {
            m_bPrepare = !m_bPrepare;
            NetworkManager.Instance.SendProtocol(new PrepareSync(0, m_bPrepare));
        }

        //search pic
        GUI.DrawTexture(CsGUI.CRect(933, 680, 80, 80), m_gSkin.GetStyle("search").normal.background);
    }

    void DrawPassword()
    {
        GUI.DrawTexture(CsGUI.CRect(238, 223, 546, 296), m_texSearchBg);

        //label
        GUI.Label(CsGUI.CRect(285, 302, 143, 52), m_sPassword, m_styLabel);
        m_sInputPwd = GUI.TextField(CsGUI.CRect(455, 299, 260, 47), m_sInputPwd, 13, m_styTexField);

        //bottom button 1
        GUI.enabled = m_sInputPwd.Length > 0;
        GUI.DrawTexture(CsGUI.CRect(287, 398, 202, 84), m_texButtonBg);
        if (GUI.Button(CsGUI.CRect(292, 402, 193, 76), m_sEnter, m_styBtn))
        {
            m_pNetwork.Connect(m_SelectRoom);
            m_SelectRoom = "";
        }
        GUI.enabled = true;

        //bottom button 2
        GUI.DrawTexture(CsGUI.CRect(526, 398, 202, 84), m_texButtonBg);
        if (GUI.Button(CsGUI.CRect(531, 402, 193, 76), m_sCancel, m_styBtn))
        {
            Back();
        }
    }

    //Showcase Window
    void DrawShowCase(int i)
    {
        m_RoomShowcasePanel.Update();
        m_RoomShowcasePanel.OnDraw();
    }

    void DrawPageIcon(HorizontalTouchSlidePanel panel)
    {
        float startX = 670 - 55 * panel.TotalPage * 0.5f;
        for (int i = 0; i < panel.TotalPage; ++i)
        {
            GUI.DrawTexture(CsGUI.CRect(startX + i * 55, 726, 22, 22), m_texMainBg);
        }
    }

    public void OnEnterRoomRe(sbyte iSlot, uint iId)
    {
        //进入房间失败，返回大厅
        if (iSlot <= 0)
        {
            Back();
        }

        Player pPlayer = new Player(iId, m_sMyPlayerName, localPlayerInfo.SelectedCharData);
        pPlayer.Slot = iSlot;
        m_pNetwork.MyRoom.PlayerMan.SetMyPlayer(pPlayer);
        AddPlayer(pPlayer);
    }

    public void RemovePlayer(uint id, int iSlot)
    {
        Destroy(m_mPlayerModel[id]);
        m_mPlayerModel.Remove(id);
        m_PlayerPositionList[iSlot-1].FindChild("Spotlight").gameObject.SetActive(false);
    }

    public void OnPrepareSync(uint id, bool prepare)
    {
        Player player = m_pNetwork.MyRoom.PlayerMan.GetPlayerById(id);
        if (player != null)
        {
            m_PlayerPositionList[player.Slot - 1].FindChild("Spotlight").gameObject.SetActive(prepare);
        }
    }

    #endregion

    public void EnterArena()
    {
        m_pNetwork.MyRoom.PlayerMan.ResetPrepare();
        MutilBattleView peer = GameObject.Find("Network Peer").AddComponent<MutilBattleView>();
        peer.myObject = m_MyPlayer;
        peer.m_mPlayerModel = m_mPlayerModel;
        peer.waitForOthers = true;
        peer.waitingPlayers = new List<uint>(m_mPlayerModel.Keys);
        Resources.UnloadUnusedAssets();
        Application.LoadLevel("testMultiplayerScene");
    }
}
