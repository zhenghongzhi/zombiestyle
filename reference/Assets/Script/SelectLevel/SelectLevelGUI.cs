using UnityEngine;
using System.Collections;
using System.Collections.Generic;
#if UNITY_EDITOR
using UnityEditor;
#endif

public class SceneView
{
    public SceneData sceneData;
    public SelectLevelGUI commonData;
    public Texture2D mainTex;
    public Vector2 positionLT;

    public SceneView()
    {

    }

    public SceneView(Vector2 pos, SceneData data, SelectLevelGUI commonData)
    {
        positionLT = pos;
        sceneData = data;
        this.commonData = commonData;
    }

    public void OnDraw(float startX, float startY)
    {
        Vector2 ltPos = new Vector2(startX + positionLT.x, startY + positionLT.y);
        //Draw Stars if Accessable
        if (commonData.playerData.IsSceneAccessable(sceneData.SceneID))
        {
            if (GUI.Button(CsGUI.CRect(ltPos.x, ltPos.y, 150, 150), sceneData.SceneDescribe + " | " + commonData.playerData.GetSceneScore(sceneData.SceneID)))
            {
                commonData.playerData.nextLevelName = sceneData.SceneName;
                commonData.playerData.sceneID = sceneData.SceneID;
                if (TaskModule.Instance.GetDataByScene(sceneData.SceneID, out commonData.maptaskData, out commonData.playertaskData, out commonData.missionData))
                {
                    commonData.m_eState = SelectLevelGUI.State.TaskPanel;
                }
            }
        }
        else
        {
            GUI.Box(CsGUI.CRect(ltPos.x, ltPos.y, 150, 150), sceneData.SceneDescribe);
            GUI.Label(CsGUI.CRect(ltPos.x + 50, ltPos.y + 100, 100, 50), "LOCKED");
        }
    }
}

public class MapView : ITouchSlidePanelView
{
    public SelectLevelGUI commonData;
    public WorldData worldData;
    public Texture2D mapTex;
    List<SceneView> scenes;
    
    public float Width
    {
        get { return CsGUI.CX(1024); }
    }

    public float Height
    {
        get { return 0; }
    }

    public MapView()
    {
        scenes = new List<SceneView>();
    }

    public MapView(WorldData worldData, SelectLevelGUI commonData, Texture2D mapTex)
    {
        this.worldData = worldData;
        this.commonData = commonData;
        this.mapTex = mapTex;
        scenes = new List<SceneView>();
    }

    public void AddSceneView(SceneView scene)
    {
        scenes.Add(scene);
    }

    public void OnDrawView(float startX, float startY)
    {
        float normX = startX / CsGUI.XRate;
        //totally out of screen, don't draw this view
        if (normX < -1024 || normX > 1024)
            return;
        //Draw World Map View
        if (commonData.playerData.IsWorldAccessable(worldData.WorldID))
        {
            GUI.DrawTexture(CsGUI.CRect(normX, 0, 1024, 668), mapTex);
            foreach (SceneView view in scenes)
            {
                view.OnDraw(normX, 0);
            }
        }
        else//Draw Locked Map View
        {
            GUI.Box(CsGUI.CRect(normX, 0, 1024, 668), "Locked" + worldData.WorldDescribe);
        }
    }
}

[System.Serializable]
public class ScenePosition
{
    public List<Vector2> positions;

    public ScenePosition()
    {
        positions = new List<Vector2>();
    }
}

public class SelectLevelGUI : MonoBehaviour {
    public GUISkin mySkin;
    public GameObject topMenuPrefab;
    public Texture2D[] worldMaps;

    public Texture texTaskPanelBg;

    public Vector2 worldMapSize;
    public Vector2 sceneIconSize;
    public ScenePosition[] mapPositions;
    [HideInInspector]
    public GlobalPlayerInfo playerData;

    HorizontalTouchSlidePanel mapPanel;

    public State m_eState = State.Map;

    private bool guiMapEnable = true;
    //任务列表中任务之间的间隔
    int taskInterval = 50;

    //用到的需本地化的文本
    string passCondition;
    string todayTask;
    string back;
    string start;
    string finished;

    //***任务相关显示数据
    //当前地图的任务数据
    public MapTaskData maptaskData;
    //用户的任务数据
    public List<TaskData> playertaskData;
    //任务的静态数据
    public List<MissionData> missionData;

    public enum State
    {
        Map,
        TaskPanel,
    }

    void Awake()
    {
        if (!GameObject.Find("TopMenuGUI"))
        {
            Object gui = Instantiate(topMenuPrefab);
            gui.name = "TopMenuGUI";
        }
        GameObject.Find("TopMenuGUI").GetComponent<TopMenuGUI>().showBack = true;
    }

    void Start()
    {
        playerData = GameObject.Find("GlobalData").GetComponent<GlobalPlayerInfo>();
        InitWorldMapPanel();

        TextScript text = StaticGameScripts.GetSceneScript("Task");
        passCondition = text.GetScript("PassCondition");
        todayTask = text.GetScript("TodayTask");
        back = text.GetScript("Back");
        start = text.GetScript("Start");
        finished = text.GetScript("Finished");
    }

    void InitWorldMapPanel()
    {
        List<ITouchSlidePanelView> contents = new List<ITouchSlidePanelView>();
        int worldIndex = 0;
        foreach (WorldData world in StaticGameData.WorldMaps)
        {
            MapView map = new MapView(world, this, worldMaps[worldIndex]);
            int sceneIndex = 0;
            foreach (SceneData scene in world.scenes)
            {
                SceneView view = new SceneView(mapPositions[worldIndex].positions[sceneIndex], scene, this);
                map.AddSceneView(view);
                sceneIndex++;
            }
            contents.Add(map);
            worldIndex++;
        }
        mapPanel = new HorizontalTouchSlidePanel(CsGUI.CRect(0, 100, 1024, 668), 0, mySkin.GetStyle("wndShowcase"), contents, 0, 800);
    }

    void Back()
    {
        switch (m_eState)
        {
            case State.Map:
                Application.LoadLevel("MainMenu");
                break;
            case State.TaskPanel:
                guiMapEnable = true;
                m_eState = State.Map;
                break;
        }
    }

    void OnGUI()
    {
        GUI.skin = mySkin;
        switch (m_eState)
        {
            case State.Map:
                GUI.Window(0, CsGUI.CRect(0, 100, 1024, 668), ShowMap, "", "wndShowcase");
                break;
            case State.TaskPanel:
                guiMapEnable = false;
                GUI.Window(0, CsGUI.CRect(0, 100, 1024, 668), ShowMap, "", "wndShowcase");
                GUI.depth = 1;
                DrawTaskPanel();
                GUI.depth = 0;
                break;
        }
    }

    void ShowMap(int id)
    {
        GUI.enabled = guiMapEnable;
        mapPanel.Update();
        mapPanel.OnDraw();
        GUI.enabled = true;
    }

    void DrawTaskPanel()
    {
        GUI.DrawTexture(CsGUI.CRect(200, 200, 624, 368), texTaskPanelBg);

        if (playertaskData.Count != 0)
        {
            GUI.Label(CsGUI.CRect(500, 200, 400, 50), passCondition);
            GUI.Label(CsGUI.CRect(300, 250, 400, 50), maptaskData.WinCondition);

            GUI.Label(CsGUI.CRect(500, 300, 400, 50), todayTask);

            for (int i = 0; i < playertaskData.Count; ++i )
            {
                //text
                GUI.Label(CsGUI.CRect(300, 350+i*taskInterval, 200, 100), missionData[i].Des);
                //process
                if (playertaskData[i].State == TaskState.Finished)
                {
                    GUI.Label(CsGUI.CRect(500, 350+i*taskInterval, 100, 100), finished);
                }
                else
                {
                    GUI.Label(CsGUI.CRect(500, 350+i*taskInterval, 100, 100), playertaskData[i].Arg+"/"+missionData[i].ObjValue2);
                }
                //reward
                GUI.Label(CsGUI.CRect(600, 350+i*taskInterval, 300, 100), missionData[i].GainGold+" "+missionData[i].GainExp);
            }
        }
        else
        {
            GUI.Label(CsGUI.CRect(500, 300, 400, 50), passCondition);
            GUI.Label(CsGUI.CRect(300, 350, 400, 50), maptaskData.WinCondition);
        }

        //back button
        if(GUI.Button(CsGUI.CRect(300, 500, 200, 50), back))
        {
            Back();
        }
        //start button
        if(GUI.Button(CsGUI.CRect(600, 500, 200, 50), start))
        {
            Application.LoadLevel("LoadingPage");
            GameObject.Destroy(GameObject.Find("TopMenuGUI"));
        }        
    }

#if UNITY_EDITOR
    void OnDrawGizmosSelected()
    {
        for (int i = 0; i < worldMaps.Length; ++i)
        {
            Gizmos.DrawGUITexture(new Rect(i * worldMapSize.x, 0, worldMapSize.x, -worldMapSize.y), worldMaps[i]);
            if (mapPositions.Length == i)
            {
                ScenePosition[] newArray = new ScenePosition[mapPositions.Length + 1];
                mapPositions.CopyTo(newArray, 0);
                newArray[i] = new ScenePosition();
                mapPositions = newArray;
            }
            int sceneIndex = 0;
            foreach (Vector2 p in mapPositions[i].positions)
            {
                Gizmos.DrawIcon(new Vector3(i * worldMapSize.x + p.x, -p.y, -0.1f), "Scene" + sceneIndex);
                DrawRect(i * worldMapSize.x + p.x, p.y, sceneIconSize.x, sceneIconSize.y);
                sceneIndex++;
            }
        }
    }

     void DrawRect(float x, float y, float width, float height)
     {
         Gizmos.DrawLine(new Vector3(x, -y, -0.1f), new Vector3(x + width, -y, -0.1f));
         Gizmos.DrawLine(new Vector3(x + width, -y, -0.1f), new Vector3(x + width, -y - height, -0.1f));
         Gizmos.DrawLine(new Vector3(x + width, -y - height, -0.1f), new Vector3(x, -y - height, -0.1f));
         Gizmos.DrawLine(new Vector3(x, -y - height, -0.1f), new Vector3(x, -y, -0.1f));
     }
#endif
}
