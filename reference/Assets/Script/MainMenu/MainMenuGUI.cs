using UnityEngine;
using System.Collections;

public class MainMenuGUI : MonoBehaviour {
    public Transform playerPosition;
    public GUISkin mySkin;
    public Texture2D playModeSelectionBg;

#if UNITY_EDITOR || UNITY_STANDALONE_WIN
    float lastPosX;
#endif
    bool rotateModel = false;

    float settingX = 1024;
    float trophyX = 1024;
    float signX = 1024;
    float drawX = 1024;
    float leftPanelX = -376;
    float shopY;

    bool initControllers = true;
    bool sweapControllers = false;

    GUIStyle gameBtnStyle;
    GameObject player;
    TextScript scripts;

    void Awake()
    {
        GameObject.Find("TopMenuGUI").GetComponent<TopMenuGUI>().showBack = true;
    }

    void Start()
    {
        CharacterModelManager.ReleaseResources();
        gameBtnStyle = new GUIStyle(mySkin.GetStyle("btnSelectGame"));
        gameBtnStyle.padding.top = (int)CsGUI.CY(gameBtnStyle.padding.top);
        gameBtnStyle.fontSize = (int)CsGUI.CY(gameBtnStyle.fontSize);
        initControllers = true;
        scripts = StaticGameScripts.GetSceneScript("MainMenu");
    }

    void OnEnable()
    {
        player = GameObject.FindGameObjectWithTag("Player");
        foreach (SkinnedMeshRenderer smr in player.GetComponentsInChildren<SkinnedMeshRenderer>())
        {
            smr.enabled = true;
        }
        player.transform.position = playerPosition.position;
        player.transform.LookAt(Camera.mainCamera.transform);
        player.transform.eulerAngles = new Vector3(0, player.transform.eulerAngles.y, 0);
        player.GetComponent<Rigidbody>().isKinematic = true;
        player.GetComponent<PlayerAnimation>().enabled = false;
        player.GetComponent<PlayerMovementMoter>().enabled = false;
        player.GetComponent<TPSController>().enabled = false;
        player.GetComponent<PadInput>().enabled = false;
        player.GetComponent<SpecialSkillsController>().enabled = false;
        player.transform.FindChild("light").gameObject.SetActive(false);
        //player.transform.FindChild("shadows").gameObject.SetActive(false);
        player.animation.Play(player.GetComponent<PlayerAnimation>().idle.name);
        initControllers = true;
        settingX = 1024;
        trophyX = 1024;
        signX = 1024;
        drawX = 1024;
        leftPanelX = -250;
        shopY = 768;
    }

    //Receive Message From TopMenue GUI
    void Back()
    {
        GameObject.Find("GlobalData").SendMessage("SaveData");
        Destroy(GameObject.Find("TopMenuGUI"));
        Destroy(GameObject.Find("GlobalData"));
        Destroy(player);
        sweapControllers = true;
        Application.LoadLevelAsync("SelectCharacter");
    }

    void Update()
    {
        if (initControllers)
        {
            if (CsGUI.SlideIntoPosition(ref leftPanelX, 0))
            {
                //Right Side Buttons
                if (CsGUI.SlideIntoPosition(ref settingX, 930))
                {
                    if (CsGUI.SlideIntoPosition(ref trophyX, 915))
                    {
                        if (CsGUI.SlideIntoPosition(ref signX, 905))
                        {
                            if (CsGUI.SlideIntoPosition(ref drawX, 920))
                            {
                                if (CsGUI.SlideIntoPosition(ref shopY, 720))
                                    initControllers = false;
                            }
                        }
                    }
                }
            }
        }
        if (sweapControllers)
        {
            CsGUI.SlideIntoPosition(ref leftPanelX, -380);
            CsGUI.SlideIntoPosition(ref settingX, 1024);
            CsGUI.SlideIntoPosition(ref trophyX, 1024);
            CsGUI.SlideIntoPosition(ref signX, 1024);
            CsGUI.SlideIntoPosition(ref drawX, 1024);
            CsGUI.SlideIntoPosition(ref shopY, 800);
        }


        #region Rotate Model
#if UNITY_EDITOR || UNITY_STANDALONE_WIN
        if (rotateModel)
        {
            Quaternion rotateY = Quaternion.AngleAxis(-Input.mousePosition.x + lastPosX, Vector3.up);
            player.transform.rotation *= rotateY;
            playerPosition.rotation *= rotateY;
            lastPosX = Input.mousePosition.x;
            if (Input.GetMouseButtonUp(0) || !CsGUI.CRect(350, 100, 500, 500).Contains(Input.mousePosition))
                rotateModel = false;
        }
        else if (Input.GetMouseButtonDown(0) && CsGUI.CRect(350, 100, 500, 500).Contains(Input.mousePosition))
        {
            lastPosX = Input.mousePosition.x;
            rotateModel = true;
        }
#endif
#if !UNITY_EDITOR
            if( rotateModel)
            {   
                Quaternion rotateY = Quaternion.AngleAxis(-Input.touches[0].deltaPosition.x, Vector3.up);
                player.transform.rotation *= rotateY;
                playerPosition.rotation *= rotateY;
                if (Input.touchCount < 1)
                    rotateModel = false;
                else if(!CsGUI.CRect(350, 100, 500, 500).Contains(Input.touches[0].position))
                    rotateModel = false;
            }
            if(Input.touchCount == 1 && Input.touches[0].phase == TouchPhase.Began &&
                CsGUI.CRect(350, 100, 500, 500).Contains(Input.touches[0].position))
            {
                rotateModel = true;
            }

#endif
        #endregion

    }


	void OnGUI()
    {
        GUI.skin = mySkin;
        if (GUI.Button(CsGUI.CRect(433, shopY, 158, 48), "", "btnShop"))
        {
            Application.LoadLevelAsync("Shop");
            sweapControllers = true;
        }
        DrawLeftSelectionPanel();
        DrawRightSideButtons();
    }

    void DrawRightSideButtons()
    {
        if (GUI.Button(CsGUI.CRect(settingX, 0, 113, 75), "", "btnSetting"))
        {

        }
        if (GUI.Button(CsGUI.CRect(trophyX, 83, 128, 88), "", "btnTrophy"))
        {

        }
        if (GUI.Button(CsGUI.CRect(signX, 180, 120, 83), "", "btnSign"))
        {

        }
        if (GUI.Button(CsGUI.CRect(drawX, 275, 120, 83), "", "btnDrawing"))
        {

        }
    }

    void DrawLeftSelectionPanel()
    {
        GUI.DrawTexture(CsGUI.CRect(leftPanelX, 120, 376, 546), playModeSelectionBg);
        //Start Single Game
        if (GUI.Button(CsGUI.CRect(leftPanelX + 47, 200, 243, 110), scripts.GetScript(0), gameBtnStyle))
        {
            sweapControllers = true;
            CharacterModelManager.ReleaseResources();
            Application.LoadLevelAsync("SelectLevel");
        }
        //Multi-player Game
        if (GUI.Button(CsGUI.CRect(leftPanelX + 47, 320, 243, 110), scripts.GetScript(1), gameBtnStyle))
        {
            sweapControllers = true;
            CharacterModelManager.ReleaseResources();
            Application.LoadLevelAsync("Lobby");
        }
        //Enter Arena
        if (GUI.Button(CsGUI.CRect(leftPanelX + 47, 440, 243, 110), scripts.GetScript(2), gameBtnStyle))
        {

        }
    }

}
