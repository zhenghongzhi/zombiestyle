﻿using UnityEngine;
using System.Collections;

public class CsConst
{
    public const int UDP_PORT = 9495;
    public static readonly int HOST_PORT = 9496;
    public static readonly int MAX_CONNECTION = 4;
    public static readonly bool USE_NAT = false;

    //Network check
    public static readonly string CHECK_URL = "www.baidu.com";
    public static readonly float CHECK_TIMEOUT = 2;

    //time sync
    public static readonly string TIME_HOST = "202.120.2.101";
    public static readonly int TIME_TIMEOUT = 1;

    //任务每次刷新的场景数
    public const int REFRESH_COUNT = 3;

    //任务进度提示持续时间
    public static readonly float PROCESS_NOTIFY_LAST = 4;
    //任务完成提示持续时间
    public static readonly float FINISH_NOTIFY_LAST = 3;

    //Lobby场景name
    public static readonly string LOBBY_SCENE = "Lobby";
    //Battle场景name
    public static readonly string BATTLE_SCENE = "testMultiplayerScene";

    public static readonly int MAX_INNATE_COUNT = 60;
    
}
