using UnityEngine;
using System.Collections.Generic;

public class CharacterModelManager
{
    static Dictionary<string, GameObject> characterPrefabs;
    static Dictionary<string, GameObject> gunPrefabs;

    static Dictionary<string, GameObject> CharacterPrefabs
    {
        get 
        {
            if(characterPrefabs == null)
                characterPrefabs = new Dictionary<string, GameObject>();
            return characterPrefabs;
        }
    }

    static Dictionary<string, GameObject> GunPrefabs
    {
        get
        {
            if(gunPrefabs == null)
                gunPrefabs = new Dictionary<string, GameObject>();
            return gunPrefabs;
        }
    }

    public static GameObject CreateNewCharacter(string charModelName, WeaponData weaponData)
    {
        //Load Prefab Resources
        if (!CharacterPrefabs.ContainsKey(charModelName))
        {
            GameObject prefab = (GameObject)Resources.Load(CharModelBasePath + charModelName);
            if(!prefab)
            {
                Debug.LogError("The Prefab " + charModelName + " Can't be Found in Resources/" + CharModelBasePath + " Folder");
                return null;
            }
            CharacterPrefabs.Add(charModelName, prefab);
        }

        //Instantiate Character GameObject And Set Components
        GameObject player = (GameObject)GameObject.Instantiate(CharacterPrefabs[charModelName]);
        player.GetComponent<Rigidbody>().isKinematic = true;
        player.GetComponent<PlayerAnimation>().enabled = false;
        player.GetComponent<PlayerMovementMoter>().enabled = false;
        player.GetComponent<TPSController>().enabled = false;
        player.GetComponent<PadInput>().enabled = false;
        player.GetComponent<SpecialSkillsController>().enabled = false;
        player.animation.Play();
        player.transform.FindChild("light").gameObject.SetActive(false);
        //player.transform.FindChild("shadows").gameObject.SetActive(false);

        //Set Guns
        player = ChangeSameTypeGun(player, weaponData);
        return player;
    }

    public static GameObject ChangeSameTypeGun(GameObject player, WeaponData weaponData)
    {
        if (!GunPrefabs.ContainsKey(weaponData.modelName))
        {
            GameObject prefab = (GameObject)Resources.Load(GunModelBasePath + weaponData.modelName);
            if (!prefab)
            {
                Debug.LogError("The Prefab " + weaponData.modelName + " Can't be Found in Resources/" + GunModelBasePath + " Folder");
                return null;
            }
            GunPrefabs.Add(weaponData.modelName, prefab);
        }

        //Find Original Gun
        int i = 0;
        foreach (Transform gunTrans in player.GetComponent<Weapon>().gunTransforms)
        {
            //Instantiate New Gun
            GameObject gun = (GameObject)GameObject.Instantiate(GunPrefabs[weaponData.modelName]);
            //Set New Gun
            gun.transform.parent = gunTrans.parent;
            gun.transform.localRotation = gunTrans.localRotation;
            gun.transform.localPosition = gunTrans.localPosition;
            gun.transform.localScale = gunTrans.localScale;
            Weapon weaponScript = player.GetComponent<Weapon>();
            weaponScript.gunTransforms[i++] = gun.transform;
            //Destroy Old Gun
            GameObject.Destroy(gunTrans.gameObject);
        }

        player.GetComponent<Weapon>().gunName = weaponData.modelName;

        return player;
    }

    public static void ReleaseResources()
    {
        CharacterPrefabs.Clear();
        GunPrefabs.Clear();
        Resources.UnloadUnusedAssets();
    }

    public static string CharModelBasePath
    {
        get { return "Battle/"; }
    }

    public static string GunModelBasePath
    {
        get { return "Weapons/weapon_"; }
    }
}
