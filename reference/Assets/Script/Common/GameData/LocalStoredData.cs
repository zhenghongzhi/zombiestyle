using UnityEngine;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Xml;
using System.Xml.Serialization;
using System;

//游戏开始进入角色选择前调用LoadData加载该类
//在游戏运行过程中作为全局变量保持角色信息
[System.Serializable]
public class PlayerStoreData
{
    [XmlAttribute]
    public int gold = 1000;
    [XmlAttribute]
    public int diamond = 10;
    [XmlAttribute]
    public int stamina = 100;
    [XmlAttribute]
    public int sceneAccess = 1;
    [XmlAttribute]
    public int selectedCharID = 1;
    [XmlAttribute]
    public string playerName = "Player";
    public GameSetting settings;
    public CharacterStoreData[] charData;
    public List<MapStoreData> mapData;
    public TaskStoreData taskData;

    private static PlayerStoreData m_StoredData;
	public static PlayerStoreData StoredData
	{
		get { return m_StoredData; }
	}

    public CharacterStoreData SelectedChar
    {
        get { return charData[selectedCharID - 1]; }
    }

    public static PlayerStoreData LoadData()
    {
        try
        {       
            if (File.Exists(Application.persistentDataPath + "\\PlayerData.xml"))
            {
                StreamReader sr = File.OpenText(Application.persistentDataPath + "\\PlayerData.xml");
                XmlSerializer ser = new XmlSerializer(typeof(PlayerStoreData));
                //MemoryStream xmlStream = new MemoryStream(Encoding.UTF8.GetBytes(data.text));
                m_StoredData = ser.Deserialize(sr) as PlayerStoreData;
                return m_StoredData;
            }
            else
            {
                PlayerStoreData result = new PlayerStoreData();
                result.charData = new CharacterStoreData[4];
                result.charData[0] = new CharacterStoreData(1, "Cohen", 0);
                result.charData[1] = new CharacterStoreData(2, "Roy", 50);
                result.charData[2] = new CharacterStoreData(3, "Ruby", 0);
                result.charData[3] = new CharacterStoreData(4, "Bernard", 50);

                result.mapData = new List<MapStoreData>();
                result.mapData.Add(new MapStoreData(101, 0));
                result.mapData.Add(new MapStoreData(102, 0));
                result.mapData.Add(new MapStoreData(103, 0));
                result.mapData.Add(new MapStoreData(104, 0));
                result.mapData.Add(new MapStoreData(105, 0));
                result.mapData.Add(new MapStoreData(106, 0));

                result.taskData = new TaskStoreData();

                m_StoredData = result;
                return result;
            }
        }
        catch (System.Exception ex)
        {
            Debug.LogException(ex);
        }
        return null;
    }

    public void SaveData()
    {
        StreamWriter writer = null;

        try
        {
            FileInfo t = new FileInfo(Application.persistentDataPath + "\\PlayerData.xml");
            if (!t.Exists)
            {
                writer = t.CreateText();
            }
            else
            {
                t.Delete();
                writer = t.CreateText();
            }
            XmlSerializer ser = new XmlSerializer(typeof(PlayerStoreData));
            ser.Serialize(writer, this);
        }
        catch (System.Exception ex)
        {
            Debug.Log(ex.Message);
        }
        finally
        {
            if (writer != null)
            {
                writer.Flush();
                writer.Close();
            }
        }

    }

    public static string FilePath
    {
        get { return "file://" + Application.persistentDataPath + "/PlayerData.xml"; }
    }

    public bool AddExp(int iExp, int iChar)
    {
        if (iChar >= 0 && iChar <= 3)
        {
            CharacterStoreData curChar = charData[iChar];
            curChar.currentExp += iExp;
            if (curChar.currentExp >= StaticGameData.GetLevelExp(curChar.currentLevel))
            {
                curChar.currentExp -= StaticGameData.GetLevelExp(curChar.currentLevel);
                curChar.currentLevel++;
            }
            return true;
        }
        return false;
    }
}

//玩家战斗过的地图信息
[System.Serializable]
public class MapStoreData
{
    [XmlAttribute]
    public int sceneId;
    [XmlAttribute]
    public int star;

    public MapStoreData()
    {

    }

    public MapStoreData(int iSceneId, int iStar)
    {
        sceneId = iSceneId;
        star = iStar;
    }
}

//玩家任务信息
[System.Serializable]
public class TaskStoreData
{
    [XmlAttribute]
    public bool actived = true;
    [XmlAttribute]
    public string flushTime;
    [XmlAttribute]
    public int envirTickcount;
    [XmlElement]
    public List<TaskData> taskData = new List<TaskData>();

    public TaskStoreData()
    {

    }
}

public class TaskData
{
    [XmlAttribute]
    public int MapId;
    [XmlAttribute]
    public int TaskId;
    [XmlAttribute]
    public TaskState State;
    [XmlAttribute]
    public int Arg;
}

#region Character Stored Data Related Classes
[System.Serializable]
public class GameSetting
{
    [XmlAttribute]
    public float music;
    [XmlAttribute]
    public float sound;

    public GameSetting()
    {
        music = .5f;
        sound = .5f;
    }
}

[System.Serializable]
public class CharacterEquipmentData
{
    [XmlAttribute]
    public int goodsID;
    [XmlAttribute]
    public int num;
}

[System.Serializable]
public class CharacterArmData
{
    [XmlAttribute]
    public int id;
    [XmlAttribute]
    public int grade;

    public CharacterArmData()
    {
        id = 0;
        grade = 0;
    }

    public CharacterArmData(int id, int grade)
    {
        this.id = id;
        this.grade = grade;
    }
}

[System.Serializable]
public class CharacterOutfitData
{
    [XmlAttribute]
    public int currentHeadID;
    [XmlAttribute]
    public int currentBodyID;
    [XmlAttribute]
    public int currentLegID;
    [XmlAttribute]
    public int currentGunID;
    public List<int> currentPackage;

    public CharacterOutfitData()
    {
        currentBodyID = 1;
        currentHeadID = 1;
        currentLegID = 1;
        currentGunID = 0;
        currentPackage = new List<int>();
    }

    public CharacterOutfitData(int gunID)
    {
        currentBodyID = 1;
        currentHeadID = 1;
        currentLegID = 1;
        currentGunID = gunID;
        currentPackage = new List<int>(4);
        for (int i = 0; i < 4; ++i)
        {
            currentPackage.Add(-1);
        }
    }
}

[System.Serializable]
public class CharacterSkillData
{
    [XmlAttribute]
    public int skillID;
    [XmlAttribute]
    public int skillLevel;

    public CharacterSkillData()
    {}

    public CharacterSkillData(int id, int level)
    {
        skillID = id;
        skillLevel = level;
    }
}

[System.Serializable]
public class CharacterItemData
{
    [XmlAttribute]
    public int itemID;
    [XmlAttribute]
    public int number;

    public CharacterItemData()
    {}

    public CharacterItemData(int id, int num)
    {
        itemID = id;
        number = num;
    }
}

[System.Serializable]
public class CharacterInnateData
{
    [XmlAttribute]
    public int innateId;
    [XmlAttribute]
    public int level;
    [XmlAttribute]
    public string upgradeTime;

    public CharacterInnateData()
    {

    }

    public CharacterInnateData(int _innateId, int _level, string _upgradeTime)
    {
        innateId = _innateId;
        level = _level;
        upgradeTime = _upgradeTime;
    }
}

//玩家角色的等级装备信息
[System.Serializable]
public class CharacterStoreData
{
    [XmlAttribute]
    public int charID;
    [XmlAttribute]
    public string charName;
    [XmlAttribute]
    public int currentLevel;
    [XmlAttribute]
    public int currentExp;
    public CharacterOutfitData outfit;
    public List<CharacterArmData> gunPack;
    public List<CharacterEquipmentData> goodsPack;
    public List<CharacterArmData> suitPack;
    public List<CharacterSkillData> obtainedSkillList;
    public List<CharacterItemData> itemsList;
    public List<CharacterInnateData> innateList;

    public CharacterStoreData()
    {
    }

    public CharacterStoreData(int id, string name, int defaultGunID)
    {
        charID = id;
        charName = name;
        currentExp = 0;
        currentLevel = 1;
        gunPack = new List<CharacterArmData>();
        gunPack.Add(new CharacterArmData(defaultGunID, 0));
        suitPack = new List<CharacterArmData>();
        goodsPack = new List<CharacterEquipmentData>();
        outfit = new CharacterOutfitData(gunPack[0].id);
        obtainedSkillList = new List<CharacterSkillData>();
        itemsList = new List<CharacterItemData>();
        innateList = new List<CharacterInnateData>();
    }

    public string Serialized()
    {
        TextWriter tw = null;
        try
        {
            tw = new StringWriter();
            XmlSerializer ser = new XmlSerializer(typeof(CharacterStoreData));
            ser.Serialize(tw, this);
        }
        catch (System.Exception ex)
        {
            Debug.LogException(ex);
        }
        finally
        {
            if (tw != null)
                tw.Close();      
        }
        return tw.ToString();
    }

    public static CharacterStoreData Deserialized(string xml)
    {
        TextReader tr = null;
        CharacterStoreData result = null;
        try
        {
            tr = new StringReader(xml);
            XmlSerializer ser = new XmlSerializer(typeof(CharacterStoreData));
            result = ser.Deserialize(tr) as CharacterStoreData;
        }
        catch (System.Exception ex)
        {
            Debug.LogException(ex);
        }
        finally
        {
            if (tr != null)
                tr.Close();
        }
        return result;
    }

    #region Gun Data Operation Methods
    public bool HasGun(int gunID)
    {
        foreach (CharacterArmData arm in gunPack)
        {
            if (arm.id == gunID)
                return true;
        }
        return false;
    }

    public CharacterArmData GetGun(int gunID)
    {
        foreach (CharacterArmData arm in gunPack)
        {
            if (arm.id == gunID)
                return arm;
        }
        return null;
    }

    public void BuyGun(int gunID)
    {
        gunPack.Add(new CharacterArmData(gunID, 0));
    }

    public void UpgradeGun(int gunID, int grade)
    {
        foreach (CharacterArmData arm in gunPack)
        {
            if (arm.id == gunID)
            {
                arm.grade = grade;
                break;
            }
        }
    }
#endregion

    #region Skill Data Operation Methods
    public bool HasSkill(int skillID)
    {
        foreach (CharacterSkillData skill in obtainedSkillList)
        {
            if (skill.skillID == skillID)
                return true;
        }
        return false;
    }

    public CharacterSkillData GetSkill(int skillID)
    {
        foreach (CharacterSkillData skill in obtainedSkillList)
        {
            if (skill.skillID == skillID)
                return skill;
        }
        return null;
    }

    public void UpgradeSkill(int skillID, int level)
    {
        foreach (CharacterSkillData skill in obtainedSkillList)
        {
            if (skill.skillID == skillID)
            {
                skill.skillLevel = level;
                return;
            }
        }
        obtainedSkillList.Add(new CharacterSkillData(skillID, level));
    }
    #endregion

    #region Item Data Operation Methods
    public bool HasItem(int itemID)
    {
        foreach (CharacterItemData item in itemsList)
        {
            if (item.itemID == itemID)
                return true;
        }
        return false;
    }

    public CharacterItemData GetItem(int itemID)
    {
        foreach (CharacterItemData item in itemsList)
        {
            if (item.itemID == itemID)
                return item;
        }
        return null;
    }

    public void BuyItem(int itemID)
    {
        CharacterItemData itemData = GetItem(itemID);
        if (itemData != null)
            itemData.number++;
        else
            itemsList.Add(new CharacterItemData(itemID, 1));
    }
    #endregion

    #region Innate Data Operation Methods
    public bool AddInnate(int id)
    {
        if (GetInnate(id) != null)
        {
            return false;
        }
        var add = new CharacterInnateData();
        add.innateId = id;
        add.level = 1;
        add.upgradeTime = DateTime.Now.ToString();
        innateList.Add(add);
        return true;
    }

    public bool UpgradeInnate(int id)
    {
        var data = GetInnate(id);
        if (data == null)
        {
            AddInnate(id);
        }
        else
        {
            if (data.level < CsConst.MAX_INNATE_COUNT)
            {
                data.level++;
                data.upgradeTime = DateTime.Now.ToString();
            }
        }
        return true;
    }

    public CharacterInnateData GetInnate(int id)
    {
        foreach (CharacterInnateData item in innateList)
        {
            if (item.innateId == id)
                return item;
        }
        return null;
    }

    public int GetInnateLvl(int id)
    {
        var data = GetInnate(id);
        if (data != null)
        {
            return data.level;
        }
        return 0;
    }
    #endregion

}
#endregion
