using UnityEngine;
using System.Collections.Generic;
using System.Xml;
using System.Xml.Serialization;//for XmlSerializer
using System.IO;//for MemoryStream
using System.Text;//for Encoding
// 
// public enum ScriptLanguage
// {
//     Chinese,
//     English,
// }
// 
// public enum ScriptPlatform
// {
//     Win,
//     iOS,
// }
// 
// public class SceneScript
// {
//     [XmlAttribute]
//     public string name;
//     [XmlAttribute]
//     public string id;
//     public string[] scripts;
// 
//     public static List<SceneScript> LoadFromXml(string xmlString)
//     {
//         try
//         {
//             XmlSerializer ser = new XmlSerializer(typeof(List<SceneScript>));
//             
//             MemoryStream xmlStream = new MemoryStream(Encoding.UTF8.GetBytes(xmlString));
//             return ser.Deserialize(xmlStream) as List<SceneScript>;
//         }
//         catch (System.Exception ex)
//         {
//             Debug.LogException(ex);
//         }
//         return null;
//     }
// }
// 
// public class LPKReader{
//     List<string> scripts = null;
// 
//     public LPKReader(string sceneName, ScriptLanguage lang = ScriptLanguage.Chinese, ScriptPlatform platform = ScriptPlatform.Win)
//     {
//         TextAsset scriptAsset = Resources.Load("Chinese") as TextAsset;
//         List<SceneScript> sceneScripts = SceneScript.LoadFromXml(scriptAsset.text);
//         foreach (SceneScript scene in sceneScripts)
//         {
//             if (scene.name == sceneName)
//             {
//                 Debug.Log(scene.scripts.Length);
//                 scripts = new List<string>(scene.scripts);
//             }
//         }
//         Resources.UnloadUnusedAssets();
//     }
// 
//     public string GetScript(int i)
//     {
//         return scripts[i];
//     }
// 
//     static string GetFileName(ScriptLanguage lang, ScriptPlatform platform)
//     {
//         StringBuilder name = new StringBuilder();
//         switch (lang)
//         {
//             case ScriptLanguage.Chinese:
//                 name.Append("chn");
//                 break;
//             case ScriptLanguage.English:
//                 name.Append("eng");
//                 break;
//         }
//         name.Append('-');
//         switch (platform)
//         {
//             case ScriptPlatform.Win:
//                 name.Append("win");
//                 break;
//             case ScriptPlatform.iOS:
//                 name.Append("ios");
//                 break;
//         }
//         //name.Append(".lpk");
//         return name.ToString();
//     }
// }
