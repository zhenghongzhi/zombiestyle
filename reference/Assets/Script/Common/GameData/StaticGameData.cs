using UnityEngine;
using System.Collections.Generic;
using System.Xml;
using System.Xml.Serialization;//for XmlSerializer
using System.IO;//for MemoryStream
using System.Text;//for Encoding

//Static Data Set Class, Used to Access the Static XML Game Data
public static class StaticGameData
{
    static List<LevelExpData> levelExps;
    static List<CharacterDataSet> charDataSetList;
    static List<WorldData> worldMaps;
    static Dictionary<int, MonsterData> monsterDatas;
    static Dictionary<int, MonsterWaveData> monsterWaves;
    static Dictionary<int, CharacterShopData> shopWeapons;
    static Dictionary<int, WeaponData> weaponDataSet;
    static Dictionary<int, List<SkillData>> skillData;
    static Dictionary<int, MissionData> missionMap;
    static Dictionary<int, MapTaskData> mapTaskMap;
    static Dictionary<int, ItemData> itemMap;
    static Dictionary<InnateKey, InnateData> innateMap;

    public static Dictionary<int, CharacterShopData> ShopWeapons
    {
        get
        {
            if (shopWeapons == null)
            {
                TextAsset text = (TextAsset)Resources.Load("Shop", typeof(TextAsset));
                List<CharacterShopData> dataList = CharacterShopData.LoadCharacterShopData(text.text);
                shopWeapons = new Dictionary<int, CharacterShopData>();
                foreach (CharacterShopData c in dataList)
                {
                    if (!shopWeapons.ContainsKey(c.charID))
                        shopWeapons.Add(c.charID, c);
                }
            }
            return shopWeapons;
        }
    }

    public static Dictionary<int, MonsterWaveData> MonsterWaves
    {
        get
        {
            if (monsterWaves == null)
            {
                TextAsset text = (TextAsset)Resources.Load("MonsterWave", typeof(TextAsset));
                List<MonsterWaveData> dataList = MonsterWaveData.LoadMonsterWaveData(text.text);
                monsterWaves = new Dictionary<int, MonsterWaveData>();
                foreach (MonsterWaveData m in dataList)
                {
                    if (!MonsterWaves.ContainsKey(m.sceneID))
                        MonsterWaves.Add(m.sceneID, m);
                }
                Resources.UnloadUnusedAssets();
            }
            return monsterWaves;
        }
    }

    public static Dictionary<int, MonsterData> MonsterDatas
    {
        get
        {
            if (monsterDatas == null)
            {
                TextAsset text = (TextAsset)Resources.Load("Monster", typeof(TextAsset));
                List<MonsterData> dataList = MonsterData.LoadMonsterData(text.text);
                monsterDatas = new Dictionary<int, MonsterData>();
                foreach (MonsterData m in dataList)
                {
                    if (!monsterDatas.ContainsKey(m.MonsterID))
                        monsterDatas.Add(m.MonsterID, m);
                }
                Resources.UnloadUnusedAssets();
            }
            return monsterDatas;
        }
    }

    public static List<WorldData> WorldMaps
    {
        get
        {
            if (worldMaps == null)
            {
                TextAsset text = (TextAsset)Resources.Load("Map", typeof(TextAsset));
                worldMaps = WorldData.LoadWorldData(text.text);
                Resources.UnloadUnusedAssets();
            }
            return worldMaps;
        }
    }

    public static List<LevelExpData> LevelExps
    {
        get 
        {
            if (levelExps == null)
            {
                TextAsset text = (TextAsset)Resources.Load("Exp", typeof(TextAsset));
                levelExps = LevelExpData.LoadLevelExpData(text.text);
                Resources.UnloadUnusedAssets();
            }
            return levelExps;
        }
    }

    public static List<CharacterDataSet> CharLevelDataSetList
    {
        get
        {
            if(charDataSetList == null)
            {
                TextAsset text = (TextAsset)Resources.Load("Char", typeof(TextAsset));
                charDataSetList = CharacterDataSet.LoadCharacterDataSet(text.text);
                Resources.UnloadUnusedAssets();
            }
            return charDataSetList;
        }
    }

    public static Dictionary<int, List<SkillData>> CharSkillData
    {
        get
        {
            if (skillData == null)
            {
                TextAsset text = (TextAsset)Resources.Load("Skill", typeof(TextAsset));
                List<SkillData> skills = SkillData.LoadData(text.text);
                skillData = new Dictionary<int, List<SkillData>>();
                foreach (SkillData skill in skills)
                {
                    if(!skillData.ContainsKey(skill.CharID))
                        skillData.Add(skill.CharID, new List<SkillData>());
                    skillData[skill.CharID].Add(skill);
                }
            }
            return skillData;
        }
    }

    public static Dictionary<int, WeaponData> WeaponDataSet
    {
        get
        {
            if (weaponDataSet == null)
            {
                TextAsset text = (TextAsset)Resources.Load("WeaponInfo", typeof(TextAsset));
                List<WeaponData> datas = WeaponData.LoadWeaponData(text.text);
                weaponDataSet = new Dictionary<int, WeaponData>();
                foreach (WeaponData data in datas)
                {
                    weaponDataSet.Add(data.id, data);
                }
            }
            return weaponDataSet;
        }
    }

    public static Dictionary<int, MissionData> MissionMap
    {
        get
        {
            if (missionMap == null)
            {
                TextAsset text = (TextAsset)Resources.Load("Mission", typeof(TextAsset));
                MissionRoot datas = MissionRoot.LoadData(text.text);
                missionMap = new Dictionary<int, MissionData>();
                foreach (MissionData data in datas.Mission)
                {
                    missionMap.Add(data.Index, data);
                }
            }
            return missionMap;
        }
    }

    public static Dictionary<int, MapTaskData> MapTaskMap
    {
        get
        {
            if (mapTaskMap == null)
            {
                TextAsset text = (TextAsset)Resources.Load("MapTask", typeof(TextAsset));
                MapTaskRoot datas = MapTaskRoot.LoadData(text.text);
                mapTaskMap = new Dictionary<int, MapTaskData>();
                foreach (MapTaskData data in datas.MapTask)
                {
                    data.m_MissionList = new List<int>();
                    string[] vMission = data.MissionList.Split(',');
                    foreach (string s in vMission)
                    {
                        data.m_MissionList.Add(int.Parse(s));
                    }
                    mapTaskMap.Add(data.MapID, data);
                }
            }
            return mapTaskMap;
        }
    }

    public static Dictionary<int, ItemData> ItemMap
    {
        get
        {
            if (itemMap == null)
            {
                TextAsset text = (TextAsset)Resources.Load("Item", typeof(TextAsset));
                List<ItemData> itemDatas = ItemData.LoadData(text.text);
                itemMap = new Dictionary<int, ItemData>();
                foreach (ItemData data in itemDatas)
                {
                    itemMap.Add(data.ItemID, data);
                }
            }
            return itemMap;
        }
    }

    public static List<ItemData> ItemList
    {
        get
        {
            List<ItemData> result = new List<ItemData>(ItemMap.Values);
            return result;
        }
    }

    public static Dictionary<InnateKey, InnateData> InnateMap
    {
        get 
        {
            if (innateMap == null)
            {
                TextAsset text = (TextAsset)Resources.Load("Innate", typeof(TextAsset));
                InnateRoot root = InnateRoot.LoadData(text.text);
                innateMap = new Dictionary<InnateKey, InnateData>();
                foreach (InnateData data in root.Innate)
                {
                    innateMap.Add(new InnateKey(data.InnateID, data.InnateLV), data);
                }
            }
            return innateMap;
        }
    }

    public static List<int> InnateList
    {
        get 
        {
            HashSet<int> idSet = new HashSet<int>();
            foreach (var p in InnateMap.Keys)
            {
                idSet.Add(p.id);
            }
            return new List<int>(idSet);
        }
    }

    public static int GetLevelExp(int level)
    {
        return LevelExps[level - 1].exp;
    }

    public static LevelData GetCharacterLevelData(int charID, int level)
    {
        foreach (CharacterDataSet c in CharLevelDataSetList)
        {
            if (c.CharID == charID)
                return c.AllLevels[level - 1];
        }
        Debug.Log("No Character Found in XML ( CharID = " + charID + " )");
        return null;
    }

    public static WeaponData GetWeaponData(int gunID)
    {
        if (WeaponDataSet.ContainsKey(gunID))
            return WeaponDataSet[gunID];
        return null;
    }

    public static CharacterShopData GetCharacterShopData(int charID)
    {
        return ShopWeapons[charID];
    }

    public static List<WeaponData> GetCharacterWeaponData(int charID)
    {
        List<WeaponData> result = new List<WeaponData>();
        foreach (int wid in ShopWeapons[charID].weapons)
        {
            result.Add(WeaponDataSet[wid]);
        }
        return result;
    }

    public static SkillData GetSkillData(int skillID)
    {
        foreach (List<SkillData> skillList in CharSkillData.Values)
        {
            foreach (SkillData skill in skillList)
            {
                if (skill.SkillID == skillID)
                    return skill;
            }
        }
        return null;
    }

    public static MissionData GetMission(int iMissionId)
    {
        if (MissionMap.ContainsKey(iMissionId))
        {
            return MissionMap[iMissionId];
        }
        return null;
    }

    public static MapTaskData GetMapTask(int iMapId)
    {
        if (MapTaskMap.ContainsKey(iMapId))
        {
            return MapTaskMap[iMapId];
        }
        return null;
    }

    public static ItemData GetItem(int iItemId)
    {
        if (ItemMap.ContainsKey(iItemId))
        {
            return ItemMap[iItemId];
        }
        return null;
    }

    public static InnateData GetInnate(int id, int level)
    {
        InnateKey key = new InnateKey(id, level);
        if (InnateMap.ContainsKey(key))
        {
            return InnateMap[key];
        }
        return null;
    }
}

#region Mutil-Language Script Data Related Class
public enum ScriptLanguage
{
    Chinese,
    English,
}

public static class StaticGameScripts
{
    static Dictionary<string, TextScript> allSceneScripts;

    static Dictionary<string, TextScript> AllSceneScripts
    {
        get
        {
            if (allSceneScripts == null)
                LoadScriptResource(ScriptLanguage.Chinese);
            return allSceneScripts;
        }
    }

    public static void LoadScriptResource(ScriptLanguage language = ScriptLanguage.Chinese)
    {
        if (allSceneScripts != null)
            return;
        string resName = "";
        switch (language)
        {
            case ScriptLanguage.Chinese:
                resName = "Chinese";
                break;
            case ScriptLanguage.English:
                resName = "English";
                break;
        }
        TextAsset scriptAsset = Resources.Load(resName+"Text") as TextAsset;
        List<TextScript> scripts = TextScript.LoadFromXml(scriptAsset.text);
        allSceneScripts = new Dictionary<string, TextScript>();
        foreach (TextScript s in scripts)
        {
            allSceneScripts.Add(s.name, s);
        }
    }

    public static TextScript GetSceneScript(string sceneName)
    {
        if (AllSceneScripts.ContainsKey(sceneName))
            return AllSceneScripts[sceneName];
        return null;
    }
}

public class Text
{
    [XmlAttribute]
    public string index;
    [XmlElement]
    public string str;
}

public class TextScript
{
    [XmlAttribute]
    public string name;
    [XmlAttribute]
    public string id;
    [XmlArrayItem]
    public List<Text> texts;

    public static List<TextScript> LoadFromXml(string xmlString)
    {
        try
        {
            XmlSerializer ser = new XmlSerializer(typeof(List<TextScript>));
            
            MemoryStream xmlStream = new MemoryStream(Encoding.UTF8.GetBytes(xmlString));
            return ser.Deserialize(xmlStream) as List<TextScript>;
        }
        catch (System.Exception ex)
        {
            Debug.LogException(ex);
        }
        return null;
    }

    public string GetScript(string key)
    {
        foreach (Text t in texts)
        {
            if (t.index == key)
            {
                return t.str;
            }
        }
        return "";
    }

    public string GetScript(int i)
    {
        return texts[i].str;
    }
}
#endregion

#region Character Level Attribute Data Related Classes
//Store the Exp that Have to Gain to Upgrade at Each Level
public class LevelExpData
{
    [XmlAttribute]
    public int level;
    [XmlAttribute]
    public int exp;

    public static List<LevelExpData> LoadLevelExpData(string xmlString)
    {
        XmlSerializer ser = new XmlSerializer(typeof(List<LevelExpData>));
        MemoryStream xmlStream = new MemoryStream(Encoding.UTF8.GetBytes(xmlString));
        return ser.Deserialize(xmlStream) as List<LevelExpData>;
    }
}

[System.Serializable]
public class LevelData{
    [XmlAttribute]
    public int LV;
    [XmlAttribute]
    public int HP;
    [XmlAttribute]
    public int ATK;
    [XmlAttribute]
    public int DEF;
    [XmlAttribute]
    public int Range;
    [XmlAttribute]
    public float Hit;
    [XmlAttribute]
    public float Crit;
    [XmlAttribute]
    public float CritRate;
    [XmlAttribute]
    public int MoveSpeed;
    [XmlAttribute]
    public int Load;

    override public string ToString()
    {
        return "level : " + LV + '\n' + "hp : " + HP + '\n' + "attack : " + ATK + '\n' + "defense : " + DEF + '\n';
    }
}

public class CharacterDataSet
{
    [XmlAttribute]
    public int CharID;
    [XmlAttribute]
    public string CharName;
    public List<LevelData> AllLevels;

    public static List<CharacterDataSet> LoadCharacterDataSet(string xmlString)
    {
        XmlSerializer ser = new XmlSerializer(typeof(List<CharacterDataSet>));
        MemoryStream xmlStream = new MemoryStream(Encoding.UTF8.GetBytes(xmlString));
        return ser.Deserialize(xmlStream) as List<CharacterDataSet>;
    }

    override public string ToString()
    {
        StringBuilder result = new StringBuilder(CharName + " : " + CharID + '\n');
        foreach (LevelData l in AllLevels)
        {
            result.Append(l.ToString());
        }
        return result.ToString();
    }
}
#endregion

#region Map Level Info Data Related Classes
public class SceneData
{
    [XmlAttribute]
    public int SceneID;//= WorldID * 100 + Scene Sequential Index
    [XmlAttribute]
    public string SceneName;
    [XmlAttribute]
    public string SceneDescribe;
    [XmlAttribute]
    public int SceneType;

    override public string ToString()
    {
        return SceneName + " : " + SceneID + '\n';
    }

    public static int GetWorldID(int sceneID)
    {
        return sceneID / 100;
    }
}

public class WorldData
{
    [XmlAttribute]
    public int WorldID;
    [XmlAttribute]
    public string WorldName;
    [XmlAttribute]
    public string WorldDescribe;
    [XmlAttribute]
    public int UnlockDiamonds;
    public List<SceneData> scenes;

    public static List<WorldData> LoadWorldData(string xmlString)
    {
        XmlSerializer ser = new XmlSerializer(typeof(List<WorldData>));
        MemoryStream xmlStream = new MemoryStream(Encoding.UTF8.GetBytes(xmlString));
        return ser.Deserialize(xmlStream) as List<WorldData>;
    }
}
#endregion

#region Zombie Wave Data Related Classes
//Store Monster Data. Multiple Monster Data can Refer to One Monster Model
[System.Serializable]
public class MonsterData
{
    [XmlAttribute]
    public int MonsterID;
    [XmlAttribute]
    public int ModelID;
    [XmlAttribute]
    public string Name;
    [XmlAttribute]
    public int Type;
    [XmlAttribute]
    public int LV;
    [XmlAttribute]
    public int Exp;
    [XmlAttribute]
    public int AtkType;
    [XmlAttribute]
    public float AtkRange;
    [XmlAttribute]
    public float AlertRange;
    [XmlAttribute]
    public int HP;
    [XmlAttribute]
    public int Atk;
    [XmlAttribute]
    public int Def;
    [XmlAttribute]
    public int Hit;
    [XmlAttribute]
    public int Crit;
    [XmlAttribute]
    public float MSpeed;
    [XmlAttribute]
    public float ASpeed;
    [XmlAttribute]
    public float OnHitDmgPer;

    public static List<MonsterData> LoadMonsterData(string xmlString)
    {
        XmlSerializer ser = new XmlSerializer(typeof(List<MonsterData>));
        MemoryStream xmlStream = new MemoryStream(Encoding.UTF8.GetBytes(xmlString));
        return ser.Deserialize(xmlStream) as List<MonsterData>;
    }
}

public class MonsterWaveData
{
    [XmlAttribute]
    public int sceneID;
    public List<WaveInfo> zombieWaves;

    public static List<MonsterWaveData> LoadMonsterWaveData(string xmlString)
    {
        XmlSerializer ser = new XmlSerializer(typeof(List<MonsterWaveData>));
        MemoryStream xmlStream = new MemoryStream(Encoding.UTF8.GetBytes(xmlString));
        return ser.Deserialize(xmlStream) as List<MonsterWaveData>;    
    }
}

public class WaveInfo
{
    public List<WaveZombie> zombieList;
}

public class WaveZombie
{
    [XmlAttribute]
    public int zombieID;
    [XmlAttribute]
    public int num;
}
#endregion

#region Shop Related Classes
public class CharacterShopData
{
    [XmlAttribute]
    public int charID;
    [XmlAttribute]
    public int[] weapons;

    public static List<CharacterShopData> LoadCharacterShopData(string xmlString)
    {
        try
        {
            XmlSerializer ser = new XmlSerializer(typeof(List<CharacterShopData>));
            MemoryStream xmlStream = new MemoryStream(Encoding.UTF8.GetBytes(xmlString));
            return ser.Deserialize(xmlStream) as List<CharacterShopData>;
        }
        catch (System.Exception ex)
        {
            Debug.LogException(ex);
        }
        return null;
    }
}

public class WeaponData
{
    [XmlAttribute]
    public int id;
    [XmlAttribute]
    public string weaponType;
    [XmlAttribute]
    public string modelName;
    [XmlAttribute]
    public int DecribeID;
    [XmlAttribute]
    public int levelLock;
    [XmlAttribute]
    public int payType;//0-gold, 1-diamond
    [XmlAttribute]
    public int price;
    [XmlAttribute]
    public int specialEffect;
    [XmlAttribute]
    public int[] dmg;
    [XmlAttribute]
    public int[] loaderSize;
    [XmlAttribute]
    public float[] atkRate;
    [XmlAttribute]
    public int[] upgradeGold;

    [XmlIgnoreAttribute]
    public string WeaponName
    {
        get 
        {
            string des = StaticGameScripts.GetSceneScript("WeaponDescribe").GetScript(DecribeID);
            return des.Substring(0, des.LastIndexOf(':'));
        }
    }
    [XmlIgnoreAttribute]
    public string WeaponDescribe
    {
        get
        {
            string des = StaticGameScripts.GetSceneScript("WeaponDescribe").GetScript(DecribeID);
            return des.Substring(des.LastIndexOf(":") + 1);
        }
    }


    public static List<WeaponData> LoadWeaponData(string xmlString)
    {
        try
        {
            XmlSerializer ser = new XmlSerializer(typeof(List<WeaponData>));
            MemoryStream xmlStream = new MemoryStream(Encoding.UTF8.GetBytes(xmlString));
            return ser.Deserialize(xmlStream) as List<WeaponData>;
        }
        catch (System.Exception e)
        {
            Debug.LogException(e);
        }
        return null;
    }
}
#endregion

#region Skill Related Classes
public class SkillData
{
    [XmlAttribute]
    public int SkillID;
    [XmlAttribute]
    public int SkillNameID;
    [XmlAttribute]
    public int CharID;
    [XmlAttribute]
    public int DescribeID;
    [XmlAttribute]
    public int SkillType;
    [XmlAttribute]
    public float CDTime;
    [XmlAttribute]
    public int MaxSkillLv;
    [XmlAttribute]
    public string NeedLv;
    [XmlAttribute]
    public string NeedPrice;

    List<int> upgradeLv;
    List<int> upgradePrice;

    public string SkillName
    {
        get { return StaticGameScripts.GetSceneScript("SkillDescribe").GetScript(SkillNameID); }
    }

    public string SkillDescribe
    {
        get { return StaticGameScripts.GetSceneScript("SkillDescribe").GetScript(DescribeID); }
    }

    public int GetUpgradeLv(int skillLevel)
    {
        return upgradeLv[skillLevel - 1];
    }

    public int GetUpgradePrice(int skillLevel)
    {
        return upgradePrice[skillLevel - 1];
    }

    public static List<SkillData> LoadData(string xmlString)
    {
        XmlSerializer ser = new XmlSerializer(typeof(List<SkillData>));
        MemoryStream xmlStream = new MemoryStream(Encoding.UTF8.GetBytes(xmlString));
        List<SkillData> result =  ser.Deserialize(xmlStream) as List<SkillData>;
        char[] sep = { ',' };
        foreach (SkillData data in result)
        {
            string[] lvs = data.NeedLv.Split(sep);
            data.upgradeLv = new List<int>();
            foreach (string s in lvs)
            {
                data.upgradeLv.Add(int.Parse(s));
            }
            string[] golds = data.NeedPrice.Split(sep);
            data.upgradePrice = new List<int>();
            foreach (string s in golds)
            {
                data.upgradePrice.Add(int.Parse(s));
            }
        }
        return result;
    }
}
#endregion

#region Item Related Classes
public class ItemData
{
    [XmlAttribute]
    public int ItemID;
    [XmlAttribute]
    public int BuffID;
    [XmlAttribute]
    public int Duration;
    [XmlAttribute]
    public int Gold;

    public string ItemName
    {
        get { return StaticGameScripts.GetSceneScript("ItemDescribe").GetScript("Name" + ItemID); }
    }

    public string ItemDescribe
    {
        get { return StaticGameScripts.GetSceneScript("ItemDescribe").GetScript("Des" + ItemID); }
    }

    public static List<ItemData> LoadData(string xmlString)
    {
        XmlSerializer ser = new XmlSerializer(typeof(List<ItemData>));
        MemoryStream xmlStream = new MemoryStream(Encoding.UTF8.GetBytes(xmlString));
        return ser.Deserialize(xmlStream) as List<ItemData>;
    }
}
#endregion

#region Mission Related Classes
public class MissionData
{
    [XmlElement]
    public byte Index;
    [XmlElement]
    public string Des;
    [XmlElement]
    public byte ObjMainType;
    [XmlElement]
    public byte ObjType;
    [XmlElement]
    public int ObjValue1;
    [XmlElement]
    public int ObjValue2;
    [XmlElement]
    public int GainGold;
    [XmlElement]
    public int GainExp;
}

[XmlRoot("Missions")]
public class MissionRoot
{
    [XmlElement]
    public List<MissionData> Mission;

    public static MissionRoot LoadData(string xmlString)
    {
        try
        {
            XmlSerializer ser = new XmlSerializer(typeof(MissionRoot));
            MemoryStream xmlStream = new MemoryStream(Encoding.UTF8.GetBytes(xmlString));
            MissionRoot result = ser.Deserialize(xmlStream) as MissionRoot;
            return result;
        }
        catch(System.Exception ex)
        {
            Debug.Log(ex.ToString());
        }
        return null;
    }
}
#endregion

#region MapTask Related Classes
public class MapTaskData
{
    [XmlElement]
    public byte MapID;
    [XmlElement]
    public string WinCondition;
    [XmlElement]
    public string MissionList;
    [XmlElement]
    public byte RollNum;

    public List<int> m_MissionList;
}

[XmlRoot("MapTasks")]
public class MapTaskRoot
{
    [XmlElement]
    public List<MapTaskData> MapTask;

    public static MapTaskRoot LoadData(string xmlString)
    {
        try
        {
            XmlSerializer ser = new XmlSerializer(typeof(MapTaskRoot));
            MemoryStream xmlStream = new MemoryStream(Encoding.UTF8.GetBytes(xmlString));
            MapTaskRoot result = ser.Deserialize(xmlStream) as MapTaskRoot;
            return result;
        }
        catch (System.Exception ex)
        {
            Debug.Log(ex.ToString());
        }
        return null;
    }
}
#endregion

#region Innate Related Classes
public class InnateKey
{
    public int id;
    public int level;

    public InnateKey(int _id, int _level)
    {
        id = _id;
        level = _level;
    }
}

public class InnateData
{
    [XmlElement]
    public int InnateID;
    [XmlElement]
    public string InnateName;
    [XmlElement]
    public string InnateIcon;
    [XmlElement]
    public byte InnateLV;
    [XmlElement]
    public string InnateDes;
    [XmlElement]
    public byte InnateType;
    [XmlElement]
    public byte NeedCharLV;
    [XmlElement]
    public string AddAttribute;
    [XmlElement]
    public int AddNumber;
    [XmlElement]
    public int AttributeValue;
    [XmlElement]
    public string Des;

    public int InnateCDTime
    {
        get { return InnateLV * 5; }
    }

    public int Cost
    {
        get { return InnateLV * 5; }
    }

    public List<int> m_MissionList;
}

[XmlRoot("Root")]
public class InnateRoot
{
    [XmlElement]
    public List<InnateData> Innate;

    public static InnateRoot LoadData(string xmlString)
    {
        try
        {
            XmlSerializer ser = new XmlSerializer(typeof(InnateRoot));
            MemoryStream xmlStream = new MemoryStream(Encoding.UTF8.GetBytes(xmlString));
            InnateRoot result = ser.Deserialize(xmlStream) as InnateRoot;
            return result;
        }
        catch (System.Exception ex)
        {
            Debug.Log(ex.ToString());
        }
        return null;
    }
}
#endregion

