using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class GlobalPlayerInfo : MonoBehaviour 
{

    public PlayerStoreData playerData;
    public ScriptLanguage language;
    [HideInInspector]
    public string nextLevelName;
    [HideInInspector]
    public int sceneID;

    Dictionary<int, List<MapStoreData>> mapAccess;


    #region Map Accessable & Score Data Related Methods
    public bool IsWorldAccessable(int worldID)
    {
        return MapAccess.ContainsKey(worldID);
    }

    public bool IsSceneAccessable(int sceneID)
    {
        int worldID = SceneData.GetWorldID(sceneID);
        if (!IsWorldAccessable(worldID))
            return false;
        foreach (MapStoreData m in MapAccess[worldID])
        {
            if (m.sceneId == sceneID)
                return true;
        }
        return false;
    }

    public void UnlockWorld(int worldID)
    {
        if (IsWorldAccessable(worldID))
            return;
        if (!MapAccess.ContainsKey(worldID))
            MapAccess.Add(worldID, new List<MapStoreData>());
        MapStoreData map = new MapStoreData(worldID * 100 + 1, 0);
        playerData.mapData.Add(map);
        MapAccess[worldID].Add(map);
    }

    public void UnlockScene(int sceneID)
    {
        if (IsSceneAccessable(sceneID))
            return;
        MapStoreData map = new MapStoreData(sceneID, 0);
        playerData.mapData.Add(map);
        int worldID = SceneData.GetWorldID(sceneID);
        if (!MapAccess.ContainsKey(worldID))
            MapAccess.Add(worldID, new List<MapStoreData>());
        MapAccess[worldID].Add(map);
    }

    public void UnlockNextScene()
    {
        int worldID = SceneData.GetWorldID(sceneID);
        int sceneIndex = sceneID - worldID * 100;
        if (StaticGameData.WorldMaps[worldID].scenes.Count == sceneIndex)
            UnlockWorld(worldID + 1);
        else
            UnlockScene(sceneID + 1);
    }

    public void SetSceneScore(int sceneID, int star)
    {
        foreach (MapStoreData map in playerData.mapData)
        {
            if (map.sceneId == sceneID && map.star < star)
                map.star = star;
        }
    }

    public int GetSceneScore(int sceneID)
    {
        foreach (MapStoreData map in playerData.mapData)
        {
            if (map.sceneId == sceneID)
                return map.star;
        }
        return 0;
    }
    #endregion

    public int SelectedCharID
    {
        get { return playerData.selectedCharID; }
        set { playerData.selectedCharID = value; }
    }

    public CharacterStoreData SelectedCharData
    {
        get
        {
            return playerData.charData[playerData.selectedCharID - 1];
        }
    }

    Dictionary<int, List<MapStoreData>> MapAccess
    {
        get 
        {
            if (mapAccess == null)
            {
                mapAccess = new Dictionary<int, List<MapStoreData>>();
                foreach (MapStoreData map in playerData.mapData)
                {
                    int worldID = SceneData.GetWorldID(map.sceneId);
                    if (!mapAccess.ContainsKey(worldID))
                        mapAccess.Add(worldID, new List<MapStoreData>());
                    mapAccess[worldID].Add(map);
                }
            }
            return mapAccess;
        }
    }

    void Awake()
    {
        playerData = PlayerStoreData.LoadData();

        //check if net is available
        //StartCoroutine(CheckNetwork.Check());

        //get internet time
        StartCoroutine(RemoteTime.TimeSync());

        DontDestroyOnLoad(gameObject);

        //协作初始化各个模块
        StartCoroutine(InitModule());
    }

    IEnumerator InitModule()
    {
        while (!RemoteTime.IsDone)
        {
            yield return new WaitForSeconds(0.5f);
        }
#if !UNITY_EDITOR
        TaskModule.Instance.Init();
#endif
    }

    public void SaveData()
    {
        playerData.SaveData();
    }

    void OnDestroy()
    {
        SaveData();
    }

	void OnApplicationQuit()
    {
        SaveData();
    }

    IEnumerator OnApplicationPause(bool bPause)
    {
        SaveData();
        if (!bPause)
        {
            yield return RemoteTime.TimeSync();
            Debug.Log("Try to refresh task when pause stoped!");
            TaskModule.Instance.Init();
        }
    }
}
