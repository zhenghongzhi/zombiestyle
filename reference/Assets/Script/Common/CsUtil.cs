using UnityEngine;
using System.Collections;

public class CsUtil
{
	public static Vector3 ToVector3(WitVector3 value)
    {
        return new Vector3(value.m_fX, value.m_fY, value.m_fZ);
    }

    public static WitVector3 ToWitVector3(Vector3 value)
    {
        return new WitVector3(value.x, value.y, value.z);
    }
}
