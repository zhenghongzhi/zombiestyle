using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public interface ITouchSlidePanelView
{
    float Width
    {
        get;
    }

    float Height
    {
        get;
    }

    void OnDrawView(float startX, float startY);
}

public enum SlideType
{
    PerView,
    PerPage,
    Free,
}

/*Algorithm Class for Controlling the Start X Position of A View. 
 * 
 * Boundary Slide Algorithm:
 *      Usually the StartX Moves Exactly the Pixels the Finger/Mouse Moves on the Screen.
 *      However When Out of Boundary, there's Decline Coefficient Parameter to Multiply.
 *      The declineCoeff = 1 / ( 1 + [OutDistance] * 0.4f)
 *      The Less You Set the Last Multiplier(0.4f), the more loose the boundary
 *      
 * Auto Slide Algorithm:
 *      When Finger/Mouse Leaves the Screen, The Auto Slide Algorithm Goes on to Move the
 *      StartX to Show an Integrated View on the Left Side. When the Last Slide Speed is more
 *      than 50 p/s,  Auto Slide follows the Last Slide Speed Direction. Otherwise the Direction
 *      is Decided by the Reveal Percentage of the Left Side View;
 *      The Auto Slide Speed Lerp from Last Slide Speed to AutoSlideSpeed.
 */
public class HorizontalTouchSlidePanel{
    enum SlideState
    {
        None,
        FingerSlide,
        AutoSlide,
    }

    //Window Attributes
    Rect rect;      //panel rect
    int windowID;
    GUIStyle style;

    //Custom Attributes
    float width = 0;    //single view width
    float interval = 0;  // interval between views
    float itemWidth = 0;     //itemWidth = width + interval
    float autoSlideSpeed;   //auto slide speed when finger released
    int itemsPerPage = 0;   //how many integrated view can be shown in the panel
    SlideType slideType;
    List<ITouchSlidePanelView> views;

    //Control Parameters
    float currentPosX;    //draw GUI start from posX
    float lastMouseX;   //Mouse position x of last frame
    float currentSlideSpeed;
    float autoSlideTargetPos;
    bool autoSlideLeft;
    bool slideBack;
    float maxPosX = 0;   //end point of view list
    int currentPage;
    int totalPage;
    SlideState currentState = SlideState.None;

    public List<ITouchSlidePanelView> Views
    {
        get { return views; }
        set { views = value; }
    }

    public int CurrentPage
    {
        get { return currentPage; }
    }

    public int TotalPage
    {
        get { return totalPage; }
    }

#region Construction Methods
    public HorizontalTouchSlidePanel(Rect rect, int windowID, GUIStyle style, List<ITouchSlidePanelView> contents, float interval = 0.0f, float autoSpeed = 200)
    {
        this.rect = new Rect(rect);
        this.windowID = windowID;
        this.interval = interval;
        this.autoSlideSpeed = autoSpeed;
        this.views = contents;
        this.style = style;
        currentPage = 0;
        if (contents.Count > 0)
        {
            SetViewAttribute(contents[0].Width);
        }
    }

    public void AddView(ITouchSlidePanelView view)
    {
        views.Add(view);
        if (width == 0)
            SetViewAttribute(views[0].Width);
    }

    void SetViewAttribute(float viewWidth)
    {
        width = viewWidth;
        itemWidth = width + interval;
        itemsPerPage = (int)(rect.width / itemWidth);
        totalPage = (views.Count + itemsPerPage - 1) / itemsPerPage;
        maxPosX = itemWidth * (views.Count - itemsPerPage);
    }
#endregion

#region GUI Methods
    public void OnDraw()
    {
        int i = 0;
        foreach (ITouchSlidePanelView view in views)
        {
            view.OnDrawView(currentPosX + i * itemWidth, 0);
            i++;
        }
    }
#endregion

#region Update Methods
    public void Update()
    {
        switch (currentState)
        {
            case SlideState.None:
                NoneUpdate();
                break;
            case SlideState.FingerSlide:
                FingerSlideUpdate();
                break;
            case SlideState.AutoSlide:
                AutoSlideUpdate();
                break;
        }
    }

    void NoneUpdate()
    {
#if UNITY_EDITOR || UNITY_STANDALONE_WIN
        if (Input.GetMouseButtonDown(0))
        {
            Vector3 mousePos = Input.mousePosition;
            mousePos.y = Screen.height - mousePos.y;
            if (rect.Contains(mousePos))
            {
                lastMouseX = mousePos.x;
                currentState = SlideState.FingerSlide;
            }
        }
#endif
#if !UNITY_EDITOR && (UNITY_ANDROID || UNITY_IPHONE)
        if(Input.touchCount == 1 && Input.touches[0].phase == TouchPhase.Began)
        {
            Vector3 touchPos = Input.touches[0].position;
            touchPos.y = Screen.height - touchPos.y;
            if(rect.Contains(touchPos))
            {
                lastMouseX = touchPos.x;
                currentState = SlideState.FingerSlide;
            }
        }
#endif
    }

    void FingerSlideUpdate()
    {
#if UNITY_EDITOR || UNITY_STANDALONE_WIN
        Vector3 mousePos = Input.mousePosition;
#endif
#if !UNITY_EDITOR && (UNITY_IPHONE || UNITY_ANDROID)
        Vector3 mousePos = Input.touches[0].position;
#endif
        float deltaX = mousePos.x - lastMouseX;
        float declineCoeff = 1;
        if (currentPosX > 0)
            declineCoeff = 1 / (1 + currentPosX * 0.4f);
        else if (currentPosX < -maxPosX)
            declineCoeff = 1 / (1 + (-maxPosX - currentPosX) * 0.4f);
        currentPosX += deltaX * declineCoeff;
        lastMouseX = mousePos.x;

        mousePos.y = Screen.height - mousePos.y;
        //End of Sliding
#if !UNITY_EDITOR && (UNITY_IPHONE || UNITY_ANDROID)
        if (Input.touchCount == 0 || Input.touches[0].phase == TouchPhase.Ended || !rect.Contains(mousePos))
        {
#endif
#if UNITY_EDITOR || UNITY_STANDALONE_WIN
        if (Input.GetMouseButtonUp(0) || !rect.Contains(mousePos))
        {
#endif
            currentState = SlideState.AutoSlide;
            currentSlideSpeed = deltaX / Time.deltaTime;
            //Out of left Bound
            if (currentPosX > 0)
            {
                autoSlideTargetPos = 0;
                autoSlideLeft = true;
                currentSlideSpeed = Mathf.Clamp(currentSlideSpeed, 0, 500);
            }
            //Out of right Bound
            else if (currentPosX < -maxPosX)
            {
                autoSlideTargetPos = -maxPosX;
                autoSlideLeft = false;
                currentSlideSpeed = Mathf.Clamp(currentSlideSpeed, -500, 0);
            }
            //Decide auto slide forward or backward
            else
            {
                int index = (int)(-currentPosX / itemWidth);
                //Finger Slide before Left Pad
                if (Mathf.Abs(deltaX) > 50)
                {
                    if (currentSlideSpeed > 0)
                    {
                        autoSlideTargetPos = -index * itemWidth;
                        autoSlideLeft = false;
                        currentSlideSpeed = Mathf.Clamp(currentSlideSpeed, 0, 500);
                    }
                    else
                    {
                        autoSlideTargetPos = -(index + 1) * itemWidth;
                        autoSlideLeft = true;
                        currentSlideSpeed = Mathf.Clamp(currentSlideSpeed, -500, 0);
                    }
                }
                //Finger Stationary before Left Pad
                else
                {
                    float dist = -currentPosX - index * itemWidth;
                    if (dist > itemWidth * .5f)
                    {
                        autoSlideTargetPos = -(index + 1) * itemWidth;
                        autoSlideLeft = true;
                    }
                    else
                    {
                        autoSlideTargetPos = -index * itemWidth;
                        autoSlideLeft = false;
                    }
                }
            }//End of Decide auto slide forward or backward
        }//End of End of Sliding
    }

    void AutoSlideUpdate()
    {
#if UNITY_EDITOR || UNITY_STANDALONE_WIN
        if (Input.GetMouseButtonDown(0))
        {
            Vector3 mousePos = Input.mousePosition;
            mousePos.y = Screen.height - mousePos.y;
            if (rect.Contains(mousePos))
            {
                lastMouseX = mousePos.x;
                currentState = SlideState.FingerSlide;
                return;
            }
        }
#endif
#if !UNITY_EDITOR && (UNITY_ANDROID || UNITY_IPHONE)
        if(Input.touchCount == 1 && Input.touches[0].phase == TouchPhase.Began)
        {
            Vector3 touchPos = Input.touches[0].position;
            touchPos.y = Screen.height - touchPos.y;
            if(rect.Contains(touchPos))
            {
                lastMouseX = touchPos.x;
                currentState = SlideState.FingerSlide;
            }
        }
#endif

        if (autoSlideLeft)
        {
            currentSlideSpeed = Mathf.Lerp(currentSlideSpeed, -autoSlideSpeed, 0.4f);
            currentPosX += currentSlideSpeed * Time.deltaTime;
            if (currentPosX < autoSlideTargetPos)
            {
                currentPosX = autoSlideTargetPos;
                currentState = SlideState.None;
                PageUpdate();
            }
        }
        else
        {
            currentSlideSpeed = Mathf.Lerp(currentSlideSpeed, autoSlideSpeed, 0.4f);
            currentPosX += currentSlideSpeed * Time.deltaTime;
            if (currentPosX > autoSlideTargetPos)
            {
                currentPosX = autoSlideTargetPos;
                currentState = SlideState.None;
                PageUpdate();
            }
        }
    }

    void PageUpdate()
    {
        currentPage = (int)((-currentPosX + 1) / (itemsPerPage * itemWidth));
    }
#endregion

}
