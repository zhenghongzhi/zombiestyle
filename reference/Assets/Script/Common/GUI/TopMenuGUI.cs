using UnityEngine;
using System.Collections;

public class TopMenuGUI : MonoBehaviour {
    public Texture2D infoBg;
    public Texture2D goldIcon;
    public Texture2D diamondIcon;
    public Texture2D expIcon;
    public Texture2D lifeIcon;
    public Texture2D expValueBar;
    public Texture2D expValueBarEnd;
    public Texture2D lifeValueBar;
    public Texture2D lifeValueBarEnd;
    public GUISkin mySkin;
    public bool showBack;

    GUIStyle numberStyle;
    GUIStyle levelStyle;

    GlobalPlayerInfo playerInfo;

    float topY;
    bool slideIn = true;

    void Awake()
    {
        topY = -CsGUI.CY(100);
    }

    void Start()
    {
        playerInfo = GameObject.Find("GlobalData").GetComponent<GlobalPlayerInfo>();
        numberStyle = new GUIStyle();
        numberStyle.normal.textColor = Color.white;
        numberStyle.fontSize = (int)CsGUI.CY(25);
        numberStyle.fontStyle = FontStyle.Bold;
        levelStyle = new GUIStyle();
        levelStyle.normal.textColor = Color.white;
        levelStyle.fontSize = (int)CsGUI.CY(15);
        levelStyle.fontStyle = FontStyle.Bold;
        levelStyle.alignment = TextAnchor.UpperCenter;
        DontDestroyOnLoad(gameObject);
    }

	// Update is called once per frame
	void Update () {
        if (slideIn)
        {
            if (CsGUI.SlideIntoPosition(ref topY, 0))
                slideIn = false;
        }
	}

    void OnGUI()
    {
        GUI.skin = mySkin;
        GUI.depth = 10;
        GUI.DrawTexture(CsGUI.CRect(685, topY, 256, 57), infoBg);
        GUI.DrawTexture(CsGUI.CRect(480, topY, 256, 57), infoBg);
        GUI.DrawTexture(CsGUI.CRect(275, topY, 256, 57), infoBg);
        GUI.DrawTexture(CsGUI.CRect(70, topY, 256, 57), infoBg);

        int expPercentage = 100 * playerInfo.SelectedCharData.currentExp / StaticGameData.GetLevelExp(playerInfo.SelectedCharData.currentLevel);
        float barWidth = 1.43f * expPercentage;
        GUI.DrawTexture(CsGUI.CRect(547, topY + 6, barWidth, 32), expValueBar, ScaleMode.StretchToFill);
        GUI.DrawTexture(CsGUI.CRect(547 + barWidth, topY + 6, 22, 32), expValueBarEnd);
        GUI.DrawTexture(CsGUI.CRect(750, topY + 6, 150, 32), lifeValueBar, ScaleMode.StretchToFill);
        GUI.DrawTexture(CsGUI.CRect(900, topY + 6, 22, 32), lifeValueBarEnd);

        GUI.DrawTexture(CsGUI.CRect(109, topY + 2, 40, 40), goldIcon);
        GUI.DrawTexture(CsGUI.CRect(318, topY + 2, 40, 40), diamondIcon);
        GUI.DrawTexture(CsGUI.CRect(524, topY + 1, 47, 46), expIcon);
        GUI.DrawTexture(CsGUI.CRect(727, topY, 43, 50), lifeIcon);


        numberStyle.alignment = TextAnchor.UpperRight;
        GUI.Label(CsGUI.CRect(150, topY + 9, 130, 25), playerInfo.playerData.gold.ToString(), numberStyle);
        GUI.Label(CsGUI.CRect(355, topY + 9, 130, 25), playerInfo.playerData.diamond.ToString(), numberStyle);
        
        GUI.contentColor = Color.black;
        GUI.Label(CsGUI.CRect(536, topY + 16, 20, 20), playerInfo.SelectedCharData.currentLevel.ToString(), levelStyle);
        GUI.contentColor = Color.white;
        GUI.Label(CsGUI.CRect(534, topY + 14, 20, 20), playerInfo.SelectedCharData.currentLevel.ToString(), levelStyle);

        GUI.Label(CsGUI.CRect(570, topY + 10, 80, 25), expPercentage + "%", numberStyle);

        if (GUI.Button(CsGUI.CRect(272, topY, 50, 50), "", "noneButton") || GUI.Button(CsGUI.CRect(285, topY + 10, 24, 24), "", "btnBuyMoney"))
        {
            playerInfo.playerData.gold += 5000;
        }
        if (GUI.Button(CsGUI.CRect(477, topY, 50, 50), "", "noneButton") || GUI.Button(CsGUI.CRect(490, topY + 10, 24, 24), "", "btnBuyMoney"))
        {
            playerInfo.playerData.diamond += 10;
        }
        if (GUI.Button(CsGUI.CRect(82, topY, 50, 50), "", "noneButton") || GUI.Button(CsGUI.CRect(695, topY + 10, 24, 24), "", "btnBuyMoney"))
        {
            playerInfo.SelectedCharData.currentLevel += 1;
        }
        if (GUI.Button(CsGUI.CRect(887, topY, 50, 50), "", "noneButton") || GUI.Button(CsGUI.CRect(900, topY + 10, 24, 24), "", "btnBuyMoney"))
        {

        }

        if (showBack)
        {
            if (GUI.Button(CsGUI.CRect(-5, 0, 120, 83), "", "btnBack"))
            {
                Camera.mainCamera.gameObject.SendMessage("Back");
            }
        }

    }
}
