using UnityEngine;
using System.Collections;

/// <summary>
/// Add to An Empty GameObject to Draw 2D Background in 3D Scene.
/// The GameObject Must in Camera's Frustum, Otherwise it'll be Cut off,
/// And the Background won't Show Up
/// </summary>
[RequireComponent(typeof(MeshRenderer))]
[RequireComponent(typeof(MeshFilter))]
public class Background : MonoBehaviour {
    public Texture2D background;//background Texture
    public Shader shader;//draw background shader "Custom/ScreenAliasBackground"

    Mesh screeAliasQuad;
    Material mat;

	void Awake()
    {
        //Create A Quad Mesh
        screeAliasQuad = new Mesh();

        Vector3[] verts = new Vector3[4];
        verts[0] = new Vector3(-1, -1, 0);
        verts[1] = new Vector3(1, -1, 0);
        verts[2] = new Vector3(1, 1, 0);
        verts[3] = new Vector3(-1, 1, 0);
        screeAliasQuad.vertices = verts;

        int[] indexs = {0, 2, 1, 2, 0, 3};
        screeAliasQuad.triangles = indexs;

        Vector2[] uvs = {new Vector2(0, 0), new Vector2(1, 0), new Vector2(1, 1), new Vector2(0, 1)};
        screeAliasQuad.uv = uvs;

        GetComponent<MeshFilter>().mesh = screeAliasQuad;

        //Create Material
        mat = new Material(shader);
        mat.mainTexture = background;
        renderer.material = mat; 
    }

    void OnDestroy()
    {
        if (mat)
            DestroyImmediate(mat);
        if (screeAliasQuad)
            DestroyImmediate(screeAliasQuad);
    }
}
