using UnityEngine;
using System.Collections;

public enum UIRelative
{
    AllRelative,
    PositionRelative,
    SizeRelative,
}

public class CsGUI {

    private static float xRate = 0;
    private static float yRate = 0;

    public static float XRate
    {
        get
        {
            if (xRate <= 0)
                xRate = Screen.width / 1024.0f;
            return xRate;
        }
    }

    public static float YRate
    {
        get
        {
            if (yRate <= 0)
                yRate = Screen.height / 768.0f;
            return yRate;
        }
    }

    public static void Reset()
    {
        xRate = Screen.width / 1024.0f;
        yRate = Screen.height / 768.0f;
    }

    public static Rect CRect(float x, float y, float width, float height, UIRelative relative = UIRelative.AllRelative)
    {
        switch (relative)
        {
            case UIRelative.AllRelative:
                return new Rect(x * XRate, y * YRate, width * XRate, height * YRate);
            case UIRelative.PositionRelative:
                return new Rect(x * XRate, y * YRate, width, height);
            case UIRelative.SizeRelative:
                return new Rect(x, y, width * XRate, height * YRate);
        }
        return new Rect();
    }

    public static float CX(float x)
    {
        return x * XRate;
    }

    public static float CY(float y)
    {
        return y * YRate;
    }

    public static bool SlideIntoPosition(ref float pos, float target)
    {
        if (pos == target)
            return true;
        float diff = target - pos;
        float speed = diff * 50;
        if (speed > 0)
            speed = Mathf.Clamp(speed, 300, 1200);
        else// (speed < 0)
            speed = Mathf.Clamp(speed, -1200, -300);
        pos += speed * Time.deltaTime;
        if ((target - pos) * diff < 0)
        {
            pos = target;
            return true;
        }
        return false;
    }

    public static bool MultiTouchButton(Rect rect, string content, GUIStyle style)
    {
#if UNITY_EDITOR || UNITY_STANDALONE_WIN
        return GUI.Button(rect, content, style);
#endif
#if !UNITY_EDITOR && (UNITY_IPHONE || UNITY_ANDROID)
        GUI.Label(rect, content, style);
        foreach (Touch t in Input.touches)
        {
            if(t.phase == TouchPhase.Began && rect.Contains(new Vector2(t.position.x, Screen.height - t.position.y)))
            {
                return true;
            }
        }
        return false;
#endif
    }

    public static Vector3 MousePosition
    {
        get
        {
#if UNITY_EDITOR || UNITY_STANDALONE_WIN
            return new Vector3(Input.mousePosition.x, Screen.height - Input.mousePosition.y, Input.mousePosition.z);
#endif
#if !UNITY_EDITOR && (UNITY_IPHONE || UNITY_ANDROID)
            return new Vector3(Input.touches[0].position.x, Screen.height - Input.mousePosition.y, Input.mousePosition.z);
#endif
        }
    }
}
