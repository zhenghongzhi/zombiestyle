using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class LoadingGUI : MonoBehaviour {

    public Texture2D loadingLogo;
    public Texture2D loadingBg;
    public Texture2D loadingBarBg;
    public Texture2D loadingBar;
    AsyncOperation loadAsync = null;
    bool enableScripts = false;
    bool loadComplete = false;
    float progress = 0;

	// Use this for initialization
	void Start () {
        loadAsync = Application.LoadLevelAdditiveAsync(GameObject.Find("GlobalData").GetComponent<GlobalPlayerInfo>().nextLevelName);
	}
	
	// Update is called once per frame
	void Update () {
        if (loadAsync.isDone && !enableScripts)
        {
            enableScripts = true;
            StartCoroutine("EnableScripts");
        }
        if (loadComplete)
            Destroy(gameObject);
	}

    IEnumerator EnableScripts()
    {
        List<MonoBehaviour> monoScipts = new List<MonoBehaviour>();
        GameObject g = null;

        PlayerScriptInit();
        progress += 0.3f;
        yield return new WaitForEndOfFrame();

        g = GameObject.FindGameObjectWithTag("MainCamera");
        foreach (MonoBehaviour m in g.GetComponents<MonoBehaviour>())
        {
            if (!m.enabled)
                monoScipts.Add(m);
        }

        g = GameObject.FindGameObjectWithTag("ZombieSpawner");
        if(g)
        {
            foreach (MonoBehaviour m in g.GetComponents<MonoBehaviour>())
            {
                if (!m.enabled)
                    monoScipts.Add(m);
            }
        }

        float deltaProgress = 0.7f / (float)monoScipts.Count;
        foreach (MonoBehaviour m in monoScipts)
        {
            m.enabled = true;
            progress += deltaProgress;
            yield return new WaitForEndOfFrame();
        }

        loadComplete = true;
    }

    void PlayerScriptInit()
    {
        GameObject player = GameObject.FindGameObjectWithTag("Player");
        player.GetComponent<Rigidbody>().isKinematic = false;
        player.GetComponent<PlayerAnimation>().enabled = true;
        player.GetComponent<PlayerMovementMoter>().enabled = true;
        player.GetComponent<TPSController>().enabled = true;
        player.GetComponent<PadInput>().enabled = true;
        player.GetComponent<SpecialSkillsController>().enabled = true;
        player.GetComponent<SpecialSkillsController>().LoadSkills();
        player.transform.FindChild("light").gameObject.SetActive(true);
        player.transform.FindChild("shadows").gameObject.SetActive(true);
        player.animation.Play();
        player.SetActive(true);
        CharacterStoreData charData = GameObject.Find("GlobalData").GetComponent<GlobalPlayerInfo>().SelectedCharData;
        player.GetComponent<Weapon>().InitWeapon(StaticGameData.GetWeaponData(charData.outfit.currentGunID), 
            player.GetComponent<CharacterStatus>(),
            charData.GetGun(charData.outfit.currentGunID).grade);
    }

    void OnGUI()
    {
        if (loadAsync != null)
        {
            float p = loadAsync.progress * .5f + progress * 0.5f;
            GUI.DrawTexture(CsGUI.CRect(0, 0, 1024, 768), loadingBg);
            GUI.DrawTexture(CsGUI.CRect(215, 200, 589, 359), loadingLogo);
            GUI.DrawTexture(CsGUI.CRect(210, 600, 602, 19), loadingBarBg);
            GUI.DrawTextureWithTexCoords(CsGUI.CRect(210, 600, 602 * p, 19), loadingBar, new Rect(0, 0, p, 1));
        }
    }
}
