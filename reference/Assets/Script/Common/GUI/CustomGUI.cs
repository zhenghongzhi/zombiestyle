using UnityEngine;
using System.Collections;

public class CustomGUI{

	public static float xRate;
	public static float yRate;

	public static void GetUse()
	{
		xRate = Screen.width / 1024.0f;
		yRate = Screen.height / 768.0f;
	}

	public static Rect CRect(float x, float y, float width, float height)
	{
		return new Rect(x * xRate, y * yRate, width * xRate, height * yRate);
	}
}
