using UnityEngine;
using System.Collections;

[System.Serializable]
public class HorizontalSlider
{
    public Texture2D scrollbarBg = null;
    public Texture2D valueBar = null;
    public Texture2D thumb = null;
    bool scrolling = false;

    public HorizontalSlider()
    {
        scrolling = false;
    }

    public HorizontalSlider(Texture2D bg, Texture2D thumb = null, Texture2D value = null)
    {
        scrollbarBg = bg;
        this.thumb = thumb;
        valueBar = value;
        scrolling = false;
    }

    public float Draw(Rect rect, float start, float end, float current)
    {
        return Draw(rect, start, end, current, new Vector2(thumb.width, thumb.height));
    }

    public float Draw(Rect rect, float start, float end, float current, Vector2 thumbSize)
    {
        GUI.DrawTexture(rect, scrollbarBg);
        float lerp = Mathf.InverseLerp(start, end, current);
        if (valueBar)
        {
            GUI.DrawTextureWithTexCoords(
                new Rect(rect.x, rect.y, rect.width * lerp, rect.height),
                valueBar,
                new Rect(0, 0, lerp, 1));
        }
        if (thumb)
        {
            float lx = rect.x + rect.width * lerp - thumbSize.x * .5f;
            float ly = rect.y + rect.height * 0.5f - thumbSize.y * .5f;
            Rect thumbRect = new Rect(lx, ly, thumbSize.x, thumbSize.y);
            GUI.DrawTexture(thumbRect, thumb, ScaleMode.StretchToFill);
            if(Input.GetMouseButtonDown(0) &&
                thumbRect.Contains(new Vector2(Input.mousePosition.x, Screen.height - Input.mousePosition.y)))
            {
                scrolling = true;
            }
            if (scrolling)
            {
                lerp = Mathf.InverseLerp(rect.x, rect.x + rect.width, Input.mousePosition.x);
                current = Mathf.Lerp(start, end, lerp);
                if (Input.GetMouseButtonUp(0))
                    scrolling = false;
            }
        }
        return current;
    }

}
