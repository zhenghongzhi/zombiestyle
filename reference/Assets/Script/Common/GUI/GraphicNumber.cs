using UnityEngine;
using System.Collections;


public enum GraphNumberAnchor
{
    LeftTop = 0,
    MiddleTop,
    RightTop,
}

public class GraphNumber
{
    Texture2D numberTex;
    Texture2D dotTex;
    float charWidth;
    float charHeight;
    float startX;
    float startY;
    GraphNumberAnchor anchor;
    int[] numbers;

    //Need .Net 4.0 to Support Default Parameter
    public GraphNumber(Texture2D number, Texture2D dot = null)
    {
        numberTex = number;
        dotTex = dot;
        charWidth = numberTex.width * 0.1f;
        charHeight = numberTex.height;
    }

    public void SetDraw(float x, float y, GraphNumberAnchor anchor = GraphNumberAnchor.RightTop)
    {
        startX = x;
        startY = y;
        this.anchor = anchor;
    }

    public void SetNumber(int number)
    {
        char[] nums = number.ToString().ToCharArray();
        numbers = new int[nums.Length];
        for (int i = 0; i < numbers.Length; ++i)
        {
            numbers[i] = nums[i] - '0';
        }
    }

    public void SetNumberToDraw(float x, float y, int number)
    {
        char[] nums = number.ToString().ToCharArray();
        numbers = new int[nums.Length];
        for (int i = 0; i < numbers.Length; ++i)
        {
            numbers[i] = nums[i] - '0';
        }
    }

    public void DrawNumber()
    {
        float x = 0;
        switch (anchor)
        {
            case GraphNumberAnchor.RightTop:
                x = startX - numbers.Length * charWidth;
                if (numbers.Length > 4 && dotTex)
                    x -= dotTex.width;
                break;
            case GraphNumberAnchor.LeftTop:
                x = startX;
                break;
            case GraphNumberAnchor.MiddleTop:
                x = startX - numbers.Length * charWidth * 0.5f;
                break;
        }
        for (int i = 0; i < numbers.Length; ++i)
        {
            if (i != 0 && (numbers.Length - i) % 4 == 0)
            {
                GUI.DrawTexture(CsGUI.CRect(x, startY, dotTex.width, charHeight), dotTex);
                x += dotTex.width;
            }
            GUI.DrawTextureWithTexCoords(
                CsGUI.CRect(x, startY, charWidth, charHeight),
                numberTex,
                new Rect(0.1f * numbers[i], 0, 0.1f, 1)
            );
            x += charWidth;
        }
    }
}