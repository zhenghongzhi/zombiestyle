using UnityEngine;
using System.Collections;
using System;

public class InnateManager
{
    /// <summary>
    /// 判断该天赋能否在Cd中
    /// </summary>
    /// <param name="id">天赋id</param>
    /// <param name="level">当前天赋等级</param>
    /// <returns>在cd中返回true，不能升级</returns>
    public static bool IsInCD(int id, int level)
    {
        var myInnate = PlayerStoreData.StoredData.SelectedChar.GetInnate(id);
        if (myInnate == null || myInnate.upgradeTime == null)
        {
            return false;
        }
        DateTime lastTime = DateTime.Parse(myInnate.upgradeTime);
        TimeSpan interval = DateTime.Now - lastTime;

        var data = StaticGameData.GetInnate(id, level);

        if (interval.TotalMinutes >= data.InnateCDTime)
        {
            return false;
        }
        
        return true;
    }

    /// <summary>
    /// 升级天赋
    /// </summary>
    /// <param name="id"></param>
    /// <param name="level"></param>
    /// <returns></returns>
    public static bool Upgrade(int id, int level)
    {
        if (IsInCD(id, level))
        {
            return false;
        }

        //TODO 判断金钱是否足够 ？怎么使用钻石强制升级

        //判断是否满足等级需求
        var myChar = PlayerStoreData.StoredData.SelectedChar;
        var data = StaticGameData.GetInnate(id, level);
        if (myChar.currentLevel < data.NeedCharLV)
        {
            return false;
        }               

        //升级
        PlayerStoreData.StoredData.SelectedChar.UpgradeInnate(id);
        
        return false;
    }

    /// <summary>
    /// 获取距离可升级剩余的CD时间（分钟），主要用于UI显示
    /// </summary>
    /// <param name="id">天赋id</param>
    /// <param name="level">当前天赋等级</param>
    /// <returns>剩余的分钟数</returns>
    public static int GetLeftCDTime(int id, int level)
    {
        var myInnate = PlayerStoreData.StoredData.SelectedChar.GetInnate(id);
        if (myInnate == null || myInnate.upgradeTime == null)
        {
            return 0;
        }
        DateTime lastTime = DateTime.Parse(myInnate.upgradeTime);
        TimeSpan interval = DateTime.Now - lastTime;

        var data = StaticGameData.GetInnate(id, level);

        if (interval.TotalMinutes >= data.InnateCDTime)
        {
            return 0;
        }

        return data.InnateCDTime - (int)interval.TotalMinutes;
    }
}
