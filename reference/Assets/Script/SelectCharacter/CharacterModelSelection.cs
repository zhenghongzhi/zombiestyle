using UnityEngine;
using System.Collections.Generic;

public class CharacterModelSelection : MonoBehaviour
{
    public GlobalPlayerInfo playerInfo;
    List<GameObject> characters;

    public Transform[] standPositions;
    public float screenPixelWidth = 1;//Slide Speed Adapter Value. eg. When Build on new Pad this value need to set to .5f to get same slide experience on Pad2

    [HideInInspector]
    public int currentModelIndex = 0;
    float rotateLerp = 0;//Use Vector3.Slerp to Rotate Model's Position between 4 Fixed Positions
    bool change = false;
    float lastPosX = 0;
    float slideSpeed = 0;
    TouchPhase touchPhase = TouchPhase.Canceled;
    float restTimeCounter = 0;

    void Start()
    {
        characters = new List<GameObject>();
        GenerateCharacterModel();
        SetStandPosition(playerInfo.SelectedCharID - 1);
    }

	// Update is called once per frame
	void Update () {

        restTimeCounter += Time.deltaTime;
        if (restTimeCounter > 10)
        {
            characters[currentModelIndex].animation.CrossFade(characters[currentModelIndex].GetComponent<PlayerAnimation>().rest.name);
            restTimeCounter = 0;
        }

#region Use Mouse to Switch Character in Editor
#if UNITY_EDITOR || UNITY_STANDALONE_WIN
        if (touchPhase == TouchPhase.Began)
        {
            touchPhase = TouchPhase.Moved;
        }
        else if(touchPhase == TouchPhase.Moved)
        {
            float deltaX = Input.mousePosition.x - lastPosX;
            lastPosX = Input.mousePosition.x;
            rotateLerp += deltaX / Screen.width;
            RotatePosition(rotateLerp < 0, Mathf.Abs(rotateLerp));
            if (Input.GetMouseButtonUp(0))
            {
                touchPhase = TouchPhase.Ended;
                float speed = deltaX / Time.deltaTime;
                if ((rotateLerp < 0 && currentModelIndex == 3) || (rotateLerp > 0 && currentModelIndex == 0))
                    change = false;
                else if (Mathf.Abs(speed) > 100 && speed * rotateLerp > 0 || Mathf.Abs(rotateLerp) > 0.3f)
                    change = true;
                else
                    change = false;
            }
        }
        else if(touchPhase == TouchPhase.Ended)
        {
            touchPhase = TouchPhase.Canceled;
        }
        else if(touchPhase == TouchPhase.Canceled)
        {
            //Automatically Rotate the Draft Round After Finger Untouched
            if (rotateLerp != 0)
            {
                if (change)//Continue the rotation. Increase rotateLerp's magnitude to 1.
                {
                    rotateLerp += 2 * (rotateLerp > 0 ? Time.deltaTime : -Time.deltaTime);
                    if(Mathf.Abs(rotateLerp) > 1)
                    {
                        SetStandPosition(currentModelIndex + (rotateLerp < 0 ? 1 : -1));
                        rotateLerp = 0;
                    }
                    RotatePosition(rotateLerp < 0, Mathf.Abs(rotateLerp));
                }
                else//Back to Original Position
                {
                    RotatePosition(rotateLerp < 0, Mathf.Abs(rotateLerp));
                    rotateLerp -= rotateLerp > 0 ? Time.deltaTime : -Time.deltaTime;
                    if(Mathf.Abs(rotateLerp) < .05f)
                    {
                        SetStandPosition(currentModelIndex);
                        rotateLerp = 0;
                    }
                }
            }
            if (Input.GetMouseButtonDown(0) && CsGUI.CRect(200, 0, 600, 618).Contains(Input.mousePosition))
            {
                touchPhase = TouchPhase.Began;
                lastPosX = Input.mousePosition.x;
            }
        }
#endif
#endregion

#region Use Finger Touch to Switch Character on Pad
#if !UNITY_EDITOR && (UNITY_ANDROID || UNITY_IPHONE)
         if(touchPhase == TouchPhase.Moved)
        {
            float deltaX = Input.touches[0].deltaPosition.x;
            lastPosX = Input.touches[0].position.x;
            rotateLerp += deltaX * 3 / Screen.width;
            RotatePosition(rotateLerp < 0, Mathf.Abs(rotateLerp));
            if (Input.touchCount == 0 || Input.touches[0].phase == TouchPhase.Ended || Input.touches[0].phase == TouchPhase.Canceled)
            {
                touchPhase = TouchPhase.Canceled;
                float speed = deltaX / Time.deltaTime;
                slideSpeed = deltaX  * 3 / Screen.width;
                if ((rotateLerp < 0 && currentModelIndex == 3) || (rotateLerp > 0 && currentModelIndex == 0))
                    change = false;
                else if (Mathf.Abs(speed) > 100 && speed * rotateLerp > 0 || Mathf.Abs(rotateLerp) > 0.3f)
                    change = true;
                else
                    change = false;
            }
        }
        else if(touchPhase == TouchPhase.Canceled)
        {
            //Automatically Rotate the Draft Round After Finger Untouched
            if (rotateLerp != 0)
            {
			slideSpeed = Mathf.Lerp(slideSpeed, 2, .2f);
                if (change)//Continue the rotation. Increase rotateLerp's magnitude to 1.
                {
                    RotatePosition(rotateLerp < 0, Mathf.Abs(rotateLerp));
                    rotateLerp += slideSpeed * (rotateLerp > 0 ? Time.deltaTime : -Time.deltaTime);
                    if(Mathf.Abs(rotateLerp) > 1)
                    {
                        SetStandPosition(currentModelIndex + (rotateLerp < 0 ? 1 : -1));
                        rotateLerp = 0;
                    }
                }
                else//Back to Original Position
                {
                    RotatePosition(rotateLerp < 0, Mathf.Abs(rotateLerp));
                    rotateLerp -= slideSpeed * (rotateLerp > 0 ? Time.deltaTime : -Time.deltaTime);
                    if(Mathf.Abs(rotateLerp) < .05f)
                    {
                        SetStandPosition(currentModelIndex);
                        rotateLerp = 0;
                    }
                }
            }
            if (Input.touchCount == 1 && CsGUI.CRect(200, 0, 600, 618).Contains(Input.touches[0].position))
            {
                touchPhase = TouchPhase.Moved;
                lastPosX = Input.touches[0].position.x;
            }
        }
#endif
#endregion
    }

    void OnDestroy()
    {
        Resources.UnloadUnusedAssets();
    }

    //Auto Rotate
    void RotatePosition(bool isLeft, float lerp)
    {
        if(isLeft && currentModelIndex < 3)
        {
            characters[0].transform.position = Vector3.Lerp(standPositions[3 - currentModelIndex].position, standPositions[2 - currentModelIndex].position, lerp);
            characters[1].transform.position = Vector3.Lerp(standPositions[4 - currentModelIndex].position, standPositions[3 - currentModelIndex].position, lerp);
            characters[2].transform.position = Vector3.Lerp(standPositions[5 - currentModelIndex].position, standPositions[4 - currentModelIndex].position, lerp);
            characters[3].transform.position = Vector3.Lerp(standPositions[6 - currentModelIndex].position, standPositions[5 - currentModelIndex].position, lerp);
        }
        else if(!isLeft && currentModelIndex > 0)
        {
            characters[0].transform.position = Vector3.Lerp(standPositions[3 - currentModelIndex].position, standPositions[4 - currentModelIndex].position, lerp);
            characters[1].transform.position = Vector3.Lerp(standPositions[4 - currentModelIndex].position, standPositions[5 - currentModelIndex].position, lerp);
            characters[2].transform.position = Vector3.Lerp(standPositions[5 - currentModelIndex].position, standPositions[6 - currentModelIndex].position, lerp);
            characters[3].transform.position = Vector3.Lerp(standPositions[6 - currentModelIndex].position, standPositions[7 - currentModelIndex].position, lerp);
        }
        //Always Look at Camera
        foreach (GameObject c in characters)
        {
            c.transform.LookAt(Camera.mainCamera.transform);
            c.transform.eulerAngles = new Vector3(0, c.transform.eulerAngles.y, 0);
        }
    }

    //Change Current Selected Character Index & Decide Stand Position
    public void SetStandPosition(int selectedCharIndex)
    {
        //Change Selected Index
        if (selectedCharIndex > 3)
            selectedCharIndex = 0;
        else if (selectedCharIndex < 0)
            selectedCharIndex = 3;
        currentModelIndex = selectedCharIndex;

        characters[currentModelIndex].animation.CrossFade(characters[currentModelIndex].GetComponent<PlayerAnimation>().rest.name);
        restTimeCounter = 0;
        SendMessage("ChangePlayer", currentModelIndex + 1);

        rotateLerp = 0;
        characters[0].transform.position = standPositions[3 - currentModelIndex].position;
        characters[1].transform.position = standPositions[4 - currentModelIndex].position;
        characters[2].transform.position = standPositions[5 - currentModelIndex].position;
        characters[3].transform.position = standPositions[6 - currentModelIndex].position;
        //Always Look at Camera
        foreach (GameObject c in characters)
        {
            c.transform.LookAt(Camera.mainCamera.transform);
            c.transform.eulerAngles = new Vector3(0, c.transform.eulerAngles.y, 0);
        }
    }

    void GenerateCharacterModel()
    {
        foreach (CharacterStoreData c in playerInfo.playerData.charData)
        {
            //Gain Weapon Info
            WeaponData weaponData = StaticGameData.GetWeaponData(c.outfit.currentGunID);
            if (weaponData == null)
            {
                Debug.LogError("No Weapon of Index of " + c.outfit.currentGunID + " for Character " + c.charID);
                continue;
            }
            
            //Load Character&Weapon Prefab
            string objectName = c.charName + "-" + weaponData.weaponType;
            GameObject character = CharacterModelManager.CreateNewCharacter(objectName, weaponData);
            characters.Add(character);

            //Change Submesh and Materials based on Outfit Data
        }
    }

    public void OnLoadNextLevel()
    {
        characters[currentModelIndex].name = "Player";
        DontDestroyOnLoad(characters[currentModelIndex]);
    }

}
