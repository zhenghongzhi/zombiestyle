using UnityEngine;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Xml;
using System.Xml.Serialization;
using System.Collections;

public class GUISelectCharacter : MonoBehaviour {
    public GlobalPlayerInfo playerInfo;
    public Texture2D leftInfoBg;

    public GUISkin mySkin;
    AsyncOperation async;
    bool isLoading = false;
    TextScript scripts;

    GUIStyle charNameStyle;
    GUIStyle charDsrbStyle;
    GUIStyle startStyle;

    void Start()
    {
        CsGUI.Reset();
        startStyle = new GUIStyle(mySkin.GetStyle("btnStart"));
        startStyle.fontSize = (int)CsGUI.CY(startStyle.fontSize);
        startStyle.padding.top = (int)CsGUI.CY(startStyle.padding.top);
        charNameStyle = new GUIStyle();
        charNameStyle.normal.textColor = Color.white;
        charNameStyle.fontSize = (int)CsGUI.CY(30);
        charNameStyle.fontStyle = FontStyle.Bold;
        charNameStyle.alignment = TextAnchor.UpperCenter;
        charDsrbStyle = new GUIStyle();
        charDsrbStyle.normal.textColor = Color.white;
        charDsrbStyle.fontSize = (int)CsGUI.CY(20);
        charDsrbStyle.fontStyle = FontStyle.Bold;
        charDsrbStyle.wordWrap = true;
        charDsrbStyle.richText = true;
        GameObject.Find("TopMenuGUI").GetComponent<TopMenuGUI>().showBack = false;
        StaticGameScripts.LoadScriptResource(GameObject.Find("GlobalData").GetComponent<GlobalPlayerInfo>().language);
        scripts = StaticGameScripts.GetSceneScript("SelectCharacter");
    }

    void OnGUI()
    {
        GUI.skin = mySkin;
        if (isLoading)
        {
          //  GUI.DrawTexture(new Rect(0, 0, Screen.width, Screen.height), )
            GUI.Label(CsGUI.CRect(400, 400, 100, 100), async.progress.ToString());
        }

        GUI.DrawTexture(CsGUI.CRect(0, 120, 376, 546), leftInfoBg);
        GUI.Label(CsGUI.CRect(114, 195, 100, 40), scripts.GetScript(playerInfo.SelectedCharID - 1), charNameStyle);
        GUI.Label(CsGUI.CRect(45, 230, 255, 300), scripts.GetScript(3 + playerInfo.SelectedCharID), charDsrbStyle);
        GUI.Label(CsGUI.CRect(120, 600, 200, 40), CsGUI.YRate + " | " + Screen.height);
        if (GUI.Button(CsGUI.CRect(794, 595, 241, 134), scripts.GetScript(8), startStyle))
        {
            SendMessage("OnLoadNextLevel");
            isLoading = true;
            async = Application.LoadLevelAsync("MainMenu");
        }
    }

    public void ChangePlayer(int charID)
    {
        playerInfo.SelectedCharID = charID;
    }
}
