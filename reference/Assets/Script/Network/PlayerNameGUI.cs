using UnityEngine;
using System.Collections;

//Attached to A GameObject With GUIText Component
//The Script Auto Set the GameObjects Position so That
//The Text Always Follow the Player Model in Screen
public class PlayerNameGUI : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
        Vector3 headPosition = transform.parent.position + new Vector3(0, 2, 0);
        transform.position = Camera.mainCamera.WorldToViewportPoint(headPosition);
	}
}
