using UnityEngine;
using System.Collections;
using Wit;

public enum EventType
{
    SESSION_START,
    SESSION_CLOSE,
    SESSION_CONNECT,
    SESSION_PROTOCOL,
}

public class EventUnit
{
    public EventType Type;
    public uint Sid;
    public WitProtocol Protocol;

    public EventUnit(EventType eType, uint iSid, WitProtocol pProto = null)
    {
        Type = eType;
        Sid = iSid;
        Protocol = pProto;
    }
}

class ServerProxy : WitNetworkProxy
{
    public override bool OnSessionStart(uint iSid, WitSessionType iSessionType, WitSocketType iSocketType, WitIpAddress pAddr)
    {
        WitDebug.Log("Start Session. Id: " + iSid);
        if (NetworkManager.Instance.Sid == 0)
        {
            NetworkManager.Instance.Sid = iSid;
        }
        return true;
    }

    public override bool OnSessionClose(uint iSid)
    {
        WitDebug.Log("Close Session. Id: " + iSid);
        //List的Add和Remove是线程安全的
        NetworkManager.Instance.ServerProtoList.Add(new EventUnit(EventType.SESSION_CLOSE, iSid));
        return true;
    }

    public override bool OnSessionProtocol(uint iSid, WitProtocol pProto, WitIpAddress pAddr)
    {
        WitDebug.Log("On Protocol. Id: " + pProto.Type);
        //List的Add和Remove是线程安全的
        NetworkManager.Instance.ServerProtoList.Add(new EventUnit(EventType.SESSION_PROTOCOL, iSid, pProto));
        return true;
    }
}

class ClientProxy : WitNetworkProxy
{
    public override bool OnSessionStart(uint iSid, WitSessionType iSessionType, WitSocketType iSocketType, WitIpAddress pAddr)
    {
        WitDebug.Log("Start Session. Id: " + iSid);
        if (NetworkManager.Instance.Sid == 0)
        {
            NetworkManager.Instance.Sid = iSid;
        }
        return true;
    }

    public override bool OnSessionClose(uint iSid)
    {
        WitDebug.Log("Close Session. Id: " + iSid);
        NetworkManager.Instance.ClientProtoList.Add(new EventUnit(EventType.SESSION_CLOSE, iSid));
        return true;
    }

    public override bool OnSessionConnect(uint iSid)
    {
        WitDebug.Log("Connect Session. Id: " + iSid);
        NetworkManager.Instance.ClientProtoList.Add(new EventUnit(EventType.SESSION_CONNECT, iSid));
        return true;
    }

    public override bool OnSessionProtocol(uint iSid, WitProtocol pProto, WitIpAddress pAddr)
    {
        WitDebug.Log("On Protocol. Id: " + pProto.Type);
        //List的Add和Remove是线程安全的
        NetworkManager.Instance.ClientProtoList.Add(new EventUnit(EventType.SESSION_PROTOCOL, iSid, pProto));
        return true;
    }
}

