using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Wit;

public class NetworkManager 
{
    private List<INetworkModule> m_vModule;

    private bool m_bServer;
    public bool IsServer
    {
        get { return m_bServer; }
    }

    private uint m_iSid;
    public uint Sid
    {
        get { return m_iSid; }
        set { m_iSid = value; }
    }

    //由于Unity限制，很多操作不能在子线程中处理，从子线程获得的协议放到协议队列中，在主线程中处理
    private List<EventUnit> m_vServerProtoList;
    public List<EventUnit> ServerProtoList
    {
        get { return m_vServerProtoList; }
    }
    private List<EventUnit> m_vClientProtoList;
    public List<EventUnit> ClientProtoList
    {
        get { return m_vClientProtoList; }
    }

    /************************************************************************/
    /* Singleton                                                            */
    /************************************************************************/
    private static NetworkManager m_pNetManager;
    public static NetworkManager Instance
    {
        get 
        {
            if (m_pNetManager == null)
            {
                m_pNetManager = new NetworkManager();
            }
            return m_pNetManager; 
        }
    }

    private NetworkManager()
    {
        //注册协议
        Register.RegisterProtocol();

        m_vModule = new List<INetworkModule>();

        m_vClientProtoList = new List<EventUnit>();
        m_vServerProtoList = new List<EventUnit>();
    }

    public void AddModule(INetworkModule module)
    {
        m_vModule.Add(module);
    }

    public void RemoveModule(INetworkModule module)
    {
        m_vModule.Remove(module);
    }

    public void ProcessEvent()
    {
        if (m_vServerProtoList.Count>0)
        {
            bool bProcessed = false;
            EventUnit unit = m_vServerProtoList[0];
            foreach (INetworkModule module in m_vModule)
            {
                if (module.ServerProcessEvent(unit))
                {
                    bProcessed = true;
                    break;
                }
            }
            if (!bProcessed)
            {
                Debug.Log(unit.Type);
                Debug.LogError("*** Not Processed Event:" + unit.Type + " ProtocolId: " + (unit.Protocol != null ? unit.Protocol.Type : (ushort)0));
            }
            m_vServerProtoList.RemoveAt(0);
        }//end if

        if (m_vClientProtoList.Count > 0)
        {
            bool bProcessed = false;
            EventUnit unit = m_vClientProtoList[0];
            foreach (INetworkModule module in m_vModule)
            {
                if (module.ClientProcessEvent(unit))
                {
                    bProcessed = true;
                    break;
                }
            }
            if (!bProcessed)
            {
                Debug.LogError("*** Not Processed Event:" + unit.Type + " ProtocolId: " + (unit.Protocol != null ? unit.Protocol.Type : (ushort)0));
            }
            m_vClientProtoList.RemoveAt(0);
        }//end if
    }

    public bool StartServer()
    {
        ServerProxy pProxy = new ServerProxy();
        WitSessionManager.Instance.SetNetworkProxy(pProxy);
        WitSessionManager.Instance.StartServer(new WitIpAddress("*", CsConst.HOST_PORT), WitSocketType.TCP);

        m_bServer = true;

        return true;
    }

    public bool StartClient(string ip)
    {
        if (m_iSid != 0)
        {
            WitSessionManager.Instance.CloseSession(m_iSid);
            m_iSid = 0;
        }
        ClientProxy pProxy = new ClientProxy();
        WitSessionManager.Instance.SetNetworkProxy(pProxy);
        WitIpAddress sIp = new WitIpAddress(ip);
        sIp.Port = CsConst.HOST_PORT;
        WitSessionManager.Instance.StartClient(sIp, WitSocketType.TCP);

        return true;
    }

    public bool Close()
    {
        WitSessionManager.Instance.CloseAllSession();
        m_bServer = false;
        m_iSid = 0;
        return false;
    }


    public bool SendProtocol(WitProtocol pProto, uint iSid = 0)
    {
        if (iSid == 0)
        {
            iSid = m_iSid;
        }
        return WitSessionManager.Instance.SendProtocol(iSid, pProto);
    }

    public bool BroadcastProtocol(WitProtocol pProto, List<uint> vSid)
    {
        foreach (uint sid in vSid)
        {
            if (sid != m_iSid)
            {
                WitSessionManager.Instance.SendProtocol(sid, pProto);
            }
        }
        return true;
    }
}
