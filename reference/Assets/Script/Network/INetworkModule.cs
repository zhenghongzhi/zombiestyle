using UnityEngine;
using System.Collections;

public interface INetworkModule
{
    void Init();

    /// <summary>
    /// 客户端处理客户端收到的消息
    /// </summary>
    /// <param name="unit"></param>
    /// <returns></returns>
    bool ClientProcessEvent(EventUnit unit);

    /// <summary>
    /// 服务器处理服务器收到的消息
    /// </summary>
    /// <param name="unit"></param>
    /// <returns></returns>
    bool ServerProcessEvent(EventUnit unit);
}
