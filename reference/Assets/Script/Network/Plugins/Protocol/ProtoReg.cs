using Wit;

public class Register
{
	public static void RegisterProtocol()
	{
		WitProtocolManager.Instance.Register(new RequestRoom());
		WitProtocolManager.Instance.Register(new RequestRoomRe());
		WitProtocolManager.Instance.Register(new CreateRoom());
		WitProtocolManager.Instance.Register(new CloseRoom());
		WitProtocolManager.Instance.Register(new RoomSync());
		WitProtocolManager.Instance.Register(new ReqEnterRoom());
		WitProtocolManager.Instance.Register(new PlayerEnterRoom());
		WitProtocolManager.Instance.Register(new EnterRoomRe());
		WitProtocolManager.Instance.Register(new PrepareSync());
		WitProtocolManager.Instance.Register(new ReqLeaveRoom());
		WitProtocolManager.Instance.Register(new PlayerLeaveRoom());
		WitProtocolManager.Instance.Register(new EnterGameScene());
		WitProtocolManager.Instance.Register(new FinishLoading());
		WitProtocolManager.Instance.Register(new StartBattle());
		WitProtocolManager.Instance.Register(new StateSync());
		WitProtocolManager.Instance.Register(new StateCorrect());
	}
}
