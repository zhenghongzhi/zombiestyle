using System.Collections.Generic;
using Wit;

public class WitVector3:WitMarshalData
{
	//构造函数
	public WitVector3(float fX = 0, float fY = 0, float fZ = 0)
	{
		m_fX = fX;
		m_fY = fY;
		m_fZ = fZ;
	}
	//克隆函数
	public override WitMarshalData Clone()
	{
		return new WitVector3();
	}
	//Marshal函数
	public override WitOctetsStream Marshal(WitOctetsStream rhsOS)
	{
		rhsOS.Write(m_fX);
		rhsOS.Write(m_fY);
		rhsOS.Write(m_fZ);
		return rhsOS;
	}
	//UnMarshal函数
	public override WitOctetsStream Unmarshal(WitOctetsStream rhsOS)
	{
		rhsOS.Read(ref m_fX);
		rhsOS.Read(ref m_fY);
		rhsOS.Read(ref m_fZ);
		return rhsOS;
	}
	//成员
	public float	m_fX;
	public float	m_fY;
	public float	m_fZ;
};

//加载完游戏场景
public class FinishLoading:WitProtocol
{
	//构造函数
	public FinishLoading() : base((ushort)ProtocolId.FINISH_LOADING)
	{

	}
	//克隆函数
	public override WitProtocol Clone()
	{
		return new FinishLoading();
	}
	//Marshal函数
	public override WitOctetsStream Marshal(WitOctetsStream rhsOS)
	{
		return rhsOS;
	}
	//UnMarshal函数
	public override WitOctetsStream Unmarshal(WitOctetsStream rhsOS)
	{
		return rhsOS;
	}
	//成员
};

//开始战斗
public class StartBattle:WitProtocol
{
	//构造函数
	public StartBattle() : base((ushort)ProtocolId.START_BATTLE)
	{

	}
	//克隆函数
	public override WitProtocol Clone()
	{
		return new StartBattle();
	}
	//Marshal函数
	public override WitOctetsStream Marshal(WitOctetsStream rhsOS)
	{
		return rhsOS;
	}
	//UnMarshal函数
	public override WitOctetsStream Unmarshal(WitOctetsStream rhsOS)
	{
		return rhsOS;
	}
	//成员
};

//状态同步
public class StateSync:WitProtocol
{
	//构造函数
	public StateSync(uint iId = 0, WitVector3 sMoveDir = null, WitVector3 sFaceDir = null) : base((ushort)ProtocolId.STATE_SYNC)
	{
		m_iId = iId;
		if(sMoveDir==null)
			m_sMoveDir = new WitVector3();
		else
			m_sMoveDir = sMoveDir;
		if(sFaceDir==null)
			m_sFaceDir = new WitVector3();
		else
			m_sFaceDir = sFaceDir;
	}
	//克隆函数
	public override WitProtocol Clone()
	{
		return new StateSync();
	}
	//Marshal函数
	public override WitOctetsStream Marshal(WitOctetsStream rhsOS)
	{
		rhsOS.Write(m_iId);
		rhsOS.Write(m_sMoveDir);
		rhsOS.Write(m_sFaceDir);
		return rhsOS;
	}
	//UnMarshal函数
	public override WitOctetsStream Unmarshal(WitOctetsStream rhsOS)
	{
		rhsOS.Read(ref m_iId);
		rhsOS.Read(m_sMoveDir);
		rhsOS.Read(m_sFaceDir);
		return rhsOS;
	}
	//成员
	public uint	m_iId;
	public WitVector3	m_sMoveDir;
	public WitVector3	m_sFaceDir;
};

//状态校正
public class StateCorrect:WitProtocol
{
	//构造函数
	public StateCorrect(uint iId = 0, WitVector3 sPos = null, WitVector3 sRotate = null) : base((ushort)ProtocolId.STATE_CORRECT)
	{
		m_iId = iId;
		if(sPos==null)
			m_sPos = new WitVector3();
		else
			m_sPos = sPos;
		if(sRotate==null)
			m_sRotate = new WitVector3();
		else
			m_sRotate = sRotate;
	}
	//克隆函数
	public override WitProtocol Clone()
	{
		return new StateCorrect();
	}
	//Marshal函数
	public override WitOctetsStream Marshal(WitOctetsStream rhsOS)
	{
		rhsOS.Write(m_iId);
		rhsOS.Write(m_sPos);
		rhsOS.Write(m_sRotate);
		return rhsOS;
	}
	//UnMarshal函数
	public override WitOctetsStream Unmarshal(WitOctetsStream rhsOS)
	{
		rhsOS.Read(ref m_iId);
		rhsOS.Read(m_sPos);
		rhsOS.Read(m_sRotate);
		return rhsOS;
	}
	//成员
	public uint	m_iId;
	public WitVector3	m_sPos;
	public WitVector3	m_sRotate;
};

