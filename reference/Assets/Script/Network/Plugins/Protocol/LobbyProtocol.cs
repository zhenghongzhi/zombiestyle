using System.Collections.Generic;
using Wit;

public class RoomInfo:WitMarshalData
{
	//构造函数
	public RoomInfo(string sRoomName = "", uint iDungeon = 0, byte iPlayer = 0, bool bPassword = false)
	{
		m_sRoomName = sRoomName;
		m_iDungeon = iDungeon;
		m_iPlayer = iPlayer;
		m_bPassword = bPassword;
	}
	//克隆函数
	public override WitMarshalData Clone()
	{
		return new RoomInfo();
	}
	//Marshal函数
	public override WitOctetsStream Marshal(WitOctetsStream rhsOS)
	{
		rhsOS.Write(m_sRoomName);
		rhsOS.Write(m_iDungeon);
		rhsOS.Write(m_iPlayer);
		rhsOS.Write(m_bPassword);
		return rhsOS;
	}
	//UnMarshal函数
	public override WitOctetsStream Unmarshal(WitOctetsStream rhsOS)
	{
		rhsOS.Read(ref m_sRoomName);
		rhsOS.Read(ref m_iDungeon);
		rhsOS.Read(ref m_iPlayer);
		rhsOS.Read(ref m_bPassword);
		return rhsOS;
	}
	//成员
	public string	m_sRoomName;
	public uint	m_iDungeon;
	public byte	m_iPlayer;
	public bool	m_bPassword;
};

public class HostInfo:WitMarshalData
{
	//构造函数
	public HostInfo(string sHost = "", RoomInfo sRoom = null)
	{
		m_sHost = sHost;
		if(sRoom==null)
			m_sRoom = new RoomInfo();
		else
			m_sRoom = sRoom;
	}
	//克隆函数
	public override WitMarshalData Clone()
	{
		return new HostInfo();
	}
	//Marshal函数
	public override WitOctetsStream Marshal(WitOctetsStream rhsOS)
	{
		rhsOS.Write(m_sHost);
		rhsOS.Write(m_sRoom);
		return rhsOS;
	}
	//UnMarshal函数
	public override WitOctetsStream Unmarshal(WitOctetsStream rhsOS)
	{
		rhsOS.Read(ref m_sHost);
		rhsOS.Read(m_sRoom);
		return rhsOS;
	}
	//成员
	public string	m_sHost;
	public RoomInfo	m_sRoom;
};

public class PlayerInfo:WitMarshalData
{
	//构造函数
	public PlayerInfo(uint iId = 0, string sName = "", string sCharXml = "", byte iSlot = 0, bool bPrepared = false)
	{
		m_iId = iId;
		m_sName = sName;
		m_sCharXml = sCharXml;
		m_iSlot = iSlot;
		m_bPrepared = bPrepared;
	}
	//克隆函数
	public override WitMarshalData Clone()
	{
		return new PlayerInfo();
	}
	//Marshal函数
	public override WitOctetsStream Marshal(WitOctetsStream rhsOS)
	{
		rhsOS.Write(m_iId);
		rhsOS.Write(m_sName);
		rhsOS.Write(m_sCharXml);
		rhsOS.Write(m_iSlot);
		rhsOS.Write(m_bPrepared);
		return rhsOS;
	}
	//UnMarshal函数
	public override WitOctetsStream Unmarshal(WitOctetsStream rhsOS)
	{
		rhsOS.Read(ref m_iId);
		rhsOS.Read(ref m_sName);
		rhsOS.Read(ref m_sCharXml);
		rhsOS.Read(ref m_iSlot);
		rhsOS.Read(ref m_bPrepared);
		return rhsOS;
	}
	//成员
	public uint	m_iId;
	public string	m_sName;
	public string	m_sCharXml;
	public byte	m_iSlot;
	public bool	m_bPrepared;
};

//广播请求房间
public class RequestRoom:WitProtocol
{
	//构造函数
	public RequestRoom() : base((ushort)ProtocolId.ROOM_REQUEST)
	{

	}
	//克隆函数
	public override WitProtocol Clone()
	{
		return new RequestRoom();
	}
	//Marshal函数
	public override WitOctetsStream Marshal(WitOctetsStream rhsOS)
	{
		return rhsOS;
	}
	//UnMarshal函数
	public override WitOctetsStream Unmarshal(WitOctetsStream rhsOS)
	{
		return rhsOS;
	}
	//成员
};

//请求房间回复
public class RequestRoomRe:WitProtocol
{
	//构造函数
	public RequestRoomRe(RoomInfo sRoom = null) : base((ushort)ProtocolId.ROOM_REQUEST_RE)
	{
		if(sRoom==null)
			m_sRoom = new RoomInfo();
		else
			m_sRoom = sRoom;
	}
	//克隆函数
	public override WitProtocol Clone()
	{
		return new RequestRoomRe();
	}
	//Marshal函数
	public override WitOctetsStream Marshal(WitOctetsStream rhsOS)
	{
		rhsOS.Write(m_sRoom);
		return rhsOS;
	}
	//UnMarshal函数
	public override WitOctetsStream Unmarshal(WitOctetsStream rhsOS)
	{
		rhsOS.Read(m_sRoom);
		return rhsOS;
	}
	//成员
	public RoomInfo	m_sRoom;
};

//创建房间
public class CreateRoom:WitProtocol
{
	//构造函数
	public CreateRoom(RoomInfo sRoom = null) : base((ushort)ProtocolId.ROOM_CREATE)
	{
		if(sRoom==null)
			m_sRoom = new RoomInfo();
		else
			m_sRoom = sRoom;
	}
	//克隆函数
	public override WitProtocol Clone()
	{
		return new CreateRoom();
	}
	//Marshal函数
	public override WitOctetsStream Marshal(WitOctetsStream rhsOS)
	{
		rhsOS.Write(m_sRoom);
		return rhsOS;
	}
	//UnMarshal函数
	public override WitOctetsStream Unmarshal(WitOctetsStream rhsOS)
	{
		rhsOS.Read(m_sRoom);
		return rhsOS;
	}
	//成员
	public RoomInfo	m_sRoom;
};

//关闭房间
public class CloseRoom:WitProtocol
{
	//构造函数
	public CloseRoom() : base((ushort)ProtocolId.ROOM_CLOSE)
	{

	}
	//克隆函数
	public override WitProtocol Clone()
	{
		return new CloseRoom();
	}
	//Marshal函数
	public override WitOctetsStream Marshal(WitOctetsStream rhsOS)
	{
		return rhsOS;
	}
	//UnMarshal函数
	public override WitOctetsStream Unmarshal(WitOctetsStream rhsOS)
	{
		return rhsOS;
	}
	//成员
};

//房间同步
public class RoomSync:WitProtocol
{
	//构造函数
	public RoomSync(byte iPlayerCount = 0) : base((ushort)ProtocolId.ROOM_SYNC)
	{
		m_iPlayerCount = iPlayerCount;
	}
	//克隆函数
	public override WitProtocol Clone()
	{
		return new RoomSync();
	}
	//Marshal函数
	public override WitOctetsStream Marshal(WitOctetsStream rhsOS)
	{
		rhsOS.Write(m_iPlayerCount);
		return rhsOS;
	}
	//UnMarshal函数
	public override WitOctetsStream Unmarshal(WitOctetsStream rhsOS)
	{
		rhsOS.Read(ref m_iPlayerCount);
		return rhsOS;
	}
	//成员
	public byte	m_iPlayerCount;
};

