public enum ProtocolId:ushort
{
	//广播请求房间
	ROOM_REQUEST	= 201,
	//请求房间回复
	ROOM_REQUEST_RE	= 202,
	//创建房间
	ROOM_CREATE	= 203,
	//关闭房间
	ROOM_CLOSE	= 204,
	//房间同步
	ROOM_SYNC	= 205,
	//请求进入房间
	REQ_ENTER_ROOM	= 251,
	//玩家进入房间
	PLAYER_ENTER_ROOM	= 252,
	//进入房间回复
	ENTER_ROOM_RE	= 253,
	//准备状态同步
	PREPARE_SYNC	= 254,
	//请求离开房间
	REQ_LEAVEROOM	= 255,
	//玩家离开房间
	PLAYER_LEAVEROOM	= 256,
	//进入游戏场景
	ENTER_GAME_SCENE	= 257,
	//加载完游戏场景
	FINISH_LOADING	= 301,
	//开始战斗
	START_BATTLE	= 302,
	//状态同步
	STATE_SYNC	= 303,
	//状态校正
	STATE_CORRECT	= 304,
}