using System.Collections.Generic;
using Wit;

//请求进入房间
public class ReqEnterRoom:WitProtocol
{
	//构造函数
	public ReqEnterRoom(PlayerInfo sPlayer = null, string sPwd = "") : base((ushort)ProtocolId.REQ_ENTER_ROOM)
	{
		if(sPlayer==null)
			m_sPlayer = new PlayerInfo();
		else
			m_sPlayer = sPlayer;
		m_sPwd = sPwd;
	}
	//克隆函数
	public override WitProtocol Clone()
	{
		return new ReqEnterRoom();
	}
	//Marshal函数
	public override WitOctetsStream Marshal(WitOctetsStream rhsOS)
	{
		rhsOS.Write(m_sPlayer);
		rhsOS.Write(m_sPwd);
		return rhsOS;
	}
	//UnMarshal函数
	public override WitOctetsStream Unmarshal(WitOctetsStream rhsOS)
	{
		rhsOS.Read(m_sPlayer);
		rhsOS.Read(ref m_sPwd);
		return rhsOS;
	}
	//成员
	public PlayerInfo	m_sPlayer;
	public string	m_sPwd;
};

//玩家进入房间
public class PlayerEnterRoom:WitProtocol
{
	//构造函数
	public PlayerEnterRoom(List<PlayerInfo> vPlayers = null) : base((ushort)ProtocolId.PLAYER_ENTER_ROOM)
	{
		if(vPlayers==null)
			m_vPlayers = new List<PlayerInfo>();
		else
			m_vPlayers = vPlayers;
	}
	//克隆函数
	public override WitProtocol Clone()
	{
		return new PlayerEnterRoom();
	}
	//Marshal函数
	public override WitOctetsStream Marshal(WitOctetsStream rhsOS)
	{
		{
			rhsOS.Write((uint)m_vPlayers.Count);
			for(int i=0; i<m_vPlayers.Count; ++i)
			{
				rhsOS.Write(m_vPlayers[i]);
			}
		}
		return rhsOS;
	}
	//UnMarshal函数
	public override WitOctetsStream Unmarshal(WitOctetsStream rhsOS)
	{
		{
			uint iSize = 0;
			rhsOS.Read(ref iSize);
			PlayerInfo temp = new PlayerInfo();
			for(int i=0; i<iSize; ++i)
			{
				rhsOS.Read(temp);
				m_vPlayers.Add(temp);
			}
		}
		return rhsOS;
	}
	//成员
	public List<PlayerInfo>	m_vPlayers;
};

//进入房间回复
public class EnterRoomRe:WitProtocol
{
	//构造函数
	public EnterRoomRe(uint iId = 0, sbyte iSlot = -1) : base((ushort)ProtocolId.ENTER_ROOM_RE)
	{
		m_iId = iId;
		m_iSlot = iSlot;
	}
	//克隆函数
	public override WitProtocol Clone()
	{
		return new EnterRoomRe();
	}
	//Marshal函数
	public override WitOctetsStream Marshal(WitOctetsStream rhsOS)
	{
		rhsOS.Write(m_iId);
		rhsOS.Write(m_iSlot);
		return rhsOS;
	}
	//UnMarshal函数
	public override WitOctetsStream Unmarshal(WitOctetsStream rhsOS)
	{
		rhsOS.Read(ref m_iId);
		rhsOS.Read(ref m_iSlot);
		return rhsOS;
	}
	//成员
	public uint	m_iId;
	public sbyte	m_iSlot;
};

//准备状态同步
public class PrepareSync:WitProtocol
{
	//构造函数
	public PrepareSync(uint iId = 0, bool iPrepared = false) : base((ushort)ProtocolId.PREPARE_SYNC)
	{
		m_iId = iId;
		m_iPrepared = iPrepared;
	}
	//克隆函数
	public override WitProtocol Clone()
	{
		return new PrepareSync();
	}
	//Marshal函数
	public override WitOctetsStream Marshal(WitOctetsStream rhsOS)
	{
		rhsOS.Write(m_iId);
		rhsOS.Write(m_iPrepared);
		return rhsOS;
	}
	//UnMarshal函数
	public override WitOctetsStream Unmarshal(WitOctetsStream rhsOS)
	{
		rhsOS.Read(ref m_iId);
		rhsOS.Read(ref m_iPrepared);
		return rhsOS;
	}
	//成员
	public uint	m_iId;
	public bool	m_iPrepared;
};

//请求离开房间
public class ReqLeaveRoom:WitProtocol
{
	//构造函数
	public ReqLeaveRoom() : base((ushort)ProtocolId.REQ_LEAVEROOM)
	{

	}
	//克隆函数
	public override WitProtocol Clone()
	{
		return new ReqLeaveRoom();
	}
	//Marshal函数
	public override WitOctetsStream Marshal(WitOctetsStream rhsOS)
	{
		return rhsOS;
	}
	//UnMarshal函数
	public override WitOctetsStream Unmarshal(WitOctetsStream rhsOS)
	{
		return rhsOS;
	}
	//成员
};

//玩家离开房间
public class PlayerLeaveRoom:WitProtocol
{
	//构造函数
	public PlayerLeaveRoom(uint iId = 0) : base((ushort)ProtocolId.PLAYER_LEAVEROOM)
	{
		m_iId = iId;
	}
	//克隆函数
	public override WitProtocol Clone()
	{
		return new PlayerLeaveRoom();
	}
	//Marshal函数
	public override WitOctetsStream Marshal(WitOctetsStream rhsOS)
	{
		rhsOS.Write(m_iId);
		return rhsOS;
	}
	//UnMarshal函数
	public override WitOctetsStream Unmarshal(WitOctetsStream rhsOS)
	{
		rhsOS.Read(ref m_iId);
		return rhsOS;
	}
	//成员
	public uint	m_iId;
};

//进入游戏场景
public class EnterGameScene:WitProtocol
{
	//构造函数
	public EnterGameScene() : base((ushort)ProtocolId.ENTER_GAME_SCENE)
	{

	}
	//克隆函数
	public override WitProtocol Clone()
	{
		return new EnterGameScene();
	}
	//Marshal函数
	public override WitOctetsStream Marshal(WitOctetsStream rhsOS)
	{
		return rhsOS;
	}
	//UnMarshal函数
	public override WitOctetsStream Unmarshal(WitOctetsStream rhsOS)
	{
		return rhsOS;
	}
	//成员
};

