using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net;
using System.Net.Sockets;
using UnityEngine;

enum ProtoType : int
{
    LocalIPRequest = 0,
    LocalIPRequestRespond = 1,
    HostInfoRequest = 2,
    HostInfoRequestRespond = 3,
    RemoveHost = 4,
}


public class LobbyClient
{
    public Dictionary<string, string> hostIPName = new Dictionary<string, string>();
    public string roomName = "";

    public bool HasGotLocalIP
    {
        get { return localIP != null; }
    }

    public string LocalIP
    {
        get
        {
            if (localIP == null)
                return "No IP Detected";
            return localIP.ToString();
        }
    }

    private UdpClient listener;
    private UdpClient sender;
    private IPAddress localIP = null;
    private IPAddress groupIP;
    private int listenPort = 11000;
    private int sendPort = 11001;

    public void Initial()
    {
        try
        {
            GetIP();
            listener = new UdpClient(listenPort);
            sender = new UdpClient(sendPort);
            StartListening();
            RequestLocalIP();
        }
        catch (Exception ex)
        {
            Debug.LogException(ex);
        }
    }

    public void Close()
    {
        if(listener != null)
            listener.Close();
        if(sender != null)
            sender.Close();
    }

    public void RequestLocalIP()
    {
        string msg = "" + (int)ProtoType.LocalIPRequest;
        byte[] send = Encoding.ASCII.GetBytes(msg);
        SendMsg(send);
    }

    public void RequestHosts()
    {
        string msg = "" + (int)ProtoType.HostInfoRequest;
        byte[] send = Encoding.ASCII.GetBytes(msg);
        SendMsg(send);
    }

    public void CreateRoom(string hostName)
    {
        if (roomName != "")
            return;
        roomName = hostName;
        if (roomName == "")
            return;
        string msg = (int)ProtoType.HostInfoRequestRespond + " " + localIP + " " + roomName;
        SendMsg(Encoding.ASCII.GetBytes(msg));
    }

    public void CloseRoom()
    {
        if (roomName == "")
            return;
        string msg = (int)ProtoType.RemoveHost + " " + localIP;
        Debug.Log("Close Room " + msg);
        SendMsg(Encoding.ASCII.GetBytes(msg));
        roomName = "";
    }

    void SendHostInfo(IPEndPoint e)
    {
        if (roomName == "")
            return;
        string msg = (int)ProtoType.HostInfoRequestRespond + " " + localIP + " " + roomName;
        byte[] send = Encoding.ASCII.GetBytes(msg);
        SendMsg(send, e.Address);
    }

    void Decoding(string receivedMsg, IPEndPoint e)
    {
        string[] commands = receivedMsg.Split(' ');
        int protoCode = int.Parse(commands[0]);
        if (protoCode == (int)ProtoType.LocalIPRequest)//请求本机IP
        {
            string msg = (int)ProtoType.LocalIPRequestRespond + " " + e.Address.ToString();
            SendMsg(Encoding.ASCII.GetBytes(msg), e.Address);
            if (localIP == null)
                RequestLocalIP();
        }
        if (protoCode == (int)ProtoType.LocalIPRequestRespond)//本机IP
        {
            if(localIP == null)
                localIP = IPAddress.Parse(commands[1]);
        }
        if (protoCode == (int)ProtoType.HostInfoRequest)//请求主机ip广播
        {
            SendHostInfo(e);
        }
        else if (protoCode == (int)ProtoType.HostInfoRequestRespond)//主机信息
        {
            if (!hostIPName.ContainsKey(commands[1]))
                hostIPName.Add(commands[1], commands[2]);
        }
        else if (protoCode == (int)ProtoType.RemoveHost)
        {
            if (hostIPName.ContainsKey(commands[1]))
                hostIPName.Remove(commands[1]);
        }
    }

    void OnReceiveMsg(IAsyncResult ar)
    {
        try
        {
            IPEndPoint e = null;
            Byte[] receiveBytes = listener.EndReceive(ar, ref e);
            string receiveString = Encoding.ASCII.GetString(receiveBytes);
            Decoding(receiveString, e);
        }
        catch (System.Exception ex)
        {
            Console.WriteLine(ex.Message);
        }
        StartListening();
    }

    void StartListening()
    {
        try
        {
            listener.BeginReceive(OnReceiveMsg, null);
        }
        catch (Exception e)
        {
            Console.WriteLine(e.ToString());
        }
    }

    void SendMsg(byte[] msg)
    {
        try
        {
            sender.Connect(groupIP, listenPort);
            sender.Send(msg, msg.Length);
        }
        catch (Exception ex)
        {
            Console.WriteLine(ex.Message);
        }
    }

    void SendMsg(byte[] msg, IPAddress addr)
    {
        try
        {
            sender.Connect(addr, listenPort);
            sender.Send(msg, msg.Length);
        }
        catch (System.Exception ex)
        {
            Console.WriteLine(ex.Message);
        }
    }

    void GetIP()
    {
        groupIP = IPAddress.Parse("255.255.255.255");
    }
}