using UnityEngine;
using System.Collections;
using System.Threading;
using System.Net;

public class CheckNetwork
{
    private static bool m_NetAvailable = false;
    public static bool Available
    {
        get { return m_NetAvailable; }
    }

    private static bool m_IsDone = false;
    public static bool IsDone
    {
        get { return m_IsDone; }
    }

	// Update is called once per frame
    public static IEnumerator Check()
	{
        m_IsDone = false;
        float startTime = Time.realtimeSinceStartup;
        IPHostEntry dnstoip = new IPHostEntry();

        try
        {
            dnstoip = Dns.GetHostEntry(CsConst.CHECK_URL);
        }
        catch(System.Exception ex)
        {
            Debug.Log(ex.ToString());
            m_IsDone = true;
            m_NetAvailable = false;
            yield break;
        }

        if (dnstoip.AddressList.Length > 0)
        {
            Debug.Log("HostToIp:" + CsConst.CHECK_URL + " : " + dnstoip.AddressList[0].ToString());
            Ping ping = new Ping(dnstoip.AddressList[0].ToString());
            while (!ping.isDone && Time.realtimeSinceStartup - startTime < CsConst.CHECK_TIMEOUT)
            {
                yield return new WaitForSeconds(0.02f);
            }
            Debug.Log("IsNetChecked: "+ping.isDone);
            Debug.Log("Net ping to host: "+ping.time);
            m_IsDone = true;
            m_NetAvailable = ping.isDone;
            yield break;
        }        
	}
}
