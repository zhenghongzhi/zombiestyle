using UnityEngine;
using System.Collections;
using NetTime;
using System;

public class RemoteTime : MonoBehaviour 
{
    private static DateTime m_OpenTime;
    public static DateTime OpenTime
    {
        get { return m_OpenTime; }
    }

    //时间同步是否完成（不论成功失败）
    private static bool m_IsDone = false;
    public static bool IsDone
    {
        get { return m_IsDone; }
    }

    //时间同步是否成功
    private static bool m_IsSuc = false;
    public static bool IsSuc
    {
        get { return m_IsSuc; }
    }

    public static IEnumerator TimeSync()
    {
        m_IsDone = false;
        NTPTimeClient client = new NTPTimeClient(CsConst.TIME_HOST, 123);
        try
        {
            client.Connect();
            m_IsDone = true;
            m_IsSuc = true;
            m_OpenTime = client.ReceiveTimestamp;
            Debug.Log("NtpTimeSync: "+m_OpenTime);
        }
        catch (System.Exception ex)
        {
            Debug.Log(ex.ToString());
            m_IsDone = true;
            m_IsSuc = false;
        }
        yield break;
    }
}
