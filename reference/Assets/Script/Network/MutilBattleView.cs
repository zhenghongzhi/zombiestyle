using UnityEngine;
using System.Collections.Generic;

public class MutilBattleView : MonoBehaviour {
    public GameObject myObject;
    public Dictionary<uint, GameObject> m_mPlayerModel;

    public int waitingPlayerNum;
    public bool waitForOthers = false;
    bool start = false;
    public List<uint> waitingPlayers;

    bool inGameScene = false;
    float syncTimeCount = 0;

    void Awake()
    {
        DontDestroyOnLoad(gameObject);
        BattleNetwork.Instance.Init();
    }

    void OnLevelWasLoaded(int level)
    {
        if (Application.loadedLevelName == "testMultiplayerScene")
        {
            start = false;
            //Time.timeScale = 0;

            foreach (uint iId in m_mPlayerModel.Keys)
            {
                InitializePlayer(iId);
            }

            myObject.name = "MyPlayer";
            Camera.mainCamera.GetComponent<CameraFollow>().player = myObject.transform;

            inGameScene = true;

            //client向服务端发送消息，load完成
            if (!NetworkManager.Instance.IsServer)
            {
                NetworkManager.Instance.SendProtocol(new FinishLoading());
            }
            //server判断是否是单人开始的
            else
            {
                LobbyNetwork.Instance.MyRoom.PlayerMan.GetPlayerBySlot(1).m_bPrepared = true;
                if (LobbyNetwork.Instance.MyRoom.PlayerMan.IsAllPrepared())
                {
                    StartGame();
                }
            }

            //Instantiate NPC if is Host
        }

    }

    void Update()
    {
        if (inGameScene)
        {
            syncTimeCount += Time.deltaTime;
            if (syncTimeCount > 5)
            {
                StateCorrect sCmd = new StateCorrect(LobbyNetwork.Instance.MyPlayerId, 
                    CsUtil.ToWitVector3(myObject.transform.position), CsUtil.ToWitVector3(myObject.transform.eulerAngles));
                if (NetworkManager.Instance.IsServer)
                {
                    NetworkManager.Instance.BroadcastProtocol(sCmd, LobbyNetwork.Instance.MyRoom.PlayerMan.CollectId());
                }
                else
                {
                    NetworkManager.Instance.SendProtocol(sCmd);
                }
                syncTimeCount = 0;
            }
        }
    }

    void FixedUpdate()
    {
        NetworkManager.Instance.ProcessEvent();
    }

    void OnGUI()
    {
        GUI.color = Color.Lerp(Color.red, Color.green, myObject.GetComponent<CharacterStatus>().currentHp / 100.0f);
        GUI.Label(new Rect(600, 100, 200, 30), "Life : " + myObject.GetComponent<CharacterStatus>().currentHp);
        GUI.color = Color.white;

        if (!start && waitForOthers)
        {
            GUI.contentColor = Color.black;
            GUI.Label(CsGUI.CRect(50, 200, 100, 40), "Waiting For Player : ");
            int i = 0;
            foreach (uint playerId in waitingPlayers)
            {
                GUI.Label(CsGUI.CRect(50, 250 + 50 * i, 100, 40), LobbyNetwork.Instance.MyRoom.PlayerMan.GetPlayerById(playerId).m_sName);
                ++i;
            }
        }
    }

    //Called by Back or Server Disconnection Messages
    void Back()
    {
         foreach (GameObject g in m_mPlayerModel.Values)
         {
             if (g != myObject)
                 Destroy(g);
         }
         m_mPlayerModel.Clear();

        myObject.GetComponent<PlayerAnimation>().enabled = false;
        myObject.GetComponent<PlayerMovementMoter>().enabled = false;
        myObject.GetComponent<TPSController>().enabled = false;
        myObject.GetComponent<PadInput>().enabled = false;
        myObject.GetComponent<Rigidbody>().isKinematic = true;
        myObject.transform.FindChild("light").gameObject.SetActive(false);
        myObject.animation.Play(myObject.GetComponent<PlayerAnimation>().idle.name);
        Destroy(gameObject);
        if (NetworkManager.Instance.IsServer)
        {
            LobbyNetwork.Instance.CloseRoom();
        }
        Application.LoadLevel("Lobby");
    }

    public bool OnDisconnectedFromServer()
    {
        if (Application.loadedLevelName != CsConst.BATTLE_SCENE)
        {
            return false;
        }

        Back();
        return true;
    }

    void InitializePlayer(uint playerId)
    {
        Player player = LobbyNetwork.Instance.MyRoom.PlayerMan.GetPlayerById(playerId);
        Transform initPos = GameObject.Find("Player Init Position " + (player.Slot - 1)).transform;
        Debug.Log("Create Remote Player. " + playerId);
        GameObject playerObject = m_mPlayerModel[playerId];
        playerObject.transform.position = initPos.position;
        playerObject.transform.rotation = initPos.rotation;

        playerObject.GetComponent<Rigidbody>().isKinematic = false;
        playerObject.GetComponent<PlayerMovementMoter>().enabled = true;
        playerObject.GetComponent<Weapon>().canKillPlayer = true;
        //  playerObject.GetComponent<Weapon>().InitWeapon();
        playerObject.GetComponent<PlayerAnimation>().enabled = true;
        playerObject.GetComponent<TPSController>().enabled = true;
        playerObject.GetComponent<PadInput>().enabled = true;
        if (playerId != LobbyNetwork.Instance.MyPlayerId)
        {
            playerObject.GetComponent<TPSController>().remotePlayer = true;
            Destroy(playerObject.GetComponent<PadInput>());
        }
        else
        {
            playerObject.GetComponent<PlayerMovementMoter>().bLocal = true;
        }
    }

    public void StartGame()
    {
        start = true;
        Time.timeScale = 1;
    }

    public void StateCorrect(uint player, WitVector3 position, WitVector3 eular)
    {
        if (m_mPlayerModel.ContainsKey(player))
        {
            Debug.Log("Correct position!"+player);
            m_mPlayerModel[player].transform.position = CsUtil.ToVector3(position);
            m_mPlayerModel[player].transform.eulerAngles = CsUtil.ToVector3(eular);
        }
    }

    public void OnStateSync( uint iSid, WitVector3 sMoveDir, WitVector3 sFaceDir )
    {
        if (iSid != LobbyNetwork.Instance.MyPlayerId && m_mPlayerModel.ContainsKey(iSid))
        {
            PlayerMovementMoter pMoter = m_mPlayerModel[iSid].GetComponent<PlayerMovementMoter>();
            if (pMoter != null)
            {
                pMoter.moveDir = CsUtil.ToVector3(sMoveDir);
                pMoter.faceDir = CsUtil.ToVector3(sMoveDir);
            }
        }
    }

    public bool OnClientDisconnected(uint id)
    {
        if (Application.loadedLevelName != CsConst.BATTLE_SCENE)
        {
            return false;
        }
        Debug.Log("OnPlayerDisconnected");
        RemoveRemotePlayer(id);
        return true;
    }

    public void RemoveRemotePlayer(uint player)
    {
        LobbyNetwork.Instance.MyRoom.PlayerMan.ClearPlayerById(player);
        if (m_mPlayerModel.ContainsKey(player))
        {
            GameObject g = m_mPlayerModel[player];
            m_mPlayerModel.Remove(player);
            Destroy(g);
        }
    }
}
