using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;
using System.Linq;

public enum TaskType:byte
{
    //以特定条件完成关卡
    Complete,
    //获得物品
    GetItem,
    //摧毁指定场景内物体
    Destroy,
    //杀死怪物
    Kill
}

public enum CompleteType
{
    //限时完成
    Time,
    //用特定角色完成
    Char,
    //完成时血量在特定范围
    HpPercent,
    //不受到怪物伤害
    HurtByMonster,
    //不受到场景破坏物伤害
    HurtBySceneObj,
    //无伤通过
    NoHurt,
}

//击杀僵尸的类型
public enum KillType
{
    //无限制击杀
    JustKill,
    //使用特定技能
    Skill,
    //使用特定场景物
    SceneObj,
    //使用道具
    Item,
}

//怪物受到的伤害类型
public enum DamageType
{
    //技能
    Skill,
    //场景破坏物
    SceneObj,
    //道具
    Item,
}

//玩家受到伤害来源
public enum DamageSource
{
    //怪物
    Monster,
    //场景破坏物
    SceneObj,
}

public enum TaskState
{
    //激活状态
    Active,
    //完成状态
    Finished,
}

public class TaskNotifyData
{
    public string TaskName;
    public int CurCount;
    public int ObjectCount;
}

public class TaskModule 
{
    //是否已经检查过任务需要刷新
    private bool m_DailyChecked = false;
    //静态数据的副本
    private TaskStoreData m_TaskData;
    //当前所在场景
    private int m_iCurScene = 0;

    //***记录通过场景中时的数据
    private bool m_bHurtedByMonster = false;
    private bool m_bHurtedByObj = false;

    //***Singleton member
    private static TaskModule m_TaskModule;
    public static TaskModule Instance
    {
        get
        {
            if (m_TaskModule == null)
            {
                m_TaskModule = new TaskModule();
            }
            return m_TaskModule;
        }
    }

    //任务系统是否已激活
    public bool IsActive
    {
        get 
        {
#if !UNITY_EDITOR
            return PlayerStoreData.StoredData.taskData.actived;
#else
            return true;
#endif
        }
    }

    public void Clear()
    {
        m_iCurScene = 0;
        m_bHurtedByMonster = false;
        m_bHurtedByObj = false;
    }

    /// <summary>
    /// 开启任务模块
    /// </summary>
    /// <returns>开启成功返回true，否则返回false</returns>
    public bool OpenTaskModule()
    {
        return false;
    }

    /// <summary>
    /// 玩家进入游戏时判断任务是否需要刷新
    /// 由于是3点刷新，将所有时间处理时减去3个小时，归为0点处理
    /// </summary>
    /// <returns>初始化是否成功</returns>
    public bool Init()
    {
        m_TaskData = PlayerStoreData.StoredData.taskData;
        //刷新当日任务
        if (!m_DailyChecked && IsActive)
        {
            string lastFlush = m_TaskData.flushTime;
            Debug.Log("LastFlushTaskTime: " + lastFlush);
            //网络可用情况下
            if (RemoteTime.IsSuc)
            {
                DateTime remoteTime = RemoteTime.OpenTime.AddHours(-3);
                //lastFlush为空说明还没有刷新过任务，直接刷新
                if (lastFlush == null)
                {
                    RefreshTask();
                    m_TaskData.flushTime = remoteTime.ToString();
                }
                //用网络的时间判断刷新时间
                else
                {
                    DateTime lastTime = DateTime.Parse(lastFlush);

                    //现在时间和lastTime对比
                    if (RemoteTime.OpenTime.DayOfYear != lastTime.DayOfYear)
                    {
                        RefreshTask();
                        m_TaskData.flushTime = remoteTime.ToString();
                    }
                }
            }
            //网络不可用，本地时间判断
            else
            {
                DateTime curTime = DateTime.Now.AddHours(-3);
                //lastFlush为空说明还没有刷新过任务，直接刷新
                if (lastFlush == null)
                {
                    RefreshTask();
                    m_TaskData.flushTime = curTime.ToString();
                }
                else
                {
                    DateTime lastTime = DateTime.Parse(lastFlush);
                    Debug.Log(lastTime);
                    //现在时间和lastTime对比
                    if (curTime.DayOfYear != lastTime.DayOfYear)
                    {
                        RefreshTask();
                        m_TaskData.flushTime = curTime.ToString();
                    }
                }                
            }
        }
        return true;
    }

    private void RefreshTask(int iCount = CsConst.REFRESH_COUNT)
    {
        //清除原有任务
        m_TaskData.taskData.Clear();

        //获取地图最后iCount个刷新
        List<MapStoreData> mapData = PlayerStoreData.StoredData.mapData;
        if (mapData.Count < iCount)
        {
            iCount = mapData.Count;
        }

        int index = 0;
        for (int i = 0; i < iCount; ++i)
        {
            index = mapData.Count - 1 - i;
            RandomTaskByMapId(mapData[index].sceneId);
        }
        Debug.Log("Random Task Success!");
    }

    private void RandomTaskByMapId(int iMapId)
    {
        MapTaskData mapTask = StaticGameData.GetMapTask(iMapId);
        TaskData taskData = null;
        if (mapTask != null)
        {
            int iCount = mapTask.RollNum;
            //如果列表中的任务少于需要roll的数量，则取最多列表的数
            if (mapTask.m_MissionList.Count<iCount)
            {
                iCount = mapTask.m_MissionList.Count;
            }

            //set用来筛选非重复任务
            HashSet<int> set = new HashSet<int>();
            int iTaskId = 0;
            for (int i = 0; i < iCount; ++i )
            {
                do 
                {
                    iTaskId = mapTask.m_MissionList[UnityEngine.Random.Range(0, mapTask.m_MissionList.Count)];
                } 
                while (set.Contains(iTaskId));

                //检验数据是否有误
                if (StaticGameData.GetMission(iTaskId)==null)
                {
                    Debug.LogError("MapTask has a TaskId that not in MissionList");
                    return;
                }
                set.Add(iTaskId);
                taskData = new TaskData();
                taskData.MapId = iMapId;
                taskData.State = (int)TaskState.Active;
                taskData.TaskId = iTaskId;

                m_TaskData.taskData.Add(taskData);
            }
        }
        else
        {
            Debug.LogError("Map:" + iMapId + " don't have corresponding TaskMapData!");
        }
    }

    public bool OnEnterScene(int iScene)
    {
        if (m_iCurScene != 0)
        {
            Debug.LogError("You mush call OnCompleteScene when you finishe a scene!");
        }
        Clear();
        m_iCurScene = iScene;
        return true;
    }

    /// <summary>
    /// 当完成一个场景战斗时调用，用以判断过关限制条件的任务
    /// </summary>
    /// <param name="iScene">完成的场景id</param>
    /// <param name="fTime">战斗总共用时</param>
    /// <param name="iChar">战斗所用角色</param>
    /// <param name="fHp">完成时生命值百分比</param>
    /// <returns></returns>
    public bool OnCompleteScene(float fTime, int iChar, float fHp)
    {
        if (m_iCurScene == 0)
        {
            Debug.LogError("You mush call OnEnterScene when you enter a scene!");
            Clear();
            return false;
        }
        int iScene = m_iCurScene;
        var task = m_TaskData.taskData;
        for (int i = 0; i < task.Count; ++i)
        {
            if (task[i].MapId == iScene)
            {
                MissionData missionData = StaticGameData.GetMission(task[i].TaskId);
                if (missionData == null)
                {
                    Debug.LogError("Mission: "+task[i].TaskId+" doesn't not exist!");
                    continue;
                }
                if (missionData.ObjMainType == (byte)TaskType.Complete)
                {
                    switch ((CompleteType)missionData.ObjType)
                    {
                        case CompleteType.Time:
                            if (fTime <= missionData.ObjValue1)
                            {
                                task[i].Arg = 1;
                                OnTaskFinish(task[i]);
                            }
                            break;
                        case CompleteType.Char:
                            if (iChar == missionData.ObjValue1)
                            {
                                task[i].Arg = 1;
                                OnTaskFinish(task[i]);
                            }
                            break;
                        case CompleteType.HpPercent:
                            if (fHp <= missionData.ObjValue1)
                            {
                                task[i].Arg = 1;
                                OnTaskFinish(task[i]);
                            }
                            break;
                        case CompleteType.HurtByMonster:
                            if (!m_bHurtedByMonster)
                            {
                                task[i].Arg = 1;
                                OnTaskFinish(task[i]);
                            }
                            break;
                        case CompleteType.HurtBySceneObj:
                            if (!m_bHurtedByObj)
                            {
                                task[i].Arg = 1;
                                OnTaskFinish(task[i]);
                            }
                            break;
                        case CompleteType.NoHurt:
                            if (!m_bHurtedByObj && !m_bHurtedByMonster)
                            {
                                task[i].Arg = 1;
                                OnTaskFinish(task[i]);
                            }
                            break;
                    }
                }
            }
        }
        Clear();
        return false;
    }

    /// <summary>
    /// 当玩家战斗受到伤害时调用
    /// </summary>
    /// <param name="iScene">所在场景id</param>
    /// <param name="eDmgSrc">伤害来源</param>
    /// <param name="iArg">附加参数，可不填</param>
    /// <returns></returns>
    public bool OnHurted(DamageSource eDmgSrc, int iArg=0)
    {
        if (eDmgSrc == DamageSource.Monster)
        {
            m_bHurtedByMonster = true;
        }
        else if (eDmgSrc == DamageSource.SceneObj)
        {
            m_bHurtedByObj = true;
        }
        return true;
    }

    /// <summary>
    /// 当玩家拾取物品时调用
    /// </summary>
    /// <param name="iScene">当前场景id</param>
    /// <param name="iItemId">拾取到的物品id</param>
    /// <returns></returns>
    public bool OnGetItem(int iItemId)
    {
        if (m_iCurScene == 0)
        {
            Debug.LogError("You mush call OnEnterScene when you enter a scene!");
            return false;
        }
        //TODO 判断道具类型为任务拾取道具

        //判断并增加拾取的道具计数
        var task = m_TaskData.taskData;
        for (int i = 0; i < task.Count; ++i)
        {
            if (task[i].State == TaskState.Active && task[i].MapId == m_iCurScene)
            {
                MissionData missionData = StaticGameData.GetMission(task[i].TaskId);
                if (missionData == null)
                {
                    Debug.LogError("Mission: "+task[i].TaskId+" doesn't not exist!");
                    continue;
                }
                if (missionData.ObjMainType == (byte)TaskType.GetItem)
                {
                    if (missionData.ObjValue1 == iItemId)
                    {
                        task[i].Arg++;
                        //判断是否已经够数量
                        if (task[i].Arg >= missionData.ObjValue2)
                        {
                            OnTaskFinish(task[i]);
                        }
                        else
                        {
                            TaskNotifyData notify = new TaskNotifyData();
                            notify.TaskName = missionData.Des;
                            notify.CurCount = task[i].Arg;
                            notify.ObjectCount = missionData.ObjValue2;
                            OnTaskNotify(notify);
                        }
                    }
                }
            }
        }
        
        return false;
    }


    /// <summary>
    /// 当玩家摧毁场景物体时调用
    /// </summary>
    /// <param name="iScene">当前场景id</param>
    /// <param name="iObjId">摧毁的道具id</param>
    /// <returns></returns>
    public bool OnDestroyObj(int iObjId)
    {
        if (m_iCurScene == 0)
        {
            Debug.LogError("You mush call OnEnterScene when you enter a scene!");
            return false;
        }

        //判断并增加摧毁的场景物计数
        var task = m_TaskData.taskData;
        for (int i = 0; i < task.Count; ++i)
        {
            if (task[i].State == TaskState.Active && task[i].MapId == m_iCurScene)
            {
                MissionData missionData = StaticGameData.GetMission(task[i].TaskId);
                if (missionData == null)
                {
                    Debug.LogError("Mission: " + task[i].TaskId + " doesn't not exist!");
                    continue;
                }
                if (missionData.ObjMainType == (byte)TaskType.Destroy)
                {
                    if (missionData.ObjValue1 == iObjId)
                    {
                        task[i].Arg++;
                        //判断是否已经够数量
                        if (task[i].Arg >= missionData.ObjValue2)
                        {
                            OnTaskFinish(task[i]);
                        }
                        else
                        {
                            TaskNotifyData notify = new TaskNotifyData();
                            notify.TaskName = missionData.Des;
                            notify.CurCount = task[i].Arg;
                            notify.ObjectCount = missionData.ObjValue2;
                            OnTaskNotify(notify);
                        }
                    }
                }
            }
        }
        return false;
    }

    /// <summary>
    /// 击杀僵尸时调用
    /// </summary>
    /// <param name="iScene">当前场景id</param>
    /// <param name="iMonsterId">怪物id</param>
    /// <param name="eDmgType">最后一击伤害类型，技能或是场景道具伤害</param>
    /// <param name="iArg">附加参数</param>
    /// <returns></returns>
    public bool OnKillMonster(int iMonsterId, DamageType eDmgType, int iArg = 0)
    {
        if (m_iCurScene == 0)
        {
            Debug.LogError("You mush call OnEnterScene when you enter a scene!");
            return false;
        }

        //判断并增加杀死的怪物
        var task = m_TaskData.taskData;
        for (int i = 0; i < task.Count; ++i)
        {
            if (task[i].State == TaskState.Active && task[i].MapId == m_iCurScene)
            {
                MissionData missionData = StaticGameData.GetMission(task[i].TaskId);
                if (missionData == null)
                {
                    Debug.LogError("Mission: " + task[i].TaskId + " doesn't not exist!");
                    continue;
                }
                if (missionData.ObjMainType == (byte)TaskType.Kill)
                {
                    if (missionData.ObjValue1 == iMonsterId)
                    {
                        switch ((KillType)missionData.ObjType)
                        {
                            case KillType.JustKill:
                                task[i].Arg++;
                                break;
                            case KillType.Skill:
                                if (eDmgType == DamageType.Skill)
                                {
                                    task[i].Arg++;
                                }
                                break;
                            case KillType.SceneObj:
                                if (eDmgType == DamageType.SceneObj)
                                {
                                    task[i].Arg++;
                                }
                                break;
                            case KillType.Item:
                                if (eDmgType == DamageType.Item)
                                {
                                    task[i].Arg++;
                                }
                                break;
                        }
                        if (task[i].Arg >= missionData.ObjValue2)
                        {
                            OnTaskFinish(task[i]);
                        }
                        else
                        {
                            TaskNotifyData notify = new TaskNotifyData();
                            notify.TaskName = missionData.Des;
                            notify.CurCount = task[i].Arg;
                            notify.ObjectCount = missionData.ObjValue2;
                            OnTaskNotify(notify);
                        }
                    }                    
                }//end if(Kill)
            }
        }//end for
        return true;
    }

    void OnTaskNotify(TaskNotifyData task)
    {
        //通知UI显示
        var obj = GameObject.Find("Main Camera");
        if (obj != null)
        {
            obj.SendMessage("TaskProcessNotify", task);
        }
    }

    void OnTaskFinish(TaskData task)
    {
        task.State = TaskState.Finished;
        //发放奖励
        MissionData mission = StaticGameData.GetMission(task.TaskId);
        if (mission != null)
        {
            PlayerStoreData.StoredData.gold += mission.GainGold;
            int curChar = PlayerStoreData.StoredData.selectedCharID;
            //增加经验
            PlayerStoreData.StoredData.AddExp(curChar, mission.GainExp);
        }

        //通知UI显示
        var obj = GameObject.Find("Main Camera");
        if (obj != null)
        {
            obj.SendMessage("TaskFinishNotify");
        }
    }

    /// <summary>
    /// 检查该任务是否完成
    /// </summary>
    /// <returns>完成返回true</returns>
    public bool CheckFinished(int iSceneId, int iTaskId)
    {
        var query = from m in m_TaskData.taskData where m.MapId == iSceneId && m.TaskId == iTaskId select m;
        if (query.Count() > 0)
        {
            return query.First().State == TaskState.Finished;
        }
        return false;
    }

    public bool GetDataByScene(int iScene, out MapTaskData maptask, out List<TaskData> task, out List<MissionData> mission)
    {
        maptask = StaticGameData.GetMapTask(iScene);
        task = new List<TaskData>();
        mission = new List<MissionData>();
        if (maptask == null)
        {
            Debug.LogError("Scene: " + iScene + " doesn't have MapTaskData!");
            return false;
        }
        for (int i = 0; i < m_TaskData.taskData.Count; ++i )
        {
            if (m_TaskData.taskData[i].MapId == iScene)
            {
                task.Add(m_TaskData.taskData[i]);
                mission.Add(StaticGameData.GetMission(m_TaskData.taskData[i].TaskId));
            }
        }
        return true;
    }
}
