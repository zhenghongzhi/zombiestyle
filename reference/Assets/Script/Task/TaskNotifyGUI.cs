using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class TaskNotifyGUI : MonoBehaviour
{
    //***游戏中所用的文本
    

    //显示开始的时间
    float m_fProcessBeginTime = float.MaxValue;
    TaskNotifyData m_sNotityData;

    class FinishNotify
    {
        public float StartTime;
        public TaskNotifyData Task;
    }

    List<FinishNotify> m_vFinishNotify = new List<FinishNotify>();

    public void OnGUI()
    {
        if (m_sNotityData != null && Time.realtimeSinceStartup - m_fProcessBeginTime <= CsConst.PROCESS_NOTIFY_LAST)
        {
            GUI.Label(CsGUI.CRect(0, 100, 200, 50), m_sNotityData.TaskName + "\t" + m_sNotityData.CurCount + "/" + m_sNotityData.ObjectCount);
        }

        if (m_vFinishNotify.Count>0)
        {
            //先判断现在显示的是否已经够时间 index为0
            if (m_vFinishNotify[0].StartTime < Time.realtimeSinceStartup - CsConst.FINISH_NOTIFY_LAST)
            {
                m_vFinishNotify.RemoveAt(0);
            }

            //显示index为0的完成的任务
            if (m_vFinishNotify.Count > 0)
            {
                if (m_vFinishNotify[0].StartTime > Time.realtimeSinceStartup)
                {
                    m_vFinishNotify[0].StartTime = Time.realtimeSinceStartup;
                }
                GUI.Label(CsGUI.CRect(300, 600, 1200, 50), m_vFinishNotify[0].Task.TaskName+" Finished!");
            }
        }
    }

    public void TaskProcessNotify(TaskNotifyData task)
    {
        m_fProcessBeginTime = Time.realtimeSinceStartup;
        m_sNotityData = task;
    }

    public void TaskFinishNotify(TaskNotifyData task)
    {
        FinishNotify notify = new FinishNotify();
        notify.StartTime = float.MaxValue;
        notify.Task = task;
        m_vFinishNotify.Add(notify);
    }
}
