using UnityEngine;
using System.Collections.Generic;

public abstract class DragableItemBase
{
    protected ItemPlace targetPlace;
    protected Vector2 centerPos;
    protected Vector2 dir;
    protected bool inPlace;

    public bool InPlace
    {
        get { return inPlace; }
    }
    public abstract int ID
    {
        get;
    }

    public DragableItemBase()
    {
        targetPlace = null;
        centerPos = Vector2.zero;
        inPlace = false;
    }

    public void SetPlace(ItemPlace place)
    {
        centerPos = place.pos;
        inPlace = true;
    }

    public void SetTargetPlace(ItemPlace place)
    {
        targetPlace = place;
        inPlace = false;
        dir = (place.pos - centerPos);
    }

    public void SetCenterPos(Vector2 p)
    {
        centerPos = p;
    }

    public void UpdatePos()
    {
        if (!inPlace)
        {
            Vector2 velocity = dir * Time.deltaTime;
            centerPos += velocity;
            if (targetPlace.IsIn(centerPos))
            {
                centerPos = targetPlace.pos;
                inPlace = true;
                targetPlace = null;
            }
        }
    }

    public abstract bool IsHit(float x, float y);

    public abstract void OnDrawItem(float startX, float startY);
}

public class DragableSkill : DragableItemBase
{
    SkillData skillData;
    Texture2D skillTex;

    public override int ID
    {
        get { return skillData.SkillID; }
    }

    public DragableSkill(SkillData data)
    {
        skillData = data;
        skillTex = Resources.Load("Skill/bg_" + data.SkillID + "_100") as Texture2D;
    }

    public override bool IsHit(float x, float y)
    {
        return new Rect(centerPos.x - CsGUI.CX(50), centerPos.y - CsGUI.CY(50), CsGUI.CY(100), CsGUI.CY(100)).Contains(new Vector2(x, y));
    }

    public override void OnDrawItem(float startX, float startY)
    {
        GUI.DrawTexture(new Rect(centerPos.x - CsGUI.CX(50), centerPos.y - CsGUI.CY(50), CsGUI.CY(100), CsGUI.CY(100)), skillTex);
    }
}

public class DragableEquipment : DragableItemBase
{
    ItemData itemData;
    Texture2D skillTex;

    public override int ID
    {
        get { return itemData.ItemID; }
    }

    public DragableEquipment(ItemData data)
    {
        itemData = data;
        //skillTex = Resources.Load("Skill/bg_" + data.ItemID + "_100") as Texture2D;
    }

    public override bool IsHit(float x, float y)
    {
        return new Rect(centerPos.x - CsGUI.CX(50), centerPos.y - CsGUI.CY(50), CsGUI.CY(100), CsGUI.CY(100)).Contains(new Vector2(x, y));
    }

    public override void OnDrawItem(float startX, float startY)
    {
        //GUI.DrawTexture(new Rect(centerPos.x - CsGUI.CX(50), centerPos.y - CsGUI.CY(50), CsGUI.CY(100), CsGUI.CY(100)), skillTex);
        GUI.Box(new Rect(centerPos.x - CsGUI.CX(50), centerPos.y - CsGUI.CY(50), CsGUI.CY(100), CsGUI.CY(100)), itemData.ItemName);
    }
}

public class ItemPlace
{
    public Vector2 pos;
    public int placeIndex;

    public ItemPlace(Vector2 pos, int index)
    {
        this.pos = pos;
        placeIndex = index;
    }

    public bool IsClose(Vector2 pos)
    {
        return (this.pos - pos).sqrMagnitude < 3400 * CsGUI.YRate * CsGUI.XRate;
    }

    public bool IsIn(Vector2 pos)
    {
        return (this.pos - pos).sqrMagnitude < 25 * CsGUI.YRate * CsGUI.XRate;
    }
}

public class PackageBoard{
    public ShopGUI commonData;
    Texture2D placeHLTex;
    List<ItemPlace> placeList;
    List<DragableItemBase> itemList;
    Dictionary<ItemPlace, DragableItemBase> itemSet;
    DragableItemBase dragItem;
    int nearestPlaceIndex = -1;
    int dragState;
    int fallbackIndex;

    public PackageBoard(Vector2[] places)
    {
        placeList = new List<ItemPlace>();
        itemList = new List<DragableItemBase>();
        itemSet = new Dictionary<ItemPlace, DragableItemBase>();
        int placeIndex = 0;
        foreach (Vector2 pos in places)
        {
            ItemPlace place = new ItemPlace(pos, placeIndex);
            itemSet.Add(place, null);
            placeList.Add(place);
            placeIndex++;
        }
    }

    public void AddItem(DragableItemBase item, int placeIndex)
    {
        if (itemSet[placeList[placeIndex]] != null)
        {
            itemList.Remove(itemSet[placeList[placeIndex]]);
        }
        item.SetPlace(placeList[placeIndex]);
        itemSet[placeList[placeIndex]] = item;
        itemList.Add(item);
        UpdateCharacterSkillOutfit();
    }

    public void SetItem(DragableItemBase item, int placeIndex)
    {
        item.SetPlace(placeList[placeIndex]);
        itemSet[placeList[placeIndex]] = item;
    }

    public void SetItem(DragableItemBase item, ItemPlace place)
    {
        item.SetPlace(place);
        itemSet[place] = item;
    }

    public void Draw(Rect rect)
    {
        Update(rect);
        GUI.Box(rect, "");
        foreach (DragableItemBase item in itemList)
        {
            if (item != null)
                item.OnDrawItem(rect.x, rect.y);
        }
        for (int i = 0; i < placeList.Count; ++i)
        {
         //   if (i != nearestPlaceIndex)
                DrawPlace(placeList[i].pos, commonData.skillIconFrame);
//             else
//                 DrawPlace(placeList[i].pos, placeHLTex);
        }
    }

    void Update(Rect rect)
    {
        foreach (DragableItemBase item in itemList)
        {
            item.UpdatePos();
        }

        if (dragState == 0)//Not Dragging
        {
            Vector2 mousePos;
#if UNITY_EDITOR || UNITY_STANDALONE_WIN
            if(Input.GetMouseButtonDown(0))
            {
                mousePos = Input.mousePosition;
                mousePos.y = Screen.height - mousePos.y;

                int placeIndex = 0;
                foreach (ItemPlace place in placeList)
                {
                    if (itemSet[place] != null && itemSet[place].IsHit(mousePos.x, mousePos.y))
                    {
                        fallbackIndex = placeIndex;
                        dragItem = itemSet[place];
                        dragState = 1;
                        itemSet[place] = null;
                        break;
                    }
                    placeIndex++;
                }
            }
#endif
#if (UNITY_IPHONE || UNITY_ANDROID) && !UNITY_EDITOR
            if (Input.touchCount == 1 && Input.touches[0].phase == TouchPhase.Began)
            {
                mousePos = Input.touches[0].position;
                mousePos.y = Screen.height - mousePos.y;
                int placeIndex = 0;
                foreach (ItemPlace place in placeList)
                {
                    if (itemSet[place] != null && itemSet[place].IsHit(mousePos.x, mousePos.y))
                    {
                        fallbackIndex = placeIndex;
                        dragItem = itemSet[place];
                        dragState = 1;
                        itemSet[place] = null;
                        break;
                    }
                    placeIndex++;
                }
            }
#endif
        }
        else if (dragState == 1)//Dragging
        {
            Vector2 centerPos;
#if UNITY_EDITOR || UNITY_STANDALONE_WIN
            //Dragging Item follow the mouse
            centerPos = Input.mousePosition;
# endif
#if (UNITY_IPHONE || UNITY_ANDROID) && !UNITY_EDITOR
            centerPos = Input.touches[0].position;
#endif
            centerPos.y = Screen.height - centerPos.y;
            centerPos.x = Mathf.Clamp(centerPos.x, rect.xMin, rect.xMax);
            centerPos.y = Mathf.Clamp(centerPos.y, rect.yMin, rect.yMax);
            dragItem.SetCenterPos(centerPos);

            ItemPlace nearestPlace = null;
            nearestPlaceIndex = 0;
            foreach (ItemPlace place in placeList)
            {
                if (place.IsClose(centerPos))
                {
                    nearestPlace = place;
                    break;
                }
                nearestPlaceIndex++;
            }
            if (nearestPlace == null)
                nearestPlaceIndex = -1;

            //Switch Other Item's Place
            if (nearestPlace != null && itemSet[nearestPlace] != null)
            {
                itemSet[nearestPlace].SetTargetPlace(placeList[fallbackIndex]);
                itemSet[placeList[fallbackIndex]] = itemSet[nearestPlace];
                itemSet[nearestPlace] = null;
                fallbackIndex = nearestPlaceIndex;
            }

#if UNITY_EDITOR || UNITY_STANDALONE_WIN
            //Drop
            if (Input.GetMouseButtonUp(0))
            {
                if (nearestPlace != null && itemSet[nearestPlace] == null)
                {
                    dragItem.SetTargetPlace(nearestPlace);
                    itemSet[nearestPlace] = dragItem;
                }
                else
                {
                    dragItem.SetTargetPlace(placeList[fallbackIndex]);
                    itemSet[placeList[fallbackIndex]] = dragItem;
                }
                UpdateCharacterSkillOutfit();
                dragState = 0;
            }
#endif
#if (UNITY_IPHONE || UNITY_ANDROID) && !UNITY_EDITOR
            //Drop
            if (Input.touchCount < 1 || Input.touches[0].phase == TouchPhase.Ended || Input.touches[0].phase == TouchPhase.Canceled)
            {
                if (nearestPlace != null && itemSet[nearestPlace] == null)
                {
                    dragItem.SetTargetPlace(nearestPlace);
                    itemSet[nearestPlace] = dragItem;
                }
                else
                {
                    dragItem.SetTargetPlace(placeList[fallbackIndex]);
                    itemSet[placeList[fallbackIndex]] = dragItem;
                }
                UpdateCharacterSkillOutfit();
                dragState = 0;
            }
#endif

        }
    }

    void DrawPlace(Vector2 centerPos, Texture2D tex)
    {
        GUI.DrawTexture(new Rect(centerPos.x - CsGUI.CX(62), centerPos.y - CsGUI.CY(62), CsGUI.CY(125), CsGUI.CY(125)), tex);
    }

    void UpdateCharacterSkillOutfit()
    {
        for (int i = 0; i < 4; ++i)
        {
            if (itemSet[placeList[i]] != null)
                commonData.characterData.outfit.currentPackage[i] = itemSet[placeList[i]].ID;
        }
    }
}
