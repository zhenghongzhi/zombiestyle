using UnityEngine;
using System.Collections;

//Draw a Column of Skill Showcase
public class SkillShowcaseView : ITouchSlidePanelView
{
    public ShopGUI commonData;
    Texture2D[] skillTex;
    SkillData[] skillData;

    public float Width
    {
        get { return CsGUI.CX(260); }
    }

    public float Height
    {
        get { return 400; }
    }

    public SkillShowcaseView(SkillData up, SkillData down)
    {
        skillTex = new Texture2D[2];
        skillData = new SkillData[2];
        skillData[0] = up;
        skillData[1] = down;
    }

    public SkillShowcaseView(SkillData up)
    {
        skillTex = new Texture2D[2];
        skillData = new SkillData[2];
        skillData[0] = up;
        skillData[1] = null;
    }

    public void LoadTexture()
    {
        if (skillData[0] != null)
            skillTex[0] = Resources.Load("Skill/bg_" + skillData[0].SkillID + "_128", typeof(Texture2D)) as Texture2D;
        if (skillData[1] != null)
            skillTex[1] = Resources.Load("Skill/bg_" + skillData[1].SkillID + "_128", typeof(Texture2D)) as Texture2D;
    }

    public void OnDrawView(float startX, float startY)
    {
        float normX = startX / CsGUI.XRate;
        DrawSellBox(normX, 10, 0);
        if (skillData[1] != null)
            DrawSellBox(normX, 260, 1);
    }

    void DrawSellBox(float posX, float posY, int i)
    {
        //Draw Weapon Icon
        GUI.DrawTexture(CsGUI.CRect(posX + 60, posY + 25, 128, 128), skillTex[i]);
        GUI.Label(CsGUI.CRect(posX, posY, 250, 180), skillData[i].SkillName);

        //Draw Frame
        GUI.DrawTexture(CsGUI.CRect(posX, posY, 260, 258), commonData.weaponFrame);
        //Draw Selected Frame
        if (commonData.selectedSkillID == skillData[i].SkillID)
        {
            GUI.DrawTexture(CsGUI.CRect(posX + 58, posY + 23, 132, 132), commonData.skillSelectedFrame);
        }
        //Invisible Selection Button
        if (GUI.Button(CsGUI.CRect(posX, posY, 250, 180), "", "btnWeaponPreview") && commonData.selectedSkillID != skillData[i].SkillID)
        {
#if !UNITY_EDITOR
            if(Input.touches[0].deltaPosition.x < 10)
            {
#endif
            commonData.selectedSkillID = skillData[i].SkillID;
#if !UNITY_EDITOR
            }
#endif
        }

        int currentSkillLevel = 0;
        CharacterSkillData skillRecord = commonData.characterData.GetSkill(skillData[i].SkillID);
        if (skillRecord != null)
            currentSkillLevel = skillRecord.skillLevel;
        if (currentSkillLevel == 0)//Not Learned Yet
        {
            //Draw Purchase Button
            if (GUI.Button(CsGUI.CRect(posX + 65, posY + 190, 120, 55), commonData.scripts.GetScript(0), commonData.buyItemStyle))
            {
                UpgradeSkill(skillData[i], currentSkillLevel + 1);
                commonData.EquipItem(skillData[i].SkillID);
            }
        }
        else
        {
            GUI.Label(CsGUI.CRect(posX + 185, posY + 145, 100, 30), "Lv:" + currentSkillLevel, commonData.skillLevelStyle);
            //Max Level Button
            if (currentSkillLevel == skillData[i].MaxSkillLv)//Up to Max Level
            {
                GUI.enabled = false;
                GUI.Label(CsGUI.CRect(posX + 7, posY + 190, 120, 55), commonData.scripts.GetScript(11), commonData.buyItemStyle);
            }
            else
            {
                //Draw Golds
                GUI.DrawTexture(CsGUI.CRect(posX + 10, posY + 143, 30, 30), commonData.goldIcon);
                GUI.Label(CsGUI.CRect(posX + 45, posY + 145, 100, 30), skillData[i].GetUpgradePrice(currentSkillLevel + 1).ToString(), commonData.goldPriceStyle);
                //Not Reach Needed Level yet
                if (skillData[i].GetUpgradeLv(currentSkillLevel + 1) > commonData.characterData.currentLevel)
                {
                    GUI.enabled = false;
                    GUI.Label(CsGUI.CRect(posX + 7, posY + 190, 120, 55), commonData.scripts.GetScript(12) + skillData[i].GetUpgradeLv(currentSkillLevel + 1), commonData.buyItemStyle);
                }
                else
                {
                    //Upgrade Button
                    if (GUI.Button(CsGUI.CRect(posX + 7, posY + 190, 120, 55), commonData.scripts.GetScript(2), commonData.buyItemStyle))
                    {
                        UpgradeSkill(skillData[i], currentSkillLevel + 1);
                    }
                }
            }
            GUI.enabled = true;
            //Suit up Button
            bool hasEquipt = false;
            foreach (int id in commonData.characterData.outfit.currentPackage)
            {
                if (id == skillData[i].SkillID)
                {
                    hasEquipt = true;
                    break;
                }
            }
            if (hasEquipt)
                GUI.enabled = false;
            if (GUI.Button(CsGUI.CRect(posX + 130, posY + 190, 120, 55), commonData.scripts.GetScript(hasEquipt ? 13 : 1), commonData.buyItemStyle))
            {
                commonData.EquipItem(skillData[i].SkillID);
            }
            GUI.enabled = true;
        }
    }

    void UpgradeSkill(SkillData skillData, int level)
    {
        if (commonData.playerData.gold >= skillData.GetUpgradePrice(level))
        {
            commonData.playerData.gold -= skillData.GetUpgradePrice(level);
            commonData.characterData.UpgradeSkill(skillData.SkillID, level);
        }
    }
}


//Draw a Column of Item Showcase
public class ItemShowcaseView : ITouchSlidePanelView
{
    public ShopGUI commonData;
    Texture2D[] itemTex;
    ItemData[] itemData;

    public float Width
    {
        get { return CsGUI.CX(260); }
    }

    public float Height
    {
        get { return 400; }
    }

    public ItemShowcaseView(ItemData up, ItemData down)
    {
        itemTex = new Texture2D[2];
        itemData = new ItemData[2];
        itemData[0] = up;
        itemData[1] = down;
    }

    public ItemShowcaseView(ItemData up)
    {
        itemTex = new Texture2D[2];
        itemData = new ItemData[2];
        itemData[0] = up;
        itemData[1] = null;
    }

    public void LoadTexture()
    {
//         if (itemData[0] != null)
//             itemTex[0] = Resources.Load("Item/bg_" + itemData[0].ItemID + "_128", typeof(Texture2D)) as Texture2D;
//         if (itemData[1] != null)
//             itemTex[1] = Resources.Load("Item/bg_" + itemData[1].ItemID + "_128", typeof(Texture2D)) as Texture2D;
    }

    public void OnDrawView(float startX, float startY)
    {
        float normX = startX / CsGUI.XRate;
        DrawSellBox(normX, 10, 0);
        if (itemData[1] != null)
            DrawSellBox(normX, 260, 1);
    }

    void DrawSellBox(float posX, float posY, int i)
    {
        //Draw Weapon Icon
      //  GUI.DrawTexture(CsGUI.CRect(posX + 60, posY + 25, 128, 128), itemTex[i]);
        GUI.Label(CsGUI.CRect(posX, posY, 250, 180), itemData[i].ItemName);

        //Draw Frame
        GUI.DrawTexture(CsGUI.CRect(posX, posY, 260, 258), commonData.weaponFrame);
        //Draw Selected Frame
        if (commonData.selectedSkillID == itemData[i].ItemID)
        {
            GUI.DrawTexture(CsGUI.CRect(posX + 58, posY + 23, 132, 132), commonData.skillSelectedFrame);
        }
        //Invisible Selection Button
        if (GUI.Button(CsGUI.CRect(posX, posY, 250, 180), "", "btnWeaponPreview") && commonData.selectedItemID != itemData[i].ItemID)
        {
#if !UNITY_EDITOR
            if(Input.touches[0].deltaPosition.x < 10)
            {
#endif
            commonData.selectedItemID = itemData[i].ItemID;
#if !UNITY_EDITOR
            }
#endif
        }

        CharacterItemData itemRecord = commonData.characterData.GetItem(itemData[i].ItemID);
        //Purchase Button
        if (GUI.Button(CsGUI.CRect(posX + 7, posY + 190, 120, 55), commonData.scripts.GetScript("Buy"), commonData.buyItemStyle))
        {
            BuyItem(itemData[i]);
        }
        //Draw Golds
        GUI.DrawTexture(CsGUI.CRect(posX + 10, posY + 143, 30, 30), commonData.goldIcon);
        GUI.Label(CsGUI.CRect(posX + 45, posY + 145, 100, 30), itemData[i].Gold.ToString(), commonData.goldPriceStyle);
        //Draw Amount
        if (itemRecord != null)
        {
            GUI.Label(CsGUI.CRect(posX + 185, posY + 145, 100, 30), "Num:" + itemRecord.number, commonData.skillLevelStyle);

            GUI.enabled = true;
            //Suit up Button
            bool hasEquipt = false;
            foreach (int id in commonData.characterData.outfit.currentPackage)
            {
                if (id == itemData[i].ItemID)
                {
                    hasEquipt = true;
                    break;
                }
            }
            if (hasEquipt)
                GUI.enabled = false;
            if (GUI.Button(CsGUI.CRect(posX + 130, posY + 190, 120, 55), commonData.scripts.GetScript(hasEquipt ? 13 : 1), commonData.buyItemStyle))
            {
                commonData.EquipItem(itemData[i].ItemID);
            }
            GUI.enabled = true;
        }
    }

    void BuyItem(ItemData itemData)
    {
        if (commonData.playerData.gold >= itemData.Gold)
        {
            commonData.playerData.gold -= itemData.Gold;
            commonData.characterData.BuyItem(itemData.ItemID);
        }
    }
}

//Draw a Column of Weapon Showcase
public class GunShowcaseView : ITouchSlidePanelView
{
    public ShopGUI commonData;
    Texture2D[] weaponTex;
    WeaponData[] weaponData;

    public float Width
    {
        get { return CsGUI.CX(260); }
    }

    public float Height
    {
        get { return 400; }
    }

    public GunShowcaseView(WeaponData up, WeaponData down)
    {
        weaponTex = new Texture2D[2];
        weaponData = new WeaponData[2];
        weaponData[0] = up;
        weaponData[1] = down;
    }

    public GunShowcaseView(WeaponData up)
    {
        weaponTex = new Texture2D[2];
        weaponData = new WeaponData[2];
        weaponData[0] = up;
        weaponData[1] = null;
    }

    public void LoadTexture()
    {
        if (weaponData[0] != null)
            weaponTex[0] = Resources.Load("Shop/bg_" + weaponData[0].modelName, typeof(Texture2D)) as Texture2D;
        if (weaponData[1] != null)
            weaponTex[1] = Resources.Load("Shop/bg_" + weaponData[1].modelName, typeof(Texture2D)) as Texture2D;
    }

    public void OnDrawView(float startX, float startY)
    {
        float normX = startX / CsGUI.XRate;
        DrawWeaponSellBox(normX, 10, 0);
        if (weaponData[1] != null)
            DrawWeaponSellBox(normX, 260, 1);
    }

    void DrawWeaponSellBox(float posX, float posY, int i)
    {
        //Draw Weapon Icon
        GUI.DrawTexture(CsGUI.CRect(posX, posY, 250, 180), weaponTex[i]);
        //Draw Frame
        GUI.DrawTexture(CsGUI.CRect(posX, posY, 260, 258), commonData.weaponFrame);
        //Draw Selected Frame
        if (commonData.selectedGunID == weaponData[i].id)
        {
            GUI.DrawTexture(CsGUI.CRect(posX, posY, 250, 180), commonData.selectedFrame);
        }
        //Invisible Selection Button
        if (GUI.Button(CsGUI.CRect(posX, posY, 250, 180), "", "btnWeaponPreview") && commonData.selectedGunID != weaponData[i].id)
        {
#if !UNITY_EDITOR
            if(Input.touches[0].deltaPosition.x < 10)
            {
#endif
            commonData.ChangeGun(weaponData[i]);
            commonData.selectedGunID = weaponData[i].id;
#if !UNITY_EDITOR
            }
#endif
        }
        //Draw Level Lock Icon
        if (weaponData[i].levelLock > commonData.characterData.currentLevel)//Weapon Locked
        {
            //Draw Lock & Grade
            GUI.Box(CsGUI.CRect(posX + 40, posY + 80, 80, 40), "Locked");
            GUI.Label(CsGUI.CRect(posX + 40, posY + 175, 180, 40), "Level : " + weaponData[i].levelLock);
        }
        else//UnLocked
        {
            CharacterArmData arm = commonData.characterData.GetGun(weaponData[i].id);
            //Brought This Weapon Already
            if (arm != null)
            {
                //Draw Grade Stars
                int grade = arm.grade;
                for (int s = 0; s < 3; ++s)
                {
                    int starID = Mathf.Clamp(grade - 2 * s, 0, 2);
                    GUI.DrawTexture(CsGUI.CRect(posX + 30 + s * 70, posY + 10, 48, 48), commonData.weaponGradeIcon[starID]);
                }
                //Suit Up Button
                if (commonData.characterData.outfit.currentGunID == weaponData[i].id)
                    GUI.enabled = false;
                if (GUI.Button(CsGUI.CRect(posX + 7, posY + 190, 120, 55), commonData.scripts.GetScript(1), commonData.operateItemStyle))
                {
                    Event.current.Use();
                    commonData.ChangeGun(weaponData[i]);
                    commonData.selectedGunID = weaponData[i].id;
                    commonData.characterData.outfit.currentGunID = weaponData[i].id;
                }
                GUI.enabled = true;
                //Upgrade Gold
                if (arm.grade == 6)
                {
                    GUI.enabled = false;
                }
                else
                {
                    GUI.DrawTexture(CsGUI.CRect(posX + 10, posY + 143, 30, 30), commonData.goldIcon );
                    GUI.Label(CsGUI.CRect(posX + 45, posY + 145, 100, 30), weaponData[i].upgradeGold[arm.grade].ToString(), commonData.goldPriceStyle);
                }
                //Upgrade Button
                if (GUI.Button(CsGUI.CRect(posX + 130, posY + 190, 120, 55), commonData.scripts.GetScript(2), commonData.operateItemStyle))
                {
                    Event.current.Use();
                    if (commonData.playerData.gold >= weaponData[i].upgradeGold[arm.grade])
                    {
                        commonData.playerData.gold -= weaponData[i].upgradeGold[arm.grade];
                        arm.grade++;
                    }
                }
                GUI.enabled = true;
            }
            else//Haven't Brought Yet
            {
                //Draw Money
                GUI.DrawTexture(CsGUI.CRect(posX + 10, posY + 143, 30, 30), (weaponData[i].payType == 0 ? commonData.goldIcon : commonData.diamondIcon));
                GUI.Label(CsGUI.CRect(posX + 45, posY + 145, 100, 30), weaponData[i].price.ToString(), (weaponData[i].payType == 0 ? commonData.goldPriceStyle : commonData.diamondPriceStyle));
                //Draw Purchase Button
                if (GUI.Button(CsGUI.CRect(posX + 67, posY + 190, 120, 55), commonData.scripts.GetScript(0), commonData.buyItemStyle))
                {
                    Event.current.Use();
                    commonData.BuyGun(weaponData[i]);
                }
            }
        }//Finish Draw Buttons
    }
}

//Draw a Column of Item Showcase
public class InnateShowcaseView : ITouchSlidePanelView
{
    public ShopGUI commonData;
    Texture innateTexUp;
    Texture innateTexDown;
    InnateData innateDataUp;
    InnateData innateDataDown;

    public float Width
    {
        get { return CsGUI.CX(260); }
    }

    public float Height
    {
        get { return 400; }
    }

    public InnateShowcaseView(InnateData up, InnateData down)
    {
        innateDataUp = up;
        innateDataDown = down;
    }

    public InnateShowcaseView(InnateData up)
    {
        innateDataUp = up;
    }

    public void LoadTexture()
    {
        if (innateDataUp != null)
            innateTexUp = Resources.Load("Innate/bg_" + innateDataUp.InnateIcon, typeof(Texture2D)) as Texture2D;
        if (innateDataDown != null)
            innateTexDown = Resources.Load("Innate/bg_" + innateDataDown.InnateIcon, typeof(Texture2D)) as Texture2D;
    }

    public void OnDrawView(float startX, float startY)
    {
        float normX = startX / CsGUI.XRate;
        DrawSellBox(normX, 10, innateDataUp);
        if (innateDataDown != null)
            DrawSellBox(normX, 260, innateDataDown);
    }

    void DrawSellBox(float posX, float posY, InnateData data)
    {
        //Draw Weapon Icon
        //  GUI.DrawTexture(CsGUI.CRect(posX + 60, posY + 25, 128, 128), itemTex[i]);
        GUI.Label(CsGUI.CRect(posX, posY, 250, 180), data.InnateName);

        //Draw Frame
        GUI.DrawTexture(CsGUI.CRect(posX, posY, 260, 258), commonData.weaponFrame);
        //Draw Selected Frame
        if (commonData.selectedInnateID == data.InnateID)
        {
            GUI.DrawTexture(CsGUI.CRect(posX + 58, posY + 23, 132, 132), commonData.skillSelectedFrame);
        }
        //Invisible Selection Button
        if (GUI.Button(CsGUI.CRect(posX, posY, 250, 180), "", "btnWeaponPreview") && commonData.selectedInnateID != data.InnateID)
        {
#if !UNITY_EDITOR
            if(Input.touches[0].deltaPosition.x < 10)
            {
#endif
            commonData.selectedInnateID = data.InnateID;
#if !UNITY_EDITOR
            }
#endif
        }

        //Purchase Button
        if (GUI.Button(CsGUI.CRect(posX + 7, posY + 190, 120, 55), commonData.scripts.GetScript("Buy"), commonData.buyItemStyle))
        {
            InnateManager.Upgrade(data.InnateID, data.InnateLV);
        }
        //Draw Golds
        GUI.DrawTexture(CsGUI.CRect(posX + 10, posY + 143, 30, 30), commonData.goldIcon);
        GUI.Label(CsGUI.CRect(posX + 45, posY + 145, 100, 30), data.Cost.ToString(), commonData.goldPriceStyle);
        //Draw Level
        GUI.Label(CsGUI.CRect(posX + 185, posY + 145, 100, 30), "Level:" + data.InnateLV, commonData.skillLevelStyle);
    }
}

