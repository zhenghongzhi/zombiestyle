using UnityEngine;
using System.Collections.Generic;
using System.Collections;

public enum ShopCategory : int
{
    New = 0,
    Weapon,
    Skill,
    Package,
    Innate,
}

public class ShopGUI : MonoBehaviour {
#region Data Related Parameters
    //CommonShopData commonShopData;
    [HideInInspector]
    public List<WeaponData> characterWeaponData = null;
    [HideInInspector]
    public PlayerStoreData playerData = null;
    [HideInInspector]
    public CharacterStoreData characterData = null;
#endregion

#region  Scene Related Parameters
    public Transform playerPosition;
    public GameObject player;
#if UNITY_EDITOR || UNITY_STANDALONE_WIN
    float lastPosX;
#endif
    bool rotateModel = false;
#endregion

#region GUI Related Parameters
    public GUISkin mySkin;
    public RenderTexture wardrobeTex;
    public Texture2D leftFrame;
    public Texture2D showcaseBg;
    public Texture2D infoBg;
    public Texture2D weaponFrame;
    public Texture2D selectedFrame;
    public Texture2D skillSelectedFrame;
    public Texture2D skillIconFrame;
    public Texture2D enablePageIcon;
    public Texture2D disablePageIcon;
    public Texture2D goldIcon;
    public Texture2D diamondIcon;
    public Texture2D enableTag;
    public Texture2D disableTag;
    public Texture2D[] weaponGradeIcon;
    public Texture2D barBackground;
    public Texture2D attackBar;
    public Texture2D rateBar;
    public Texture2D armBar;
    public ShopCategory category = ShopCategory.Weapon;
    [HideInInspector]
    public GUIStyle buyItemStyle;
    [HideInInspector]
    public GUIStyle operateItemStyle;
    [HideInInspector]
    public GUIStyle goldPriceStyle;
    [HideInInspector]
    public GUIStyle diamondPriceStyle;
    [HideInInspector]
    public GUIStyle gunInfoStyle;
    [HideInInspector]
    public GUIStyle skillInfoStyle;
    [HideInInspector]
    public GUIStyle skillLevelStyle;
    [HideInInspector]
    public int selectedGunID = 0;
    [HideInInspector]
    public int selectedSkillID = 0;
    [HideInInspector]
    public int selectedItemID = 0;
    [HideInInspector]
    public int selectedInnateID = 0;

    [HideInInspector]
    public TextScript scripts;

    HorizontalTouchSlidePanel weaponShowcasePanel;
    HorizontalTouchSlidePanel skillShowcasePanel;
    HorizontalTouchSlidePanel itemShowcasePanel;
    HorizontalTouchSlidePanel innateShowcasePanel;
    PackageBoard packageBoard;

#endregion

    void Awake()
    {
        //Set Character Model Transform in this Scene
        player = GameObject.FindGameObjectWithTag("Player");
        if (player)
        {
            player.transform.position = playerPosition.position;
            player.transform.rotation = playerPosition.rotation;
        }
    }

    void Start()
    {
        //Init Game Data
        if (GameObject.Find("GlobalData"))
        {
            playerData = GameObject.Find("GlobalData").GetComponent<GlobalPlayerInfo>().playerData;
            characterData = GameObject.Find("GlobalData").GetComponent<GlobalPlayerInfo>().SelectedCharData;
            characterWeaponData = StaticGameData.GetCharacterWeaponData(characterData.charID);
            selectedGunID = characterData.outfit.currentGunID;
        }
        buyItemStyle = new GUIStyle(mySkin.GetStyle("btnBuyItem"));
        buyItemStyle.padding.top = (int)CsGUI.CY(buyItemStyle.padding.top);
        buyItemStyle.fontSize = (int)CsGUI.CY(buyItemStyle.fontSize);
        operateItemStyle = new GUIStyle(mySkin.GetStyle("btnOperateItem"));
        operateItemStyle.padding.top = (int)CsGUI.CY(operateItemStyle.padding.top);
        operateItemStyle.fontSize = (int)CsGUI.CY(operateItemStyle.fontSize);
        goldPriceStyle = new GUIStyle(mySkin.GetStyle("labGoldPrice"));
        goldPriceStyle.fontSize = (int)CsGUI.CY(goldPriceStyle.fontSize);
        diamondPriceStyle = new GUIStyle(mySkin.GetStyle("labDiamondPrice"));
        diamondPriceStyle.fontSize = (int)CsGUI.CY(diamondPriceStyle.fontSize);
        gunInfoStyle = new GUIStyle(mySkin.GetStyle("labGunInfo"));
        gunInfoStyle.fontSize = (int)CsGUI.CY(gunInfoStyle.fontSize);
        skillInfoStyle = new GUIStyle(mySkin.GetStyle("labSkillInfo"));
        skillInfoStyle.fontSize = (int)CsGUI.CY(skillInfoStyle.fontSize);
        skillLevelStyle = new GUIStyle(mySkin.GetStyle("labSkillLevel"));
        skillLevelStyle.fontSize = (int)CsGUI.CY(skillLevelStyle.fontSize);
        GameObject.Find("TopMenuGUI").GetComponent<TopMenuGUI>().showBack = true;

        scripts = StaticGameScripts.GetSceneScript("Shop");

        LoadShowCases();
        Vector2[] packagePlaces = { new Vector2(CsGUI.CX(280), CsGUI.CY(124)), new Vector2(CsGUI.CX(145), CsGUI.CY(160)), new Vector2(CsGUI.CX(98), CsGUI.CY(285)), new Vector2(CsGUI.CX(91), CsGUI.CY(421)) };
        packageBoard = new PackageBoard(packagePlaces);
        packageBoard.commonData = this;
        for(int i = 0; i < 4; ++i)
        {
            if (characterData.outfit.currentPackage[i] != -1)
                packageBoard.AddItem(new DragableSkill(StaticGameData.GetSkillData(characterData.outfit.currentPackage[i])), i);
        }
    }

    void LoadShowCases()
    {
        List<ITouchSlidePanelView> contents = new List<ITouchSlidePanelView>();
        for (int i = 0; i < characterWeaponData.Count; i+=2)
        {
            GunShowcaseView view = null;
            if (characterWeaponData.Count - i > 1)
                view = new GunShowcaseView(characterWeaponData[i], characterWeaponData[i + 1]);
            else
                view = new GunShowcaseView(characterWeaponData[i]);
            view.commonData = this;
            contents.Add(view);
        }
        foreach (GunShowcaseView view in contents)
        {
            view.LoadTexture();
        }
        weaponShowcasePanel = new HorizontalTouchSlidePanel(CsGUI.CRect(388, 179, 636, 550), (int)ShopCategory.Weapon, mySkin.GetStyle("wndShowcase"), contents, 0, 300);

        contents = new List<ITouchSlidePanelView>();
        List<SkillData> characterSkills = StaticGameData.CharSkillData[playerData.selectedCharID];
        selectedSkillID = characterSkills[0].SkillID;
        for (int i = 0; i < characterSkills.Count; i+=2)
        {
            SkillShowcaseView view = null;
            if (characterSkills.Count - i > 1)
                view = new SkillShowcaseView(characterSkills[i], characterSkills[i + 1]);
            else
                view = new SkillShowcaseView(characterSkills[i]);
            view.commonData = this;
            view.LoadTexture();
            contents.Add(view);
        }
        skillShowcasePanel = new HorizontalTouchSlidePanel(CsGUI.CRect(388, 179, 636, 550), (int)ShopCategory.Skill, mySkin.GetStyle("wndShowcase"), contents, 0, 300);

        contents = new List<ITouchSlidePanelView>();
        List<ItemData> charcaterItems = StaticGameData.ItemList;
        selectedItemID = charcaterItems[0].ItemID;
        for (int i = 0; i < charcaterItems.Count; i += 2)
        {
            ItemShowcaseView view = null;
            if (charcaterItems.Count - i > 1)
                view = new ItemShowcaseView(charcaterItems[i], charcaterItems[i + 1]);
            else
                view = new ItemShowcaseView(charcaterItems[i]);
            view.commonData = this;
            view.LoadTexture();
            contents.Add(view);
        }
        itemShowcasePanel = new HorizontalTouchSlidePanel(CsGUI.CRect(388, 179, 636, 550), (int)ShopCategory.Package, mySkin.GetStyle("wndShowcase"), contents, 0, 300);
    
        contents = new List<ITouchSlidePanelView>();
        var storedInnate = PlayerStoreData.StoredData.SelectedChar;
        var innateIdList = StaticGameData.InnateList;
        selectedInnateID = innateIdList[0];
        for (int i = 0; i < innateIdList.Count; i += 2)
        {
            InnateShowcaseView view = null;
            if (innateIdList.Count - i > 1)
            {
                view = new InnateShowcaseView(StaticGameData.GetInnate(innateIdList[i], storedInnate.GetInnateLvl(innateIdList[i])), 
                   StaticGameData.GetInnate(innateIdList[i+1], storedInnate.GetInnateLvl(innateIdList[i+1])));
            }
            else
            {
                view = new InnateShowcaseView(StaticGameData.GetInnate(innateIdList[i], storedInnate.GetInnateLvl(innateIdList[i])));
            }
            view.commonData = this;
            view.LoadTexture();
            contents.Add(view);
        }
        innateShowcasePanel = new HorizontalTouchSlidePanel(CsGUI.CRect(388, 179, 636, 550), (int)ShopCategory.Package, mySkin.GetStyle("wndShowcase"), contents, 0, 300);
    }

    void Update()
    {
#region Rotate Model
#if UNITY_EDITOR || UNITY_STANDALONE_WIN
        if (rotateModel)
        {
            Quaternion rotateY = Quaternion.AngleAxis(-Input.mousePosition.x + lastPosX, Vector3.up);
            player.transform.rotation *= rotateY;
            playerPosition.rotation *= rotateY;
            lastPosX = Input.mousePosition.x;
            if (Input.GetMouseButtonUp(0) || !CsGUI.CRect(0, 130, 280, 500).Contains(Input.mousePosition))
                rotateModel = false;
        }
        else if (Input.GetMouseButtonDown(0) && CsGUI.CRect(0, 130, 280, 500).Contains(Input.mousePosition))
        {
            lastPosX = Input.mousePosition.x;
            rotateModel = true;
        }
#endif
#if !UNITY_EDITOR
            if( rotateModel)
            {   
                Quaternion rotateY = Quaternion.AngleAxis(-Input.touches[0].deltaPosition.x, Vector3.up);
                player.transform.rotation *= rotateY;
                playerPosition.rotation *= rotateY;
                if (Input.touchCount < 1)
                    rotateModel = false;
                else if(!CsGUI.CRect(0, 130, 280, 500).Contains(Input.touches[0].position))
                    rotateModel = false;
            }
            if(Input.touchCount == 1 && Input.touches[0].phase == TouchPhase.Began &&
                CsGUI.CRect(0, 130, 280, 500).Contains(Input.touches[0].position))
            {
                rotateModel = true;
            }

#endif
#endregion
    }

    //Receive Message From TopMenue GUI
    void Back()
    {
        if (selectedGunID != characterData.outfit.currentGunID)
            ChangeGun(StaticGameData.GetWeaponData(characterData.outfit.currentGunID));
        Application.LoadLevel("MainMenu");
    }

    void OnDestroy()
    {
        Resources.UnloadUnusedAssets();
    }

#region GUI Related Methods
	void OnGUI()
    {
        GUI.skin = mySkin;
        GUI.depth = 20;
        switch (category)
        {
            case ShopCategory.Weapon:
                GUI.DrawTexture(new Rect(-CsGUI.CX(55), CsGUI.CY(36), CsGUI.CX(490), CsGUI.CY(490)), wardrobeTex, ScaleMode.ScaleAndCrop, false, 1);
                break;
            case ShopCategory.Skill:
                packageBoard.Draw(new Rect(CsGUI.CX(5), CsGUI.CY(79), CsGUI.CX(350), CsGUI.CY(420)));
                break;
            case ShopCategory.Package:
                packageBoard.Draw(new Rect(CsGUI.CX(5), CsGUI.CY(79), CsGUI.CX(350), CsGUI.CY(420)));
                break;
            case ShopCategory.Innate:
                packageBoard.Draw(new Rect(CsGUI.CX(5), CsGUI.CY(79), CsGUI.CX(350), CsGUI.CY(420)));
                break;
        }

        for (int i = 0; i < 25; ++i )
            GUI.DrawTexture(CsGUI.CRect(389 + 27 * i, 151, 27, 620), showcaseBg);
        GUI.DrawTexture(CsGUI.CRect(334, 145, 55, 625), leftFrame);

        CategorySelection();

        DrawInfoBoard();

        GUI.Window((int)category, CsGUI.CRect(388, 179, 642, 550), DrawShowCase, "", "wndShowcase");
        switch (category)
        {
            case ShopCategory.Weapon:
                DrawPageIcon(weaponShowcasePanel);
                break;
            case ShopCategory.Skill:
                DrawPageIcon(skillShowcasePanel);
                break; ;
            case ShopCategory.Package:
                DrawPageIcon(itemShowcasePanel);
                break;
            case ShopCategory.Innate:
                DrawPageIcon(innateShowcasePanel);
                break;
        }
    }

    void DrawInfoBoard()
    {
        GUI.DrawTexture(CsGUI.CRect(0, 478, 387, 293), infoBg);
        switch (category)
        {
            case ShopCategory.Weapon:
                {
                    GUI.contentColor = Color.red;
                    GUI.Label(CsGUI.CRect(40, 550, 130, 50), scripts.GetScript(3), gunInfoStyle);//Damage
                    GUI.contentColor = Color.cyan;
                    GUI.Label(CsGUI.CRect(40, 615, 130, 50), scripts.GetScript(4), gunInfoStyle);//Rate
                    GUI.contentColor = Color.green;
                    GUI.Label(CsGUI.CRect(40, 680, 130, 50), scripts.GetScript(5), gunInfoStyle);//Loader Size
                    GUI.contentColor = Color.white;
                    WeaponData selectedWeaponData = StaticGameData.GetWeaponData(selectedGunID);
                    int gunGrade = 0;
                    if (characterData.HasGun(selectedGunID))
                        gunGrade = characterData.GetGun(selectedGunID).grade;
                    float dmgPer = selectedWeaponData.dmg[gunGrade] / 310.0f;
                    if (selectedWeaponData.weaponType == "shotgun")
                        dmgPer *= 5;
                    float loaderPer = selectedWeaponData.loaderSize[gunGrade] / 150.0f;
                    float ratePer = 0.08f / selectedWeaponData.atkRate[gunGrade];
                    GUI.DrawTexture(CsGUI.CRect(155, 575, 181, 13), barBackground);
                    GUI.DrawTexture(CsGUI.CRect(155, 635, 181, 13), barBackground);
                    GUI.DrawTexture(CsGUI.CRect(155, 695, 181, 13), barBackground);
                    GUI.DrawTextureWithTexCoords(CsGUI.CRect(155, 575, 181 * dmgPer, 13), attackBar, new Rect(0, 0, dmgPer, 1));
                    GUI.DrawTextureWithTexCoords(CsGUI.CRect(155, 635, 181 * ratePer, 13), rateBar, new Rect(0, 0, ratePer, 1));
                    GUI.DrawTextureWithTexCoords(CsGUI.CRect(155, 695, 181 * loaderPer, 13), armBar, new Rect(0, 0, loaderPer, 1));
                }
                break;
            case ShopCategory.Skill:
                {
                    SkillData skill = StaticGameData.GetSkillData(selectedSkillID);
                    GUI.Label(CsGUI.CRect(40, 550, 300, 280), scripts.GetScript(6) + " : " + scripts.GetScript(8 - skill.SkillType) + "\n" +
                        scripts.GetScript(9) + " : " + (skill.CDTime == 0 ? "-" : skill.CDTime + "s") + "\n" +
                        scripts.GetScript(10) + " : " + StaticGameData.GetSkillData(selectedSkillID).SkillDescribe, skillInfoStyle);
                }
                break;
        }
    }

    void DrawLeftShowBoard()
    {

    }

    //Select The Category of Showcase
    void CategorySelection()
    {
        for (int i = 4; i >= 0; --i)
        {
            GUI.DrawTexture(CsGUI.CRect(360 + i * 123, 88, 183, 64), disableTag);
            if (GUI.Toggle(CsGUI.CRect(380 + i * 123, 88, 123, 64), category == (ShopCategory)i, "", "noneToggle"))
            {
                category = (ShopCategory)i;
            }
        }
        GUI.DrawTexture(CsGUI.CRect(360 + (int)category * 123, 78, 176, 114), enableTag);
    }

    //Showcase Window
    void DrawShowCase(int i)
    {
        switch (i)
        {
            case (int)ShopCategory.Weapon:
                weaponShowcasePanel.Update();
                weaponShowcasePanel.OnDraw();
                break;
            case (int)ShopCategory.Skill:
                skillShowcasePanel.Update();
                skillShowcasePanel.OnDraw();
                break;
            case (int)ShopCategory.Package:
                itemShowcasePanel.Update();
                itemShowcasePanel.OnDraw();
                break;
        }
    }

    void DrawPageIcon(HorizontalTouchSlidePanel panel)
    {
        float startX = 670 - 55 * panel.TotalPage * 0.5f;
        for (int i = 0; i < panel.TotalPage; ++i)
        {
            GUI.DrawTexture(CsGUI.CRect(startX + i * 55, 726, 22, 22), (i == panel.CurrentPage) ? enablePageIcon : disablePageIcon);
        }
    }
#endregion

#region Button Methods
    //Change Character Model -> Change Same Type Gun
    public void ChangeGun(WeaponData weaponData)
    {
        //Change Same Type of Gun, Don't need to Change Model Prefab
        if(StaticGameData.GetWeaponData(selectedGunID).weaponType == weaponData.weaponType)
        {
            player = CharacterModelManager.ChangeSameTypeGun(player, weaponData);
        }
        else
        {
            string modelName = characterData.charName + "-" + weaponData.weaponType;
            Destroy(player);
            player = CharacterModelManager.CreateNewCharacter(modelName, weaponData);
            player.transform.position = playerPosition.position;
            player.transform.rotation = playerPosition.rotation;
            DontDestroyOnLoad(player);
        }
        player.GetComponent<Weapon>().gunTransforms[0].GetComponents<AudioSource>()[1].Play();
    }

    public void BuyGun(WeaponData weaponData)
    {
        if (weaponData.payType == 0)//Pay by Gold
        {
            if(playerData.gold >= weaponData.price)
            {
                playerData.gold -= weaponData.price;
                characterData.BuyGun(weaponData.id);
                ChangeGun(weaponData);
                selectedGunID = weaponData.id;
                characterData.outfit.currentGunID = weaponData.id;
            }
        }
        else//Pay by Diamond
        {
            if (playerData.diamond >= weaponData.price)
            {
                playerData.diamond -= weaponData.price;
                characterData.BuyGun(weaponData.id);
                ChangeGun(weaponData);
                selectedGunID = weaponData.id;
                characterData.outfit.currentGunID = weaponData.id;
            }
        }
    }

    public void EquipItem(int itemID)
    {
        int placeIndex = 0;
        foreach (int id in characterData.outfit.currentPackage)
        {
            if (id == -1)
                break;
            placeIndex++;
        }
        DragableItemBase item = null;
        if (itemID < 5000)
            item = new DragableEquipment(StaticGameData.GetItem(itemID));
        else
            item = new DragableSkill(StaticGameData.GetSkillData(itemID));
        if (placeIndex < 4)
            packageBoard.AddItem(item, placeIndex);
        else
            packageBoard.AddItem(item, 0);
    }
#endregion

}
