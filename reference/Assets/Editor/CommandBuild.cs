/**
/Applications/Unity/Unity.app/Contents/MacOS/Unity \
  -batchmode \
  -quit \
  -projectPath $PROJECT_PATH \
  -executeMethod CommandBuild.BuildAndroid
*/

using UnityEditor;
using UnityEngine;

public class CommandBuild : MonoBehaviour 
{
    public static void BuildAndroid()
    {
        string[] levels = { "Assets/Scene/SelectCharacter.unity", 
                            "Assets/Scene/MainMenu.unity", 
                            "Assets/Scene/SelectLevel.unity", 
                            "Assets/Scene/Shop.unity", 
                            "Assets/Scene/zombies_style.unity", 
                            "Assets/Scene/loadingPage.unity", 
                            "Assets/Scene/Lobby.unity", 
                            "Assets/Scene/testMultiplayerScene.unity", 
                          };
        BuildPipeline.BuildPlayer(levels, "Zombie.apk", BuildTarget.Android, BuildOptions.AutoRunPlayer);
    }
}
