using UnityEngine;
using UnityEditor;
using System.IO;
using System.Collections;

public class SetAutoTransparent : ScriptableWizard {

    public GameObject staticSceneRoot;
    public LayerMask blockLayer;

    [MenuItem("ZombieStyle/Build Auto Transparent")]
    static void CreateWizard()
    {
        ScriptableWizard.DisplayWizard<SetAutoTransparent>("Set Auto Transparent", "Build");
    }

    void OnWizardCreate()
    {
        foreach (Transform t in staticSceneRoot.GetComponentsInChildren<Transform>())
        {
            if (blockLayer.value >> (t.gameObject.layer - 1) > 0)
            {
                foreach (Renderer r in t.GetComponentsInChildren<Renderer>())
                {
                    Material tMat = BuildTransparentMaterial(r.sharedMaterial);
                    AutoTransparent script = r.GetComponent<AutoTransparent>();
                    if (script == null)
                        script = r.gameObject.AddComponent<AutoTransparent>();
                    script.originalMat = r.sharedMaterial;
                    script.transMat = tMat;
                }
            }
        }
    }

    Material BuildTransparentMaterial(Material m)
    {
        string path = AssetDatabase.GetAssetPath(m);
        path = path.Substring(0, path.LastIndexOf("/")) + "/Tmat";
        if (!Directory.Exists(path))
            Directory.CreateDirectory(path);
        if (!File.Exists(path + "/" + m.name + "-t.mat"))
        {
            Material tmat = new Material(Shader.Find("Transparent/Diffuse"));
            tmat.mainTexture = m.mainTexture;
            tmat.color = new Color(1, 1, 1, .3f);
            AssetDatabase.CreateAsset(tmat, path + "/" + m.name + "-t.mat");
        }

        Debug.Log("Build Transparent Material : " + path + m.name + "-t.mat");
        return (Material)AssetDatabase.LoadAssetAtPath(path + "/" + m.name + "-t.mat", typeof(Material));
    }
}
