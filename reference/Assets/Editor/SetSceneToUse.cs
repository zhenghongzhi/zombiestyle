using UnityEngine;
using UnityEditor;
using System.Collections;

public class SetSceneToUse : EditorWindow {

    int sceneID;

    [MenuItem("ZombieStyle/Set Scene to Use")]
    static void Init()
    {
        // Get existing open window or if none, make a new one:
        EditorWindow.GetWindow(typeof(SetSceneToUse));
    }

    void OnGUI()
    {
        sceneID = EditorGUILayout.IntField("����ID:", sceneID);
        if (GUILayout.Button("����"))
        {
            SetScene();
        }
    }

    void SetScene()
    {
        GameObject spawner = GameObject.FindGameObjectWithTag("ZombieSpawner");
        if(!spawner)
        {
            spawner = AssetDatabase.LoadAssetAtPath("Prefabs/ZombieSpawner", typeof(GameObject)) as GameObject;
        }
        if (spawner)
        {
            spawner.GetComponent<ZombieWaveSpawn>().sceneID = sceneID;
        }
    }
}
