using UnityEngine;
using UnityEditor;
using System.Collections;

public class AddZombieAI{

	 [MenuItem(@"Component/Zombie Style/Zombie AI State Controller")]
    static void AddAIStateComponent()
    {
        GameObject g = Selection.activeGameObject;
        if (g == null)
        {
            Debug.Log("No Object Select! Can't add component");
            return;
        }
        if (g.animation == null)
        {
            Debug.Log("Add the Component to a GameObject which Have a Animation Component");
            return;
        }
        Rigidbody r = g.AddComponent<Rigidbody>();
        r.constraints = RigidbodyConstraints.FreezeRotationX | RigidbodyConstraints.FreezeRotationZ;

        BoxCollider collider = g.AddComponent<BoxCollider>();
        collider.size = new Vector3(0.3f, 1.8f, 0.3f);
        collider.center = new Vector3(0, 0.9f, 0);

        g.AddComponent<PlayerMovementMoter>();
        ZombieAIStateController stateController = g.AddComponent<ZombieAIStateController>();
        g.AddComponent<ZombieAIBirthState>();
        g.AddComponent<ZombieAIIdleState>();
        g.AddComponent<ZombieAIWonderState>();
        g.AddComponent<ZombieAIChaseState>();
        g.AddComponent<ZombieAIAttackState>();

        stateController.walkAnim = g.animation.GetClip("walk");
        stateController.idleAnim = g.animation.GetClip("breath");
        stateController.birthAnim = g.animation.GetClip("birth");
        stateController.runAnim = g.animation.GetClip("run");
        stateController.deadAnim = g.animation.GetClip("dead");
        stateController.attackAnim = g.animation.GetClip("attack");
    }
}
