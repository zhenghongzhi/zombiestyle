using UnityEngine;
using UnityEditor;
using System.Collections.Generic;

public class AddTPSController{

    [MenuItem(@"ZombieStyle/Third Person Controller")]
    static void AddTPSControllerComponent()
    {
        GameObject g = Selection.activeGameObject;
        if (g == null)
        {
            Debug.Log("No Object Select! Can't add component");
            return;
        }
        if (g.animation == null)
        {
            Debug.Log("Add the Component to a GameObject which Have a Animation Component");
            return;
        }
        g.tag = "Player";
        Rigidbody r = g.AddComponent<Rigidbody>();
        r.constraints = RigidbodyConstraints.FreezeRotationX | RigidbodyConstraints.FreezeRotationZ;

        CapsuleCollider collider = g.AddComponent<CapsuleCollider>();
        collider.radius = 0.3f;
        collider.height = 1.8f;
        collider.center = new Vector3(0, 0.9f, 0);

        g.AddComponent<PlayerMovementMoter>();
        PlayerAnimation pa = g.AddComponent<PlayerAnimation>();
        g.AddComponent<PadInput>();
        g.AddComponent<TPSController>();

        Transform rootBone = g.transform.FindChild("Bip01");
        Transform upperRootBone = g.transform.FindChild("Bip01/Bip01 Spine");
        if (rootBone == null)
            Debug.LogWarning("Can't Find Root Bone. Remember to Set it Manually in PlayerAnimation Component");
        if (upperRootBone == null)
            Debug.LogWarning("Can't Find Upper Root Bone. Remember to Set it Manually in PlayerAnimation Component");
        pa.rootBone = rootBone;
        pa.upperBodyBone = upperRootBone;

        /*
        pa.idle = g.animation["Take 001"].clip;
        pa.run = g.animation["Take 001"].clip;
        pa.shoot = g.animation["attack_Lance"].clip;
         * */
        pa.moveAnimations = new MoveAnimation[4];
        pa.moveAnimations[0].clip = g.animation["run_forward"].clip;
        pa.moveAnimations[0].velocity = new Vector3(0, 0, 6);
        pa.moveAnimations[1].clip = g.animation["run_backward"].clip;
        pa.moveAnimations[1].velocity = new Vector3(0, 0, -6);
        pa.moveAnimations[2].clip = g.animation["run_right"].clip;
        pa.moveAnimations[2].velocity = new Vector3(6, 0, 0);
        pa.moveAnimations[3].clip = g.animation["run_left"].clip;
        pa.moveAnimations[3].velocity = new Vector3(-6, 0, 0);
    }
}
