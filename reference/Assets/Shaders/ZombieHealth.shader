Shader "ZombieStyle/ZombieHealth" {
	Properties {
		_MainColor ("Main color", Color) = (.4, .4, .4, 1)
		_HealthColor ("Health color", Color) = (1, 0, 0, 1)
		_Health ("Health value", Range(0.0, 1.0)) = 1.0
	}
	SubShader {
	Pass
	{
		Tags { "RenderType"="Opaque" }
		LOD 200
		
		CGPROGRAM
		#pragma vertex vert
		#pragma fragment frag
		#include "UnityCG.cginc"

		struct appdata {
			float4 vertex : POSITION;
			float4 texcoord : TEXCOORD0;
		};

		struct v2f {
			float4 pos : SV_POSITION;
			float2 texCoord : TEXCOORD0;
		 };

		 float4 _MainColor;
		 float4 _HealthColor;
		 float _Health;

		v2f vert (appdata v)
		{
			v2f o;
			o.pos = mul (UNITY_MATRIX_MVP, v.vertex);
			o.texCoord = v.texcoord.xy;
			return o;
		}

		half4 frag (v2f i) : COLOR0 
		{
			half4 c = _MainColor;
			if(i.texCoord.x < _Health)
				c = _HealthColor;
			return c; 
		}
		ENDCG
	}
	} 
	FallBack "Diffuse"
}
