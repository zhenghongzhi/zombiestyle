Shader "ZombieStyle/ScreenAliasCdMask" {
	Properties {
		_MainTex ("Base (RGB) Mask(A)", 2D) = "white" {}
		_CDAlphaMask ("Alpha Cut Off", Range(0, 1)) = 0
		_DurationAlphaMask ("Alpha Cut Off", Range(0, 1)) = 0
	}
	SubShader {
		Tags { "Queue"="Overlay" }
		LOD 200


		Pass {
			Cull Off

			CGPROGRAM

			#pragma vertex vert
			#pragma fragment frag
			#include "UnityCG.cginc"

			struct Input  {
				float4 vertex : POSITION;
				float4 texcoord : TEXCOORD0;
			};

			struct Output {
				float4 position : POSITION;
				float2 texcoord[2] : TEXCOORD0;
			};
			
			half4 _MainTex_TexelSize;

			Output vert(Input i) {
				Output o;
				o.position = mul(UNITY_MATRIX_MVP, i.vertex);
				o.texcoord[0] =  i.texcoord.xy;
				o.texcoord[1] = o.position.xy;
		
	//			#if UNITY_UV_STARTS_AT_TOP
 				if(_MainTex_TexelSize.y<0.0)
 					o.texcoord[0].y = 1.0-o.texcoord[0].y;
 	//			#endif
		
				return o;
			}

			sampler2D _MainTex;
			float _CDAlphaMask;
			float _DurationAlphaMask;

			half4 frag(Output o) : Color {
				float angle = -atan2(-o.texcoord[1].x, -o.texcoord[1].y);
				if(angle < 0)
					angle = 6.2832f + angle;
				angle /= 6.2832f;
				
				half4 color = tex2D(_MainTex, o.texcoord[0]);
				if(angle > _DurationAlphaMask)
					color = half4(1, .2, .2, color.a);
				if(angle >= _CDAlphaMask)
					return color;
				else
					return half4(0, 0, 0, 0);
			}
			ENDCG
		}
	} 
	FallBack "Diffuse"
}
