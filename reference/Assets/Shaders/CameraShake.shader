Shader "ZombieStyle/CameraShake" {
	Properties {
		_MainTex ("Base (RGB)", 2D) = "white" {}
		_DeltaX ("x", Range(-0.05, 0.05)) = 0
		_DeltaY ("y", Range(-0.05, 0.05)) = 0
	}

	SubShader {
		Tags { "RenderType"="Opaque" }
		LOD 200
		
		CGPROGRAM
		#pragma surface surf Lambert

		sampler2D _MainTex;
		half _DeltaX;
		half _DeltaY;

		struct Input {
			float2 uv_MainTex;
		};

		void surf (Input IN, inout SurfaceOutput o) {
			half4 c = tex2D (_MainTex, IN.uv_MainTex + half2(_DeltaX, _DeltaY));
			o.Albedo = c.rgb;
			o.Alpha = c.a;
		}
		ENDCG
	} 
	FallBack "Diffuse"
}
