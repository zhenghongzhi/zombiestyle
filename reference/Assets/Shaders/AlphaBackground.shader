Shader "ZombieStyle/AlphaBackground" {
	Properties {
		_MainTex ("Base (RGB)", 2D) = "" {}
		_Depth("Depth", Range(0,1)) = 1
	}
	SubShader {
		//Tags { "Queue"="Background+1" }
		Tags { "Queue"="Transparent" "IgnoreProjector"="True" "RenderType"="Transparent" }
		
		Lighting Off 
        Fog { Mode Off }
        ZWrite Off
		Blend SrcAlpha OneMinusSrcAlpha

		Pass {
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			float _Depth;
			sampler2D _MainTex;

			struct Input  {
				float4 vertex : POSITION;
				fixed4 color : COLOR;
				float4 texcoord : TEXCOORD0;
			};

			struct Output {
				float4 position : POSITION;
				fixed4 color : COLOR;
				float2 texcoord : TEXCOORD0;
			};

			Output vert(Input i) {
				Output o;
				o.position = float4(i.vertex.xy, _Depth, 1);
				o.color = i.color;
				o.texcoord = i.texcoord.xy;
				return o;
			}

			half4 frag(Output o) : Color {
				return tex2D(_MainTex, o.texcoord);
			}
			ENDCG
		}
	} 
	FallBack "Diffuse"
}
