Shader "ZombieStyle/ScreenAliasBackground" {
	Properties {
		_MainTex ("Base (RGB)", 2D) = "white" {}
	}
	SubShader {
		Tags { "Queue"="Background+1" }
		LOD 200

		Pass {
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag

			struct Input  {
				float4 vertex : POSITION;
				float4 texcoord : TEXCOORD0;
			};

			struct Output {
				float4 position : POSITION;
				float2 texcoord : TEXCOORD0;
			};

			Output vert(Input i) {
				Output o;
				o.position = float4(i.vertex.xy, 1, 1);
				o.texcoord = i.texcoord.xy;
				return o;
			}

			sampler2D _MainTex;

			half4 frag(Output o) : Color {
				return tex2D(_MainTex, o.texcoord);
			}
			ENDCG
		}
	} 
	FallBack "Diffuse"
}
